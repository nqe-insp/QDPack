! linkedlist.f90 --
!     Include file for defining linked lists where each element holds
!     the same kind of data
!
!     See the example/test program for the way to use this
!
!     Note:
!     You should only use pointer variables of this type, no
!     ordinary variables, as sometimes the memory pointed to
!     will be deallocated. The subroutines and functions
!     are designed to minimize mistakes (for instance: using
!     = instead of =>)
!
!     $Id: linkedlist.f90,v 1.3 2007/01/26 09:56:43 arjenmarkus Exp $
!
! Define the linked-list data type
!
MODULE linkedlist
    IMPLICIT NONE

    type, abstract :: LIST_DATA
    CONTAINS
        PROCEDURE(LIST_DATA_SUB), deferred :: destroy
    end type

    abstract interface
        subroutine LIST_DATA_SUB(self)
            IMPORT :: LIST_DATA
            IMPLICIT NONE
            CLASS(LIST_DATA) :: self
        end subroutine LIST_DATA_SUB
    end interface

    type LINKED_LIST
        class(LINKED_LIST), pointer :: next => null()
        class(LINKED_LIST), pointer :: last=>null()
        class(LIST_DATA), pointer   :: data => null()

        integer :: size=-1

    CONTAINS
        PROCEDURE :: destroy => list_destroy
        PROCEDURE :: get_size => list_size
        PROCEDURE :: get_next => list_next
        PROCEDURE :: insert => list_insert
   !     PROCEDURE :: insert_head => list_insert_head
        PROCEDURE :: append => list_append
        PROCEDURE :: get_data => list_get_data
        PROCEDURE :: put_data => list_put_data
        PROCEDURE :: delete_element => list_delete_element
    end type LINKED_LIST

    PRIVATE
    PUBLIC :: LINKED_LIST, LIST_DATA

    INTERFACE LINKED_LIST
        PROCEDURE list_create
    END INTERFACE

    !
    ! define a private (!) interface to prevent
    ! mistakes with ordinary assignment
    !
    !interface assignment(=)
    !    module procedure list_assign
    !end interface
    !private :: list_assign

    !
    ! Define the subroutines and functions
    !
CONTAINS

    ! list_assign
    !     Subroutine to prevent errors with assignment
    ! Arguments:
    !     list_left   List on the left-hand side
    !     list_right  List on the right-hand side
    !
    ! NOTE:
    !     This does not work because of a private/public
    !     conflict
    !
    !subroutine list_assign( list_left, list_right )
    !    type(LINKED_LIST), INTENT(OUT)  :: list_left
    !    type(LINKED_LIST), INTENT(IN)   :: list_right
    !   !type(LINKED_LIST), pointer      :: list_left
    !   !type(LINKED_LIST), pointer      :: list_right
    !
    !    !
    !    ! Note the order!
    !    !
    !    stop 'Error: ordinary assignment for lists'
    !    list_left%next => null()
    !end subroutine list_assign

    ! list_create --
    !     Create and initialise a list
    ! Arguments:
    !     list       Pointer to new linked list
    !     data       The data for the first element
    ! Note:
    !     This version assumes a shallow copy is enough
    !     (that is, there are no pointers within the data
    !     to be stored)
    !     It also assumes the argument list does not already
    !     refer to a list. Use list_destroy first to
    !     destroy up an old list.
    !
    function list_create( data ) result(list)
        class(LINKED_LIST), pointer  :: list
        class(LIST_DATA), intent(in) :: data

        allocate( list )
        list%next => null()
        allocate(list%data, source=data)
        list%size=1
        list%last => list
    end function list_create

    ! list_destroy --
    !     Destroy an entire list
    ! Arguments:
    !     list       Pointer to the list to be destroyed
    ! Note:
    !     This version assumes that there are no
    !     pointers within the data that need deallocation
    !
    subroutine list_destroy( self )
        class(LINKED_LIST), target  :: self

        class(LINKED_LIST), pointer  :: current
        class(LINKED_LIST), pointer  :: next

        current => self
        do while ( associated(current%next) )
            next => current%next
            call current%data%destroy()
            deallocate( current%data )
            deallocate( current )
            current => next
        enddo
    end subroutine list_destroy


    integer function list_size( self )
        class(LINKED_LIST)  :: self

        list_size=self%size

    end function list_size

    ! list_next
    !     Return the next element (if any)
    ! Arguments:
    !     elem       Element in the linked list
    ! Result:
    !
    function list_next(self) result(next)
        class(LINKED_LIST) :: self
        class(LINKED_LIST), pointer :: next

        next => self%next

    end function list_next

    ! list_insert
    !     Insert a new element
    ! Arguments:
    !     elem       Element in the linked list after
    !                which to insert the new element
    !     data       The data for the new element
    !
    subroutine list_insert( self, elem, data )
        class(LINKED_LIST)  :: self
        class(LINKED_LIST), pointer  :: elem
        class(LIST_DATA), intent(in) :: data
        class(LINKED_LIST), pointer :: next

        allocate(next)

        next%next => elem%next
        elem%next => next
        allocate(next%data, source=data)

        self%size=self%size+1
        if(.not. associated(next%next)) self%last => next
    end subroutine list_insert

    ! list_insert_head
    !     Insert a new element before the first element
    ! Arguments:
    !     list       Start of the list
    !     data       The data for the new element
    !
    ! subroutine list_insert_head( self, data )
    !     class(LINKED_LIST)  :: self
    !     class(LIST_DATA), intent(in) :: data

    !     class(LINKED_LIST), pointer :: elem

    !     allocate(elem)
    !     allocate(elem%data, source=data)

    !     elem%size = self%size+1
    !     elem%last => self%last

    !     self%size = -1
    !     self%last => null()

    !     elem%next => self
    !     self      => elem
        
    ! end subroutine list_insert_head

    subroutine list_append(self,data)
        class(LINKED_LIST)  :: self
        class(LIST_DATA), intent(in) :: data

        class(LINKED_LIST), pointer :: elem

        allocate(elem)
        allocate(elem%data, source=data)
        elem%next=>null()
        self%last%next => elem
        self%last => elem
        self%size=self%size+1

    end subroutine list_append

    ! list_delete_element
    !     Delete an element from the list
    ! Arguments:
    !     list       Header of the list
    !     elem       Element in the linked list to be
    !                removed
    !
    subroutine list_delete_element( self, elem )
        class(LINKED_LIST), target  :: self
        class(LINKED_LIST), pointer  :: elem

        class(LINKED_LIST), pointer  :: current
        class(LINKED_LIST), pointer  :: prev

        if ( associated(elem,self) ) then
            write(0,*) "[linkedlist.f90] Warning: cannot delete head of a list!"
            ! if(associated(elem%next)) then
            !     elem%next%last=>self%last
            !     elem%next%size = self%size-1
            !     self => elem%next
            !     deallocate( elem%data )
            !     deallocate( elem )
            ! else
            !     deallocate(self%data)
            !     deallocate(self)
            ! endif
        else
            current => self
            prev    => self
            do while ( associated(current) )
                if ( associated(current,elem) ) then
                    prev%next => current%next
                    deallocate( current%data )
                    deallocate( current ) ! Is also "elem"
                    self%size=self%size-1
                    exit
                endif
                prev    => current
                current => current%next
            enddo
        endif
    !    allocate(next)
    !
    !    next%next => elem%next
    !    elem%next => next
    !    next%data =  data
    end subroutine list_delete_element

    ! list_get_data
    !     Get the data stored with a list element
    ! Arguments:
    !     elem       Element in the linked list
    !
    function list_get_data( self ) result(data)
        class(LINKED_LIST) :: self
        class(LIST_DATA),pointer :: data

        data => self%data
        
    end function list_get_data

    ! list_put_data
    !     Store new data with a list element
    ! Arguments:
    !     elem       Element in the linked list
    !     data       The data to be stored
    !
    subroutine list_put_data( self, data )
        class(LINKED_LIST)  :: self
        class(LIST_DATA), intent(in) :: data

        if(associated(self%data)) deallocate(self%data)
        allocate(self%data, source=data)
    end subroutine list_put_data

END MODULE linkedlist