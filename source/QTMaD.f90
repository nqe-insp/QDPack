PROGRAM QTMaD
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
  use system_commons
  use correlation_functions
  use qtb_types
  use potential
  use pi_sampler, only : sample_PI_momentum,get_dipole_ring_polymer
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library	
  type(tcf_type) :: tcf,tcf_fluct

  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real ::  remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  integer :: avg_counter
  logical :: verbose

  integer :: nan_count,max_nan,c
  integer :: ux,up
  logical :: save_trajectory,ignore_point

  integer :: nsteps,nblocks,iblock,write_stride,istep,n_threads,nblocks_therm
  real(dp) :: block_time
  logical :: resample_momentum
  real(dp) :: gamma_smooth, spectrum_cutoff
  real(dp) :: dt

  integer :: nu,nmats,k0
  real(dp) :: gamma0,gamma_centroid

  real(dp):: temp_avg,Ekin_avg,temp_centroid_avg

  real(dp), allocatable :: Tnm(:,:)
  logical :: optimal_thermostat
  real(dp),allocatable :: gamma_modes(:)

  integer :: i,iom,nom,i0
  real(dp), allocatable :: X(:,:,:),P(:,:,:),Xtraj(:,:,:,:),Ptraj(:,:,:,:),Pctraj(:,:,:),mutraj(:,:)
  real(dp), allocatable :: mu0(:),mutmp(:)

  character(:), allocatable :: tcf_name,qtb_method
  TYPE(QTB_TYPE), allocatable :: qtbs(:)
  real(dp), allocatable :: power_spectrum(:)
  real(dp) :: dom,u02

  logical :: fast_forward_langevin

  call full_timer%start()
  call cpu_time(cpu_time_start)
  avg_counter = 0
  step_time_avg=0.
  step_cpu_time_avg=0.

  call initialize_framework(param_library)
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	
  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE  
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)

  tcf_name="QTMaD"
  dt = param_library%get("parameters/dt")
  !! @input_file parameters/dt [REAL] (QTMaD)
  nu=param_library%get("parameters/nbeads")
  !! @input_file parameters/nbeads [INTEGER] (QTMaD)
  nmats=param_library%get("parameters/n_matsubara")
  !! @input_file parameters/n_matsubara [INTEGER] (QTMaD)
  allocate(Tnm(nu,nmats))
  call compute_Tnm(nu,nmats,Tnm,k0)

  fast_forward_langevin = param_library%get("parameters/sampler_fast_forward_langevin",default=.FALSE.) 
  !! @input_file parameters/sampler_fast_forward_langevin [LOGICAL] (QTMaD, default=.FALSE.)
  if(fast_forward_langevin) write(*,*) "QTMaD: fast forward langevin."
  gamma0 = param_library%get("parameters/gamma")
  !! @input_file parameters/gamma [REAL] (QTMaD)
  gamma_centroid = param_library%get("parameters/gamma_centroid",default=gamma0)
  !! @input_file parameters/gamma_centroid [REAL] (QTMaD, default=parameters/gamma)
  allocate(gamma_modes(nmats))
  optimal_thermostat = param_library%get("parameters/optimal_thermostat",default=.TRUE.)
  !! @input_file parameters/optimal_thermostat [LOGICAL] (QTMaD, default=.TRUE.)
  i0 = param_library%get("parameters/imode_start_overdamp",default=1)
  !! @input_file parameters/imode_start_overdamp [INTEGER] (QTMaD, default=1)
  if(i0>k0) i0=k0
  gamma_modes(:)=gamma0
  gamma_modes(k0)=gamma_centroid
  do i=1,i0-1
    gamma_modes(k0+i)=gamma_centroid    
    gamma_modes(k0-i)=gamma_centroid  
  enddo
  if(optimal_thermostat) then
    do i=i0,k0-1
      u02 = sqrt(1._dp*(i-i0+1))*gamma0 !max(gamma0, sqrt(2._dp*i*pi)*kT)
      gamma_modes(k0+i)=u02
      gamma_modes(k0-i)=u02
    enddo
  endif

  temp_avg = 0._dp
  Ekin_avg = 0._dp
  temp_centroid_avg = 0._dp

  block_time = param_library%get("parameters/block_time") 
  !! @input_file parameters/block_time [REAL] (QTMaD)
  nsteps=nint(block_time/dt)
  nblocks = param_library%get("parameters/nblocks")
  !! @input_file parameters/nblocks [INTEGER] (QTMaD)
  nblocks_therm = param_library%get("parameters/nblocks_therm",default=0)
  !! @input_file parameters/nblocks_therm [INTEGER] (QTMaD, default=0)
  write_stride = param_library%get("parameters/write_stride",default=1)
  !! @input_file parameters/write_stride [INTEGER] (QTMaD, default=1)
  save_trajectory = param_library%get("parameters/save_trajectory",default=.TRUE.)
  !! @input_file parameters/save_trajectory [LOGICAL] (QTMaD, default=.TRUE.)

  gamma_smooth = param_library%get("parameters/gamma_smooth",default=-1._dp)
  !! @input_file parameters/gamma_smooth [REAL] (QTMaD, default=-1.)
  spectrum_cutoff = param_library%get("parameters/spectrum_cutoff",default=-1._dp)   
  !! @input_file parameters/spectrum_cutoff [REAL] (QTMaD, default=-1.)

  call initialize_tcf(tcf,tcf_name,dt,nsteps,1 &
                        ,gamma_smooth,spectrum_cutoff,kubo_transform=.FALSE.)
  call initialize_tcf(tcf_fluct,"QTMaDFluct",dt,nsteps,1 &
                        ,gamma_smooth,spectrum_cutoff,kubo_transform=.TRUE.)

  allocate(X(nat,ndim,nmats),P(nat,ndim,nmats))
  X(:,:,:) = 0._dp
  X(:,:,k0)=Xinit(:,:)
  P(:,:,:) = 0._dp
  !call sample_PI_momentum(P)
  
  allocate(Xtraj(nat,ndim,nu,nsteps),Ptraj(nat,ndim,nu,nsteps),Pctraj(nat,ndim,nsteps))
  Xtraj(:,:,:,:)=0._dp
  Ptraj(:,:,:,:)=0._dp
  Pctraj(:,:,:)=0._dp
  if(charges_provided) then
    allocate(mutraj(ndim,nsteps),mu0(ndim),mutmp(ndim))
    mutraj(:,:)=0._dp
  endif

  max_nan = param_library%get("parameters/max_nan",default=10)
  !! @input_file parameters/max_nan [INTEGER] (QTMaD, default=10)
  nan_count=0

  allocate(qtbs(nmats))
  call qtbs(k0)%initialize(param_library,qtb_method, dt &
          ,gamma_modes(k0),.FALSE., classical_kernel=.TRUE.,bead_index=0)
  qtbs(k0)%n_beads = nmats
  nom=qtbs(k0)%nom
  dom=qtbs(k0)%domega
  allocate(power_spectrum(nom))
  do i=1,k0-1
    power_spectrum(1)=0._dp
    open(newunit=ux,file=output%working_directory//"power_spectrum.mats"//int_to_str(i,nmats))
    do iom=1,nom-1
      u02=(0.5*iom*dom/kT)**2
      power_spectrum(iom+1)=kT/(1._dp+(i*pi)**2/u02)
      write(ux,*) iom*dom*au%cm1, power_spectrum(iom+1)*au%kelvin
    enddo
    close(ux)
    call qtbs(k0+i)%initialize(param_library,qtb_method, dt &
          ,gamma_modes(k0+i), .FALSE., power_spectrum_in=power_spectrum &
          ,bead_index=i)
    call qtbs(k0-i)%initialize(param_library,qtb_method, dt &
          ,gamma_modes(k0-i), .FALSE., power_spectrum_in=power_spectrum &
          ,bead_index=nmats-i+1)

    qtbs(k0+i)%n_beads = nmats
    qtbs(k0-i)%n_beads = nmats
  enddo

  !-----------------------------------
  call open_output_files(param_library)

  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  if(nblocks_therm>0) then
    write(*,*)
    write(*,*) "Starting "//int_to_str(nblocks_therm)//" blocks of thermalization"
    DO iblock=1,nblocks_therm
      call compute_block(iblock,.TRUE.)
    ENDDO
    write(*,*) "Thermalization done."
    write(*,*)
  endif

  if(qtbs(1)%save_average_spectra) then    
    call qtbs(k0)%write_average_spectra("QTB_spectra_0.thermalization.out",restart_average=.TRUE.)
    do i=1,k0-1
      call qtbs(k0+i)%write_average_spectra("QTB_spectra_"//int_to_str(i)//".thermalization.out",restart_average=.TRUE.)
      call qtbs(k0-i)%write_average_spectra("QTB_spectra_"//int_to_str(nmats-i+1)//".thermalization.out",restart_average=.TRUE.)
    enddo
  endif
  
  DO iblock=1,nblocks
    call compute_block(iblock,.FALSE.)
  ENDDO

  call write_tcf_results(tcf,1._dp)
   call write_tcf_results(tcf_fluct,1._dp)

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS

subroutine update_block_QTMaD(X,P,nsteps,verbose,Xtraj,Ptraj,Pctraj)
  IMPLICIT none
  real(dp), intent(inout) :: X(:,:,:),P(:,:,:),Xtraj(:,:,:,:),Ptraj(:,:,:,:),Pctraj(:,:,:)
  integer, intent(in) :: nsteps
  logical, intent(in) :: verbose
  integer :: istep,i,j,imulti
    real(dp) :: temp,temp_sum,sqrtnu
    real(dp) :: Ekin,Ekin_sum,temp_centroid,temp_centroid_sum
    real(dp), allocatable :: F(:,:,:)


    allocate(F(nat,ndim,nmats))

    temp_sum = 0.
    temp_centroid_sum = 0.
    Ekin_sum = 0.

    ! GET NORMAL MODES POSITIONS
    call update_matsubara_MF_forces(X,F)

    do istep = 1,nsteps 
      P = P + 0.5*dt*F
      do i=1,nmats
        do j=1,ndim
          X(:,j,i)=X(:,j,i)+0.5*dt*P(:,j,i)/mass(:)
        enddo
      enddo
      call apply_matsubara_thermostat(P)
      do i=1,nmats
        do j=1,ndim
          X(:,j,i)=X(:,j,i)+0.5*dt*P(:,j,i)/mass(:)
        enddo
      enddo
      call update_matsubara_MF_forces(X,F)
      P = P + 0.5*dt*F      

      call to_beads_vector(X(:,:,:),Tnm,Xtraj(:,:,:,istep))
      call to_beads_vector(P(:,:,:),Tnm,Ptraj(:,:,:,istep))
      Pctraj(:,:,istep)=P(:,:,k0)
      call compute_matsubara_kinetic_energy(P,Ekin,temp,temp_centroid)
      temp_sum = temp_sum + temp
      Ekin_sum = Ekin_sum + Ekin
      temp_centroid_sum = temp_centroid_sum + temp_centroid
    enddo  

    temp_avg = temp_avg + (temp_sum/nsteps - temp_avg)/avg_counter
    temp_centroid_avg = temp_centroid_avg + (temp_centroid_sum/nsteps - temp_centroid_avg)/avg_counter
    Ekin_avg = Ekin_avg + (Ekin_sum/nsteps - Ekin_avg)/avg_counter

    if(verbose) then
      write(*,*) "Matsubara_effective_temperature= ",real(temp_avg,sp),"K"
      write(*,*) "Matsubara_centroid_temperature= ",real(temp_centroid_avg,sp),"K"
      write(*,*) "Ekin= ",real(Ekin_avg,sp)
      temp_avg=0._dp
      temp_centroid_avg = 0._dp
      Ekin_avg = 0._dp
    endif

    deallocate(F)

end subroutine update_block_QTMaD

subroutine compute_Tnm(N,M,Tnm,k0)
    IMPLICIT none
    integer, intent(in) :: M,N
    integer,intent(out) :: k0
    real(dp), intent(inout) :: Tnm(:,:)
    real(dp) :: dom(N)
    integer :: i,j

    if(mod(M,2)==0) STOP "[QTMaD.f90] Error: number of matsubara mode must be odd!"
    k0=1+(M-1)/2
    Tnm(:,:)=0._dp

    do i=1,N
      dom(i)=2._dp*pi*real(i,dp)/real(N,dp)
    enddo
  
    Tnm(:,k0)=1
    do i=1,k0-1
    !  write(*,*) k0+i, k0-i
      Tnm(:,k0+i)=sqrt(2._dp)*sin(dom(:)*i)
      Tnm(:,k0-i)=sqrt(2._dp)*cos(dom(:)*i)
    enddo

end subroutine compute_Tnm

subroutine to_matsubara_vector(X,Tnm,EigX)
    implicit none
    real(dp), intent(in) :: X(:,:,:),Tnm(:,:)
    real(dp) :: EigX(:,:,:)
    integer :: i,j

    do j=1,ndim ; do i=1,nat
      !call DGEMV('N',nu,nu,1._8,EigMatTr,nu,X(i,j,:),1,0._8,EigX(i,j,:),1)
      EigX(i,j,:)=matmul(transpose(Tnm),X(i,j,:))
    enddo ; enddo

  end subroutine to_matsubara_vector

  subroutine to_beads_vector(EigX,Tnm,X)
    implicit none
    real(dp), intent(in) :: EigX(:,:,:),Tnm(:,:)
    real(dp), intent(inout) :: X(:,:,:)
    integer :: i,j
    
    do j=1,ndim ; do i=1,nat
      !call DGEMV('N',nu,nu,1._8,EigMatTr,nu,X(i,j,:),1,0._8,EigX(i,j,:),1)
      X(i,j,:)=matmul(Tnm,EigX(i,j,:))
    enddo ; enddo

  end subroutine to_beads_vector

  subroutine update_matsubara_forces(EigX,EigF)
    implicit none
    real(dp), intent(in) :: EigX(:,:,:)
    real(dp), intent(inout) :: EigF(:,:,:)
    integer :: k
    real(dp), allocatable :: X(:,:,:),F(:,:,:)

    allocate(X(nat,ndim,nu),F(nat,ndim,nu))

    call to_beads_vector(EigX,Tnm,X)
    
    do k=1,nu
      F(:,:,k)=-dPot(X(:,:,k)) / nu
    enddo
    !COMPUTE FORCE FOR NORMAL MODES
    call to_matsubara_vector(F,Tnm,EigF)

    deallocate(X,F)

  end subroutine update_matsubara_forces
  
  subroutine update_matsubara_MF_forces(EigX,EigF)
    implicit none
    real(dp), intent(in) :: EigX(:,:,:)
    real(dp), intent(inout) :: EigF(:,:,:)
    integer :: i,j,k,k0n,istep,nsteps_aux
    real(dp), allocatable :: X(:,:,:),F(:,:,:),EigXn(:,:,:)
    real(dp), allocatable :: Tnn(:,:),P(:,:,:)
    real(dp), allocatable :: wn(:),R(:)
    allocate(X(nat,ndim,nu),F(nat,ndim,nu),P(nat,ndim,nu))
    allocate(Tnn(nu,nu),R(nat),EigXn(nat,ndim,nu))
    allocate(wn(nu))
    nsteps_aux=100
    call compute_Tnm(nu,nu,Tnn,k0n)
    wn(k0n)=0._dp
    do k=1,k0n-1
      wn(k0n+k)=2.*nu*kT*sin(k*pi/nu)
      wn(k0n-k)=wn(k0n+k)
    enddo
    EigF(:,:,:)=0._dp
    EigXn(:,:,:)=0._dp
    EigXn(:,:,k0n)=eigX(:,:,k0)
    do k=1,k0-1
      EigXn(:,:,k0n+k)=EigX(:,:,k0+k)
      EigXn(:,:,k0n-k)=EigX(:,:,k0-k)
    enddo
    do k=k0,k0n-1
      do j=1,ndim
        call randGaussN(R)
        P(:,j,k0n+k)=sqrt(mass(:)*kT)*R(:)
        call randGaussN(R)
        P(:,j,k0n-k)=sqrt(mass(:)*kT)*R(:)
      enddo
    enddo
    do istep=1,nsteps_aux 
      call apply_A_RPMD_constrained(EigXn,P,wn,k0n,0.5*dt)
      call apply_O_RPMD_constrained(P,wn,k0n,dt,R)
      call apply_A_RPMD_constrained(EigXn,P,wn,k0n,0.5*dt)
      call to_beads_vector(EigXn,Tnn,X)
      do k=1,nu
        F(:,:,k)=-dPot(X(:,:,k)) / nu
      enddo
      call to_matsubara_vector(F,Tnn,F)
      call apply_B_RPMD_constrained(P,k0n,dt,F)
      EigF(:,:,k0)=EigF(:,:,k0)+F(:,:,k0n)
      do k=1,k0-1
        EigF(:,:,k0+k)=EigF(:,:,k0+k)+F(:,:,k0n+k)
        EigF(:,:,k0-k)=EigF(:,:,k0-k)+F(:,:,k0n-k)
      enddo
    enddo

    EigF(:,:,:)=EigF(:,:,:)/real(nsteps_aux,dp)

  end subroutine update_matsubara_MF_forces

  subroutine apply_A_RPMD_constrained(X,P,wn,k0n,tau)
    implicit none
    real(dp), intent(inout) :: X(:,:,:),P(:,:,:)
    real(dp), intent(in) :: wn(:),tau
    integer, intent(in) :: k0n
    real(dp) :: X0,P0
    integer :: s(2), i,ku,j,is,k
    s(1)=1; s(2)=-1
    do is=1,2 ;do ku=k0,k0n-1
      k=k0n+s(is)*ku
      do j=1,ndim; do i=1,nat
        X0=X(i,j,k)
        P0=P(i,j,k)
        X(i,j,k)=X0*cos(wn(k)*tau)+P0*sin(wn(k)*tau)/(mass(i)*wn(k))
        P(i,j,k)=P0*cos(wn(k)*tau)-X0*sin(wn(k)*tau)*(mass(i)*wn(k))
      enddo; enddo
    enddo ; enddo
  end subroutine apply_A_RPMD_constrained
  subroutine apply_O_RPMD_constrained(P,wn,k0n,tau,R)
    implicit none
    real(dp), intent(inout) :: P(:,:,:),R(:)
    real(dp), intent(in) :: wn(:),tau
    integer, intent(in) :: k0n
    real(dp) :: gamma,gamma_exp,sigma2
    integer :: s(2), i,ku,j,is,k
    s(1)=1; s(2)=-1
    do is=1,2 ;do ku=k0,k0n-1
      k=k0n+s(is)*ku
      gamma=2.*wn(k)
      gamma_exp=exp(-gamma*tau)
      sigma2=kT*(1.-gamma_exp**2)
      do j=1,ndim
        call randGaussN(R)
        P(:,j,k)=P(:,j,k)*gamma_exp + R(:)*sqrt(sigma2*mass(:))
      enddo
    enddo ; enddo
  end subroutine apply_O_RPMD_constrained
  subroutine apply_B_RPMD_constrained(P,k0n,tau,F)
    implicit none
    real(dp), intent(inout) :: P(:,:,:),F(:,:,:)
    real(dp), intent(in) :: tau
    integer, intent(in) :: k0n
    real(dp) :: gamma,gamma_exp,sigma2
    integer :: s(2), i,ku,j,is,k
    s(1)=1; s(2)=-1
    do is=1,2 ;do ku=k0,k0n-1
      k=k0n+s(is)*ku
      P(:,:,k)=P(:,:,k)+tau*F(:,:,k)
    enddo ; enddo
  end subroutine apply_B_RPMD_constrained

  subroutine apply_matsubara_thermostat(P)
    implicit none
    real(dp), intent(inout) :: P(:,:,:)
    real(dp), allocatable :: R(:,:)
    real(dp) :: gamma_exp
    integer :: i,j,k
    real(dp) :: Pold, Pnew

    allocate(R(nat,ndim))
    if(fast_forward_langevin) then
      do k=1,nmats   
        gamma_exp = exp(-gamma_modes(k)*dt)
        R = qtbs(k)%get_force(P(:,:,k))*dt
        do i=1,nat
          Pold = NORM2(P(i,:,k))
          Pnew = NORM2(P(i,:,k)*gamma_exp + R(i,:))
          P(i,:,k) = Pnew * P(i,:,k) / Pold
          !P(:,:,k) = P(:,:,k)*gamma_exp + R(:,:) 
        enddo
      enddo
    else
      do k=1,nmats   
        gamma_exp = exp(-gamma_modes(k)*dt)
        R = qtbs(k)%get_force(P(:,:,k))*dt
        P(:,:,k) = P(:,:,k)*gamma_exp + R(:,:)
      enddo
    endif

  end subroutine apply_matsubara_thermostat

  subroutine compute_matsubara_kinetic_energy(P,Ekin,temp,temp_centroid)
    implicit none
    real(dp), intent(in) :: P(:,:,:)
    real(dp), intent(out) :: Ekin,temp,temp_centroid
    integer :: i,j
    real(dp) :: Ekc
    
    Ekc = 0._dp
    do j=1,ndim
      Ekc=Ekc + 0.5_dp*SUM(P(:,j,k0)**2/mass(:))
    enddo
    temp_centroid = au%kelvin*2._dp*Ekc/ndof

    Ekin=Ekc
    do i=1,k0-1
      do j=1,ndim
        Ekin=Ekin + 0.5_dp*SUM(P(:,j,k0+i)**2/mass(:))
        Ekin=Ekin + 0.5_dp*SUM(P(:,j,k0-i)**2/mass(:))
      enddo
    enddo
    temp = au%kelvin*2._dp*Ekin/ndof

  end subroutine compute_matsubara_kinetic_energy

  subroutine compute_block(iblock,thermalization)
    IMPLICIT NONE
    integer, intent(in) :: iblock
    logical, intent(in) :: thermalization
    logical :: save_results

    save_results = .NOT. thermalization

    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1

    if(mod(iblock,write_stride)==0) then
      write(*,*)
      if(thermalization) then
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks_therm)
      else
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks)
      endif
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    call update_block_QTMaD(X,P,nsteps,verbose,Xtraj,Ptraj,Pctraj)       

    ignore_point = ANY(ISNAN(Xtraj)) .OR. ANY(ISNAN(Pctraj))
    if(ignore_point) then
      nan_count = nan_count+1
      if(nan_count>=max_nan) then
        write(0,*) "[QTMaD.f90] Error: ",max_nan," NaN trajectories reached!"
        STOP "Execution stopped."
      endif
      P(:,:,:) = 0._dp
      !call sample_PI_momentum(P)
    elseif(save_results) then
      call compute_autoTCF_WK(RESHAPE(Pctraj,(/ndof,nsteps/)),nsteps,dt,tcf%Cpp(:,:,1))
      call compute_autoTCF_WK(RESHAPE(Ptraj(:,:,1,:),(/ndof,nsteps/)),nsteps,dt,tcf_fluct%Cpp(:,:,1))
      if(charges_provided) then
        DO istep=1,nsteps
          call get_dipole_ring_polymer(Xtraj(:,:,:,istep),Pctraj(:,:,istep),mutraj(:,istep))      
        ENDDO
        call compute_autoTCF_WK(mutraj,nsteps,dt,tcf%Cmumu(:,:,1))
      endif
      call update_tcf_avg(tcf)
      call update_tcf_avg(tcf_fluct)

      !write block trajectory
      if(save_trajectory) then
        do i=1,1
          open(newunit=ux,file=output%working_directory//"X.traj.bead"//int_to_str(i,nu)//".block"//int_to_str(iblock,nblocks))
          DO istep=1,nsteps
            write(ux,*) Xtraj(:,:,i,istep)
          ENDDO
          close(ux)
        enddo
        do i=1,1
          open(newunit=ux,file=output%working_directory//"P.traj.bead"//int_to_str(i,nu)//".block"//int_to_str(iblock,nblocks))
          DO istep=1,nsteps
            write(ux,*) Ptraj(:,:,i,istep)
          ENDDO
          close(ux)
        enddo
        open(newunit=up,file=output%working_directory//"Pc.traj.block"//int_to_str(iblock,nblocks))
        DO istep=1,nsteps
          write(up,*) Pctraj(:,:,istep)
        ENDDO
        close(up)
      endif
    endif 

    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter
    
    if(verbose) then
      if(nan_count>0) write(*,*) "NaN_count= "//int_to_str(nan_count)
      write(*,*) "Average time per block: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."
      if(thermalization) then
        remaining_time = step_time_avg*(nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated thermalization time : "//sec2human(job_time)
        write(*,*) "estimated remaining thermalization time : "//sec2human(remaining_time)
        remaining_time = step_time_avg*(nblocks+nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      else
        remaining_time = step_time_avg*(nblocks-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      endif

      ! RESET WINDOW AVERAGES
      avg_counter=0
      step_time_avg=0.
      step_cpu_time_avg=0.

      ! WRITE INTERMEDIATE RESULTS
      if(save_results) then
        call write_tcf_results(tcf,1._dp)
        call write_tcf_results(tcf_fluct,1._dp)
      endif
    endif

  end subroutine compute_block

END PROGRAM QTMaD
