module fk_initial
  use kinds
  USE nested_dictionaries
  USE random, only: RandGaussN
  USE matrix_operations
  USE atomic_units
  USE potential
  USE system_commons
  implicit none
  INTEGER, SAVE :: maxiter,nsteps_om2
  REAL(dp), ALLOCATABLE, SAVE :: Om2(:,:)
  REAL(dp), ALLOCATABLE, SAVE :: OmEig(:),EigMat(:,:),LEig(:)
  REAL(dp), SAVE :: Va2, Vcentroid
  REAL(dp), ALLOCATABLE, SAVE :: A2inv(:,:)
  REAL(dp), SAVE :: conv_thr
  LOGICAL, SAVE :: verbose, gradient_sampling
  LOGICAL, ALLOCATABLE, SAVE :: unphysical_om2(:)
  REAL(dp), SAVE ::  omega_min

  PRIVATE
  PUBLIC :: initialize_fk, compute_FK_parameters, sample_fk_initial &
          ,get_sigma2P_fk
contains

  subroutine initialize_fk(param_library)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    INTEGER :: i,j,c
    
    maxiter=param_library%get("FKLPI/max_iter",DEFAULT=20)
    !! @input_file FKLPI/max_iter [INTEGER] (fk_initial, default=20)
    conv_thr=param_library%get("FKLPI/conv_thr",DEFAULT=0.02)
    !! @input_file FKLPI/conv_thr [REAL] (fk_initial, default=0.02)
    nsteps_om2=param_library%get("FKLPI/n_steps_om2",DEFAULT=100)
    !! @input_file FKLPI/n_steps_om2 [INTEGER] (fk_initial, default=100)
    verbose=param_library%get("FKLPI/verbose",DEFAULT=.FALSE.)
    !! @input_file FKLPI/verbose [LOGICAL] (fk_initial, default=.FALSE.)

    if(hessian_available) then
      gradient_sampling = .FALSE.
    else
      write(*,*) "FK: Hessian not available, using gradient sampling => Om2 may be noisier"
      gradient_sampling = .TRUE.
    endif

    omega_min=1._dp/au%cm1

    allocate(Om2(ndof,ndof),LEig(ndof))
    allocate(OmEig(ndof),EigMat(ndof,ndof))
    allocate(unphysical_om2(ndof))
    unphysical_om2(:)=.FALSE.
    Om2(:,:)=0._dp
    OmEig(:)=0._dp
    EigMat(:,:)=0._dp
    Va2=0._dp
    Vcentroid=0._dp
    
    ! initialize L to the classical limit (deBroglie length/sqrt(12) )
    c=0
    DO j=1,ndim ; DO i=1,nat
      c=c+1
      LEig(c)=1/SQRT(12*kT)
      EigMat(c,c)=1
    ENDDO ; ENDDO 

    if(gradient_sampling) then
      allocate(A2inv(ndof,ndof))
      A2inv(:,:)=0._dp
      c=0
      DO j=1,ndim ; DO i=1,nat
        c=c+1
        A2inv(c,c)=12*kT*mass(i)
      ENDDO ; ENDDO 
    endif

  end subroutine initialize_fk

  subroutine sample_fk_initial(Xc,X0,P0)
    implicit none
    REAL(dp), INTENT(in) :: Xc(:,:)
    REAL(dp), INTENT(inout) :: X0(:,:),P0(:,:)
    REAL(dp), ALLOCATABLE :: xEig(:),pEig(:)
    REAL(dp) :: u0
    INTEGER :: i,j

    ALLOCATE(xEig(ndof),pEig(ndof))
    call randGaussN(xEig)
    call randGaussN(pEig)
    DO i=1,ndof
      xEig(i)=xEig(i)*LEig(i)
      u0=0.5*OmEig(i)/kT
      if(unphysical_om2(i)) then
        ! LIU & MILLER ANSATZ WHEN IMAGINARY FREQUENCY
        pEig(i)=pEig(i)*sqrt(kT*tanh(u0)/u0)
      else
        pEig(i)=pEig(i)*sqrt(kT*u0/tanh(u0))
      endif
      !write(*,*) OmEig(i)*cm1,kT*u0/tanh(u0)*kelvin
    ENDDO

    P0(:,:) = RESHAPE(matmul(EigMat,pEig),(/nat,ndim/) )
    X0(:,:) = RESHAPE(matmul(EigMat,xEig),(/nat,ndim/) )
    DO i=1,ndim
      X0(:,i)=Xc(:,i) + X0(:,i)/sqrt(mass(:))
			P0(:,i)=P0(:,i)*sqrt(mass(:))
		ENDDO  

    deallocate(xEig,pEig)
    
  end subroutine sample_fk_initial

  subroutine get_sigma2P_fk(sigma2)
    implicit none
    REAL(dp), INTENT(inout) :: sigma2(:,:)
    REAL(dp) :: u0
    INTEGER :: i,j

    sigma2(:,:)=0._dp
    DO i=1,ndof
      u0=0.5*OmEig(i)/kT
      if(unphysical_om2(i)) then
        ! LIU & MILLER ANSATZ WHEN IMAGINARY FREQUENCY
        sigma2(i,i)=kT*tanh(u0)/u0
      else
        sigma2(i,i)=kT*u0/tanh(u0)
      endif
      !write(*,*) OmEig(i)*cm1,kT*u0/tanh(u0)*kelvin
    ENDDO

    sigma2 = matmul(eigMat,matmul(sigma2,transpose(eigMat)))

  end subroutine get_sigma2P_fk

  subroutine compute_FK_parameters(Xc,iconv)
    implicit none
    REAL(dp), INTENT(in) :: Xc(:,:)
    integer, intent(out) :: iconv
    INTEGER :: i,j
    REAL(dp) :: LEigOld(ndof)
    
    iconv = maxiter
    DO i=1,maxiter
      LEigOld=LEig
      call sample_Om2(Xc)
      call update_eigenvalues()
      if(maxval(abs(LEigOld-LEig)/LEigOld)<=conv_thr) THEN
        iconv=i
        EXIT
      ENDIF
    ENDDO

    ! if(any(unphysical_om2==0)) then
    !   write(*,*) "FK Warning: Found an unmanageable negative eigenvalue of Om2(xc) !"
    ! endif
    
    call compute_centroid_potential()

    IF(verbose) THEN
      IF(iconv<maxiter) then
        write(*,*) "FK: converged Om2 in ",iconv,"steps"
      else
        write(*,*) "FK Warning: Om2 did not converged to precision ",conv_thr,"in ",maxiter,"steps"
      endif
    ENDIF

  end subroutine compute_FK_parameters

  subroutine sample_Om2(Xc)
    implicit none
    REAL(dp), INTENT(in) :: Xc(:,:)
    REAL(dp), ALLOCATABLE :: z(:),dU(:)
    REAL(dp), ALLOCATABLE :: hess(:,:),delta(:,:)
    INTEGER :: istep,i,j,k,c

    ALLOCATE(z(ndof),hess(ndof,ndof))
    ALLOCATE(delta(nat,ndim))
    if(gradient_sampling) then
      ALLOCATE(dU(ndof))
    endif

    Om2(:,:)=0._dp
    Va2=0._dp
    DO istep=1,nsteps_om2
      !sample z in eigenspace and convert to coordinates
      call RandGaussN(z)
      z=MATMUL(EigMat,z*LEig)
      c=0
      DO j=1,ndim ; DO i=1,nat
        c=c+1
        z(c)=z(c)/sqrt(mass(i))
        delta(i,j)=z(c)
      ENDDO ; ENDDO
      
      Va2=Va2+Pot(Xc+delta)  
     
      if(gradient_sampling) then
        ! GRADIENT SAMPLING
        dU(:)=RESHAPE(dPot(Xc+delta), (/ndof/) )
        DO j=1,ndof ; DO i=1,ndof
          hess(i,j)=z(j)*dU(i)
        enddo ; enddo
      else
        ! DIRECT HESSIAN
        hess=compute_hessian(Xc+delta)
      endif

      Om2(:,:)=Om2(:,:)+hess(:,:)
            
    ENDDO
    Om2(:,:)=Om2(:,:)/REAL(nsteps_om2,dp)   
    Va2=Va2/REAL(nsteps_om2,dp)

    if(gradient_sampling) then
      Om2=matmul(Om2,A2inv)
    endif

    !mass normalization
    c=0
    DO j=1,ndim; do i=1,nat
        c=c+1
        DO k=1,ndim
          Om2((k-1)*nat+1:k*nat,c) &
                =Om2((k-1)*nat+1:k*nat,c) &
                    /sqrt(mass(i)*mass(:))
        ENDDO
    ENDDO ; ENDDO

    !symmetrize Om2
    Om2=0.5_dp*(Om2+transpose(Om2))

  end subroutine sample_Om2

  subroutine update_eigenvalues()
    implicit none
    INTEGER :: INFO,i,j,k,c

    unphysical_om2(:) = .FALSE.

    call sym_mat_diag(Om2,OmEig,EigMat,INFO)
    ! write(*,*) "****************************"
    ! write(*,*) "omega FK:"
    ! write(*,*) "max negative om=",-pi*kT*au%cm1
    IF(INFO/=0) STOP "[fk_initial.f90] Error: could not diagonalize Om2(xc) !"
    DO i=1,ndof
      if(OmEig(i)<0) then
        OmEig(i)=sqrt(-OmEig(i))
        ! write(*,*) -OmEig(i)*au%cm1 
        unphysical_om2(i) = .TRUE.
        LEig(i)=sqrt((kT/OmEig(i)**2)*( 1._dp - OmEig(i)/(2*kT*tan(0.5*OmEig(i)/kT))))
      else
        OmEig(i)=sqrt(OmEig(i))   
        ! write(*,*) OmEig(i)*au%cm1
        LEig(i)=sqrt((kT/OmEig(i)**2)*( OmEig(i)/(2*kT*tanh(0.5*OmEig(i)/kT))  - 1._dp))    
      endif
      if(OmEig(i)<omega_min) then
         LEig(i)=1._dp/SQRT(12._dp*kT)
      endif
      !   LEig(i)=sqrt((kT/OmEig(i)**2)*( OmEig(i)/(2*kT*tanh(0.5*OmEig(i)/kT))  - 1._dp))
      ! endif
    ENDDO

    if(gradient_sampling) then
      A2inv(:,:)=0._dp
      DO i=1,ndof
        A2inv(i,i)=1._dp/Leig(i)**2
      ENDDO
      A2inv=matmul(EigMat, matmul(A2inv,transpose(EigMat)))
      c=0
      DO j=1,ndim; do i=1,nat
          c=c+1
          DO k=1,ndim
            A2inv((k-1)*nat+1:k*nat,c) &
                  =A2inv((k-1)*nat+1:k*nat,c) &
                      *sqrt(mass(i)*mass(:))
          ENDDO
      ENDDO ; ENDDO
    endif


  end subroutine update_eigenvalues
  
  subroutine compute_centroid_potential()
     implicit none
     integer :: i
     real(dp) :: uFK(ndof)
     
     uFK(:)=0.5_dp*OmEig(:)/kT

     Vcentroid = Va2 + SUM( &
          -0.5_dp*(Leig*OmEig)**2 &
          +kT*log(sinh(uFK)/uFK) &
        )

  end subroutine compute_centroid_potential

end module fk_initial
