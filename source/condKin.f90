module condKin
  use kinds
  use nested_dictionaries
  use system_commons
  use timer_module
  use random
  use potential
  use matrix_operations
  use wigner_EW, only: WIGAUX, WIG_AUX_SIM_TYPE, WIG_POLYMER_TYPE
  implicit none

  PRIVATE
  PUBLIC :: compute_CK_parameters &
            ,initialize_CK_commons &
            ,initialize_CK_auxiliary_simulation

  integer, save :: n_threads

  real(dp), save :: dt
  INTEGER, save :: nu !number of beads
  real(dp), save :: dBeta
  real(dp), save :: cutoff_radius_k2


  real(dp), allocatable, save :: sqrt_mass(:)
  real(dp), save :: delta_mass_factor, sqrt_delta_mass_factor  

  ! POLYMER PARAMETERS
  real(dp), allocatable, save :: EigMat(:,:)
  real(dp), allocatable, save :: OmK(:)
  real(dp), allocatable, save :: mOmK(:,:)

  !PIOUD PARAMETERS
  ! real(dp), allocatable :: mu(:,:,:)
  real(dp), allocatable, save :: OUsig(:,:,:,:)
  real(dp), allocatable, save :: expOmk(:,:,:,:)

  ! BAOAB PARAMETERS
  real(dp), allocatable, save :: gammaExp(:,:)
  real(dp), allocatable, save :: sigmaBAOAB(:,:)
  LOGICAL, save :: BAOAB_num

CONTAINS

  subroutine compute_CK_parameters(X,auxsim)
    !$ USE OMP_LIB
    IMPLICIT NONE
		REAL(dp), intent(in) :: X(:,:)
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    INTEGER :: i,j,k,INFO,i_thread,c,u,i1,j1,i2,j2,c1,c2
    TYPE(timer_type) :: timer_full, timer_elem
    real(dp), allocatable :: k2tmp(:,:)
    REAL(dp), allocatable :: hessian(:,:)


    if(ANY(ISNAN(X))) STOP "[condKin.f90] Error: system diverged!"

    call timer_full%start()

    allocate(hessian(ndof,ndof))
    ! GET CLASSICAL FORCE     
    call get_pot_info(X,auxsim%Potcl,auxsim%Fcl,hessian)    

    i_thread=1
    !$OMP PARALLEL DO LASTPRIVATE(i_thread)    
    DO i=1,auxsim%NUM_AUX_SIM
        !$ i_thread = OMP_GET_THREAD_NUM()+1         
        call sample_forces(X,auxsim%nsteps,auxsim%nsteps_therm,auxsim%polymers(i) &
            ,auxsim%compute_WiLD_forces,auxsim%compute_Cew)
    ENDDO
    !$OMP END PARALLEL DO

    if(auxsim%NUM_AUX_SIM>1) then
      
      auxsim%k2(:,:)=0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        auxsim%k2(:,:)=auxsim%k2(:,:) + auxsim%polymers(i)%k2(:,:)
      ENDDO
      auxsim%k2(:,:)=auxsim%k2(:,:)/auxsim%NUM_AUX_SIM

      if(auxsim%compute_Cew) then
        auxsim%k4=0
        auxsim%k6=0
        DO i=1,auxsim%NUM_AUX_SIM
          auxsim%k4 = auxsim%k4 + auxsim%polymers(i)%k4
          auxsim%k6 = auxsim%k6 + auxsim%polymers(i)%k6
        ENDDO
        auxsim%k4 = auxsim%k4/auxsim%NUM_AUX_SIM
        auxsim%k6 = auxsim%k6/auxsim%NUM_AUX_SIM
      endif

      allocate(k2tmp(ndof,ndof))
      auxsim%k2var(:,:)=0._dp 
      DO i=1,auxsim%NUM_AUX_SIM
        k2tmp(:,:) = auxsim%polymers(i)%k2(:,:) - auxsim%k2(:,:)
        auxsim%k2var(:,:) = auxsim%k2var +  k2tmp(:,:)**2
      ENDDO
      auxsim%k2var = auxsim%k2var/REAL(auxsim%NUM_AUX_SIM*(auxsim%NUM_AUX_SIM-1),dp)
      deallocate(k2tmp)
    else
      auxsim%k2(:,:) = auxsim%polymers(1)%k2(:,:)
      if(auxsim%compute_Cew) then
        auxsim%k4=auxsim%polymers(1)%k4
        auxsim%k6=auxsim%polymers(1)%k6
      endif
    endif

    allocate(k2tmp(ndof,ndof))
    k2tmp=0._dp
    c1=0
    DO j1=1,ndim ; do i1=1,nat
      c1=c1+1
      k2tmp(c1,c1) = 0.5_dp*mass(i1)*nu*kT
      c2=0
      DO j2=1,ndim ; do i2=1,nat
        c2=c2+1
        k2tmp(c1,c2)=k2tmp(c1,c2) - 0.25_dp*mass(i1)*mass(i2)*(nu*kT)**2*auxsim%k2(c1,c2)
      ENDDO ; ENDDO
    ENDDO ; ENDDO

    !write(*,*) "hessian/chain=",hessian/(4*nu*kT)/k2tmp

    k2tmp = k2tmp + hessian/(4*nu*kT)

    call  remove_negative_eigenvalues(k2tmp)

    call sym_mat_diag(k2tmp,auxsim%k2Evals,auxsim%k2EMat,INFO)
    IF(INFO/=0) STOP "[condKin.f90] Error: could not diagonalize k2"  
    auxsim%k2Evals = 1._dp/auxsim%k2Evals
    auxsim%k2 = 0._dp
    DO i=1,ndof
      auxsim%k2(i,i)=auxsim%k2Evals(i)
    ENDDO
    auxsim%k2 = matmul(auxsim%k2EMat,matmul(auxsim%k2,transpose(auxsim%k2EMat)))
    
    auxsim%computation_time = timer_full%elapsed_time()

  end subroutine compute_CK_parameters

  subroutine sample_forces(X,nsteps,nsteps_therm,polymer,compute_WiLD,compute_Cew)
    IMPLICIT NONE
	  real(dp), intent(in) :: X(:,:)
    integer, intent(in) :: nsteps,nsteps_therm
    logical, intent(in) :: compute_WiLD,compute_Cew
    CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    INTEGER :: m,n_dof,ios
    INTEGER :: i,j,k,l,c,j1,i1
    LOGICAL :: move_accepted
    REAL(dp) :: D2
    REAL(dp), allocatable :: F(:)

    ! reinitialize polymer
    call sample_momenta(polymer)
    if(.not. compute_WiLD) then
      polymer%X(:,:,:)=0._dp
      polymer%EigX(:,:,:)=0._dp
    endif

    ! initialize averages
    polymer%k2(:,:) = 0._dp
    if(compute_WiLD) then
      allocate(F(ndof))
      F=0._dp
      polymer%F(:) = 0._dp
      polymer%G(:,:,:) = 0._dp
    endif
    
    if(compute_Cew) then
      polymer%k4=0._dp
      polymer%k6=0._dp
    endif

    !THERMALIZATION
    DO m=1,nsteps_therm
      CALL PIOUD_step(X,polymer)
    ENDDO

    !PRODUCTION
    DO m=1,nsteps
      !GENERATE A NEW CONFIG
      CALL PIOUD_step(X,polymer)

      polymer%Delta=RESHAPE(polymer%X(1,:,:)-polymer%X(nu-1,:,:),(/ndof/))
      !COMPUTE ONLY THE LOWER TRIANGULAR PART
      do j=1,ndof ; do i=j,ndof
        polymer%k2(i,j)=polymer%k2(i,j)  &
          + polymer%Delta(i)*polymer%Delta(j)
      enddo ; enddo

      if(compute_Cew) then
        do l=1,ndof ; do k=l,ndof ;	do j=k,ndof ; do i=j,ndof
            polymer%k4(i,j,k,l)= polymer%k4(i,j,k,l) &
                      + polymer%Delta(i)*polymer%Delta(j) &
                          *polymer%Delta(k)*polymer%Delta(l)
        enddo ; enddo ; enddo ; enddo
        polymer%k6 = polymer%k6 + polymer%Delta(1)**6
      endif

      if(compute_WiLD) then
        F=RESHAPE(SUM(polymer%F_beads(:,:,:),dim=1),(/ndof/))/real(nu,dp)
        polymer%F=polymer%F + F      
        DO c=1,ndof
          do j=1,ndof ; do i=j,ndof            
            polymer%G(i,j,c)=polymer%G(i,j,c) + polymer%Delta(i)*polymer%Delta(j)*F(c)
          ENDDO ; ENDDO
        ENDDO
      endif
    ENDDO

    !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
    DO j=2,ndof ; DO i=1,j-1
      polymer%k2(i,j)=polymer%k2(j,i)
    ENDDO ; ENDDO    
    polymer%k2 = polymer%k2/real(nsteps,dp)
    call cutoff_k2(polymer%k2,X)

    !symmetrize k4
    if(compute_Cew) then
      do l=1,ndof ; do k=l,ndof ; do j=k,ndof ; do i=j,ndof		
        polymer%k4(i,j,k,l) = polymer%k4(i,j,k,l)/real(nsteps,dp)
        call fill_permutations(i,j,k,l,polymer%k4)
      enddo ; enddo ; enddo ; enddo
      polymer%k6 = polymer%k6/real(nsteps,dp)
    endif
    

    if(compute_WiLD) then
      polymer%F = polymer%F/real(nsteps,dp)
      do c=1,ndof
        DO j=2,ndof ; DO i=1,j-1
          polymer%G(i,j,c)=polymer%G(j,i,c)
        ENDDO ; ENDDO
      enddo
      polymer%G = polymer%G/real(nsteps,dp)
    endif

  end subroutine sample_forces

!-----------------------------------------------------------------
! INITIALIZATIONS

  subroutine initialize_CK_commons(param_library)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    integer :: i,nbeads

    nu = param_library%get(WIGAUX//"/nbeads")
    !! @input_file EW_NLGA/nbeads [INTEGER] (condKin) 
    dt = param_library%get(WIGAUX//"/dt")
    !! @input_file EW_NLGA/dt [REAL] (condKin) 
    dBeta=beta/REAL(nu,dp)

    cutoff_radius_k2 = param_library%get(WIGAUX//"/k2_cutoff_radius",default=-1._dp)
    !! @input_file EW_NLGA/k2_cutoff_radius [REAL] (condKin, default=-1.) 

    call get_polymer_masses(param_library)
    call initialize_PIOUD()

    ! GET NUMBER OF THREADS        
    n_threads=1
    !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

  end subroutine initialize_CK_commons

  subroutine initialize_CK_auxiliary_simulation(n_auxsim,auxsim)
    integer, intent(in) :: n_auxsim
    TYPE(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: i,ibead 

    call deallocate_auxiliary_simulation(auxsim)

    auxsim%NUM_AUX_SIM = n_auxsim    
    auxsim%nu = nu

    allocate(auxsim%Fcl(nat,ndim)) ; auxsim%Fcl = 0._dp
    allocate(auxsim%k2(ndof,ndof)) ; auxsim%k2 = 0._dp
    if(n_auxsim>1) then
      allocate(auxsim%k2var(ndof,ndof))
    endif
    allocate(auxsim%k2Emat(ndof,ndof)) ; auxsim%k2Emat = 0._dp
    allocate(auxsim%k2Evals(ndof)) ; auxsim%k2Evals = 0._dp

    write(*,*) nu, auxsim%NUM_AUX_SIM
    allocate(auxsim%polymers(n_auxsim))
    DO i=1,auxsim%NUM_AUX_SIM
        allocate( &
            auxsim%polymers(i)%X(nu-1,nat,ndim), &
            auxsim%polymers(i)%EigV(nu-1,nat,ndim), &
            auxsim%polymers(i)%EigX(nu-1,nat,ndim), &
            auxsim%polymers(i)%F_beads(nu-1,nat,ndim), &
            auxsim%polymers(i)%Pot_beads(nu-1), &
            auxsim%polymers(i)%Delta(ndof), &
            auxsim%polymers(i)%k2(ndof,ndof) &
        )
        auxsim%polymers(i)%Delta=0._dp
        auxsim%polymers(i)%X=0._dp
        auxsim%polymers(i)%EigX=0._dp
        call sample_momenta(auxsim%polymers(i))
        ! polymers(i)%P_prev=polymers(i)%P
        ! polymers(i)%X_prev=polymers(i)%X    
    ENDDO

    if(auxsim%compute_WiLD_forces) then
      allocate(auxsim%FWiLD(ndof)) ; auxsim%FWiLD = 0._dp
      allocate(auxsim%CWiLD(ndof,ndof,ndof)) ; auxsim%CWiLD = 0._dp
      allocate(auxsim%sigmaWiLD(ndof,ndof)) ; auxsim%sigmaWiLD = 0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        allocate( &
            auxsim%polymers(i)%F(ndof), &
            auxsim%polymers(i)%G(ndof,ndof,ndof) &
        )
        auxsim%polymers(i)%F = 0._dp
        auxsim%polymers(i)%G = 0._dp
        ! polymers(i)%X_prev=polymers(i)%X    
      ENDDO
    endif

    if(auxsim%compute_Cew) then
      allocate(auxsim%k4(ndof,ndof,ndof,ndof)) ; auxsim%k4 = 0._dp
      auxsim%k6 = 0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        allocate( auxsim%polymers(i)%k4(ndof,ndof,ndof,ndof) )
        auxsim%polymers(i)%k4(:,:,:,:) = 0._dp
        auxsim%polymers(i)%k6 = 0._dp
      ENDDO
    endif


  end subroutine initialize_CK_auxiliary_simulation

  subroutine get_polymer_masses(param_library)
    IMPLICIT NONE
    TYPE(DICT_STRUCT), intent(in) :: param_library
    TYPE(DICT_STRUCT), POINTER :: m_lib
    CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
    LOGICAL, ALLOCATABLE :: is_sub(:)
    REAL(dp) :: m_tmp
    INTEGER :: i
    CHARACTER(2) :: symbol

    ALLOCATE(sqrt_mass(nat))
    
    if(.not. param_library%has_key(WIGAUX//"/auxiliary_masses")) then
        sqrt_mass=SQRT(mass)
        return
    endif

    m_lib => param_library%get_child(WIGAUX//"/auxiliary_masses")
    !! @input_file EW_NLGA/auxiliary_masses [DICT_STRUCT] (condKin, optional)
    write(*,*)
    write(*,*) "g-WiLD auxiliary masses:"
    call dict_list_of_keys(m_lib,keys,is_sub)
    DO i=1,size(keys)
      if(is_sub(i)) CYCLE
      m_tmp=m_lib%get(keys(i))
      symbol=trim(keys(i))
      symbol(1:1)=to_upper_case(symbol(1:1))
      write(*,*) symbol," : ", real(m_tmp/au%Mprot),"amu"
    ENDDO
      write(*,*)
      do i=1, nat
          sqrt_mass(i)=m_lib%get(trim(element_symbols(i)) &
                                          ,default=mass(i) )
          !if(sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", sqrt_mass(i)/Mprot," amu"                    
    enddo
    sqrt_mass=sqrt(sqrt_mass)

  end subroutine get_polymer_masses


  subroutine initialize_PIOUD()
    IMPLICIT NONE
    INTEGER :: i,j,INFO
    real(dp), allocatable ::thetaInv(:,:,:,:),OUsig2(:,:,:,:),TMP(:)
    real(dp) :: Id(2,2)

    allocate( &
        EigMat(nu-1,nu-1), &
        OmK(nu-1), &
        mOmK(nu-1,nat), &
        gammaExp(nu-1,nat), &
        sigmaBAOAB(nu-1,nat) &
    )

    !INITIALIZE DYNAMICAL MATRIX
    EigMat=0
    do i=1,nu-2
        EigMat(i,i)=2
        EigMat(i+1,i)=-1
        EigMat(i,i+1)=-1
    enddo
    EigMat(nu-1,nu-1)=2

    !SOLVE EIGENPROBLEM
    allocate(TMP(3*nu-1))
    CALL DSYEV('V','L',nu-1,EigMat,nu-1,Omk,TMP,3*nu-1,INFO)
    if(INFO/=0) then
        write(0,*) "[condKin.f90] Error during computation of eigenvalues for EigMat. code:",INFO
        stop 
    endif
    deallocate(TMP)
    Omk=SQRT(Omk)/dBeta
    DO i=1,nat
        mOmk(:,i)=Omk*sqrt(mass(i))/sqrt_mass(i) 
    ENDDO
    ! write(*,*)
    ! write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
    ! do i=1,nu
    !     write(*,*) i, Omk(i)*au%THz/(2*pi),"THz"
    ! enddo
    ! write(*,*)  
        

    allocate(expOmk(nu-1,2,2,nat))  

    Id=0
    Id(1,1)=1
    Id(2,2)=1

    allocate( &
        thetaInv(nu-1,2,2,nat), &
        OUsig2(nu-1,2,2,nat) &
    )
    if(allocated(OUsig)) deallocate(OUsig)
    allocate(OUsig(nu-1,2,2,nat)) 
    if(allocated(expOmk)) deallocate(expOmk)
    allocate(expOmk(nu-1,2,2,nat))              

    DO i=1,nat
        expOmk(:,1,1,i)=exp(-mOmk(:,i)*dt) &
                                *(1-mOmk(:,i)*dt)
        expOmk(:,1,2,i)=exp(-mOmk(:,i)*dt) &
                                *(-dt*mOmk(:,i)**2)
        expOmk(:,2,1,i)=exp(-mOmk(:,i)*dt)*dt
        expOmk(:,2,2,i)=exp(-mOmk(:,i)*dt) &
                                *(1+mOmk(:,i)*dt)

        thetaInv(:,1,1,i)=0._dp
        thetaInv(:,1,2,i)=-1._dp
        thetaInv(:,2,1,i)=1._dp/mOmk(:,i)**2
        thetaInv(:,2,2,i)=2._dp/mOmk(:,i)
    ENDDO

    !COMPUTE COVARIANCE MATRIX
    DO i=1,nat
        OUsig2(:,1,1,i)=(1._dp- (1._dp-2._dp*dt*mOmk(:,i) &
                            +2._dp*(dt*mOmk(:,i))**2 &
                        )*exp(-2._dp*mOmk(:,i)*dt) &
                        )/dBeta
        OUsig2(:,2,2,i)=(1._dp- (1._dp+2._dp*dt*mOmk(:,i) &
                            +2._dp*(dt*mOmk(:,i))**2 &
                        )*exp(-2._dp*mOmk(:,i)*dt) &
                        )/(dBeta*mOmk(:,i)**2)
        OUsig2(:,2,1,i)=2._dp*mOmk(:,i)*(dt**2) &
                        *exp(-2._dp*mOmk(:,i)*dt)/dBeta
        OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

        !COMPUTE CHOLESKY DECOMPOSITION
        OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
        OUsig(:,1,2,i)=0
        OUsig(:,2,1,i)=OUsig2(:,2,1,i)/OUsig(:,1,1,i)
        OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-OUsig(:,2,1,i)**2)
    ENDDO

    !CHECK CHOLESKY DECOMPOSITION
    ! write(*,*) "sum of squared errors on cholesky decomposition:"
    ! do i=1,nu
    ! 	write(*,*) i, sum( (matmul(OUsig(i,:,:),transpose(OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
    ! enddo

  end subroutine initialize_PIOUD

!-------------------------------------------------------------------

  subroutine deallocate_auxiliary_simulation(auxsim)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: i

    if(allocated(auxsim%polymers)) then
      do i = 1, size(auxsim%polymers)
        call deallocate_polymer(auxsim%polymers(i))
      enddo
      deallocate(auxsim%polymers)
    endif

  end subroutine deallocate_auxiliary_simulation

  subroutine deallocate_polymer(polymer)
    implicit none
    type(WIG_POLYMER_TYPE), intent(inout) :: polymer

    if(allocated(polymer%X)) deallocate(polymer%X)
    if(allocated(polymer%EigV)) deallocate(polymer%EigV)
    if(allocated(polymer%EigX)) deallocate(polymer%EigX)
    if(allocated(polymer%F_beads)) deallocate(polymer%F_beads)
    if(allocated(polymer%Pot_beads)) deallocate(polymer%Pot_beads)
    if(allocated(polymer%Delta)) deallocate(polymer%Delta)
    if(allocated(polymer%k2)) deallocate(polymer%k2)
    if(allocated(polymer%F)) deallocate(polymer%F)

    end subroutine deallocate_polymer

  subroutine sample_momenta(polymer)
    IMPLICIT NONE
    CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    INTEGER :: i,j,k

    DO k=1,ndim ; DO j=1,nat          
        call randGaussN(polymer%EigV(:,j,k))
        polymer%EigV(:,j,k)=polymer%EigV(:,j,k)/sqrt(dBeta)   !*sqrt_mass(j)    
    ENDDO  ; ENDDO
  end subroutine sample_momenta

  subroutine update_beads_forces(q,polymer)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(dp), INTENT(in) :: q(:,:)
		INTEGER :: i
    real(dp), allocatable :: F(:,:)

    allocate(F(nat,ndim))
        
	  do i=1,nu-1
      call get_pot_info(q+polymer%X(i,:,:),polymer%Pot_beads(i),F)
      polymer%F_beads(i,:,:) = F(:,:)
	  enddo
    deallocate(F)

	end subroutine update_beads_forces

  subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(dp), INTENT(in) :: tau
		INTEGER :: i,j
		real(dp), allocatable ::EigF(:),F(:)
    
    allocate(EigF(nu-1),F(nu-1))
    do j=1,ndim ; DO i=1,nat
        !COMPUTE FORCE FOR NORMAL MODES
        F(:)=polymer%F_beads(:,i,j)/sqrt_mass(i)      
        call DGEMV('T',nu-1,nu-1,1._dp,Eigmat,nu-1,F,1,0._dp,EigF,1)
        !EigF=matmul(EigmatTr,F)

        polymer%EigV(:,i,j)=polymer%EigV(:,i,j)+tau*EigF(:)
    ENDDO ; ENDDO
    deallocate(EigF,F)

	end subroutine apply_forces

  subroutine PIOUD_step(X,polymer)
		IMPLICIT NONE
    real(dp), intent(in) :: X(:,:)
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		integer :: i,j,k
    real(dp) :: EigV0,EigX0
    REAL(dp), allocatable :: R1(:),R2(:)

    allocate(R1(nu-1),R2(nu-1))
    !CALL apply_forces(polymer,0.5_dp*dt)

    do j=1,ndim ; do i=1,nat
        !GENERATE RANDOM VECTORS
        CALL RandGaussN(R1)
        CALL RandGaussN(R2)

        DO k=1,nu-1
          !SAVE INITIAL VALUE TEMPORARILY
          EigV0=polymer%EigV(k,i,j)
          EigX0=polymer%EigX(k,i,j)          

          !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
          polymer%EigV(k,i,j)=EigV0*expOmk(k,1,1,i) &
              +EigX0*expOmk(k,1,2,i) &
              +OUsig(k,1,1,i)*R1(k)

          polymer%EigX(k,i,j)=EigV0*expOmk(k,2,1,i) &
              +EigX0*expOmk(k,2,2,i) &
              +OUsig(k,2,1,i)*R1(k)+OUsig(k,2,2,i)*R2(k)
        ENDDO
    enddo ; enddo

    deallocate(R1,R2)
        
    do j=1,ndim ; do i=1,nat    
        !TRANSFORM BACK IN COORDINATES
        !polymer%X(:,i,j)=matmul(EigMat,polymer%EigX(:,i,j))/sqrt_mass(i)
        call DGEMV('N',nu-1,nu-1,1._dp/sqrt_mass(i),EigMat,nu-1,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
    enddo ; enddo
    

    !COMPUTE FORCES	
    CALL update_beads_forces(X,polymer) 

    !APPLY FORCES
    CALL apply_forces(polymer,dt)    

	end subroutine PIOUD_step

!----------------------------------------------------------

  subroutine cutoff_k2(k2,X)
    implicit none
    real(dp), intent(in) :: X(:,:)
    real(dp),intent(inout) :: k2(:,:)
    integer :: i1,j1,i2,j2,c1,c2
    real(dp) :: r

    if(cutoff_radius_k2<=0) return

    do i1=1,nat-1 ; do i2=i1+1,nat  
      r = NORM2(X(i1,:)-X(i2,:))
      if(r>=cutoff_radius_k2) then
        do j1=1,ndim 
          c1=i1+(j1-1)*nat
          do j2=1,ndim
            c2=i2+(j2-1)*nat
            k2(c1,c2)=0._dp
            k2(c2,c1)=0._dp
          enddo
        enddo
      endif    
    enddo ; enddo

  end subroutine cutoff_k2

  subroutine remove_negative_eigenvalues(k2)
    implicit none
    real(dp), intent(inout) :: k2(:,:)
    real(dp), allocatable :: k2evals(:), k2emat(:,:)
    INTEGER :: INFO,i1,j1,i2,j2,c1,c2,i
    
    c1=0
    DO j1=1,ndim ; do i1=1,nat
      c1=c1+1
      !k2tmp(c1,c1) = mass(i1)*nu*kT
      c2=0
      DO j2=1,ndim ; do i2=1,nat
        c2=c2+1
        k2(c1,c2)=k2(c1,c2)/sqrt(mass(i1)*mass(i2))/kT
      ENDDO ; ENDDO
    ENDDO ; ENDDO

    allocate(k2evals(ndof),k2emat(ndof,ndof))
    call sym_mat_diag(k2,k2Evals,k2EMat,INFO)
    IF(INFO/=0) STOP "[condKin.f90] Error: could not diagonalize Phi"  
    k2=0._dp
    DO i=1,ndof
      if(k2Evals(i)<0) then
        write(*,*) "CK: found a negative Phi eigenvalue ",k2Evals(i),i
        k2Evals(i)=1.e-8
      endif
      k2(i,i)=k2Evals(i)
    ENDDO
    k2 = matmul(k2EMat,matmul(k2,transpose(k2EMat)))
    deallocate(k2evals,k2emat)

    c1=0
    DO j1=1,ndim ; do i1=1,nat
      c1=c1+1
      !k2tmp(c1,c1) = mass(i1)*nu*kT
      c2=0
      DO j2=1,ndim ; do i2=1,nat
        c2=c2+1
        k2(c1,c2)=k2(c1,c2)*sqrt(mass(i1)*mass(i2))*kT
      ENDDO ; ENDDO
    ENDDO ; ENDDO

    

  end subroutine remove_negative_eigenvalues

end module condKin
