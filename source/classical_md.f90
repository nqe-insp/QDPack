module classical_md
  use kinds
  use system_commons
  use nested_dictionaries
  use file_handler
  use matrix_operations
  use potential
  use qtb_types
  use correlation_functions, only: compute_autoTCF_WK
  implicit none

  real(dp), save :: gamma0,gamma_exp
  real(dp), save :: dt,dt2
  integer, save :: avg_counter
  real(dp), save :: temp_avg
  real(dp), save :: Ekin_avg
  logical, save :: fast_forward_langevin, stochastic
  logical, save :: qtb_thermostat
  type(QTB_type), save :: qtb

  PRIVATE
  PUBLIC :: initialize_cl_sampler, update_cl_sample,qtb_thermostat,qtb &
            ,classical_dynamics_IVR

contains

  subroutine initialize_cl_sampler(param_library,tcf_name,dt_sampler,category)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    character(:), allocatable, intent(inout) :: tcf_name
    real(dp), intent(out),OPTIONAL ::dt_sampler
    character(*), intent(in),optional :: category
    character(:),allocatable :: category_

    if(present(category)) then
      category_ = category
    else   
      category_ = "parameters"
    endif
    
    dt = param_library%get(category_//"/dt")
    !! @input_file parameters/dt [REAL] (classical_md)
    if(present(dt_sampler)) dt_sampler = dt
    
    if(dt<=0) then
      write(0,*) "[classical_md.f90] Error: "//category_//"/dt must be positive"
      STOP "Execution stopped."
    endif
    dt2 = 0.5_dp*dt    

    gamma0 = param_library%get(category_//"/gamma",default=0._dp)
    !! @input_file parameters/gamma [REAL] (classical_md, default=0.)
    stochastic = gamma0 > 0
    fast_forward_langevin = param_library%get(category_//"/sampler_fast_forward_langevin",default=.FALSE.)    
    !! @input_file parameters/sampler_fast_forward_langevin [LOGICAL] (classical_md, default=.FALSE.)
    if(stochastic .AND. fast_forward_langevin) write(*,*) "fast forward langevin for sampler"    
    if(stochastic) then
      gamma_exp = exp(-gamma0*dt)
    endif
    
    qtb_thermostat = param_library%get(category_//"/use_qtb_thermostat",default=.FALSE.)
    !! @input_file parameters/use_qtb_thermostat [LOGICAL] (classical_md, default=.FALSE.)
    if(qtb_thermostat) then
      call qtb%initialize(param_library, tcf_name,dt ,gamma0, .FALSE.)
    else
      tcf_name = "ClMD"
    endif

    Ekin_avg = 0._dp
    temp_avg = 0._dp
    avg_counter = 0 

  end subroutine initialize_cl_sampler

  subroutine update_cl_sample(X,P,nsteps,verbose,Xtraj,Ptraj)
    implicit none
    real(dp), intent(inout) :: X(:,:),P(:,:)
    real(dp), intent(inout), OPTIONAL :: Xtraj(:,:,:),Ptraj(:,:,:)
    integer, intent(in) :: nsteps
    logical, intent(in) :: verbose
    integer :: istep,i,j
    real(dp) :: temp,temp_sum
    real(dp) :: Ekin,Ekin_sum
    real(dp), allocatable :: F(:,:)
    logical :: store_traj

    allocate(F(nat,ndim))

    store_traj = present(Xtraj) .AND. present(Ptraj)

    temp_sum = 0.
    Ekin_sum = 0.
    avg_counter = avg_counter + 1

    F=-dPot(X)

    do istep = 1,nsteps 

      P(:,:) = P(:,:) + dt2*F(:,:)
      call apply_A(X,P,dt2)
      if(stochastic) then
        if(fast_forward_langevin) then
          call apply_O_FFL(P)
        else
          call apply_O(P)
        endif        
      endif
      call apply_A(X,P,dt2)

      F=-dPot(X)
      P(:,:) = P(:,:) + dt2*F(:,:)

      if(store_traj) then
        Xtraj(:,:,istep)=X(:,:)
        Ptraj(:,:,istep)=P(:,:)
      endif
      call compute_kinetic_energy(P,Ekin,temp)
      temp_sum = temp_sum + temp
      Ekin_sum = Ekin_sum + Ekin
    enddo  

    temp_avg = temp_avg + (temp_sum/nsteps - temp_avg)/avg_counter
    Ekin_avg = Ekin_avg + (Ekin_sum/nsteps - Ekin_avg)/avg_counter

    if(verbose) then
      write(*,*) "temperature= ",real(temp_avg,sp),"K"
      write(*,*) "Ekin= ",real(Ekin_avg,sp)
      temp_avg=0._dp
      Ekin_avg=0._dp
      avg_counter=0
    endif

    deallocate(F)

  end subroutine update_cl_sample


!-------------------------------------------------

  subroutine apply_A(X,P,tau)
    implicit none
    real(dp), intent(inout) :: X(:,:),P(:,:)
    real(dp), intent(in) :: tau
    integer :: i,j,k

    do j=1,ndim
      X(:,j)=X(:,j)+tau*P(:,j)/mass(:)
    enddo

  end subroutine apply_A

  subroutine apply_O(P)
    implicit none
    real(dp), intent(inout) :: P(:,:)
    real(dp), allocatable :: R(:,:)
    integer :: j

    allocate(R(nat,ndim))
    if(qtb_thermostat) then
      R = qtb%get_force(P)*dt
    else
      do j=1,ndim
        call randGaussN(R(:,j))
        R(:,j)=R(:,j)*SQRT(mass(:)*kT*(1._dp-gamma_exp**2))
      enddo  
    endif    
    
    do j=1,ndim      
      P(:,j) = P(:,j)*gamma_exp + R(:,j) 
    enddo
  end subroutine apply_O

  subroutine apply_O_FFL(P)
    implicit none
    real(dp), intent(inout) :: P(:,:)
    real(dp), allocatable :: R(:,:)
    integer :: i,j,k
    real(dp) :: Pold, Pnew

    allocate(R(nat,ndim))
    if(qtb_thermostat) then
      R = qtb%get_force(P)*dt
    else
      do j=1,ndim
        call randGaussN(R(:,j))
        R(:,j)=R(:,j)*SQRT(mass(:)*kT*(1._dp-gamma_exp**2))
      enddo
    endif

    do i=1,nat
      Pold = NORM2(P(i,:))
      Pnew = NORM2(P(i,:)*gamma_exp + R(i,:))
      P(i,:) = Pnew * P(i,:) / Pold
    enddo
  end subroutine apply_O_FFL

  subroutine classical_dynamics_IVR(X0,P0,nsteps,dt,Cpp,Ek,Cmumu,use_WK)
    implicit none
    real(dp), intent(in) :: X0(:,:),P0(:,:)
    integer, intent(in) :: nsteps
    real(dp), intent(in) :: dt
    real(dp), intent(inout) :: Cpp(:,:),Ek(:,:)
    real(dp), intent(inout), OPTIONAL ::  Cmumu(:,:)
    LOGICAL, intent(in), OPTIONAL :: use_WK
    real(dp), allocatable :: X(:,:),P(:,:,:),F(:,:),mutraj(:,:)
    real(dp) :: dt2,temp
    real(dp) :: mu0(ndim),mu(ndim)
    integer :: istep,i,j,c 
    LOGICAL :: use_WK_

    call compute_kinetic_energy_array(P0,Ek(:,1))
    
    allocate(X(nat,ndim),P(nat,ndim,nsteps),F(nat,ndim))
    X(:,:)=X0(:,:) ; P(:,:,1)=P0(:,:)
    F = -dPot(X)
    dt2=0.5_dp*dt    

    if(present(Cmumu)) then
      allocate(mutraj(ndim,nsteps))
      Cmumu(:,:) = 0._dp   
      call get_dipole(X0,P0,mu0) 
      mutraj(:,1)=mu0
    endif    
    
    do istep = 2,nsteps
      ! VELOCITY VERLET PROPAGATION
      P(:,:,istep) = P(:,:,istep-1) + dt2*F
      do j=1,ndim
        X(:,j) = X(:,j) + dt*P(:,j,istep)/mass(:)
      enddo
      F = -dPot(X)
      P(:,:,istep) = P(:,:,istep) + dt2*F

      call compute_kinetic_energy_array(P(:,:,istep),Ek(:,istep))
      if(present(Cmumu)) then  
        call get_dipole(X,P(:,:,istep),mutraj(:,istep))
      endif
    enddo

    use_WK_=.FALSE.
    if(present(use_WK)) use_WK_=use_WK

    if(use_WK_) then
      call compute_autoTCF_WK(RESHAPE(P,(/ndof,nsteps/)),nsteps,dt,Cpp)
    else
      do i=1,nsteps
        Cpp(:,istep)=RESHAPE(P0*P(:,:,istep),(/ndof/))
      enddo
    endif
    if(present(Cmumu)) then
      if(use_WK_) then
        call compute_autoTCF_WK(mutraj,nsteps,dt,Cmumu)
      else
        do istep=1,nsteps
          Cmumu(:,istep)=mu0(:)*mutraj(:,istep)
        enddo
      endif
      deallocate(mutraj)
    endif


    deallocate(X,P,F)

  end subroutine classical_dynamics_IVR

end module classical_md
