module wigner_EW
  use kinds
  use nested_dictionaries
  use system_commons
  use timer_module
  use random
  use potential
  use matrix_operations
  implicit none

  PRIVATE
  PUBLIC :: WIGAUX &
            ,initialize_wigner_commons &
            ,initialize_auxiliary_simulation &
            ,compute_EW_parameters &
            ,WIG_AUX_SIM_TYPE &
            ,sample_momentum_EW &
            ,compute_P_weights_EW &
            ,sample_wigner_momentum &
            ,compute_EW_control &
            ,compute_EW_correction &
            , WIG_POLYMER_TYPE

  integer, save :: n_threads
  

  CHARACTER(*), PARAMETER :: WIGAUX = "EW_NLGA"

  real(dp), save :: dt
  INTEGER, save :: nu !number of beads
  real(dp), save :: dBeta
  real(dp), save :: cutoff_radius_k2


  real(dp), allocatable, save :: sqrt_mass(:)
  real(dp), save :: delta_mass_factor, sqrt_delta_mass_factor  

  ! POLYMER PARAMETERS
  real(dp), allocatable, save :: EigMat(:,:)
  real(dp), allocatable, save :: OmK(:)
  real(dp), allocatable, save :: mOmK(:,:)

  !PIOUD PARAMETERS
  ! real(dp), allocatable :: mu(:,:,:)
  real(dp), allocatable, save :: OUsig(:,:,:,:)
  real(dp), allocatable, save :: expOmk(:,:,:,:)

  ! BAOAB PARAMETERS
  real(dp), allocatable, save :: gammaExp(:,:)
  real(dp), allocatable, save :: sigmaBAOAB(:,:)
  LOGICAL, save :: BAOAB_num

  TYPE WIG_POLYMER_TYPE
    real(dp), allocatable :: X(:,:,:)
    real(dp), allocatable :: EigX(:,:,:)
    real(dp), allocatable :: EigV(:,:,:)

    real(dp), allocatable :: F_beads(:,:,:)
    real(dp), allocatable :: Pot_beads(:)

    real(dp), allocatable :: Delta(:)

    real(dp), allocatable :: k2(:,:)
    real(dp), allocatable :: F(:),G(:,:,:)

    real(dp), allocatable :: k4(:,:,:,:)
    real(dp) :: k6

  END TYPE WIG_POLYMER_TYPE

  TYPE WIG_AUX_SIM_TYPE
    INTEGER :: NUM_AUX_SIM
    INTEGER :: nsteps, nsteps_therm
    
    integer :: nu

    real(dp), allocatable :: k2(:,:)
    real(dp), allocatable :: Fcl(:,:)
    real(dp), allocatable :: k2var(:,:)
    real(dp) :: Potcl
    real(dp) :: Cew(2),k6
    real(dp), allocatable :: k4(:,:,:,:)

    real(dp), allocatable :: k2Emat(:,:),K2Evals(:)

    real :: computation_time

    logical :: compute_WiLD_forces=.FALSE.
    logical :: compute_Cew=.FALSE.
    real(dp), allocatable :: CWiLD(:,:,:),FWiLD(:),sigmaWiLD(:,:)

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), allocatable :: polymers
  END TYPE WIG_AUX_SIM_TYPE    

CONTAINS

  subroutine compute_EW_parameters(X,auxsim)
    !$ USE OMP_LIB
    IMPLICIT NONE
		REAL(dp), intent(in) :: X(:,:)
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    INTEGER :: i,j,k,INFO,i_thread,c,u
    TYPE(timer_type) :: timer_full, timer_elem
    real(dp), allocatable :: k2tmp(:,:)

    if(ANY(ISNAN(X))) STOP "[wigner_EW.f90] ERROR: system diverged!"

    call timer_full%start()

    ! GET CLASSICAL FORCE     
    call get_pot_info(X,auxsim%Potcl,auxsim%Fcl)        

    i_thread=1
    !$OMP PARALLEL DO LASTPRIVATE(i_thread)    
    DO i=1,auxsim%NUM_AUX_SIM
        !$ i_thread = OMP_GET_THREAD_NUM()+1         
        call sample_forces(X,auxsim%nsteps,auxsim%nsteps_therm,auxsim%polymers(i) &
            ,auxsim%compute_WiLD_forces,auxsim%compute_Cew)
    ENDDO
    !$OMP END PARALLEL DO

    if(auxsim%NUM_AUX_SIM>1) then
      
      auxsim%k2(:,:)=0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        auxsim%k2(:,:)=auxsim%k2(:,:) + auxsim%polymers(i)%k2(:,:)
      ENDDO
      auxsim%k2(:,:)=auxsim%k2(:,:)/auxsim%NUM_AUX_SIM

      if(auxsim%compute_Cew) then
        auxsim%k4=0
        auxsim%k6=0
        DO i=1,auxsim%NUM_AUX_SIM
          auxsim%k4 = auxsim%k4 + auxsim%polymers(i)%k4
          auxsim%k6 = auxsim%k6 + auxsim%polymers(i)%k6
        ENDDO
        auxsim%k4 = auxsim%k4/auxsim%NUM_AUX_SIM
        auxsim%k6 = auxsim%k6/auxsim%NUM_AUX_SIM
      endif

      allocate(k2tmp(ndof,ndof))
      auxsim%k2var(:,:)=0._dp 
      DO i=1,auxsim%NUM_AUX_SIM
        k2tmp(:,:) = auxsim%polymers(i)%k2(:,:) - auxsim%k2(:,:)
        auxsim%k2var(:,:) = auxsim%k2var +  k2tmp(:,:)**2
      ENDDO
      auxsim%k2var = auxsim%k2var/REAL(auxsim%NUM_AUX_SIM*(auxsim%NUM_AUX_SIM-1),dp)
      deallocate(k2tmp)
    else
      auxsim%k2(:,:) = auxsim%polymers(1)%k2(:,:)
      if(auxsim%compute_Cew) then
        auxsim%k4=auxsim%polymers(1)%k4
        auxsim%k6=auxsim%polymers(1)%k6
      endif
    endif

    call sym_mat_diag(auxsim%k2,auxsim%k2Evals,auxsim%k2EMat,INFO)
    IF(INFO/=0) STOP "[wigner_EW.f90] Error: could not diagonalize k2"  
    
    auxsim%computation_time = timer_full%elapsed_time()

  end subroutine compute_EW_parameters

  subroutine sample_forces(X,nsteps,nsteps_therm,polymer,compute_WiLD,compute_Cew)
    IMPLICIT NONE
	real(dp), intent(in) :: X(:,:)
    integer, intent(in) :: nsteps,nsteps_therm
    logical, intent(in) :: compute_WiLD,compute_Cew
    CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    INTEGER :: m,n_dof,ios
    INTEGER :: i,j,k,l,c,j1,i1
    LOGICAL :: move_accepted
    REAL(dp) :: D2
    REAL(dp), allocatable :: F(:)

    ! reinitialize polymer
    call sample_momenta(polymer)
    if(.not. compute_WiLD) then
      polymer%X(:,:,:)=0._dp
      polymer%EigX(:,:,:)=0._dp
    endif

    ! initialize averages
    polymer%k2(:,:) = 0._dp
    if(compute_WiLD) then
      allocate(F(ndof))
      F=0._dp
      polymer%F(:) = 0._dp
      polymer%G(:,:,:) = 0._dp
    endif
    
    if(compute_Cew) then
      polymer%k4=0._dp
      polymer%k6=0._dp
    endif

    !THERMALIZATION
    DO m=1,nsteps_therm
      CALL PIOUD_step(X,polymer)
    ENDDO

    !PRODUCTION
    DO m=1,nsteps
      !GENERATE A NEW CONFIG
      CALL PIOUD_step(X,polymer)

      polymer%Delta=RESHAPE(2._dp*polymer%X(nu,:,:),(/ndof/))
      !COMPUTE ONLY THE LOWER TRIANGULAR PART
      do j=1,ndof ; do i=j,ndof
        polymer%k2(i,j)=polymer%k2(i,j)  &
          + polymer%Delta(i)*polymer%Delta(j)
      enddo ; enddo

      if(compute_Cew) then
        do l=1,ndof ; do k=l,ndof ;	do j=k,ndof ; do i=j,ndof
            polymer%k4(i,j,k,l)= polymer%k4(i,j,k,l) &
                      + polymer%Delta(i)*polymer%Delta(j) &
                          *polymer%Delta(k)*polymer%Delta(l)
        enddo ; enddo ; enddo ; enddo
        polymer%k6 = polymer%k6 + polymer%Delta(1)**6
      endif

      if(compute_WiLD) then
        F=RESHAPE(SUM(polymer%F_beads(:,:,:),dim=1),(/ndof/))/real(nu,dp)
        polymer%F=polymer%F + F      
        DO c=1,ndof
          do j=1,ndof ; do i=j,ndof            
            polymer%G(i,j,c)=polymer%G(i,j,c) + polymer%Delta(i)*polymer%Delta(j)*F(c)
          ENDDO ; ENDDO
        ENDDO
      endif
    ENDDO

    !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
    DO j=2,ndof ; DO i=1,j-1
      polymer%k2(i,j)=polymer%k2(j,i)
    ENDDO ; ENDDO    
    polymer%k2 = polymer%k2/real(nsteps,dp)
    call cutoff_k2(polymer%k2,X)

    !symmetrize k4
    if(compute_Cew) then
      do l=1,ndof ; do k=l,ndof ; do j=k,ndof ; do i=j,ndof		
        polymer%k4(i,j,k,l) = polymer%k4(i,j,k,l)/real(nsteps,dp)
        call fill_permutations(i,j,k,l,polymer%k4)
      enddo ; enddo ; enddo ; enddo
      polymer%k6 = polymer%k6/real(nsteps,dp)
    endif
    

    if(compute_WiLD) then
      polymer%F = polymer%F/real(nsteps,dp)
      do c=1,ndof
        DO j=2,ndof ; DO i=1,j-1
          polymer%G(i,j,c)=polymer%G(j,i,c)
        ENDDO ; ENDDO
      enddo
      polymer%G = polymer%G/real(nsteps,dp)
    endif

  end subroutine sample_forces

!-----------------------------------------------------------------
! INITIALIZATIONS

  subroutine initialize_wigner_commons(param_library)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    integer :: i,nbeads

    nu = param_library%get(WIGAUX//"/nbeads")
    !! @input_file EW_NLGA/n_beads [INTEGER] (wigner_EW)
    dt = param_library%get(WIGAUX//"/dt")
    !! @input_file EW_NLGA/dt [REAL] (wigner_EW)
    dBeta=beta/REAL(nu,dp)

    cutoff_radius_k2 = param_library%get(WIGAUX//"/k2_cutoff_radius",default=-1._dp)
    !! @input_file EW_NLGA/k2_cutoff_radius [REAL] (wigner_EW, default=-1.)

    call get_polymer_masses(param_library)
    call initialize_PIOUD()

    ! GET NUMBER OF THREADS        
    n_threads=1
    !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

  end subroutine initialize_wigner_commons

  subroutine initialize_auxiliary_simulation(n_auxsim,auxsim)
    integer, intent(in) :: n_auxsim
    TYPE(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: i,ibead 

    call deallocate_auxiliary_simulation(auxsim)

    auxsim%NUM_AUX_SIM = n_auxsim    
    auxsim%nu = nu

    allocate(auxsim%Fcl(nat,ndim)) ; auxsim%Fcl = 0._dp
    allocate(auxsim%k2(ndof,ndof)) ; auxsim%k2 = 0._dp
    if(n_auxsim>1) then
      allocate(auxsim%k2var(ndof,ndof))
    endif
    allocate(auxsim%k2Emat(ndof,ndof)) ; auxsim%k2Emat = 0._dp
    allocate(auxsim%k2Evals(ndof)) ; auxsim%k2Evals = 0._dp

    allocate(auxsim%polymers(n_auxsim))
    DO i=1,auxsim%NUM_AUX_SIM
        allocate( &
            auxsim%polymers(i)%X(nu,nat,ndim), &
            auxsim%polymers(i)%EigV(nu,nat,ndim), &
            auxsim%polymers(i)%EigX(nu,nat,ndim), &
            auxsim%polymers(i)%F_beads(0:nu,nat,ndim), &
            auxsim%polymers(i)%Pot_beads(0:nu), &
            auxsim%polymers(i)%Delta(ndof), &
            auxsim%polymers(i)%k2(ndof,ndof) &
        )
        auxsim%polymers(i)%Delta=0._dp
        auxsim%polymers(i)%X=0._dp
        auxsim%polymers(i)%EigX=0._dp
        call sample_momenta(auxsim%polymers(i))
        ! polymers(i)%P_prev=polymers(i)%P
        ! polymers(i)%X_prev=polymers(i)%X    
    ENDDO

    if(auxsim%compute_WiLD_forces) then
      allocate(auxsim%FWiLD(ndof)) ; auxsim%FWiLD = 0._dp
      allocate(auxsim%CWiLD(ndof,ndof,ndof)) ; auxsim%CWiLD = 0._dp
      allocate(auxsim%sigmaWiLD(ndof,ndof)) ; auxsim%sigmaWiLD = 0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        allocate( &
            auxsim%polymers(i)%F(ndof), &
            auxsim%polymers(i)%G(ndof,ndof,ndof) &
        )
        auxsim%polymers(i)%F = 0._dp
        auxsim%polymers(i)%G = 0._dp
        ! polymers(i)%X_prev=polymers(i)%X    
      ENDDO
    endif

    if(auxsim%compute_Cew) then
      allocate(auxsim%k4(ndof,ndof,ndof,ndof)) ; auxsim%k4 = 0._dp
      auxsim%k6 = 0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        allocate( auxsim%polymers(i)%k4(ndof,ndof,ndof,ndof) )
        auxsim%polymers(i)%k4(:,:,:,:) = 0._dp
        auxsim%polymers(i)%k6 = 0._dp
      ENDDO
    endif


  end subroutine initialize_auxiliary_simulation

  subroutine get_polymer_masses(param_library)
    IMPLICIT NONE
    TYPE(DICT_STRUCT), intent(in) :: param_library
    TYPE(DICT_STRUCT), POINTER :: m_lib
    CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
    LOGICAL, ALLOCATABLE :: is_sub(:)
    REAL(dp) :: m_tmp
    INTEGER :: i
    CHARACTER(2) :: symbol

    ALLOCATE(sqrt_mass(nat))
    
    if(.not. param_library%has_key(WIGAUX//"/auxiliary_masses")) then
        sqrt_mass=SQRT(mass)
        return
    endif

    m_lib => param_library%get_child(WIGAUX//"/auxiliary_masses")
    !! @input_file EW_NLGA/auxiliary_masses [DICT_STRUCT] (wigner_EW, optional)
    write(*,*)
    write(*,*) "g-WiLD auxiliary masses:"
    call dict_list_of_keys(m_lib,keys,is_sub)
    DO i=1,size(keys)
      if(is_sub(i)) CYCLE
      m_tmp=m_lib%get(keys(i))
      symbol=trim(keys(i))
      symbol(1:1)=to_upper_case(symbol(1:1))
      write(*,*) symbol," : ", real(m_tmp/au%Mprot),"amu"
    ENDDO
      write(*,*)
      do i=1, nat
          sqrt_mass(i)=m_lib%get(trim(element_symbols(i)) &
                                          ,default=mass(i) )
          !if(sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", sqrt_mass(i)/Mprot," amu"                    
    enddo
    sqrt_mass=sqrt(sqrt_mass)

  end subroutine get_polymer_masses


  subroutine initialize_PIOUD()
    IMPLICIT NONE
    INTEGER :: i,j,INFO
    real(dp), allocatable ::thetaInv(:,:,:,:),OUsig2(:,:,:,:),TMP(:)
    real(dp) :: Id(2,2)

    allocate( &
        EigMat(nu,nu), &
        OmK(nu), &
        mOmK(nu,nat), &
        gammaExp(nu,nat), &
        sigmaBAOAB(nu,nat) &
    )

    !INITIALIZE DYNAMICAL MATRIX
    EigMat=0
    do i=1,nu-1
        EigMat(i,i)=2
        EigMat(i+1,i)=-1
        EigMat(i,i+1)=-1
    enddo
    EigMat(nu,nu)=2
    EigMat(1,nu)=-1
    EigMat(nu,1)=-1
    EigMat(nu-1,nu)=1
    EigMat(nu,nu-1)=1

    !SOLVE EIGENPROBLEM
    allocate(TMP(3*nu-1))
    CALL DSYEV('V','L',nu,EigMat,nu,Omk,TMP,3*nu-1,INFO)
    if(INFO/=0) then
        write(0,*) "[wigner_EW.f90] Error during computation of eigenvalues for EigMat. code:",INFO
        stop 
    endif
    deallocate(TMP)
    Omk=SQRT(Omk)/dBeta
    DO i=1,nat
        mOmk(:,i)=Omk*sqrt(mass(i))/sqrt_mass(i) 
    ENDDO
    ! write(*,*)
    ! write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
    ! do i=1,nu
    !     write(*,*) i, Omk(i)*au%THz/(2*pi),"THz"
    ! enddo
    ! write(*,*)  
        

    allocate(expOmk(nu,2,2,nat))  

    Id=0
    Id(1,1)=1
    Id(2,2)=1

    allocate( &
        thetaInv(nu,2,2,nat), &
        OUsig2(nu,2,2,nat) &
    )
    if(allocated(OUsig)) deallocate(OUsig)
    allocate(OUsig(nu,2,2,nat)) 
    if(allocated(expOmk)) deallocate(expOmk)
    allocate(expOmk(nu,2,2,nat))              

    DO i=1,nat
        expOmk(:,1,1,i)=exp(-mOmk(:,i)*dt) &
                                *(1-mOmk(:,i)*dt)
        expOmk(:,1,2,i)=exp(-mOmk(:,i)*dt) &
                                *(-dt*mOmk(:,i)**2)
        expOmk(:,2,1,i)=exp(-mOmk(:,i)*dt)*dt
        expOmk(:,2,2,i)=exp(-mOmk(:,i)*dt) &
                                *(1+mOmk(:,i)*dt)

        thetaInv(:,1,1,i)=0._dp
        thetaInv(:,1,2,i)=-1._dp
        thetaInv(:,2,1,i)=1._dp/mOmk(:,i)**2
        thetaInv(:,2,2,i)=2._dp/mOmk(:,i)
    ENDDO

    !COMPUTE COVARIANCE MATRIX
    DO i=1,nat
        OUsig2(:,1,1,i)=(1._dp- (1._dp-2._dp*dt*mOmk(:,i) &
                            +2._dp*(dt*mOmk(:,i))**2 &
                        )*exp(-2._dp*mOmk(:,i)*dt) &
                        )/dBeta
        OUsig2(:,2,2,i)=(1._dp- (1._dp+2._dp*dt*mOmk(:,i) &
                            +2._dp*(dt*mOmk(:,i))**2 &
                        )*exp(-2._dp*mOmk(:,i)*dt) &
                        )/(dBeta*mOmk(:,i)**2)
        OUsig2(:,2,1,i)=2._dp*mOmk(:,i)*(dt**2) &
                        *exp(-2._dp*mOmk(:,i)*dt)/dBeta
        OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

        !COMPUTE CHOLESKY DECOMPOSITION
        OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
        OUsig(:,1,2,i)=0
        OUsig(:,2,1,i)=OUsig2(:,2,1,i)/OUsig(:,1,1,i)
        OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-OUsig(:,2,1,i)**2)
    ENDDO

    !CHECK CHOLESKY DECOMPOSITION
    ! write(*,*) "sum of squared errors on cholesky decomposition:"
    ! do i=1,nu
    ! 	write(*,*) i, sum( (matmul(OUsig(i,:,:),transpose(OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
    ! enddo

  end subroutine initialize_PIOUD

!-------------------------------------------------------------------

  subroutine deallocate_auxiliary_simulation(auxsim)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: i

    if(allocated(auxsim%polymers)) then
      do i = 1, size(auxsim%polymers)
        call deallocate_polymer(auxsim%polymers(i))
      enddo
      deallocate(auxsim%polymers)
    endif

  end subroutine deallocate_auxiliary_simulation

  subroutine deallocate_polymer(polymer)
    implicit none
    type(WIG_POLYMER_TYPE), intent(inout) :: polymer

    if(allocated(polymer%X)) deallocate(polymer%X)
    if(allocated(polymer%EigV)) deallocate(polymer%EigV)
    if(allocated(polymer%EigX)) deallocate(polymer%EigX)
    if(allocated(polymer%F_beads)) deallocate(polymer%F_beads)
    if(allocated(polymer%Pot_beads)) deallocate(polymer%Pot_beads)
    if(allocated(polymer%Delta)) deallocate(polymer%Delta)
    if(allocated(polymer%k2)) deallocate(polymer%k2)
    if(allocated(polymer%F)) deallocate(polymer%F)

    end subroutine deallocate_polymer

  subroutine sample_momenta(polymer)
    IMPLICIT NONE
    CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    INTEGER :: i,j,k

    DO k=1,ndim ; DO j=1,nat          
        call randGaussN(polymer%EigV(:,j,k))
        polymer%EigV(:,j,k)=polymer%EigV(:,j,k)/sqrt(dBeta)   !*sqrt_mass(j)    
    ENDDO  ; ENDDO
  end subroutine sample_momenta

  subroutine update_beads_forces(q,polymer)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(dp), INTENT(in) :: q(:,:)
		INTEGER :: i
    real(dp), allocatable :: F(:,:)

    allocate(F(nat,ndim))
        
	do i=1,nu-1
      call get_pot_info(q+polymer%X(i,:,:),polymer%Pot_beads(i),F)
      polymer%F_beads(i,:,:) = F(:,:)
	  enddo
    call get_pot_info(q+polymer%X(nu,:,:),polymer%Pot_beads(0),F)
    polymer%F_beads(0,:,:)=0.5_dp*F(:,:)

    call get_pot_info(q-polymer%X(nu,:,:),polymer%Pot_beads(nu),F)
    polymer%F_beads(nu,:,:)=0.5_dp*F(:,:)

    deallocate(F)
    polymer%Pot_beads(0)=0.5_dp*polymer%Pot_beads(0)
    polymer%Pot_beads(nu)=0.5_dp*polymer%Pot_beads(nu)

	end subroutine update_beads_forces

  subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(dp), INTENT(in) :: tau
		INTEGER :: i,j
		real(dp), allocatable ::EigF(:),F(:)
    
    allocate(EigF(nu),F(nu))
    do j=1,ndim ; DO i=1,nat
        !COMPUTE FORCE FOR NORMAL MODES
        F(1:nu-1)=polymer%F_beads(1:nu-1,i,j)/sqrt_mass(i)
        F(nu)=(polymer%F_beads(0,i,j)-polymer%F_beads(nu,i,j))/sqrt_mass(i)        
        call DGEMV('T',nu,nu,1._dp,Eigmat,nu,F,1,0._dp,EigF,1)
        !EigF=matmul(EigmatTr,F)

        polymer%EigV(:,i,j)=polymer%EigV(:,i,j)+tau*EigF(:)
    ENDDO ; ENDDO
    deallocate(EigF,F)

	end subroutine apply_forces

  subroutine PIOUD_step(X,polymer)
		IMPLICIT NONE
    real(dp), intent(in) :: X(:,:)
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		integer :: i,j,k
    real(dp) :: EigV0,EigX0
    REAL(dp), allocatable :: R1(:),R2(:)

    allocate(R1(nu),R2(nu))
    !CALL apply_forces(polymer,0.5_dp*dt)

    do j=1,ndim ; do i=1,nat
        !GENERATE RANDOM VECTORS
        CALL RandGaussN(R1)
        CALL RandGaussN(R2)

        DO k=1,nu
          !SAVE INITIAL VALUE TEMPORARILY
          EigV0=polymer%EigV(k,i,j)
          EigX0=polymer%EigX(k,i,j)          

          !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
          polymer%EigV(k,i,j)=EigV0*expOmk(k,1,1,i) &
              +EigX0*expOmk(k,1,2,i) &
              +OUsig(k,1,1,i)*R1(k)

          polymer%EigX(k,i,j)=EigV0*expOmk(k,2,1,i) &
              +EigX0*expOmk(k,2,2,i) &
              +OUsig(k,2,1,i)*R1(k)+OUsig(k,2,2,i)*R2(k)
        ENDDO
    enddo ; enddo

    deallocate(R1,R2)
        
    do j=1,ndim ; do i=1,nat    
        !TRANSFORM BACK IN COORDINATES
        !polymer%X(:,i,j)=matmul(EigMat,polymer%EigX(:,i,j))/sqrt_mass(i)
        call DGEMV('N',nu,nu,1._dp/sqrt_mass(i),EigMat,nu,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
    enddo ; enddo
    

    !COMPUTE FORCES	
    CALL update_beads_forces(X,polymer) 

    !APPLY FORCES
    CALL apply_forces(polymer,dt)    

	end subroutine PIOUD_step

!----------------------------------------------------------

  subroutine sample_momentum_EW(X,P,auxsim,verbose)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    logical, intent(in) :: verbose
    REAL(dp), intent(in) :: X(:,:)
    REAL(dp), intent(inout) :: P(:,:,:)
    integer :: nsamples, isample

    ! COMPUTE k2
    call compute_EW_parameters(X,auxsim)

    nsamples = size(P,dim=3)  
    do isample = 1,nsamples
      call sample_wigner_momentum(P(:,:,isample),auxsim)
    enddo

  end subroutine sample_momentum_EW

  subroutine sample_wigner_momentum(P,auxsim)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(inout) :: P(:,:)
    real(dp), allocatable :: P0(:),sqrtk2inv(:,:)

    allocate(P0(ndof))!,sqrtk2inv(ndof,ndof))
    !call compute_corrected_sqrtk2inv(sqrtk2inv,auxsim)
    call RandGaussN(P0)
    !P(:,:,isample)  = RESHAPE( matmul(sqrtk2inv,P0), (/nat,ndim/))
    P0 = P0/sqrt(auxsim%k2Evals)
    P(:,:) = RESHAPE( matmul(auxsim%k2Emat,P0), (/nat,ndim/) )

    deallocate(P0)

  end subroutine sample_wigner_momentum

  subroutine compute_corrected_sqrtk2inv(sqrtk2inv,auxsim)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(inout) :: sqrtk2inv(:,:)
    real(dp), allocatable :: k2inv(:,:)
    real(dp), allocatable :: corr(:,:),Xi(:,:)
    integer :: isim,nsim,i
    real(dp) :: norm

    nsim = auxsim%num_aux_sim
    norm=3._dp/(8._dp*nsim*(nsim-1))
    allocate(corr(ndof,ndof),k2inv(ndof,ndof),Xi(ndof,ndof))
    corr(:,:)=0._dp
    sqrtk2inv(:,:)=0._dp
    k2inv(:,:)=0._dp
    do i=1,ndof
      k2inv(i,i) = 1._dp/auxsim%k2Evals(i)
      sqrtk2inv(i,i) = 1._dp/sqrt(auxsim%k2Evals(i))
    enddo
    sqrtk2inv = matmul(auxsim%k2EMat,matmul(sqrtk2inv,transpose(auxsim%k2EMat)))
    k2inv = matmul(auxsim%k2EMat,matmul(k2inv,transpose(auxsim%k2EMat)))

    do isim = 1, nsim
      Xi = auxsim%polymers(isim)%k2 - auxsim%k2
      corr = corr + matmul(Xi(:,:),matmul(k2inv,Xi(:,:)))
    enddo

    sqrtk2inv = sqrtk2inv - norm*matmul(k2inv,matmul(corr,sqrtk2inv))

    deallocate(Xi,corr,k2inv)

  end subroutine compute_corrected_sqrtk2inv

  subroutine compute_P_weights_EW(P,auxsim,weights)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(in) :: P(:,:,:)
    real(dp), intent(inout) :: weights(:)
    integer :: c, i,j,k,nsamples, isample, INFO,naux
    real(dp), allocatable :: Xi(:,:),pXp(:),k2invX(:,:,:),P0(:)
    real(dp), allocatable :: pXp2Corr(:),k2inv(:,:),pXpk2XCorr(:)
    real(dp) :: k2invXDet, k2invXCorr,k2invXtrace

    nsamples=size(P,dim=3)
    naux=auxsim%num_aux_sim

    allocate(k2inv(ndof,ndof))
    k2inv(:,:)=0._dp
    do i=1,ndof
      k2inv(i,i) = 1._dp/auxsim%k2Evals(i)
    enddo
    k2inv = matmul(auxsim%k2EMat,matmul(k2inv,transpose(auxsim%k2EMat)))

    allocate(Xi(ndof,ndof),k2invX(ndof,ndof,naux),pxp(nsamples),P0(ndof))
    allocate(pXp2Corr(nsamples),pXpk2XCorr(nsamples))
    pXp2Corr=0._dp
    pXpk2XCorr=0._dp
    do i=1,naux
      Xi(:,:)=auxsim%polymers(i)%k2(:,:)-auxsim%k2
      k2invX(:,:,i)=matmul(k2inv,Xi)
      do isample=1,nsamples
        P0=reshape(P(:,:,isample),(/ndof/))
        pXp(isample)=dot_product(P0,matmul(Xi(:,:),P0))
      enddo
      k2invXtrace=0._dp
      Do j=1,ndof
        k2invXtrace = k2invXtrace + k2invX(j,j,i)
      enddo
      pXp2Corr = pXp2Corr + pXp(:)**2
      pXpk2XCorr = pXpk2XCorr + pXp(:)*k2invXtrace
    enddo
    pXp2Corr= pXp2Corr/(8._dp*naux*(naux-1))
    pXpk2XCorr = -pXpk2XCorr/(4._dp*naux*(naux-1))

    k2invXCorr = 0._dp
    do c=1,naux
      k2invXDet=0._dp
      do i=1,ndof
        k2invXDet=k2invXDet - k2invX(i,i,c)**2/8._dp
        do j=1,ndof
          if(i==j) cycle
          k2invXDet=k2invXDet + 3._dp*k2invX(i,i,c)*k2invX(j,j,c)/8._dp - 0.5_dp*k2invX(i,j,c)*k2invX(j,i,c)
        enddo
      enddo
      k2invXCorr = k2invXCorr + k2invXDet
    enddo
    k2invXCorr = k2invXCorr/(naux*(naux-1._dp))


    weights = 1._dp - pXp2Corr - pXpk2XCorr - k2invXCorr

  end subroutine compute_P_weights_EW

  function compute_EW_control(P,auxsim,weights) result(control)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(in) :: auxsim
    real(dp), intent(in) :: P(:,:,:)
    real(dp), intent(in), optional :: weights(:)
    real(dp) :: control
    integer :: is,nsamples
    real(dp), allocatable :: ctrls(:),P0(:)

    nsamples=size(P,dim=3)
    allocate(ctrls(nsamples),P0(ndof))
    DO is=1,nsamples
      P0=RESHAPE(P(:,:,is),(/ndof/))
      ctrls(is) = dot_product(P0,matmul(auxsim%k2,P0))/ndof
    ENDDO

    if(present(weights)) ctrls(:)=ctrls(:)*weights(:)

    control = SUM(ctrls)/nsamples
    
    deallocate(P0,ctrls)

  end function compute_EW_control

  function compute_EW_correction(Pin,auxsim) result(Cew)
		implicit none
		type(WIG_AUX_SIM_TYPE), intent(in) :: auxsim
    real(dp), intent(in) :: Pin(:,:)
		INTEGER :: i,j,k,l,m,is1,is2,nprods
    real(dp) :: Cew(2)
		REAL(dp) :: C4,C6,k22,EW4_avg,EW4,EW6,EW6_avg
    real(dp), allocatable :: P(:),k2inv(:,:)

    allocate(P(ndof),k2inv(ndof,ndof))
    P=RESHAPE(Pin, (/ndof/))        

    Cew(:) = 1._dp
    if(.not. auxsim%compute_Cew) return

    k2inv(:,:)=0._dp
    do i=1,ndof
      k2inv(i,i) = 1._dp/auxsim%k2Evals(i)
    enddo
    k2inv = matmul(auxsim%k2EMat,matmul(k2inv,transpose(auxsim%k2EMat)))

    EW4 = 0._dp
    EW4_avg=0._dp
    do m=1,ndof ; do k=1,ndof ; do j=1,ndof ; do i=1,ndof
      C4 = auxsim%k4(i,j,k,m) - auxsim%k2(i,j)*auxsim%k2(k,m) &
                              - auxsim%k2(i,k)*auxsim%k2(j,m) &
                              - auxsim%k2(i,m)*auxsim%k2(j,k)

      EW4 = EW4 + C4* (P(i)*P(j)*P(k)*P(m))
      EW4_avg =  EW4_avg +  C4*( &
            k2inv(i,j)*k2inv(k,m) &
          + k2inv(i,k)*k2inv(j,m) &
          + k2inv(i,m)*k2inv(j,k) &
        )        
    enddo ; enddo ; enddo ; enddo
    EW4 = EW4/24._dp
    EW4_avg = EW4_avg/24._dp
		Cew(1) = (1._dp +  EW4) / (1._dp + EW4_avg)

    if(ndof==1) then
      C6 = auxsim%k6 - 15._dp*auxsim%k2(1,1)*auxsim%k4(1,1,1,1) + 30._dp*auxsim%k2(1,1)**3
			EW6 = -(P(1)**6) * C6/720._dp		
      EW6_avg = -(15._dp/auxsim%k2(1,1)**3) * C6/720._dp
			Cew(2) = (1._dp + EW4 + EW6 )/(1._dp + EW4_avg + EW6_avg )
		endif

    deallocate(P)
		
	end function compute_EW_correction

  subroutine cutoff_k2(k2,X)
    implicit none
    real(dp), intent(in) :: X(:,:)
    real(dp),intent(inout) :: k2(:,:)
    integer :: i1,j1,i2,j2,c1,c2
    real(dp) :: r

    if(cutoff_radius_k2<=0) return

    do i1=1,nat-1 ; do i2=i1+1,nat  
      r = NORM2(X(i1,:)-X(i2,:))
      if(r>=cutoff_radius_k2) then
        do j1=1,ndim 
          c1=i1+(j1-1)*nat
          do j2=1,ndim
            c2=i2+(j2-1)*nat
            k2(c1,c2)=0._dp
            k2(c2,c1)=0._dp
          enddo
        enddo
      endif    
    enddo ; enddo

  end subroutine cutoff_k2

end module wigner_EW
