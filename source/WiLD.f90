PROGRAM WiLD
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
  use wigner_EW
  use system_commons
  use correlation_functions
  use potential, only : Pot
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library	
  type(tcf_type) :: tcf

  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real ::  remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  integer :: avg_counter
  real(dp) :: Ekin_avg, temp_avg, Epot_avg
  logical :: verbose

  integer :: nan_count,max_nan,c
  integer :: ux,up
  logical :: save_trajectory,ignore_point, kubo_transform=.FALSE.

  logical :: fast_forward_langevin
  integer :: nsteps,nblocks,iblock,write_stride,istep,n_threads,nblocks_therm
  real(dp) :: block_time,dt,gamma0
  logical :: resample_momentum
  real(dp) :: gamma_smooth, spectrum_cutoff

  real(dp), allocatable :: X(:,:),P(:,:),Xtraj(:,:,:),Ptraj(:,:,:),mutraj(:,:)
  real(dp), allocatable :: mu0(:),mutmp(:)

  integer :: num_aux_sim
  type(WIG_AUX_SIM_TYPE) :: auxsim

  character(:), allocatable :: tcf_name

  call full_timer%start()
  call cpu_time(cpu_time_start)
  avg_counter = 0
  step_cpu_time_avg = 0
  step_time_avg =0
  Ekin_avg = 0._dp
  temp_avg = 0._dp
  Epot_avg = 0._dp

  call initialize_framework(param_library)
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	
  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE  
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)

  dt = param_library%get("parameters/dt")
  !! @input_file parameters/dt [REAL] (WiLD)
  if(dt<=0._dp) STOP "[WiLD.f90] Error: parameters/dt must be positive!"
  gamma0 = param_library%get("parameters/gamma")
  !! @input_file parameters/gamma [REAL] (WiLD)
  if(gamma0<=0._dp) STOP "[WiLD.f90] Error: parameters/gamma must be positive!"

  fast_forward_langevin = param_library%get("parameters/sampler_fast_forward_langevin",default=.FALSE.) 
  !! @input_file parameters/sampler_fast_forward_langevin [LOGICAL] (WiLD, default=.FALSE.)  
  if(fast_forward_langevin) write(*,*) "WiLD: fast forward langevin"   

  block_time = param_library%get("parameters/block_time") 
  !! @input_file parameters/block_time [REAL] (WiLD)
  nsteps=nint(block_time/dt)
  nblocks = param_library%get("parameters/nblocks")
  !! @input_file parameters/nblocks [INTEGER] (WiLD)
  nblocks_therm = param_library%get("parameters/nblocks_therm",default=0)
  !! @input_file parameters/nblocks_therm [INTEGER] (WiLD, default=0)
  write_stride = param_library%get("parameters/write_stride",default=1)
  !! @input_file parameters/write_stride [INTEGER] (WiLD, default=1)
  resample_momentum = param_library%get("parameters/resample_momentum",default=.FALSE.)
  !! @input_file parameters/resample_momentum [LOGICAL] (WiLD, default=.FALSE.)
  save_trajectory = param_library%get("parameters/save_trajectory",default=.TRUE.)
  !! @input_file parameters/save_trajectory [LOGICAL] (WiLD, default=.TRUE.)

  gamma_smooth = param_library%get("parameters/gamma_smooth",default=-1._dp)
  !! @input_file parameters/gamma_smooth [REAL] (WiLD, default=-1.)
  spectrum_cutoff = param_library%get("parameters/spectrum_cutoff",default=-1._dp)   
  !! @input_file parameters/spectrum_cutoff [REAL] (WiLD, default=-1.)
  
  tcf_name="WiLD"
  kubo_transform = .TRUE.
  resample_momentum = .FALSE.
  call initialize_tcf(tcf,tcf_name,dt,nsteps,1 &
                          ,gamma_smooth,spectrum_cutoff,kubo_transform)
  

  allocate(X(nat,ndim),P(nat,ndim))
  X(:,:)=Xinit(:,:)

  call initialize_wigner_commons(param_library) 
  auxsim%compute_WiLD_forces = .TRUE.
  auxsim%nsteps = param_library%get(WIGAUX//"/nsteps")  
  !! @input_file EW_NLGA/nsteps [INTEGER] (WiLD)
  auxsim%nsteps_therm = param_library%get(WIGAUX//"/nsteps_therm")
  !! @input_file EW_NLGA/nsteps_therm [INTEGER] (WiLD)
  num_aux_sim = param_library%get(WIGAUX//"/num_aux_sim")
  !! @input_file EW_NLGA/num_aux_sim [INTEGER] (WiLD)
  if(num_aux_sim>1) write(*,*) "WiLD: "//int_to_str(num_aux_sim)//" auxiliary simulations"   
  call initialize_auxiliary_simulation(num_aux_sim,auxsim)
  ! WARM UP AUXILIARY SIMULATION  
  write(*,*) "WiLD: sampling initial momentum..."
  call compute_EW_parameters(X,auxsim)
  call sample_wigner_momentum(P,auxsim)
  write(*,*) "done." 
  
  allocate(Xtraj(nat,ndim,nsteps),Ptraj(nat,ndim,nsteps))
  Xtraj(:,:,:)=0._dp
  Ptraj(:,:,:)=0._dp
  if(charges_provided) then
    allocate(mutraj(ndim,nsteps),mu0(ndim),mutmp(ndim))
    mutraj(:,:)=0._dp
  endif

  max_nan = param_library%get("parameters/max_nan",default=10)
  !! @input_file parameters/max_nan [INTEGER] (WiLD, default=10)
  nan_count=0

  !-----------------------------------
  call open_output_files(param_library)

  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  ! THERMALIZATION
  if(nblocks_therm>0) then
    write(*,*)
    write(*,*) "Starting "//int_to_str(nblocks_therm)//" blocks of thermalization"
    DO iblock=1,nblocks_therm
      call compute_block(iblock,.TRUE.)
    ENDDO
    write(*,*) "Thermalization done."
    write(*,*)
  endif
  
  write(*,*)
  write(*,*) "Starting "//int_to_str(nblocks)//" blocks of "//tcf_name
  DO iblock=1,nblocks    
    call compute_block(iblock,.FALSE.)
  ENDDO

  call write_tcf_results(tcf,1._dp)

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS

  subroutine compute_block(iblock,thermalization)
    IMPLICIT NONE
    integer, intent(in) :: iblock
    logical, intent(in) :: thermalization
    logical :: save_results

    save_results = .NOT. thermalization

    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1

    if(mod(iblock,write_stride)==0) then
      write(*,*)
      if(thermalization) then
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks_therm)
      else
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks)
      endif
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    if(resample_momentum) then
      call compute_EW_parameters(X,auxsim)
      call sample_wigner_momentum(P,auxsim)
    endif
    call update_WiLD_sample(X,P,auxsim,nsteps,dt,verbose,Xtraj,Ptraj) 

    ignore_point = ANY(ISNAN(Xtraj)) .OR. ANY(ISNAN(Ptraj))
    if(ignore_point) then

      nan_count = nan_count+1
      if(nan_count>=max_nan) then
        write(0,*) "[WiLD.f90] Error: ",max_nan," NaN trajectories reached!"
        STOP "Execution stopped."
      endif
      call compute_EW_parameters(X,auxsim)
      call sample_wigner_momentum(P,auxsim)

    elseif(save_results) then

      call compute_autoTCF_WK(RESHAPE(Ptraj,(/ndof,nsteps/)),nsteps,dt,tcf%Cpp(:,:,1))
      if(charges_provided) then
        DO istep=1,nsteps
          call get_dipole(Xtraj(:,:,istep),Ptraj(:,:,istep),mutraj(:,istep))          
        ENDDO
        call compute_autoTCF_WK(mutraj,nsteps,dt,tcf%Cmumu(:,:,1))
      endif
      call update_tcf_avg(tcf)
      
      !write block trajectory
      if(save_trajectory) then
        open(newunit=ux,file=output%working_directory//"X.traj.block"//int_to_str(iblock,nblocks))
        open(newunit=up,file=output%working_directory//"P.traj.block"//int_to_str(iblock,nblocks))
        DO istep=1,nsteps
          write(ux,*) Xtraj(:,:,istep)
          write(up,*) Ptraj(:,:,istep)
        ENDDO
        close(ux)
        close(up)
      endif

    endif 

    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter
    
    if(verbose) then
      if(nan_count>0) write(*,*) "NaN_count= "//int_to_str(nan_count)
      write(*,*) "Average time per block: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."
      
      if(thermalization) then
        remaining_time = step_time_avg*(nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated thermalization time : "//sec2human(job_time)
        write(*,*) "estimated remaining thermalization time : "//sec2human(remaining_time)
        remaining_time = step_time_avg*(nblocks+nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      else
        remaining_time = step_time_avg*(nblocks-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      endif

      ! RESET WINDOW AVERAGES
      avg_counter=0
      step_time_avg=0.
      step_cpu_time_avg=0.

      ! WRITE INTERMEDIATE RESULTS
      if(save_results) then
        call write_tcf_results(tcf,1._dp)
      endif
    endif

  end subroutine compute_block

  subroutine update_WiLD_sample(X,P,auxsim,nsteps,dt,verbose,Xtraj,Ptraj)
    implicit none
    real(dp), intent(inout) :: X(:,:),P(:,:)
    real(dp), intent(in) :: dt
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(inout) :: Xtraj(:,:,:),Ptraj(:,:,:)
    integer, intent(in) :: nsteps
    logical, intent(in) :: verbose
    integer :: istep,i,j
    real(dp) :: temp,temp_sum,dt2
    real(dp) :: Ekin,Ekin_sum,Epot_sum

    temp_sum = 0._dp
    Ekin_sum = 0._dp
    Epot_sum = 0._dp

    dt2=0.5_dp*dt
    do istep = 1,nsteps 
  
      call apply_A(X,P,dt2)

      call compute_EW_parameters(X,auxsim)
      call compute_WiLD_forces(X,auxsim)
      
      call apply_B(P,auxsim,dt2)  
      if(fast_forward_langevin) then
        call apply_O_FFL(P,auxsim,dt)
      else  
        call apply_O(P,auxsim,dt)    
      endif
      call apply_B(P,auxsim,dt2)
        
      call apply_A(X,P,dt2)

      Xtraj(:,:,istep)=X(:,:)
      Ptraj(:,:,istep)=P(:,:)
      call compute_kinetic_energy(P,Ekin,temp)
      temp_sum = temp_sum + temp
      Ekin_sum = Ekin_sum + Ekin
      Epot_sum = Epot_sum + Pot(X)
    enddo  

    temp_avg = temp_avg + (temp_sum/nsteps - temp_avg)/avg_counter
    Ekin_avg = Ekin_avg + (Ekin_sum/nsteps - Ekin_avg)/avg_counter
    Epot_avg = Epot_avg + (Epot_sum/nsteps - Epot_avg)/avg_counter

    if(verbose) then
      write(*,*) "temperature= ",real(temp_avg,sp),"K"
      write(*,*) "Ekin= ",real(Ekin_avg,sp)
      write(*,*) "Epot= ",real(Epot_avg,sp)
      temp_avg=0._dp
      Ekin_avg=0._dp
      Epot_avg=0._dp
    endif

  end subroutine update_WiLD_sample

  subroutine apply_A(X,P,tau)
    implicit none
    real(dp), intent(inout) :: X(:,:),P(:,:)
    real(dp), intent(in) :: tau
    integer :: i,j,k

    do j=1,ndim
      X(:,j)=X(:,j)+tau*P(:,j)/mass(:)
    enddo

  end subroutine apply_A

  subroutine apply_B(P,auxsim,tau)
		IMPLICIT NONE
		real(dp), intent(inout) :: P(:,:)
		type(WIG_AUX_SIM_TYPE), intent(in) :: auxsim
		REAL(dp), INTENT(in) :: tau
		REAL(dp), allocatable :: P0(:),W(:)
		INTEGER :: k

    allocate(P0(ndof),W(ndof))
    P0 = RESHAPE(P,(/ndof/))
		do k=1,ndof
			W(k)=-0.5_dp*dot_product(P0,matmul(auxsim%CWiLD(:,:,k),P0(:)))
		enddo
			
    P0(:)=P0(:) + 0.5_dp*tau*(auxsim%FWiLD(:)+W(:))

    do k=1,ndof
      W(k)=-0.5_dp*dot_product(P0(:),matmul(auxsim%CWiLD(:,:,k),P0(:)))
    enddo
    P(:,:)= P(:,:) + tau*RESHAPE( auxsim%FWiLD(:)+W(:) , (/nat,ndim/))

    deallocate(P0,W)
		
	end subroutine apply_B

  subroutine apply_O(P,auxsim,tau)
		IMPLICIT NONE
		real(dp), intent(inout) :: P(:,:)
		type(WIG_AUX_SIM_TYPE), intent(in) :: auxsim
		REAL(dp), INTENT(in) :: tau
		REAL(dp), allocatable :: R(:),rand_force(:,:)
    real(dp):: gexp
		INTEGER :: i

    gexp = exp(-tau*gamma0)

    allocate(R(ndof),rand_force(nat,ndim))
		call RandGaussN(R)
    rand_force = sqrt(1._dp - gexp**2) &
                  *RESHAPE( matmul(auxsim%sigmaWiLD,R), (/nat,ndim/) )

		P(:,:) = gexp*P(:,:) + rand_force(:,:)
		
		deallocate(R,rand_force)
	end subroutine apply_O

  subroutine apply_O_FFL(P,auxsim,tau)
		IMPLICIT NONE
		real(dp), intent(inout) :: P(:,:)
		type(WIG_AUX_SIM_TYPE), intent(in) :: auxsim
		REAL(dp), INTENT(in) :: tau
		REAL(dp), allocatable :: R(:),rand_force(:,:)
    real(dp):: gexp,Pold,Pnew
		INTEGER :: i

    gexp = exp(-tau*gamma0)

    allocate(R(ndof),rand_force(nat,ndim))
		call RandGaussN(R)
    rand_force = sqrt(1._dp - gexp**2) &
                  *RESHAPE( matmul(auxsim%sigmaWiLD,R), (/nat,ndim/) )

    do i=1,nat
      Pold = NORM2(P(i,:))
      Pnew = NORM2(P(i,:)*gexp + rand_force(i,:))
      P(i,:) = Pnew * P(i,:) / Pold
    enddo
    
    deallocate(R,rand_force)
    
	end subroutine apply_O_FFL
	
	subroutine compute_WiLD_forces(X,auxsim)
    implicit none
    REAL(dp), intent(in) :: X(:,:)
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: NUM_AUX_SIM,i,j,k,c,isim
    real(dp), allocatable :: k2inv(:,:),V(:),F(:),dk2(:,:,:),xi(:,:),corr(:,:)
    real(dp) :: norm

    num_aux_sim = auxsim%num_aux_sim
    allocate(k2inv(ndof,ndof),V(ndof),F(ndof),dk2(ndof,ndof,ndof))

    F=0._dp
    dk2=0._dp
    do isim=1,num_aux_sim
      F = F + auxsim%polymers(isim)%F
      dk2 = dk2 + auxsim%polymers(isim)%G
    enddo
    F = F/num_aux_sim
    dk2 = dk2/num_aux_sim

    ! MULTIPLY BY Lth^2
    c=0
    DO j=1,ndim ; DO i=1,nat
        c=c+1
        F(c)=F(c)*beta/mass(i)
        dk2(:,:,c)=dk2(:,:,c)*beta/mass(i) - auxsim%k2(:,:)*F(c)
    ENDDO ; ENDDO
    
    k2inv(:,:)=0._dp
    auxsim%sigmaWiLD(:,:) = 0._dp
    do i=1,ndof
      k2inv(i,i) = 1._dp/auxsim%k2Evals(i)
      auxsim%sigmaWiLD(i,i) = 1._dp/sqrt(auxsim%k2Evals(i))
    enddo
    k2inv = matmul(auxsim%k2EMat,matmul(k2inv,transpose(auxsim%k2EMat)))
    auxsim%sigmaWiLD = matmul(auxsim%k2EMat &
          ,matmul(auxsim%sigmaWiLD,transpose(auxsim%k2EMat)))
    
    !call compute_corrected_sqrtk2inv(auxsim%sigmaWiLD,auxsim)
    allocate(xi(ndof,ndof),corr(ndof,ndof))
    corr=0._dp
    do isim = 1, num_aux_sim
      Xi = auxsim%polymers(isim)%k2 - auxsim%k2
      corr = corr + matmul(Xi(:,:),matmul(k2inv,Xi(:,:)))
    enddo
    corr = matmul(k2inv,corr)/real(num_aux_sim*(num_aux_sim-1),dp)

    auxsim%sigmaWiLD = auxsim%sigmaWiLD - 3._dp/8._dp*matmul(corr,auxsim%sigmaWiLD)
    k2inv = k2inv - matmul(corr,k2inv)
    
    deallocate(xi,corr)

    auxsim%CWiLD(:,:,:)=0._dp
    do i = 1,ndof ; do j=1,ndof      
      auxsim%CWiLD(:,:,i) = auxsim%CWiLD(:,:,i) + k2inv(i,j)*dk2(:,:,j)
    enddo ; enddo

    V(:) = 0._dp
    do j=1,ndof ; do k=1,ndof
      V(j) = V(j) + auxsim%CWiLD(k,j,k)
    enddo ; enddo

    auxsim%FWiLD(:) = matmul(k2inv, F - V)

    deallocate(F,V,k2inv,dk2)

  end subroutine compute_WiLD_forces

END PROGRAM WiLD
