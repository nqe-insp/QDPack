MODULE ch5_pes

!*MODULE: CH5+ Potential Energy Surface
!> @brief Calculation of potential energy given for example of the CH5+ system.

!***********************************************************************
! CH5+ analytic ground state potential energy surface. The main subroutine
! get_ch5_pot uses all the remaining subroutine in this module (see get_ch5_pot 
! subroutine for more details). All subroutines, except for get_ch5_pot and get_ch5_grad,
! are private to minimize the interaction with the remaining part of the code.
! This is the complete list of all the subroutines within this module:
!
!  get_ch5_pot
!  get_ch5_grad
!  calcr
!  calcdist
!  calcangle
!  cspline
!  spline
!  splint
!  getch3pot
!  geth2pot
!  getfit
!  getd0
!  getvec
!  getrvec
!  getlr
!
!***********************************************************************

  use kinds 
  use potential, only: hessian_finitediff
  IMPLICIT NONE

  PUBLIC :: get_ch5_pot_info, Pot_ch5, dPot_ch5 

  !! Internal error code. The default value for no error is 0. In case of 
  !! internal error, the corresponding value is exported.

  !! No error
  INTEGER, PARAMETER :: CH5OK = 0
  !! Error in splint. x exceeded minimal value.
  INTEGER, PARAMETER :: CH5ERROR1 = 1
  !! Error in splint. x exceeded maximal value.
  INTEGER, PARAMETER :: CH5ERROR2 = 2
  !! Error in splint. Bad xa input in splint.
  INTEGER, PARAMETER :: CH5ERROR3 = 3

  PUBLIC :: CH5OK
  PUBLIC :: CH5ERROR1
  PUBLIC :: CH5ERROR2
  PUBLIC :: CH5ERROR3

  PRIVATE

CONTAINS

  SUBROUTINE get_ch5_pot_info(X,Pot,Forces,hessian,vir)
     IMPLICIT NONE
     real(dp), intent(in)  :: X(:,:)
     real(dp), intent(out) :: Forces(size(X,1),size(X,2))
     real(dp), intent(out) :: Pot
     real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
     real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
     integer :: pot_status,grad_status 

     call get_ch5_pot(X,Pot,pot_status)
     call get_ch5_grad(X,grad=Forces,grad_status=grad_status) 
     Forces(:,:) = -Forces(:,:) 

     if(grad_status/=0 .OR. pot_status/=0) then 
        write(0,*) "Error in ch5_pot_info:"
        write(0,*) "grad_status=",grad_status
        write(0,*) "pot_status=",pot_status
        STOP "Execution stopped"
     endif
     if(present(vir)) then
        write(0,*) "Virial not implemented for CH5+ potential" 
        STOP "Execution stopped."
     endif
     if(present(hessian)) then
      Hessian = hessian_finitediff(X)
     endif
  
  END SUBROUTINE get_ch5_pot_info

  function Pot_ch5(X) result(U)
    implicit none
    real(dp),intent(in)  :: X(:,:)
    REAL(dp) :: U
    INTEGER :: pot_status

    call get_ch5_pot(X,U,pot_status)
     if(pot_status/=0) then 
        write(0,*) "Error in Pot_ch5:"
        write(0,*) "pot_status=",pot_status
        STOP "Execution stopped"
     endif
  end function Pot_ch5

  function dPot_ch5(X) result(dU)
    implicit none
    real(dp),intent(in)  :: X(:,:)
    real(dp) :: dU(size(X,1),size(X,2))
    integer :: grad_status
    call get_ch5_grad(X,grad=dU,grad_status=grad_status)
     if(grad_status/=0) then 
        write(0,*) "Error in dPot_ch5:"
        write(0,*) "grad_status=",grad_status
        STOP "Execution stopped"
     endif
  end function dPot_ch5


  !****************************************************************************

  SUBROUTINE get_ch5_pot( x_in, epot, pot_status )
!> @brief Calculation of the CH5+ system ground electronic state potential
!>
!> @detail Input variables:
!>    x_in - Atomic coordinates
!>
!> @detailOutput variables:
!>    epot - Value of the potential calculated at inputted coordinates
!>

!! Previous comments
!***********************************************************************
! Main subroutine for calculating the CH5+ analytic potential. Calls the
! remaining subroutines within this module for exact calculation.
!
! The following comments are kept from the previous versions.
!***********************************************************************
! changes by xgw, Sep19,2005, again Apr12,2006
!  replace x with xn (which is used before)
!  remove r
!***********************************************************************
!CH5+ Potential Energy Surface, version 7a, 2005-10-18
!   by Zhong Jin, Bastiaan J. Braams, Joel M. Bowman
!
!reference:
!   Zhong Jin, Bastiaan J. Braams, Joel M. Bowman
!   Journal of Physical Chemistry A  accepted(2005)
!
!notes:
!
!   x(6,3) is the Cartesian coordinates for six atoms in order of
!   C H H H H H. (in Bohr)
!
!   r is defined as the distance between the center of H2 and Carbon
!   in a.u.
!
!   Potential energy epot is returned in a.u.
!
!   CCSD(T)/aug-cc-pVTZ based
!
!All rights reserved. Contact bowman@euch4e.chem.emory.edu for
!details or any latest updates.
!************************************************************************

      integer, parameter :: natoms = 6

      double precision :: theta, vfit
      real(dp), intent(out) :: epot
      real(dp), dimension(1:natoms,1:3), intent(in) :: x_in
      double precision, dimension(3,natoms) :: xn
      double precision, dimension(4,3) :: xch3
      double precision, dimension(2,3) :: xh2
      double precision :: epotch3, epoth2, elr
      double precision :: r, rh2, rp
      double precision, parameter :: de = -40.57833477d0
      double precision, parameter :: rmin = 11.d0, rmax = 15.d0
      double precision, parameter :: a = 0.d0, b = 0.d0
      INTEGER, INTENT( OUT ) :: pot_status
      INTEGER :: error_code

    !--------------------------------------------------------------------------

      !! Default no error status
      pot_status = CH5OK
      error_code = CH5OK

      xn = TRANSPOSE( x_in )

      CALL calcr(xn,xch3,xh2,r,rh2,theta)

      IF( r <= rmax ) THEN
          CALL getfit(xn,vfit)
      END IF

      rp = (r - rmin)/(rmax - rmin)
      IF( r < rmin ) THEN
          epot = vfit
      ELSE
          CALL getch3pot(xch3,epotch3)
          CALL geth2pot(xh2,epoth2)
          CALL getlr(r,rh2,theta,elr,error_code)
          IF( error_code /= CH5OK ) THEN
              pot_status = error_code
              error_code = CH5OK
              epot = 0.0D0

          ELSE
              IF( r > rmax ) THEN
                  epot = elr + epoth2 + epotch3 + de
              ELSE
                  epot = (1.d0-sw(a,b,rp))*vfit + sw(a,b,rp)*(elr + epoth2 + epotch3 + de)
              END IF
          END IF
      END IF

  END SUBROUTINE get_ch5_pot

  !****************************************************************************

  SUBROUTINE get_ch5_grad( x_in, delta_in, grad, grad_status )
    !> @brief Numerical calculation of the potential energy gradient. Subroutine calls
    !> get_ch5_pes subroutine.
    !>
    !> @detail Input variables:
    !>    x_in - Atomic coordinates
    !>    delta_in - Coordinate displacement for the numerical derivation (Optional)
    !>               Default value set to 1.D-4 a.u.
    !>
    !> @detail Output variables:
    !>    grad - Gradient vector of the potential at specified coordinate
    !>
    
    !! Previous comments
    !***********************************************************************
    ! Numerical derivation of the CH5+ potential energy surface. The input and
    ! output variables are of Cartesian type. Subroutine calls get_ch5_pot for the
    ! numerical value of the energy. The negative gradient or the force is the
    ! output.
    !***********************************************************************

    real(dp), DIMENSION(:,:), INTENT( IN )                 :: x_in
    real(dp), INTENT( IN ), OPTIONAL                       :: delta_in
    real(dp), DIMENSION(1:SIZE(x_in,1),1:3), INTENT( OUT ) :: grad
    INTEGER, INTENT( OUT )                                         :: grad_status

    !! Local variables
    DOUBLE PRECISION                                               :: delta
    DOUBLE PRECISION, DIMENSION(1:SIZE(x_in,1),1:3)                :: x_temp
    DOUBLE PRECISION                                               :: neg_dis
    DOUBLE PRECISION                                               :: pos_dis
    INTEGER                                                        :: pot_status
    INTEGER                                                        :: i
    INTEGER                                                        :: j

    !--------------------------------------------------------------------------

    !! Default grad error status
    grad_status = CH5OK

    IF( .NOT.PRESENT( delta_in ) ) THEN
        delta = 1.D-4
    ELSE
        delta = delta_in
    END IF

    grad = 0.D0
    DO i = 1, SIZE( x_in, 1 )
        DO j = 1, 3
            x_temp = x_in
            x_temp(i,j) = x_in(i,j)-delta
            CALL get_ch5_pot( x_temp, neg_dis, pot_status )
            x_temp(i,j) = x_in(i,j) + delta
            CALL get_ch5_pot( x_temp, pos_dis, pot_status )
            grad(i,j) = pos_dis - neg_dis
        END DO
    END DO
    grad = 0.5D0 * grad / delta

    IF( pot_status /= CH5OK ) THEN
        grad_status = pot_status
        grad = 0.0D0
    END IF

  END SUBROUTINE get_ch5_grad

  !****************************************************************************

  SUBROUTINE calcr(x,xch3,xh2a,dist,h2dist,theta)
!***********************************************************************
!.... Subroutine to calculate R, which is the distance between C and the
!.... center of H2 in CH5+
!
!.....History:
!.... Date     Modified by  Comment
!.... ----     -----------  -------
!.... 02/20/05   Zhong      created
!.... 03/01/05   Zhong      refine it

!.... dist ....  R
!.... h2dist ... bond length of H2
!.... theta ...  the angle between H2 and axis of C-center of H2
!************************************************************************

      integer :: i, j, npp1, npp2, jr
      integer, parameter :: natoms = 6

      double precision, dimension(3,natoms) :: x
      double precision, dimension(natoms-1) :: rch
      double precision, dimension(3) :: xh2m, xh1, xh2
      double precision, dimension(2) :: rchmax
      double precision :: dist, h2dist, h2dist5, theta
      double precision, dimension(3,4) :: xch3t
      double precision, dimension(3,2) :: xh2at
      double precision, dimension(4,3) :: xch3
      double precision, dimension(2,3) :: xh2a

    !--------------------------------------------------------------------------
       
      do j = 1, natoms - 1
         rch(j) = sqrt((x(1,j+1)-x(1,1))**2+(x(2,j+1)-x(2,1))**2 &
                  +(x(3,j+1)-x(3,1))**2)
      end do
      rchmax(1) = 0.d0
      rchmax(2) = 0.d0
      npp1 = 0
      npp2 = 0

      do j = 1, natoms - 1
         if (rchmax(1)<rch(j)) then
            rchmax(1) = rch(j)
            npp1 = j
         end if
      end do

      do j = 1, natoms - 1
         if (npp1==j) then
         else
            if (rchmax(2)<rch(j)) then
               rchmax(2) = rch(j)
               npp2 = j
            end if
         end if
      end do

      !.... Find the Cartisian coordinate of the center of H2
      do i = 1, 3
         xh2m(i) = 0.5d0*(x(i,npp1+1)+x(i,npp2+1))
      end do

      !.... Obtain the R -- dist
      call calcdist(x,xh2m,dist)

      !.... The Cartisian coordinate of H2
      do i = 1, 3
         xh1(i) = x(i,npp1+1)
         xh2(i) = x(i,npp2+1)
      end do

      do i = 1, 3
         xh2at(i,1) = x(i,npp1+1)
         xh2at(i,2) = x(i,npp2+1)
      end do

      jr = 0
      do j = 1, natoms
         if (j==npp1+1.or.j==npp2+1) then
         else
            jr = jr + 1
            do i = 1, 3
               xch3t(i,jr) = x(i,j)
            end do
         end if
      end do

      do i = 1, 3
         do j = 1, 4
            xch3(j,i) = xch3t(i,j)
         end do
      end do

      do i = 1, 3
         do j = 1, 2
            xh2a(j,i) = xh2at(i,j)
         end do
      end do

      !.... Calculate the bond distance of H2
      call calcdist(xh1,xh2,h2dist)

      h2dist5 = h2dist/2.d0
      call calcangle(rch(npp2),dist,h2dist5,theta)

  END SUBROUTINE calcr

  !****************************************************************************

  SUBROUTINE calcdist(x1,x2,dist)
!***********************************************************************
!.... a subroutine to calculate the bond length
!.... x1, x2 are Cartesian coordinate
!***********************************************************************
 
      double precision, dimension(3) :: x1, x2
      double precision :: dist

    !--------------------------------------------------------------------------

      dist = dsqrt((x1(1) - x2(1))**2 + (x1(2) - x2(2))**2 + (x1(3) - x2(3))**2)

  END SUBROUTINE calcdist

  !****************************************************************************

  SUBROUTINE calcangle(a,b,c,theta)
!***********************************************************************
!.... program to calculate theta
!.... a, b, c are the length of sides of triangle
!***********************************************************************

      double precision :: a, b, c, theta, pi, d

    !--------------------------------------------------------------------------

      pi = 4.d0*datan(1.d0)

      if (abs(b-c-a).le.1e-5) then
         theta = 0.d0
      else
         d = b**2 + c**2 - a**2
         theta = 180.d0*dacos(d/(2.d0*b*c))/pi
      end if

  END SUBROUTINE calcangle

  !****************************************************************************

  SUBROUTINE cspline(x,y0,y90,yq,error_code)
!*****************************************************************************
!...  subroutine for cubic spline
!...  driver for routine splint, which calls spline
!...  x ....       H-H bond distance
!     y0 ...       alpha parallel
!     y90 ...      alpha perpendicular
!     yq ...       quadrupole
!*****************************************************************************

      integer, parameter :: NP = 21, NP2 = 257
      integer :: i, nfunc
      double precision :: x, y0, yp01, yp0n, y90, yp901, yp90n
      double precision :: ypq1, ypqn, yq
      double precision, dimension(1:NP,1:4) :: alpha_all
      double precision, dimension(1:NP2,1:2) :: q_all
      double precision, dimension(1:NP) :: xa, ya0, ya90, y2s0, y2s90
      double precision, dimension(1:NP2) :: xa1, ya1, yq2
      INTEGER, INTENT( OUT ) :: error_code

    !--------------------------------------------------------------------------

      !! W. Kolos and L. Wolniewicz, Polarizability of the Hydrogen Molecule, J. Chem.
      !! Phys. 46, 1426 (1967); http://dx.doi.org/10.1063/1.1840870
      alpha_all=TRANSPOSE(RESHAPE([&
      0.40,      1.92895,     1.85033,      1.87653, &
      0.60,      2.49001,     2.28254,      2.35170, &
      0.80,      3.20436,     2.78253,      2.92314, &
      1.00,      4.08782,     3.34102,      3.58995, &
      1.20,      5.14652,     3.94397,      4.34482, &
      1.35,      6.05577,     4.41713,      4.96334, &
      1.40,      6.38049,     4.57769,      5.17862, &
      1.45,      6.71562,     4.73931,      5.39808, &
      1.60,      7.78072,     5.22760,      6.07864, &
      1.80,      9.32045,     5.87797,      7.02546, &
      2.00,     10.96438,     6.51086,      7.99537, &
      2.20,     12.63765,     7.10967,      8.95233, &
      2.40,     14.25680,     7.65682,      9.85682, &
      2.60,     15.72316,     8.13575,     10.66489, &
      2.80,     16.92119,     8.53839,     11.33265, &
      3.00,     17.77307,     8.85286,     11.82627, &
      3.20,     18.21389,     9.07902,     12.12397, &
      3.40,     18.23872,     9.22278,     12.22809, &
      3.60,     17.89054,     9.29657,     12.16123, &
      3.80,     17.24953,     9.31506,     11.95988, &
      4.00,     16.41833,     9.29535,     11.66968  &
      ],[4,np]))
      xa=alpha_all(1:np,1)
      ya0=alpha_all(1:np,2)
      ya90=alpha_all(1:np,3)
      yp01 = 0.d0
      yp0n = 0.d0
      yp901 = 0.d0
      yp90n = 0.d0

      !! SPLINE to get second derivatives
      call spline(xa,ya0,NP,yp01,yp0n,y2s0)
      call spline(xa,ya90,NP,yp901,yp90n,y2s90)

      !! SPLINT for interpolations
      call splint(xa,ya0,y2s0,NP,x,y0,error_code)
      IF( error_code /= 0 ) GOTO 987
      call splint(xa,ya90,y2s90,NP,x,y90,error_code)
      IF( error_code /= 0 ) GOTO 987

      !! L. Wolniewicz, I. Simbotin and A. Dalgarno, Quadrupole Transition
      !! Probabilities for the Excited Rovibrational States of H2, The Astrophysical
      !! Journal Supplement Series 115, 293 (1998); http://stacks.iop.org/0067-0049/115/i=2/a=293
      q_all=TRANSPOSE(RESHAPE([&
       0.20,  0.23418027E-01, &
       0.25,  0.36397351E-01, &
       0.30,  0.52103114E-01, &
       0.35,  0.70462009E-01, &
       0.40,  0.91398768E-01, &
       0.45,  0.11483405E+00, &
       0.50,  0.14068604E+00, &
       0.55,  0.16887175E+00, &
       0.60,  0.19930366E+00, &
       0.65,  0.23189486E+00, &
       0.70,  0.26655464E+00, &
       0.75,  0.30319016E+00, &
       0.80,  0.34170724E+00, &
       0.85,  0.38200822E+00, &
       0.90,  0.42399420E+00, &
       0.95,  0.46756336E+00, &
       1.00,  0.51261156E+00, &
       1.05,  0.55903207E+00, &
       1.10,  0.60671557E+00, &
       1.15,  0.65555002E+00, &
       1.20,  0.70542060E+00, &
       1.25,  0.75620966E+00, &
       1.30,  0.80779664E+00, &
       1.35,  0.86005803E+00, &
       1.40,  0.91286725E+00, &
       1.45,  0.96609474E+00, &
       1.50,  0.10196078E+01, &
       1.55,  0.10732709E+01, &
       1.60,  0.11269450E+01, &
       1.65,  0.11804886E+01, &
       1.70,  0.12337570E+01, &
       1.75,  0.12866027E+01, &
       1.80,  0.13388757E+01, &
       1.85,  0.13904235E+01, &
       1.90,  0.14410914E+01, &
       1.95,  0.14907228E+01, &
       2.00,  0.15391595E+01, &
       2.05,  0.15862426E+01, &
       2.10,  0.16318122E+01, &
       2.15,  0.16757087E+01, &
       2.20,  0.17177731E+01, &
       2.25,  0.17578479E+01, &
       2.30,  0.17957779E+01, &
       2.35,  0.18314113E+01, &
       2.40,  0.18646002E+01, &
       2.45,  0.18952029E+01, &
       2.50,  0.19230829E+01, &
       2.55,  0.19481134E+01, &
       2.60,  0.19701753E+01, &
       2.65,  0.19891607E+01, &
       2.70,  0.20049726E+01, &
       2.75,  0.20175293E+01, &
       2.80,  0.20267612E+01, &
       2.85,  0.20326159E+01, &
       2.90,  0.20350580E+01, &
       2.95,  0.20340692E+01, &
       3.00,  0.20296507E+01, &
       3.05,  0.20218226E+01, &
       3.10,  0.20106254E+01, &
       3.15,  0.19961198E+01, &
       3.20,  0.19783858E+01, &
       3.25,  0.19575230E+01, &
       3.30,  0.19336498E+01, &
       3.35,  0.19069018E+01, &
       3.40,  0.18774313E+01, & 
       3.45,  0.18454058E+01, & 
       3.50,  0.18110057E+01, & 
       3.55,  0.17744224E+01, & 
       3.60,  0.17358562E+01, & 
       3.65,  0.16955143E+01, & 
       3.70,  0.16536081E+01, & 
       3.75,  0.16103512E+01, & 
       3.80,  0.15659572E+01, & 
       3.85,  0.15206373E+01, & 
       3.90,  0.14745986E+01, & 
       3.95,  0.14280418E+01, & 
       4.00,  0.13811604E+01, & 
       4.05,  0.13341385E+01, & 
       4.10,  0.12871498E+01, & 
       4.15,  0.12403573E+01, & 
       4.20,  0.11939114E+01, & 
       4.25,  0.11479507E+01, & 
       4.30,  0.11026007E+01, & 
       4.35,  0.10579743E+01, & 
       4.40,  0.10141719E+01, & 
       4.45,  0.97128117E+00, & 
       4.50,  0.92937814E+00, & 
       4.55,  0.88852705E+00, & 
       4.60,  0.84878130E+00, & 
       4.65,  0.81018400E+00, & 
       4.70,  0.77276865E+00, & 
       4.75,  0.73655984E+00, & 
       4.80,  0.70157401E+00, & 
       4.85,  0.66782020E+00, & 
       4.90,  0.63530071E+00, & 
       4.95,  0.60401186E+00, & 
       5.00,  0.57394464E+00, & 
       5.05,  0.54508534E+00, & 
       5.10,  0.51741619E+00, & 
       5.15,  0.49091590E+00, & 
       5.20,  0.46556020E+00, & 
       5.25,  0.44132230E+00, & 
       5.30,  0.41817339E+00, & 
       5.35,  0.39608298E+00, & 
       5.40,  0.37501929E+00, & 
       5.45,  0.35494960E+00, & 
       5.50,  0.33584051E+00, & 
       5.55,  0.31765823E+00, & 
       5.60,  0.30036877E+00, & 
       5.65,  0.28393817E+00, & 
       5.70,  0.26833271E+00, & 
       5.75,  0.25351894E+00, & 
       5.80,  0.23946396E+00, & 
       5.85,  0.22613541E+00, & 
       5.90,  0.21350162E+00, & 
       5.95,  0.20153169E+00, & 
       6.00,  0.19019550E+00, & 
       6.05,  0.17946381E+00, & 
       6.10,  0.16930826E+00, & 
       6.15,  0.15970142E+00, & 
       6.20,  0.15061681E+00, & 
       6.25,  0.14202888E+00, & 
       6.30,  0.13391303E+00, & 
       6.35,  0.12624564E+00, & 
       6.40,  0.11900400E+00, & 
       6.45,  0.11216635E+00, & 
       6.50,  0.10571184E+00, &
       6.55,  0.99620525E-01, &
       6.60,  0.93873338E-01, &
       6.65,  0.88452069E-01, &
       6.70,  0.83339334E-01, &
       6.75,  0.78518575E-01, &
       6.80,  0.73974006E-01, &
       6.85,  0.69690600E-01, &
       6.90,  0.65654062E-01, &
       6.95,  0.61850802E-01, &
       7.00,  0.58267905E-01, &
       7.05,  0.54893108E-01, &
       7.10,  0.51714774E-01, &
       7.15,  0.48721861E-01, &
       7.20,  0.45903901E-01, &
       7.25,  0.43250976E-01, &
       7.30,  0.40753688E-01, &
       7.35,  0.38403141E-01, &
       7.40,  0.36190915E-01, &
       7.45,  0.34109043E-01, &
       7.50,  0.32149994E-01, &
       7.55,  0.30306645E-01, &
       7.60,  0.28572266E-01, &
       7.65,  0.26940501E-01, &
       7.70,  0.25405345E-01, &
       7.75,  0.23961130E-01, &
       7.80,  0.22602508E-01, &
       7.85,  0.21324429E-01, &
       7.90,  0.20122133E-01, &
       7.95,  0.18991132E-01, &
       8.00,  0.17927193E-01, &
       8.05,  0.16926325E-01, &
       8.10,  0.15984771E-01, &
       8.15,  0.15098988E-01, &
       8.20,  0.14265643E-01, &
       8.25,  0.13481594E-01, &
       8.30,  0.12743885E-01, &
       8.35,  0.12049732E-01, &
       8.40,  0.11396518E-01, &
       8.45,  0.10781776E-01, &
       8.50,  0.10203190E-01, &
       8.55,  0.96585778E-02, &
       8.60,  0.91458907E-02, &
       8.65,  0.86631960E-02, &
       8.70,  0.82086925E-02, &
       8.75,  0.77806662E-02, &
       8.80,  0.73775176E-02, &
       8.85,  0.69977453E-02, &
       8.90,  0.66399332E-02, &
       8.95,  0.63027539E-02, &
       9.00,  0.59849595E-02, &
       9.05,  0.56853773E-02, &
       9.10,  0.54029071E-02, &
       9.15,  0.51365147E-02, &
       9.20,  0.48852776E-02, &
       9.25,  0.46481420E-02, &
       9.30,  0.44243990E-02, &
       9.35,  0.42132189E-02, &
       9.40,  0.40137578E-02, &
       9.45,  0.38253973E-02, &
       9.50,  0.36474250E-02, &
       9.55,  0.34791933E-02, &
       9.60,  0.33201374E-02, &
       9.65,  0.31698550E-02, &
       9.70,  0.30277642E-02, &
       9.75,  0.28933597E-02, &
       9.80,  0.27661169E-02, &
       9.85,  0.26456326E-02, &
       9.90,  0.25315094E-02, &
       9.95,  0.24233877E-02, &
      10.00,  0.23209097E-02, &
      10.10,  0.21316058E-02, &
      10.20,  0.19611723E-02, &
      10.30,  0.18075066E-02, &
      10.40,  0.16687301E-02, &
      10.50,  0.15432050E-02, &
      10.60,  0.14294697E-02, &
      10.70,  0.13262495E-02, &
      10.80,  0.12324114E-02, &
      10.90,  0.11469579E-02, &
      11.00,  0.10690085E-02, &
      11.10,  0.99777018E-03, &
      11.20,  0.93256666E-03, &
      11.30,  0.87278179E-03, &
      11.40,  0.81786792E-03, &
      11.50,  0.76735242E-03, &
      11.60,  0.72079763E-03, &
      11.70,  0.67783475E-03, &
      11.80,  0.63811706E-03, &
      11.90,  0.60133865E-03, &
      12.00,  0.56724771E-03, &
      12.20,  0.50532927E-03, &
      12.40,  0.45266165E-03, &
      12.60,  0.40642715E-03, &
      12.80,  0.36581820E-03, &
      13.00,  0.33063448E-03, &
      13.20,  0.29931472E-03, &
      13.40,  0.27150227E-03, &
      13.60,  0.24681769E-03, &
      13.80,  0.22485861E-03, &
      14.00,  0.20519275E-03, &
      14.20,  0.18759440E-03, &
      14.40,  0.17178412E-03, &
      14.60,  0.15753811E-03, &
      14.80,  0.14465876E-03, &
      15.00,  0.13302653E-03, &
      15.20,  0.12248494E-03, &
      15.40,  0.11291672E-03, &
      15.60,  0.10420406E-03, &
      15.80,  0.96291296E-04, &
      16.00,  0.89077578E-04, &
      16.20,  0.82486750E-04, &
      16.40,  0.76458152E-04, &
      16.60,  0.70949356E-04, &
      16.80,  0.65896493E-04, &
      17.00,  0.61254868E-04, &
      17.20,  0.56999400E-04, &
      17.40,  0.53078917E-04, &
      17.60,  0.49470819E-04, &
      17.80,  0.46152116E-04, &
      18.00,  0.43086710E-04, &
      18.20,  0.40263786E-04, &
      18.40,  0.37652875E-04, &
      18.60,  0.35237379E-04, &
      18.80,  0.32997065E-04, &
      19.00,  0.30925710E-04, &
      19.20,  0.28996712E-04, &
      19.40,  0.27220055E-04, &
      19.60,  0.25573337E-04, &
      19.80,  0.24033069E-04, &
      20.00,  0.22597807E-04  &
      ],[2,np2]))
      xa1=q_all(1:np2,1)
      ya1=q_all(1:np2,2)
      ypq1=0.d0
      ypqn=0.d0

      call spline(xa1,ya1,NP2,ypq1,ypqn,yq2)

      call splint(xa1,ya1,yq2,NP2,x,yq,error_code)

987   RETURN

  END SUBROUTINE cspline

  !****************************************************************************

  SUBROUTINE spline(x,y,n,yp1,ypn,y2)
!***********************************************************************
!...  Cubic spline code
!...  Original cubic spline code from Numerical Recipe has been modified
!...  to adapt double precision
!...  02/23/05    Zhong
!***********************************************************************

      integer :: i, n

      double precision :: yp1, ypn
      double precision :: p, qn, sig, un
      double precision, dimension(n) :: x, y, y2
      double precision, dimension(n) :: u

    !--------------------------------------------------------------------------

      if (yp1 > 0.99d99) then
         y2(1)=0.d0
         u(1)=0.d0
      else
         y2(1)=-0.5d0
         u(1)=(3.d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      end if

      do i = 2, n-1
         sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
         p=sig*y2(i-1)+2.d0
         y2(i)=(sig-1.d0)/p
         u(i)=(y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1))
         u(i)=(6.d0*u(i)/(x(i+1)-x(i-1))-sig*u(i-1))/p
      end do

      if (ypn > 0.99d99) then
         qn=0.d0
         un=0.d0
      else
         qn=0.5d0
         un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif

      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)

      do i = n-1, 1, -1
         y2(i)=y2(i)*y2(i+1)+u(i)
      end do

  END SUBROUTINE spline

  !****************************************************************************

  SUBROUTINE splint(xa,ya,y2a,n,x,y,error_code)

      integer :: n
      integer :: k, khi, klo
      INTEGER, INTENT( OUT ) :: error_code

      double precision :: x, y
      double precision, dimension(n) :: xa, y2a, ya
      double precision :: a, b, h

    !--------------------------------------------------------------------------

      error_code = 0
!      IF(x<xa(1)) STOP 'Error in splint. x exceeded minimal value.'
!      IF(x>xa(n)) STOP 'Error in splint. x exceeded maximal value.'
      IF( x < xa(1) ) THEN
          error_code = CH5ERROR1
          PRINT '(a)', 'Warning! Error in splint. x exceeded minimal value.'
          PRINT *, 'x=', x
      ELSE IF( x > xa(n) ) THEN
          error_code = CH5ERROR2
          PRINT '(a)', 'Warning! Error in splint. x exceeded maximal value.'
          PRINT *, 'x=', x
      ELSE
          DO k=1,n-1
              IF((xa(k)<x).AND.(x<xa(k+1))) THEN
                  klo=k
                  EXIT
              END IF
          END DO
          khi=klo+1
          h=xa(khi)-xa(klo)
!          if (h.eq.0.d0) STOP 'Error in splint. Bad xa input in splint.'
          IF( h == 0.0D0 ) THEN
              error_code = CH5ERROR3
              PRINT '(a)', 'Warning! Error in splint. Bad xa input in splint.'
              PRINT *, xa
          ELSE
              a=(xa(khi)-x)/h
              b=(x-xa(klo))/h
              y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
          END IF
      END IF

  END SUBROUTINE splint

  !****************************************************************************

  SUBROUTINE getch3pot(xyz1,v)
!****************************************************************************
!...  program is to calculate the potential energy of CH3+
!...  Based on pes6n4
!****************************************************************************

      integer, parameter :: natoms = 6
      integer, parameter :: natoms1 = 4
      integer :: i, j

      double precision, dimension(natoms1,3) :: xyz1
      double precision, dimension(natoms,3) :: xyz
      double precision, dimension(3,natoms) :: xyzt, f
      double precision :: v, h2pot, h2potab, ch30
      double precision, parameter :: ang = 219474.6d0

      character(len=1), dimension(natoms) :: xname

    !--------------------------------------------------------------------------

      do i = 1, natoms1
         do j = 1, 3
            xyz(i,j) = xyz1(i,j)
         end do
      end do

      xyz(5,1) = 0.7035d0
      xyz(5,2) = 0.d0
      xyz(5,3) = 14.0750143d0
      xyz(6,1) = -0.7035d0
      xyz(6,2) = 0.d0
      xyz(6,3) = 14.0750143d0

      do i = 1, natoms
         do j = 1, 3
            xyzt(j,i) = xyz(i,j)
         end do
      end do

      call getfit(xyzt,v)

      h2pot = -1.1745554d0 + 0.0006728d0/2.d0
!      h2potab = -1.17263405d0
      ch30 = -39.4046592d0 + 0.0006415d0/2.d0
!      ch30 = -39.40570072d0

      v = v - h2pot - ch30

      if ((v*ang.le.5.d0).and.(v*ang.ge.-5.d0)) then
         v = 0.d0
      end if
  END SUBROUTINE getch3pot

  !****************************************************************************

  SUBROUTINE geth2pot(xyz1,v)
!****************************************************************************
!...  program is to calculate the potential energy of H2
!...  Based on pes6n4
!****************************************************************************

      integer, parameter :: natoms = 6
      integer, parameter :: natoms1 = 2
      integer :: i, j

      double precision, dimension(5:6,3) :: xyz1
      double precision, dimension(natoms,3) :: xyz
      double precision, dimension(3,natoms) :: xyzt, f
      double precision :: v, ch3pot, ch3potab, h20
      double precision, parameter :: ang = 219474.6d0

      character(len=1), dimension(natoms) :: xname

    !--------------------------------------------------------------------------

      do i = 5, 6
         do j = 1, 3
            xyz(i,j) = xyz1(i,j)
         end do
      end do

      xyz(1,1) = 0.d0
      xyz(1,2) = 0.d0
      xyz(1,3) = 0.d0
      xyz(2,1) = 0.d0
      xyz(2,2) = 2.0657297d0
      xyz(2,3) = 0.d0
      xyz(3,1) = 1.7889744d0
      xyz(3,2) = -1.0328649d0
      xyz(3,3) = 0.d0
      xyz(4,1) = -1.7889744d0
      xyz(4,2) = -1.0328649d0
      xyz(4,3) = 0.d0

      do i = 1, natoms
         do j = 1, 3
            xyzt(j,i) = xyz(i,j)
         end do
      end do

      call getfit(xyzt,v)

      ch3pot = -39.4046592d0 + 0.0006415d0/2.d0

!      ch3potab = -39.40570072d0
      h20 = -1.1745554d0 + 0.0006728d0/2.d0

      v = v - ch3pot - h20
      if ((v*ang.le.5.d0).and.(v*ang.ge.-5.d0)) then
         v = 0.d0
      end if
  END SUBROUTINE geth2pot

  !****************************************************************************

  SUBROUTINE getfit(xn,f0)

      integer :: i, j, l, k
      integer, parameter :: m = 2239
      integer, parameter :: mr = 32

      double precision, dimension(0:2,0:5) :: xn
      double precision, dimension(0:2,0:5) :: xn1
      double precision, dimension(0:2,0:5) :: gf0
      double precision, dimension(0:5,0:5) :: d0, r0
      double precision, dimension(0:2) :: rvec
      double precision, dimension(0:m+2*mr-1) :: vec,vec0,vec1
      double precision :: f0
      double precision :: t0,t1
      double precision, parameter :: dd = 1.0d-6

!.... Data for the potential energy fit

!.... fit to 36173 ab initio data points (In this fit, I include the data in
!.... all energy region) pes6n4

!.... The weighted fit is used: for pe<30,000 cm-1, weight=1; pe>=30,000 cm-1,
!.... weight=1/5 

!.... There is a small bug which affects the permutational invariance in those
!.....old versions of pes and is fixed in this version
!.... 10/18/05 Zhong Jin

!     m = numbers of coefficients for the fit

!      integer, parameter :: m = 2239
!      integer, parameter :: mr = 32

      double precision :: dc0 = 0.30547300513d0
      double precision :: dc1 = 0.19476072488d0
      double precision :: dw0 = 0.09846745640d0
      double precision :: dw1 = 0.12149234254d0

!     coef = the coefficients of the fit

      double precision,dimension(0:m+2*mr-1) :: coef

    !--------------------------------------------------------------------------

!     m + 2*mr-1 = 2302

!      coef(0:m+2*mr-1) = [                                        &
      coef(0:999) =      [                                        &
              -23.7917058830000d0,         -6.4301935395000d0,    & 
                9.6555537013000d0,         -0.4222307764200d0,    &
                3.8416925064000d0,         -8.0394789073000d0,    &
               -1.1749698623000d0,         20.5106759540000d0,    &
               -7.8741555553000d0,         89.7946532500000d0,    &
               -1.3905663994000d0,         15.7498029470000d0,    &
               -8.0407892798000d0,         10.8675074470000d0,    &
                9.4268687918000d0,        -47.2902909680000d0,    &
              118.0310107300000d0,          9.8375163247000d0,    &
               -1.4557339743000d0,         27.9512764570000d0,    &
               -5.8018583029000d0,        -23.0334008190000d0,    &
               -3.8887411082000d0,          3.2878271533000d0,    &
               31.9479893470000d0,         -2.0216901853000d0,    &
              -44.7316659050000d0,        -35.6689756460000d0,    &
                0.0409225128805d0,         -0.3990277736600d0,    &
               -2.4971757869000d0,          3.1792711580000d0,    &
                1.6118993510000d0,         -9.2484596114000d0,    &
              -26.2277969820000d0,         -4.9586213639000d0,    &
               32.5448577340000d0,         -4.9549013363000d0,    &
              202.4518640700000d0,         26.0039766220000d0,    &
                1.6466589111000d0,        -38.3344672320000d0,    &
              -18.7530617160000d0,        -36.9943894750000d0,    &
                7.8627400642000d0,         -8.3924041243000d0,    &
               84.2350867210000d0,         -2.4120390997000d0,    &
              -66.6708900540000d0,       -128.6204724700000d0,    &
               -4.7305662380000d0,         -6.3125744466000d0,    &
               -4.4705677655000d0,          9.3123692082000d0,    &
                0.4331123049300d0,          1.8625956008000d0,    &
              -31.8911433930000d0,         -5.2409826471000d0,    &
               -6.6709016188000d0,          0.1676687551000d0,    &
               -3.0766181737000d0,          7.8625375751000d0,    &
               -1.8980031118000d0,        -25.0898149520000d0,    &
               42.2404351860000d0,          4.9614104411000d0,    &
               21.0527538120000d0,         -1.6427783170000d0,    &
                4.6907232478000d0,          3.5682167388000d0,    &
               -1.0071025436000d0,         -0.0337604162688d0,    &
               -5.6128359332000d0,         37.7657929930000d0,    &
               -3.7435975625000d0,         23.8160898200000d0,    &
                1.3620412323000d0,          6.0009498501000d0,    &
               -1.0407960597000d0,         -4.7953432966000d0,    &
                0.0398010474751d0,         -9.1987579816000d0,    &
               36.7599467320000d0,          0.2620841427300d0,    &
               -1.5125299966000d0,         35.6014834900000d0,    &
               42.6114192400000d0,         10.9057843650000d0,    &
               20.8204598010000d0,        -11.2115370360000d0,    &
               -5.8216393052000d0,        -14.9146280180000d0,    &
                0.3533974373400d0,        -25.1964661760000d0,    &
               -2.1317935470000d0,        -11.0911020820000d0,    &
               -4.5805700404000d0,         -0.0729461562496d0,    &
               -9.0696488362000d0,         11.5911050450000d0,    &
                1.9035105205000d0,         -0.0525760730412d0,    &
                5.9036598586000d0,         -7.4687698147000d0,    &
                6.0678916301000d0,          5.8422887930000d0,    &
              -29.6447838780000d0,        116.2460272000000d0,    &
               17.5530862900000d0,         -6.3738854640000d0,    &
               18.5789435210000d0,         -9.0466842870000d0,    &
              -43.2000173050000d0,         -3.3057611356000d0,    &
                6.1677979559000d0,         78.6115732790000d0,    &
               -5.7171090474000d0,        -91.1330609910000d0,    &
              -30.4986567560000d0,         -1.4687625435000d0,    &
               -4.8647675528000d0,         -4.5336044174000d0,    &
                5.9821730789000d0,         -8.1073414018000d0,    &
               -0.0819743010258d0,         -9.8817918787000d0,    &
              -12.9714187890000d0,        -11.1670194860000d0,    &
               -2.1338453749000d0,        -22.2464674780000d0,    &
              -10.2036631460000d0,         -4.1605670628000d0,    &
              -22.3043852590000d0,         19.5781133370000d0,    &
               18.4463503680000d0,         71.2233501070000d0,    &
               -1.7064550815000d0,         10.9101083310000d0,    &
               -2.7915388253000d0,          2.5376261331000d0,    &
                3.5154186230000d0,          8.0142950524000d0,    &
              -79.7709113420000d0,         -6.5056557298000d0,    &
                7.1978737352000d0,          4.3574479821000d0,    &
               20.3217968200000d0,         -5.4754279738000d0,    &
                4.0412897294000d0,         -4.2025219897000d0,    &
                2.4218945407000d0,         23.4445582280000d0,    &
                0.5459046063500d0,          1.8497512302000d0,    &
               22.6143326600000d0,         27.7815809350000d0,    &
                8.4831214321000d0,          7.5970578816000d0,    &
               -7.4571849150000d0,         12.6369147080000d0,    &
               -9.9377808003000d0,          0.3925625057400d0,    &
              -15.8058026170000d0,         -6.5225895505000d0,    &
              -33.9441232770000d0,         -8.9777442582000d0,    &
               -1.1380383783000d0,        -28.2996327220000d0,    &
               33.4282217500000d0,          6.1367453962000d0,    &
                0.2978384617100d0,         -0.0554456069632d0,    &
               15.3932549310000d0,         -0.7778284871800d0,    &
               -0.6448810599300d0,          0.7949153908400d0,    &
               -1.1152853610000d0,         -7.2643548512000d0,    &
               -0.3062874564900d0,          7.2648619196000d0,    &
              -20.8103491060000d0,          0.8681822635700d0,    &
                2.2349854485000d0,         -0.1865945062700d0,    &
                0.3153445853500d0,         -0.9705854735800d0,    &
               -3.1025503108000d0,         -1.4256457892000d0,    &
               -0.4478402685800d0,         -6.7098616259000d0,    &
                0.6985263846100d0,        -10.9669661980000d0,    &
                0.1765879061800d0,          0.3146179896900d0,    &
               -0.3702390943900d0,          1.1221895317000d0,    &
                2.0694149663000d0,          5.7517470657000d0,    &
               -9.0687788943000d0,          0.1047230856900d0,    &
                1.0701117685000d0,        -10.6857709090000d0,    &
              -15.9305790910000d0,          3.1026636685000d0,    &
               -8.3535043424000d0,          3.4194759111000d0,    &
                5.4567441371000d0,          2.3843240939000d0,    &
               -0.5574520515500d0,         10.6583576960000d0,    &
               -0.4568900272800d0,         -1.8592134299000d0,    &
                0.0269042155292d0,          0.5166209640600d0,    &
               -0.8631974336300d0,          0.7648171385800d0,    &
                0.0224183228661d0,          2.0423334712000d0,    &
               -0.5195312421500d0,         -3.5408894712000d0,    &
               -0.0381600521014d0,         -1.6576303184000d0,    &
               -9.0361655225000d0,          0.7198767690600d0,    &
                2.6793767991000d0,         -0.1827093114500d0,    &
                0.6388400698200d0,         -0.4196772535300d0,    &
               40.8067646230000d0,         -2.0566467482000d0,    &
                1.3781944361000d0,        -12.7579464780000d0,    &
                2.2233973232000d0,          0.6660000333700d0,    &
                0.2430481737400d0,          0.1219846006600d0,    &
               -0.6301215698500d0,         -1.2698434986000d0,    &
                1.6145287067000d0,          0.3799221862100d0,    &
                0.6396777982300d0,          1.3373222198000d0,    &
                0.4570817562700d0,         -0.1747395610100d0,    &
               -0.4637838520200d0,          0.1586051518500d0,    &
               -0.0486184696625d0,         -3.7439897730000d0,    &
                2.0473629191000d0,          3.6286495556000d0,    &
                1.8365322477000d0,          6.4303409509000d0,    &
                6.4164476157000d0,          0.1588220412600d0,    &
                0.9780979358600d0,         -0.1258344619900d0,    &
                0.1277828386900d0,         -2.1040371913000d0,    &
                1.5477623801000d0,         -1.0687244497000d0,    &
               -1.4128937614000d0,         -1.3429075198000d0,    &
               -0.1076326251300d0,         -0.0272557159819d0,    &
               -0.4412434486800d0,          0.9524744253300d0,    &
               -0.3835844942800d0,          0.1682660151900d0,    &
               -1.9110312315000d0,         -1.1494734422000d0,    &
               -1.5826420224000d0,         -3.9376654488000d0,    &
               -4.7607317861000d0,         -2.9417106581000d0,    &
               -0.8370793221900d0,          1.9663737891000d0,    &
               -1.7904800556000d0,         -0.8086614837800d0,    &
                0.9411005813200d0,          2.4697037626000d0,    &
                3.2992266443000d0,          1.4247685321000d0,    &
                0.2396760698300d0,         -0.0688874914793d0,    &
               -0.4080176741300d0,          0.8133921257100d0,    &
                0.5542738598500d0,         -1.8746183203000d0,    &
                0.1056273918300d0,          0.2525538649300d0,    &
                0.8432227113000d0,          0.6159382674800d0,    &
               -0.4193843604900d0,         -0.2437142065200d0,    &
                0.2119854037800d0,          0.1284323019500d0,    &
               -0.7187690074300d0,         -0.9852865497000d0,    &
               -0.6131551240000d0,          0.9978317731200d0,    &
               -3.0223991102000d0,        -23.1854056200000d0,    &
               -1.3141368430000d0,         14.6773487570000d0,    &
                0.4804707075400d0,        165.2262225400000d0,    &
               -1.2880575262000d0,          2.4791972533000d0,    &
              -31.3009001080000d0,          2.7760418456000d0,    &
                3.6090115737000d0,          0.4378343164700d0,    &
               -6.6951273461000d0,         88.1444617340000d0,    &
               -2.6482679412000d0,       -227.7848561400000d0,    &
             -301.7644864800000d0,         -1.3001719710000d0,    &
               -5.2763229205000d0,          0.0730214788533d0,    &
               -0.5559251897100d0,          1.0859619965000d0,    &
               31.7559421580000d0,          4.8942877411000d0,    &
               -0.4139651200700d0,         -3.6439743497000d0,    &
               -0.7721873946200d0,        -10.7798940710000d0,    &
              -11.1228604480000d0,         19.0312461410000d0,    &
               -3.7843015618000d0,        -13.0198331190000d0,    &
               -0.4455400830000d0,          6.3092124623000d0,    &
                1.2128561738000d0,          1.2933660586000d0,    &
                0.5475357796000d0,         -8.6265725212000d0,    &
               -4.9743049911000d0,       -154.0756609800000d0,    &
              497.4383226900000d0,         -8.0849043752000d0,    &
                2.9035878160000d0,         -0.4736471376800d0,    &
                1.0282256396000d0,         -1.9467495518000d0,    &
               -0.1290570760300d0,          3.1703302120000d0,    &
              -63.0859036920000d0,          9.7120172133000d0,    &
                1.9044873797000d0,        -56.0701832430000d0,    &
                3.6785311098000d0,          3.5030959299000d0,    &
               24.1008556450000d0,         -1.7938159171000d0,    &
              110.5952263900000d0,        115.7112754900000d0,    &
              -96.9110518170000d0,          1.8748625864000d0,    &
                4.8612449864000d0,          0.7589638049600d0,    &
               -2.2510329717000d0,         -0.3105052922800d0,    &
               -2.6184250728000d0,         -1.3614052770000d0,    &
                1.9012086272000d0,          0.2913482275600d0,    &
                8.0042288484000d0,          0.5614283503800d0,    &
               -3.6678711336000d0,        -15.7547856390000d0,    &
              -18.1813029870000d0,         -9.9187486274000d0,    &
               -1.4719022567000d0,          0.1763088160800d0,    &
                0.7762085651600d0,          3.3389971165000d0,    &
                6.9584918628000d0,          9.1809037287000d0,    &
               40.8834947340000d0,         -2.0876329952000d0,    &
                9.5742153878000d0,         -0.4157007061200d0,    &
              -40.6043087720000d0,         -0.7738031985700d0,    &
               -2.6341627124000d0,         -0.5542674397200d0,    &
               -0.9324672953000d0,          9.3773392769000d0,    &
                2.8768513259000d0,         17.5275097280000d0,    &
               -4.6043062906000d0,          0.1638723037100d0,    &
               12.1228766610000d0,          2.7439633312000d0,    &
                3.8495164476000d0,         -6.0899431470000d0,    &
                0.2071055536800d0,         -8.6306379227000d0,    &
               -3.1443082022000d0,        -10.6339913550000d0,    &
              -10.1326944680000d0,          4.5747644280000d0,    &
                5.8391563019000d0,          0.5650015442900d0,    &
               -3.4684320005000d0,         -5.6506948986000d0,    &
               -1.9223390230000d0,        -22.5460951620000d0,    &
               -6.7696650522000d0,          5.0906552365000d0,    &
              -23.8621057670000d0,         17.2831879680000d0,    &
                3.9193707948000d0,         -0.2251488840500d0,    &
                1.6464693300000d0,        -23.1027708020000d0,    &
                0.6457839985300d0,         86.3376181580000d0,    &
              200.3843307200000d0,          0.2936611878200d0,    &
                1.6357731090000d0,          0.4309607560500d0,    &
                0.1234583491800d0,          1.8180276730000d0,    &
             -306.2528464100000d0,         18.9921575730000d0,    &
                2.7781902051000d0,        -30.5535482600000d0,    &
               -0.0412552195969d0,          1.9955557881000d0,    &
              -15.4616753920000d0,         -1.3842832909000d0,    &
               -0.3866262265600d0,          1.3741553831000d0,    &
                1.7767317808000d0,          4.1413959064000d0,    &
                0.5085398150600d0,         -2.2168145543000d0,    &
               -0.4691501747600d0,          3.1713963649000d0,    &
                0.6620563898300d0,          0.5760959124700d0,    &
               -3.2157606062000d0,         43.0643163880000d0,    &
               -0.6246380155800d0,         -3.5628482024000d0,    &
               -1.7461109920000d0,          8.5053761293000d0,    &
                5.1251992287000d0,         -0.5457634987800d0,    &
               -8.0435750912000d0,         -0.2422404972000d0,    &
                3.8747956674000d0,          0.0427943410040d0,    &
               -0.8621352167500d0,         34.0672486970000d0,    &
               -3.3103089214000d0,         -6.7068975197000d0,    &
               -0.4795894167100d0,         35.3666768300000d0,    &
               -1.6331941294000d0,        -29.9647416760000d0,    &
                0.3770152287500d0,         -0.9171115947400d0,    &
                8.2648348816000d0,          1.9856675836000d0,    &
                1.5839138505000d0,        -30.1271080280000d0,    &
               -9.6478593722000d0,          4.9854299767000d0,    &
                0.1132026216000d0,         -0.4999751669100d0,    &
               17.5978160110000d0,          7.4854679071000d0,    &
               91.6380212020000d0,          1.0384717320000d0,    &
                1.5537946170000d0,         -1.4486232347000d0,    &
                0.4554408906400d0,         -0.6844550688300d0,    &
                1.8168236300000d0,          3.2107443473000d0,    &
                2.6491441450000d0,         -2.0014163560000d0,    &
                7.4402569836000d0,         -5.2159513169000d0,    &
              -10.3858398310000d0,        -12.9440301100000d0,    &
               -5.0454817528000d0,         -4.5000378595000d0,    &
                6.0579417835000d0,          2.3137226614000d0,    &
                0.4707280602600d0,          2.2885523257000d0,    &
                0.2812774209300d0,         -0.6121052152200d0,    &
               -0.1358295295700d0,         -8.0771982265000d0,    &
                0.4345298390600d0,          1.4288725939000d0,    &
               -0.0598663875418d0,         -0.1721105082600d0,    &
                9.3485987727000d0,         -0.1738822413400d0,    &
               -3.9152031024000d0,          8.6062508178000d0,    &
                0.0459397429042d0,         -2.1395267233000d0,    &
               -0.4318279077900d0,         -0.8183270411700d0,    &
               -3.6535863947000d0,         -0.1612376110000d0,    &
               -0.1220986455800d0,          1.0340270782000d0,    &
                3.5003247017000d0,         28.1972803410000d0,    &
               26.2777361590000d0,          0.7525205993000d0,    &
                1.0031451220000d0,          1.1263976830000d0,    &
                2.1488126808000d0,          0.0743831525778d0,    &
                1.3169941056000d0,        -12.8978282230000d0,    &
               -0.3062961289700d0,          0.4214815901500d0,    &
                1.8054108664000d0,          5.6452077830000d0,    &
              -42.5861372230000d0,          5.2528389101000d0,    &
               -0.3695542574300d0,          0.1178024356900d0,    &
               -0.2286310585600d0,          0.1505002889500d0,    &
               -9.3482632484000d0,         -0.7778029387100d0,    &
               -0.5610938231500d0,         -0.3917028411500d0,    &
                1.6706229717000d0,          0.7879670416700d0,    &
               -0.6374003595900d0,         -0.5054458305400d0,    &
                0.0626654298961d0,         10.6708016280000d0,    &
                0.1814369478000d0,          0.7722712128500d0,    &
                2.1111611812000d0,          1.0590879290000d0,    &
                0.3238528309500d0,         -0.9907568503200d0,    &
               -0.2071413945700d0,         -0.5025723994600d0,    &
                3.6488257393000d0,         -1.9384359090000d0,    &
                0.2574314641300d0,         -1.8507268895000d0,    &
               -2.9350149502000d0,         -7.6612449421000d0,    &
              -11.7201626860000d0,        -65.9835454480000d0,    &
                1.4336859621000d0,        -11.2041785060000d0,    &
                0.0589339429267d0,          1.9939963320000d0,    &
               -6.2432712432000d0,          0.6580163187100d0,    &
                6.0408729073000d0,        -26.2576685220000d0,    &
               -0.9539482046300d0,         -3.6574877717000d0,    &
                0.3383909200000d0,         -0.4219170779500d0,    &
                0.5032563362400d0,         -1.2587814814000d0,    &
              -12.1335868300000d0,          0.1093007043800d0,    &
                3.1136043918000d0,         -1.1726294240000d0,    &
               -1.5241939520000d0,         -2.0902010499000d0,    &
                0.4397275982800d0,         -0.5321432897600d0,    &
               -0.0162980657261d0,         -4.1876530129000d0,    &
                0.0036486837138d0,         -3.0221149536000d0,    &
                1.0600155549000d0,          0.8146877270600d0,    &
                0.4544366707200d0,         -1.0792627885000d0,    &
               -1.4817906482000d0,          0.2586378262200d0,    &
               -0.6712379989400d0,         -7.7822031094000d0,    &
              -12.5091879040000d0,        -17.9045601360000d0,    &
               -0.5062678397400d0,         -3.3938238280000d0,    &
               40.8539739860000d0,         14.2205995960000d0,    &
                1.8029183814000d0,        -15.5941787270000d0,    &
               -7.0595298230000d0,         -0.7368219709000d0,    &
                0.7224501456200d0,          1.9113685990000d0,    &
               -0.9940153073900d0,         -7.5441741792000d0,    &
                0.0611667161571d0,         -7.3840071362000d0,    &
                7.2539399585000d0,         -0.4191620995800d0,    &
                0.5218206029400d0,         50.7572969870000d0,    &
                0.9322537790100d0,          4.1455049451000d0,    &
                2.1922738100000d0,         -1.0821350736000d0,    &
               -1.0928279217000d0,          0.9629710268300d0,    &
                0.8699004661700d0,         -1.0321216996000d0,    &
               -3.2978084523000d0,         -1.1458398809000d0,    &
               -0.0296393276013d0,          0.2800753093500d0,    &
                0.7434249088300d0,         13.3487753330000d0,    &
              -52.1875686940000d0,         -0.1478834773900d0,    &
                0.8689568368700d0,          0.1927762654700d0,    &
               -0.3418625762200d0,         -0.1716739311300d0,    &
                0.0258247433991d0,         -0.0585973731523d0,    &
                5.8808700926000d0,          0.1490722678900d0,    &
                0.0113357929730d0,          6.5616462001000d0,    &
               -0.2755714474700d0,          0.6839189125600d0,    &
               -0.0517753437554d0,         -1.4133476355000d0,    &
               -7.0877413036000d0,        -29.0074744540000d0,    &
               26.0064673640000d0,         -0.4622519202400d0,    &
               -0.2091938055300d0,         -0.3183481362300d0,    &
                0.2573775665400d0,         -0.1245836899100d0,    &
                0.4509115784800d0,          0.5954672641300d0,    &
               -1.1492917581000d0,         -0.2072219935700d0,    &
                6.5308329139000d0,         -2.3902746252000d0,    &
               -4.8565423760000d0,         -2.1214504211000d0,    &
              -24.2498537420000d0,         -0.2453717448200d0,    &
               -0.1899374441200d0,         67.4338212860000d0,    &
               -0.7792432348800d0,         -0.2808950248500d0,    &
              -14.5259988780000d0,         -1.5778197835000d0,    &
               14.3022313840000d0,         11.7466921100000d0,    &
               20.7699713350000d0,          4.9724827569000d0,    &
                0.1280575013300d0,         -3.6664856294000d0,    &
                0.4297085354200d0,         -1.5240012396000d0,    &
                3.5260881222000d0,         -0.6666131924600d0,    &
               32.2735628630000d0,          9.2855548094000d0,    &
               -0.6542673542500d0,         27.5102015580000d0,    &
              -12.6468953780000d0,         -4.5240411829000d0,    &
               -0.1581818940700d0,         -0.7893183205900d0,    &
               -0.4043647854400d0,         -0.3186737624300d0,    &
                0.0447575350464d0,         -1.8477050617000d0,    &
               -6.9452200050000d0,          0.0350282041500d0,    &
               -0.4879933723700d0,         -3.4164094577000d0,    &
               -7.5930932258000d0,         11.2817640570000d0,    &
               -2.4735487549000d0,          0.3094056299700d0,    &
               -1.1012737359000d0,          0.4142022701100d0,    &
                0.1322531204700d0,         12.9216465730000d0,    &
                0.2739774422200d0,          1.1192779901000d0,    &
                0.7642245402700d0,          0.0072479731871d0,    &
                0.8881027919100d0,         -0.6868087677700d0,    &
               -0.1469084498200d0,          0.2631651743800d0,    &
               -8.0495418638000d0,         -0.0039318249858d0,    &
                0.4028026604000d0,          7.6547897066000d0,    &
                0.3522616608800d0,          0.1850472795800d0,    &
                1.8981576984000d0,          1.4134130459000d0,    &
               20.1423165990000d0,          2.6891819878000d0,    &
                0.7327120794700d0,          0.4916184079000d0,    &
                0.0321844998852d0,          0.3213177556800d0,    &
               -7.9306482080000d0,          0.5688726774800d0,    &
                3.3271284884000d0,          1.8442532457000d0,    &
               -0.1307891620800d0,          0.5288908585700d0,    &
               -1.3519456785000d0,         -0.3684005563700d0,    &
                0.0077418846778d0,         -0.0674084770959d0,    &
                5.2541586184000d0,         -0.0331128283690d0,    &
                1.7173771718000d0,        -22.5733854570000d0,    &
               -0.0136488067517d0,         -0.1354176338700d0,    &
               -0.1377868200600d0,         -0.0344550444378d0,    &
               24.7584710160000d0,         -0.4738525678000d0,    &
               -6.9629070097000d0,          6.4568165484000d0,    &
                3.9379501875000d0,         -1.2618490818000d0,    &
                0.5547188804300d0,          0.3442142842000d0,    &
                0.0746584892588d0,         -3.1714619793000d0,    &
               -0.5220586066300d0,          3.0340225276000d0,    &
                1.0216133047000d0,         -6.4365292769000d0,    &
                3.0328246913000d0,         -1.6145943530000d0,    &
                3.3629843103000d0,        -22.3636830700000d0,    &
                3.4723774169000d0,          1.6994698068000d0,    &
               -0.3846069158500d0,          0.1495685367800d0,    &
               -0.0778579106266d0,         -0.2237207911300d0,    &
               -5.8229220756000d0,        -18.9495943060000d0,    &
               -2.7683440952000d0,         -7.4709530781000d0,    &
               -3.1883218374000d0,         -2.5931139889000d0,    &
               -5.4424141686000d0,          2.2698672224000d0,    &
               11.0128541540000d0,         -1.8096833422000d0,    &
              -10.3955365350000d0,         -0.0508073306580d0,    &
               -0.1622973539700d0,          0.0229561557140d0,    &
                0.1228369941200d0,          2.4616124560000d0,    &
                0.0781795420522d0,         -0.2381969509300d0,    &
                0.0552137853643d0,          0.0170693338353d0,    &
               -0.0809915087564d0,        -10.8261897950000d0,    &
                0.0607362688482d0,          3.5397986728000d0,    &
               22.4099623430000d0,         -0.0809808614077d0,    &
               -6.2710517428000d0,          0.1775727275900d0,    &
                1.3339810886000d0,         17.0861754910000d0,    &
               14.1287543600000d0,         -0.3005200338700d0,    &
               20.3654704640000d0,         -0.8370327209700d0,    &
               22.7582896460000d0,         -0.1986608126100d0,    &
                0.4240858922700d0,          0.4998994956600d0,    &
                1.9267080140000d0,          0.2572130793000d0,    &
                3.0002229291000d0,         11.1875908590000d0,    &
                2.0986504645000d0,        -28.0002510650000d0,    &
               -1.5400602458000d0,         -0.4545276583700d0,    &
               -0.4915937178300d0,         -0.0530480753972d0,    &
                9.7630614645000d0,         -1.5846189997000d0,    &
               17.0051858270000d0,          0.5767431519600d0,    &
               -1.1205120900000d0,         17.4215602350000d0,    &
               -1.3721510402000d0,          7.5745272181000d0,    &
               -3.7538513191000d0,         -1.6147843452000d0,    &
               -3.8931310208000d0,         12.6359144900000d0,    &
               -2.0214407067000d0,        -36.5490778200000d0,    &
                0.1072496114100d0,         -5.2135556105000d0,    &
              -16.2649547040000d0,        -11.0303011610000d0,    &
               -4.6212387003000d0,          0.3144172060200d0,    &
               -0.7662074648200d0,         -0.0829802522225d0,    &
               -0.1078129676200d0,         -0.5506898114800d0,    &
                1.0049587621000d0,         -1.2727270315000d0,    &
                1.0571248117000d0,          1.9267788142000d0,    &
               -0.3518970421900d0,          0.1526361317800d0,    &
               -1.7505381080000d0,         -0.2204689364300d0,    &
               -0.4922396700800d0,          0.2238938043300d0,    &
               -0.2993061156500d0,         -2.4173055174000d0,    &
               -0.2706112856300d0,          1.0254853248000d0,    &
              -16.6416310520000d0,         82.5646350900000d0,    &
                4.5410492826000d0,          0.3211897953900d0,    &
              -35.2573915090000d0,          3.2663288207000d0,    &
              -42.9867488540000d0,          0.0755750084331d0,    &
               -0.5979769089300d0,        128.2093599300000d0,    &
                0.1270063128200d0,       -239.1244346400000d0,    &
              -85.7746504790000d0,         -0.6332370964600d0,    &
               -2.8813990426000d0,         -1.5004127270000d0,    &
               -1.4143317358000d0,         -0.1837129265200d0,    &
                7.4498720893000d0,         17.6331512050000d0,    &
                2.6093583338000d0,         -3.3503399895000d0,    &
               -0.0980981549000d0,        -11.4857000240000d0,    &
              -46.8601850620000d0,         20.9251008770000d0,    &
              126.3407311600000d0,         80.6021109520000d0,    &
               -2.7945478507000d0,         -0.0082176629264d0,    &
                3.3516064066000d0,          0.1501405599700d0,    &
                0.1281124711500d0,         -1.0673369480000d0,    &
               17.5501369140000d0,        -52.9783273680000d0,    &
              121.8623510800000d0,          2.7711093010000d0,    &
              -21.7235445860000d0,         -1.2195247322000d0,    &
                2.5822633911000d0,          0.9940686987400d0,    &
                0.0005917592410d0,          0.2632064287000d0,    &
              -72.9297652200000d0,          1.6067954479000d0,    &
               -0.0260842339317d0,        -46.7885129180000d0,    &
                5.7639803687000d0,         -6.9177506963000d0,    &
               17.6711246520000d0,         14.7280355360000d0,    &
              114.9553723600000d0,         23.6370525140000d0,    &
              -21.2343100450000d0,         10.7196870100000d0,    &
               12.6322467040000d0,          1.9165944683000d0,    &
               -1.5184466205000d0,          1.3944918943000d0,    &
               -5.4711406735000d0,          0.1997312732400d0,    &
               -0.8113061396400d0,         -0.1573112323100d0,    &
                4.6788222682000d0,         -9.4413671097000d0,    &
               -1.0713655638000d0,        -10.0201517180000d0,    &
               -8.3140577266000d0,         -0.2269536533100d0,    &
                2.1538874857000d0,         -6.6909779530000d0,    &
              -10.7536923440000d0,         10.1456041380000d0,    &
                3.9428215741000d0,          7.1884588205000d0,    &
               22.1784754730000d0,         -2.8182794524000d0,    &
                5.4494791209000d0,         -0.0974419368245d0,    &
              -45.9343409820000d0,         -2.9013228027000d0,    &
               43.9625835280000d0,        -99.5443146310000d0,    &
               -7.5529999414000d0,         18.6261773260000d0,    &
                1.6061733635000d0,          8.2251439216000d0,    &
               -5.6207314636000d0,          0.0025091660784d0,    &
               14.8475351860000d0,         28.4362786560000d0,    &
               12.4845678720000d0,         -6.5064362145000d0,    &
               23.0882331460000d0,         -8.8629808571000d0,    &
                1.1645922682000d0,          5.4057263403000d0,    &
              -23.3699913060000d0,        -62.9290283880000d0,    &
              -35.6669006700000d0,         13.2472994520000d0,    &
                0.7560078570000d0,         -2.6017397815000d0,    &
               -2.7503328828000d0,        -15.5629545820000d0,    &
               -3.2151660696000d0,          7.4235052875000d0,    &
              -15.3186941710000d0,          9.9378146313000d0,    &
                2.1116036472000d0,         -0.0748070480226d0,    &
                0.3684941357800d0,        -62.6011913970000d0,    &
               -0.1033567341900d0,         85.7011315350000d0,    &
               16.0972594550000d0,          1.1278382245000d0,    &
                5.2359389180000d0,          1.4605856561000d0,    &
                2.2704077049000d0,          0.5180588890300d0,    &
               55.7023201340000d0,        -38.2140066170000d0,    &
                3.6303272833000d0,          4.7618976759000d0,    &
              -14.3977250970000d0,          3.8126726699000d0,    &
              -23.5294851530000d0,         -4.3771728759000d0,    &
               -2.1361800651000d0,         -8.3874098812000d0,    &
                0.0336830972086d0,          7.6284750952000d0,    &
                0.3389840415100d0,         -9.3432817297000d0,    &
               -3.0401749380000d0,          1.9183149291000d0,    &
                2.6016557199000d0,          0.9850319972400d0,    &
               -2.2385177501000d0,          2.7636512734000d0,    &
                8.4389046068000d0,        -14.0821959970000d0,    &
                1.5058301768000d0,         -1.0351563190000d0     ]

      coef(1000:1999) =  [                                        &
                0.5802687066000d0,         -0.4612103798600d0,    &
                2.0333154704000d0,          0.7281524404600d0,    &
                3.4479234309000d0,         -0.0066612069162d0,    &
               -0.0808250260177d0,         30.9793947820000d0,    &
               -0.7411645585900d0,        -11.6992264720000d0,    &
                0.0212308627178d0,         26.8715608110000d0,    &
                0.3421995872900d0,        -31.7463506210000d0,    &
               -2.6899787332000d0,          3.1345962763000d0,    &
               18.8446165600000d0,          0.8911884571200d0,    &
                2.4627470812000d0,         -6.7844306843000d0,    &
               -5.5965130584000d0,         10.1287681460000d0,    &
               -3.0377148372000d0,         -4.0212371194000d0,    &
               35.0836692210000d0,         13.3506072550000d0,    &
               32.6347398740000d0,          0.7805871450400d0,    &
               -2.7098514698000d0,          1.4032509370000d0,    &
                0.8431066653900d0,         -4.6002012363000d0,    &
                4.3606078140000d0,          1.0642547636000d0,    &
                2.0079182096000d0,         -4.2709939638000d0,    &
                7.5380606082000d0,         -2.5804777648000d0,    &
               -4.4781389546000d0,         -8.5450735818000d0,    &
               -4.9969268415000d0,         -5.3847324840000d0,    &
                5.8987042165000d0,          1.2250191068000d0,    &
               -0.3863974262800d0,         -0.3784799163500d0,    &
               -0.3473645139300d0,         -3.4740891343000d0,    &
               -0.3223429042700d0,         -3.9842415060000d0,    &
                4.6453152932000d0,          6.7327426298000d0,    &
                2.0795992794000d0,         -1.2149349495000d0,    &
                7.4224795164000d0,         -0.3887881382200d0,    &
               -6.6176148853000d0,          6.4215267394000d0,    &
               -2.1805065154000d0,        -15.1255346400000d0,    &
               -0.1177193329900d0,         -4.5002078828000d0,    &
                1.1929483017000d0,         12.3706056790000d0,    &
                0.0088643216415d0,          2.5379303465000d0,    &
                7.2479785202000d0,         15.0804231800000d0,    &
                5.2591082559000d0,          3.7228173543000d0,    &
                2.3759829539000d0,          8.0056970948000d0,    &
               -1.1957574741000d0,         -3.5007585625000d0,    &
                4.0913842280000d0,        -12.8682631770000d0,    &
                3.3844548522000d0,          0.9373723861400d0,    &
                6.1493731683000d0,          6.5303312172000d0,    &
              -31.2119617210000d0,          9.2094072638000d0,    &
               -1.1488305126000d0,          3.2825468136000d0,    &
               -0.7550229167900d0,         -1.0665766285000d0,    &
               -7.5382417705000d0,         -2.5146056002000d0,    &
                2.8314945066000d0,         -0.9565411486000d0,    &
                1.0327034925000d0,          9.1347457527000d0,    &
               -5.0417405753000d0,         -2.3645855825000d0,    &
                0.0364107670248d0,         12.1378723390000d0,    &
               16.7825563710000d0,          1.1699974159000d0,    &
              -44.1510702040000d0,        -42.3634113960000d0,    &
                0.1668210566000d0,         -2.4986864995000d0,    &
               -0.0985159998603d0,         -1.2653349486000d0,    &
                5.6260102267000d0,         38.4886144300000d0,    &
               -0.1423776903200d0,          4.1156302820000d0,    &
              -19.7777347930000d0,        -29.7502255300000d0,    &
               -7.4864558014000d0,        -16.6851015190000d0,    &
                2.0284917572000d0,         -1.8808477682000d0,    &
                1.6132734178000d0,          4.4425971295000d0,    &
               -4.0290280380000d0,          1.5161809027000d0,    &
                9.5555429075000d0,        -16.7995991780000d0,    &
               -6.2467824774000d0,        -19.6617508320000d0,    &
                0.3670883978100d0,         -1.5538309749000d0,    &
              -10.8007233310000d0,         -1.9580993658000d0,    &
               -0.6562066032500d0,         -0.2789202903300d0,    &
                9.5689175907000d0,          0.3395889683300d0,    &
               -5.8333268449000d0,         -7.7628271364000d0,    &
                1.1569879316000d0,         -4.5794708482000d0,    &
                0.0102475255705d0,         -5.4598778434000d0,    &
              -13.0477466640000d0,         -6.6394073343000d0,    &
                6.9810816939000d0,          0.4833038145400d0,    &
              -14.0441441050000d0,         -1.9960039931000d0,    &
               18.5575132890000d0,         -0.1137599329100d0,    &
               -2.5088977841000d0,         -9.6884667278000d0,    &
              -11.4288850320000d0,        -12.6453425980000d0,    &
               12.1874514040000d0,        -11.3536555580000d0,    &
               34.1793299520000d0,          9.8034298857000d0,    &
                3.9371097919000d0,        -21.9221430960000d0,    &
               -9.8353205414000d0,        -17.0787254100000d0,    &
                1.3328084512000d0,          5.5944457315000d0,    &
               -3.1509998866000d0,         -4.5416659612000d0,    &
               -1.0033615723000d0,         -4.8992636793000d0,    &
                5.8200815677000d0,         -0.8871365888000d0,    &
               -0.5512822229000d0,         36.2591291430000d0,    &
                5.6274905620000d0,         18.2750733810000d0,    &
               13.1368674340000d0,          0.7473679672900d0,    &
                0.6329642876300d0,          0.4805988944000d0,    &
                2.6339885324000d0,         -2.8051151555000d0,    &
               -8.9425445173000d0,         -3.2640094796000d0,    &
               -0.0120315773893d0,          0.1642429531800d0,    &
               -4.6050715197000d0,         -6.4421744888000d0,    &
               80.6972341900000d0,         -0.8567323709100d0,    &
               20.2154682800000d0,          0.2242946899000d0,    &
               -1.1168265928000d0,         -0.2916649609500d0,    &
                0.0443753604088d0,         -0.1108251075100d0,    &
               14.5475566600000d0,         -1.5567983875000d0,    &
                0.0366672336107d0,          4.0973209520000d0,    &
               -5.4086496059000d0,          4.6614649101000d0,    &
              -20.9106876870000d0,        -11.9254492320000d0,    &
               -5.7978117883000d0,         11.4938289380000d0,    &
               12.2080264310000d0,        -10.4926398400000d0,    &
              -14.5690449260000d0,         -0.2538542673400d0,    &
                1.5538475732000d0,          0.0015947094058d0,    &
               -0.1781354672700d0,          1.1541335875000d0,    &
               -3.9625076101000d0,         -0.7571863671700d0,    &
              -58.6826026700000d0,         -2.5790139199000d0,    &
                2.2995498426000d0,          0.9792502929700d0,    &
               -8.7344689277000d0,         -4.0567955738000d0,    &
               -0.1575780223800d0,        -62.5739651130000d0,    &
               18.7664962800000d0,          1.2090455792000d0,    &
              -16.7201923190000d0,          3.5936095504000d0,    &
               18.2497368400000d0,         18.1149930110000d0,    &
               36.9579579110000d0,          8.7675041320000d0,    &
               -0.5108137764100d0,         29.9355192360000d0,    &
               -6.2860017517000d0,        -22.3875790790000d0,    &
                2.9102144120000d0,         -1.4611969305000d0,    &
               12.0306269670000d0,         -0.5076344891700d0,    &
               -2.2517826299000d0,         11.8303288840000d0,    &
                5.2052844149000d0,         -0.5195295272600d0,    &
               -0.6195370884300d0,         -3.8524171873000d0,    &
               -1.2568071980000d0,          3.3847605630000d0,    &
               -0.5578713085800d0,         -3.4900546288000d0,    &
               -5.6479645003000d0,         -0.2568223420800d0,    &
               -0.9193384922000d0,         -4.9741650693000d0,    &
               -7.5194072117000d0,          4.0958664855000d0,    &
               -2.8570092909000d0,          1.0102619180000d0,    &
               -2.4391002431000d0,          0.5366382537600d0,    &
                0.1180063743200d0,          9.1146100351000d0,    &
                1.0721134032000d0,          7.0104088726000d0,    &
                1.0089585069000d0,          0.1265692333600d0,    &
                4.9035017012000d0,         -5.8695293620000d0,    &
               -0.9911597324200d0,          0.0492770390650d0,    &
                5.4969695158000d0,         -2.7681130607000d0,    &
               -0.1701881864300d0,          2.3534028770000d0,    &
               -0.8604866796900d0,          0.8595464497100d0,    &
               -7.5374887216000d0,         -5.3311871605000d0,    &
                3.2446733610000d0,          0.3587381337400d0,    &
                3.5246912388000d0,         -1.4209389751000d0,    &
               -0.2502344144200d0,          1.8426648486000d0,    &
               -3.6491788596000d0,          1.7078112995000d0,    &
               11.9584917760000d0,          4.9433703520000d0,    &
               -0.8685170168400d0,          2.2658494877000d0,    &
               -6.6998714087000d0,         -1.0210437702000d0,    &
               -0.0081540411694d0,         -0.0535620142350d0,    &
               17.4571866430000d0,         -0.0412016659994d0,    &
                1.4485552751000d0,         24.9223544340000d0,    &
               -0.6384419397500d0,         -2.4447594638000d0,    &
               -0.1298516519500d0,         -0.7871548115100d0,    &
              -27.4444003810000d0,         10.8816084800000d0,    &
               -8.1497086135000d0,         13.4132982440000d0,    &
                2.1124530590000d0,         -1.9661837462000d0,    &
                2.5955083427000d0,          0.9519181247200d0,    &
                0.2050110996500d0,         -3.0911557355000d0,    &
               -3.8784260004000d0,         20.9940207730000d0,    &
               17.8521081690000d0,         -3.5708930545000d0,    &
               -2.3981333109000d0,         -1.5426213376000d0,    &
                1.3941015280000d0,        -12.5563769990000d0,    &
                5.6487624325000d0,        -10.2251492700000d0,    &
               -0.7209411115100d0,          0.6806961699200d0,    &
               -0.0296712173170d0,          0.2668978604700d0,    &
               -2.6647723652000d0,        -13.9682094270000d0,    &
               -3.2292631692000d0,        -10.9626770360000d0,    &
               -2.2627826957000d0,         -3.3878355934000d0,    &
                7.7382001260000d0,          3.5167536260000d0,    &
                2.0984450020000d0,         -0.9487877058300d0,    &
               -8.3860710991000d0,          0.0469898938036d0,    &
                0.3297690765200d0,          0.2847881874200d0,    &
                0.6917026287900d0,          6.7576750395000d0,    &
                0.3612996468300d0,         -0.3944879281200d0,    &
                0.0273122871971d0,          0.0889109564964d0,    &
                0.0350851091224d0,         -7.6039511029000d0,    &
                0.0933078442652d0,          5.7963904231000d0,    &
               12.2645742330000d0,         -0.0156543178736d0,    &
               -3.5772141527000d0,          0.2625707434800d0,    &
                1.8623032807000d0,         12.3028324820000d0,    &
                9.8983007223000d0,         -0.2025148714700d0,    &
               13.6250761360000d0,         -2.3939351603000d0,    &
               18.0912436440000d0,         -1.2770503822000d0,    &
                1.1082279739000d0,          0.7440724258200d0,    &
               -5.4179862840000d0,         -0.0986322439779d0,    &
                4.1716848282000d0,          7.6379525502000d0,    &
                3.4108559714000d0,        -13.9130358620000d0,    &
               -2.7882569209000d0,         -0.2479158488300d0,    &
               -0.8152148489200d0,         -0.3505119224500d0,    &
                7.2926877630000d0,         -6.2715318613000d0,    &
               15.7011899530000d0,         -0.1851680061000d0,    &
                0.1652837683400d0,         17.0022076800000d0,    &
               -2.5170850834000d0,          7.9059503775000d0,    &
               -6.8356674538000d0,         -1.6861950508000d0,    &
               -7.8492432301000d0,          6.5880350420000d0,    &
               -5.2146586341000d0,        -30.1314437120000d0,    &
               -1.2044131729000d0,         -4.2745412271000d0,    &
               -9.4194351413000d0,         -5.4248159597000d0,    &
               -4.1823419175000d0,          1.3912473545000d0,    &
                2.3952503107000d0,          0.1740589326100d0,    &
                0.6395405517500d0,         -2.0280987680000d0,    &
                5.1645964141000d0,         -3.3820890817000d0,    &
                3.6139685886000d0,          6.8736424810000d0,    &
                1.0138338833000d0,          3.6026846448000d0,    &
               -4.9178562755000d0,          1.3633177742000d0,    &
               -0.1811980006700d0,         -0.0083996130340d0,    &
               -0.0546797708150d0,          2.0199109193000d0,    &
                0.0079008442424d0,          0.0183732291787d0,    &
               -0.0011401622510d0,          0.3455993846200d0,    &
               -3.3101756133000d0,         -0.1505118008700d0,    &
                2.2278621468000d0,         -3.2366406228000d0,    &
                0.0728641518115d0,          0.0522925770432d0,    &
                0.0083753584008d0,         -0.0083683860747d0,    &
                0.8617208806800d0,          0.3695192992700d0,    &
                0.0274995653266d0,          0.2698426871600d0,    &
               -0.3454200799300d0,         -8.1704054520000d0,    &
               -3.8576490620000d0,          0.0025395057053d0,    &
               -0.1867299343500d0,          0.0841295660015d0,    &
               -0.4023023847800d0,          0.1318891288700d0,    &
               -0.9896068931900d0,          6.1582123633000d0,    &
                0.0316320942628d0,         -0.2177954208300d0,    &
               -1.6923279277000d0,         -2.7229971934000d0,    &
               17.1678687630000d0,         -2.5038041658000d0,    &
                0.0623428914908d0,         -1.6297921158000d0,    &
                0.3567144748600d0,          0.4964672094200d0,    &
                4.2146018444000d0,         -0.1271391977600d0,    &
               -0.3033083603300d0,         -0.1504606615300d0,    &
               -0.0343036511579d0,         -0.1428655707600d0,    &
               -0.1179243647600d0,         -0.0056636687442d0,    &
               -1.2199212142000d0,         -3.0505642234000d0,    &
               -0.6808540503400d0,         -0.5134326148100d0,    &
               -0.9109081357500d0,         -1.6033995523000d0,    &
                0.1437810593800d0,          0.4910215651900d0,    &
               -0.0731100477369d0,          0.0628439336189d0,    &
                0.3921887651300d0,         -1.0682471609000d0,    &
                0.9566998240000d0,          0.2272448127800d0,    &
                6.3311754927000d0,          6.5180254591000d0,    &
                4.6777321677000d0,         14.5221661920000d0,    &
               -0.5192735489000d0,          1.5508130595000d0,    &
               -0.0307190373099d0,         -0.1667889153100d0,    &
                0.7038548199200d0,          0.6145504632200d0,    &
               -3.6558090052000d0,          8.8021291341000d0,    &
               -0.0831500709418d0,          0.0625307610975d0,    &
                0.0101267896424d0,          0.0840782241809d0,    &
                0.0052840948595d0,         -0.4204508457500d0,    &
                2.3345114039000d0,          1.3031317005000d0,    &
               -4.9179265689000d0,         -3.3474966237000d0,    &
                0.1049203641700d0,          0.3463942360600d0,    &
               -0.1679611530900d0,          0.0306690252229d0,    &
                0.6557105431800d0,          0.6675224366300d0,    &
               -0.1014511772500d0,          1.2156660237000d0,    &
               -2.9556739081000d0,         -0.3375142529900d0,    &
               -0.0258433025404d0,          0.3467208458000d0,    &
               -0.0803349025028d0,         -0.0057339249697d0,    &
                0.1871527812600d0,          2.4328274509000d0,    &
                5.7942119529000d0,          6.0684682567000d0,    &
               -0.2587839396200d0,          2.9324117752000d0,    &
              -21.1429232130000d0,         -5.0747508062000d0,    &
               -0.6302409488600d0,          7.6977835959000d0,    &
                3.5542957575000d0,         -0.0678805964060d0,    &
               -0.6337942839200d0,         -1.0660158907000d0,    &
                0.2333257216500d0,          1.9005990205000d0,    &
                0.5220784795800d0,          1.5971439190000d0,    &
               -3.0139289668000d0,         -0.5066530863200d0,    &
                0.9183210497800d0,        -21.0206179110000d0,    &
                0.1792368984800d0,          0.3094804314100d0,    &
                0.4545146551400d0,          0.0019952431469d0,    &
                0.0223071244471d0,          0.2761238381800d0,    &
                0.2505820720400d0,         -0.0187550362142d0,    &
                0.0059958679787d0,         -0.0304886807813d0,    &
               -0.0031472382610d0,          0.2007457557300d0,    &
                0.2496773355500d0,         -2.7511076151000d0,    &
                1.5546079968000d0,          0.1433736525400d0,    &
                0.2318735239900d0,          0.0904880975014d0,    &
               -0.0114690225602d0,         -1.3761427327000d0,    &
               -0.0066003496926d0,         -0.0931949714754d0,    &
               -3.2842775014000d0,          0.5821849957500d0,    &
               -0.0742312533683d0,         -1.8383917894000d0,    &
                0.5482906978800d0,         -0.1646396606700d0,    &
                2.7345041832000d0,          0.4932689454500d0,    &
                2.1143910061000d0,          5.0678461088000d0,    &
               -6.7427567772000d0,         -0.0349812580132d0,    &
                0.5053804303000d0,         -0.2748772735200d0,    &
               -1.0374137219000d0,         -0.2989306708600d0,    &
                0.2787380351700d0,         -0.3614077649600d0,    &
                0.5535739155200d0,          0.0432079185778d0,    &
               -0.4764337000000d0,         -0.4373361465900d0,    &
               -6.2726974201000d0,         -0.5496370687100d0,    &
                6.9410197200000d0,         -0.9147853789800d0,    &
                0.0442337498443d0,         -6.7484634794000d0,    &
               -1.5993812125000d0,          0.3405863605200d0,    &
                7.1128398362000d0,          0.5117497134600d0,    &
               -6.3326210652000d0,         -4.1055470333000d0,    &
              -11.3852549550000d0,         -1.7886455301000d0,    &
                0.0519641184870d0,         -2.5250062989000d0,    &
               -1.4484936768000d0,          2.7941361785000d0,    &
               -3.2590015165000d0,          0.1517290779100d0,    &
               -9.1584372116000d0,         -0.6897993131200d0,    &
                0.5652121147900d0,         -6.8667189823000d0,    &
               -1.6434817235000d0,          0.8705118771900d0,    &
                0.0528903511472d0,          0.1687255857000d0,    &
                0.1362552483000d0,         -0.3913875242500d0,    &
               -0.1184611504300d0,          0.8351014388500d0,    &
                3.5000711134000d0,         -0.1554568705000d0,    &
                0.1671250552800d0,          2.9155431909000d0,    &
                4.3503278176000d0,         -1.0919293859000d0,    &
                1.6738456180000d0,         -0.0155853937454d0,    &
                1.3092666219000d0,         -0.3184756512100d0,    &
                0.6460261847600d0,         -4.7094407030000d0,    &
               -0.0917412068756d0,          0.0257951556663d0,    &
               -0.4428296545600d0,          0.0553932967289d0,    &
                0.0018913785034d0,          0.3843107843700d0,    &
                0.0429423492830d0,         -0.3160415104200d0,    &
                1.5503498898000d0,         -0.4217600904400d0,    &
                1.4031638011000d0,         -4.0525692200000d0,    &
               -0.1572242108900d0,         -0.0408588045392d0,    &
                1.9738968862000d0,          0.0764386496801d0,    &
               -7.4000794562000d0,         -0.8219434937400d0,    &
                0.6353047119000d0,          5.3226829182000d0,    &
               -0.6768835697400d0,         -1.0578034900000d0,    &
                2.5348080748000d0,         -0.3253766614000d0,    &
               -0.7304478775800d0,         -0.1969403716300d0,    &
                0.5312366839400d0,         -0.1844639328000d0,    &
                0.5561584767300d0,         -0.0892537161691d0,    &
               -0.0016999105972d0,         -0.0169746115938d0,    &
               -4.8486804429000d0,         -0.0129847659047d0,    &
               -0.9199785860900d0,         -0.2862746862300d0,    &
                0.0611751887234d0,          0.0628051093674d0,    &
               -0.0981827041160d0,          0.0363440023849d0,    &
               -1.5135253450000d0,          2.2989672029000d0,    &
                3.1914579064000d0,          3.5039848167000d0,    &
                5.8163845330000d0,         -0.4917820316900d0,    &
               -4.1958479140000d0,         -0.1150174346300d0,    &
               -0.8557179113300d0,          0.4129495190900d0,    &
               -0.0018948043950d0,         -4.9365156343000d0,    &
               -7.3063609568000d0,          2.3400929179000d0,    &
                3.6695104206000d0,          0.9561503283000d0,    &
               -0.4089688603900d0,          6.7986513845000d0,    &
               -1.9217854255000d0,         -1.1224305623000d0,    &
               -0.6423737882100d0,         -3.3712553910000d0,    &
               -0.0003468424852d0,         -0.6735843028800d0,    &
                0.3937224691600d0,         -1.8253105323000d0,    &
                1.1830605914000d0,          3.6850061978000d0,    &
                1.2832384986000d0,          0.7932207023700d0,    &
               -6.1143785107000d0,         -1.7780256675000d0,    &
                1.5270136722000d0,          0.6424250639500d0,    &
                4.9851843983000d0,          0.0142925913206d0,    &
               -0.0290356861910d0,         -0.0210508588305d0,    &
               -0.0178458934533d0,         -0.0969217855411d0,    &
               -0.1301433090600d0,         -0.2249685347300d0,    &
                0.0216824533201d0,         -0.0363390877115d0,    &
               -0.0592842719420d0,          4.0008547480000d0,    &
                0.0210690201017d0,         -1.0457635222000d0,    &
               -6.6071967121000d0,          0.0056221120899d0,    &
                2.0249900849000d0,         -0.0147254289275d0,    &
               -0.3900993723800d0,         -6.4907303612000d0,    &
               -4.7013311578000d0,         -0.0352121967580d0,    &
               -7.2019264493000d0,          0.6925008303800d0,    &
               -9.9036544539000d0,          0.3164979555400d0,    &
               -0.0813904221610d0,         -0.0548689169632d0,    &
                3.8824296714000d0,          0.0835694834398d0,    &
               -0.6709414991600d0,         -4.3492379641000d0,    &
               -0.5882754753700d0,          7.2448982176000d0,    &
                0.6083632955500d0,          0.0064748909234d0,    &
                0.1938654366600d0,          0.0245881357468d0,    &
               -3.6907951597000d0,          1.0927686151000d0,    &
               -7.9144485895000d0,          0.0139138858894d0,    &
               -0.0780094723332d0,         -8.9554476226000d0,    &
                0.5023998077100d0,         -4.0761198303000d0,    &
                1.4141134481000d0,          0.5526322507900d0,    &
                1.4388307394000d0,         -3.8630250508000d0,    &
                0.9035390967800d0,         17.0771254760000d0,    &
                0.2679592500800d0,          2.3464257547000d0,    &
                4.7498097571000d0,          2.7772957276000d0,    &
                2.4594652329000d0,         -0.5616137893300d0,    &
                0.6449955239700d0,         -0.1515606229500d0,    &
                0.1468373046300d0,          0.1672286942800d0,    &
                0.3420148778100d0,          0.1807715888000d0,    &
                0.0974324511646d0,          0.0574007089718d0,    &
               -0.0305450197923d0,          0.4683421995200d0,    &
                0.0764315852199d0,          0.2540182171400d0,    &
               -0.3092718534000d0,          0.0030908034582d0,    &
                0.0140872181406d0,          3.7411349141000d0,    &
                0.0062516298256d0,          0.0893737121324d0,    &
               11.3100689790000d0,         -0.1174698317700d0,    &
               -0.7119329636900d0,         -0.2077446228600d0,    &
               -0.2209878952900d0,         -0.0741991771832d0,    &
              -31.2396945510000d0,          9.0182904174000d0,    &
               -0.5396528879200d0,        -10.1163609810000d0,    &
              -11.0139688310000d0,         -0.0125760771982d0,    &
               -1.8655698727000d0,         -0.4703662496900d0,    &
               -0.5641004820700d0,          1.5301357060000d0,    &
                0.0201876767115d0,         -0.1228152394600d0,    &
                0.3513949813500d0,          0.6895852577500d0,    &
                0.5313224274800d0,         -0.1645508178100d0,    &
               -0.3158205775400d0,          0.0897863662150d0,    &
               -0.1244671363200d0,          4.6104670707000d0,    &
               -3.5033070035000d0,          0.4177056866700d0,    &
               -0.4525141592400d0,          2.6485500665000d0,    &
                2.4712923685000d0,         -0.1186700234600d0,    &
                0.9059028772800d0,          0.4366996690300d0,    &
                0.4812085487600d0,         -0.0082672776121d0,    &
               -0.0028077953064d0,         -1.7634266819000d0,    &
               -0.0414403542187d0,         -0.1602863148100d0,    &
               -0.0004965431893d0,          0.0475634085541d0,    &
               -0.7272896663000d0,          0.0020865040636d0,    &
                1.9084994598000d0,         -0.4378644068500d0,    &
               -0.3452111088300d0,          0.3536024670600d0,    &
               -0.1029136117700d0,         -5.4369528700000d0,    &
                4.5782760036000d0,         -1.3592979902000d0,    &
               -0.4493767603400d0,          3.0809847028000d0,    &
               -0.6123444295500d0,          0.0988844712604d0,    &
               -0.0599601612915d0,          0.0184449424087d0,    &
                1.9557052629000d0,         -1.5751431889000d0,    &
                0.0814347772635d0,          3.7093501608000d0,    &
               -0.7451828520300d0,         -1.8047754315000d0,    &
                0.3305318926600d0,          3.9734690459000d0,    &
               -1.1656278805000d0,          0.2573880986500d0,    &
                0.8777493088700d0,          0.5265820729600d0,    &
               -0.2574558750500d0,         -0.3790874974500d0,    &
                0.4412982115800d0,          0.1591505506900d0,    &
               -0.1521741850000d0,         -0.9423803727400d0,    &
               -0.1182192842200d0,        -12.7079771140000d0,    &
               30.5102412050000d0,         -1.6417742178000d0,    &
               -2.7223537533000d0,        -14.8792829950000d0,    &
                6.5711156625000d0,          0.3385015931700d0,    &
                3.0677796344000d0,          0.5861221547300d0,    &
                1.4174850243000d0,         -0.8239984189200d0,    &
                0.0262415716543d0,         -0.1701714980500d0,    &
               -0.1161930808100d0,         -2.3010526524000d0,    &
               -0.4659989217400d0,          0.1004614365500d0,    &
                0.4742851926300d0,          0.0839012919421d0,    &
                0.1169117795100d0,         -2.6520880545000d0,    &
               -4.3733909996000d0,          0.7077954541200d0,    &
               -0.5682377587500d0,         -0.1811477194600d0,    &
               12.4315988490000d0,         -0.6185668799600d0,    &
               -1.2534053982000d0,         -0.0201635184846d0,    &
                0.3540300505000d0,          0.0177986603762d0,    &
               21.1679915130000d0,         -5.7604129608000d0,    &
                1.1092684304000d0,         -0.3005314072000d0,    &
                0.1812088000600d0,         -1.1435207545000d0,    &
               -0.1546736951300d0,         -0.6020734577300d0,    &
                4.4146036383000d0,          1.1249792775000d0,    &
                0.0601477926072d0,          1.3518346451000d0,    &
                1.1135213147000d0,        -11.3041371450000d0,    &
                5.0783459898000d0,          0.2585697484200d0,    &
                0.5642578332200d0,          2.8184950586000d0,    &
                0.6231384316800d0,          1.3040663890000d0,    &
                1.5679739944000d0,          1.0026666106000d0,    &
                0.2011564748300d0,          0.7232054462900d0,    &
                0.7307815561200d0,          6.1219774665000d0,    &
                3.5123918869000d0,         -0.6504154979500d0,    &
                2.3839728898000d0,          1.9841942135000d0,    &
               -0.2116028058900d0,          0.0455533877918d0,    &
                0.1106045090500d0,          0.1131921149100d0,    &
               -0.1444528424400d0,         -0.0612065779165d0,    &
               -0.1491225229300d0,          0.0786094900374d0,    &
               -0.0971711445843d0,         -0.1298289428400d0,    &
               -0.2550559905300d0,          0.0084244923294d0,    &
               -0.2213204056200d0,          0.2656044505100d0,    &
               -0.2616510200500d0,          0.1045387318400d0,    &
                0.3101323183300d0,         -0.0051295772010d0,    &
               -0.0116658005937d0,         -0.0126740486192d0,    &
               -0.0118910845844d0,          0.0602894441565d0,    &
               -0.1918303818300d0,         -0.4330259990300d0,    &
               -0.9287935037900d0,          0.1585681774600d0,    &
               -0.8859440204100d0,         -0.1578021760000d0,    &
               -0.3853419835000d0,          0.0375070446497d0,    &
               -0.1067024287500d0,         -0.8240526342100d0,    &
                0.4134208235700d0,          0.0975365126706d0,    &
               -0.0795803169284d0,         -0.2543888900700d0,    &
                0.0164184845530d0,          0.0962626071532d0,    &
                0.0372774652826d0,         -0.2044283519500d0,    &
               -0.2167348847300d0,         -0.0881273712384d0,    &
               -0.1778136605200d0,         -0.2551358440000d0,    &
               -0.2079664828600d0,          0.0244446368858d0,    &
               -0.8142640248800d0,         -0.3887199910200d0,    &
               -0.2508481637100d0,         -0.1588983726900d0,    &
               -0.1390493736300d0,         -0.0967795810865d0,    &
                0.0114740910198d0,          0.1612732421100d0,    &
                0.0731993661319d0,          0.1293010132100d0,    &
                0.0754742116363d0,         -0.4433979408900d0,    &
               -0.0277175684079d0,         -0.1461139002300d0,    &
                0.0245247929568d0,         -0.5539315866800d0,    &
               -0.0862889243104d0,         -0.0040345151848d0,    &
               -0.0166988419500d0,         -0.0061672647163d0,    &
                0.0040884491665d0,          0.0028628966079d0,    &
               -0.0027472591336d0,         -0.0064667591650d0,    &
                0.0014786236571d0,         -0.0083126237343d0,    &
                0.0099425725904d0,         -0.0260198026139d0,    &
                2.8468443017000d0,         -1.4303661751000d0,    &
                0.5929298773300d0,          4.0683802380000d0,    &
                1.9453393217000d0,         -0.1045200667100d0,    &
                0.3826017498400d0,          0.2828815433000d0,    &
                0.2823672829200d0,         -2.4678955015000d0,    &
                1.0751062012000d0,          0.4815970548300d0,    &
               -0.5071136714200d0,          1.5146956444000d0,    &
               -0.0051388050679d0,          0.2197891621100d0,    &
                0.1976520595500d0,         -0.4308439423900d0,    &
               -1.1062078032000d0,         -0.0316082373336d0,    &
                2.4872266674000d0,          1.0268590107000d0,    &
                0.4668351565900d0,          0.7245286820000d0,    &
               -5.0530545728000d0,          5.9522327664000d0,    &
                2.2064814354000d0,         -1.0061976368000d0,    &
                1.6305425753000d0,          0.6453175059300d0,    &
                0.1282089934300d0,          0.9234314444300d0     ]


      coef(2000:m+2*mr-1) = [                                     &
                0.2283803793500d0,          1.1906415431000d0,    &
                1.5954483154000d0,         -2.1465968239000d0,    &
                2.3749604143000d0,          1.4149220459000d0,    &
                2.4658263737000d0,         -3.7441380409000d0,    &
                4.2136850189000d0,          0.1854353270700d0,    &
                0.6396453539900d0,          0.4243154675600d0,    &
               -0.2613857829300d0,         -0.3058907862900d0,    &
                0.2680938048800d0,          0.1039731444700d0,    &
               -0.2431870708600d0,         -0.9492162839900d0,    &
               -0.1643256671300d0,          0.0028381990959d0,    &
                0.0268710862556d0,         -1.8475781982000d0,    &
                1.3944671885000d0,          0.0171018405274d0,    &
               -0.0022428916601d0,          1.3817248320000d0,    &
                0.1537042656400d0,          6.5808496521000d0,    &
                1.3644679809000d0,          0.0126668535437d0,    &
               -5.6008048199000d0,          0.0236959315721d0,    &
                2.3973181958000d0,          3.2522720433000d0,    &
               -0.0375262391222d0,         -0.1530805894100d0,    &
               -0.0148809451288d0,          0.1710512676800d0,    &
               -0.0258855462992d0,          0.2956164874700d0,    &
                0.0567026327808d0,          0.0048775001775d0,    &
               15.0010442880000d0,         -3.2443257047000d0,    &
               -1.3891345854000d0,          0.1463228309200d0,    &
               -0.7247786707500d0,         -1.6418311352000d0,    &
               -2.4244280700000d0,         -5.9473708818000d0,    &
               -2.4461766346000d0,         -2.0316496376000d0,    &
               -4.3950510488000d0,          0.9912856951600d0,    &
                1.1958932050000d0,         -1.4331664358000d0,    &
                0.5501387661700d0,          2.6122294277000d0,    &
                1.1247659839000d0,          0.1340584254800d0,    &
                1.2958985232000d0,         -2.3323703567000d0,    &
               -0.5696208257500d0,         -2.6914235607000d0,    &
               -2.3398783134000d0,          0.3576296268000d0,    &
               -0.3587099312300d0,          0.9158426353900d0,    &
               -0.3053469444600d0,         -0.3735584661500d0,    &
               -1.0906416450000d0,          1.5121008491000d0,    &
               -1.4873302277000d0,         -1.6174685658000d0,    &
                0.5304770204300d0,          0.9017669953600d0,    &
               -2.3671627695000d0,          1.6131867376000d0,    &
               -0.2001849804300d0,         -1.0686667067000d0,    &
               -0.4140167992300d0,          0.0689613471936d0,    &
               -0.7011658051400d0,          1.1701933554000d0,    &
                0.0818602982532d0,          0.0041272725021d0,    &
                0.1232758954400d0,          0.9021406289400d0,    &
               -0.0153398320092d0,          0.0309326406127d0,    &
                0.2104548755300d0,          1.0845637852000d0,    &
                0.4748355442000d0,          0.5562877897100d0,    &
                1.7557047020000d0,         -0.4691625262100d0,    &
                2.4588276375000d0,         -0.1851113438300d0,    &
                3.3217978350000d0,          0.1152360474000d0,    &
                0.4532410146500d0,          2.1668905532000d0,    &
                0.8542097048600d0,         -0.0274449852566d0,    &
                1.1610003099000d0,         -1.9538560154000d0,    &
               -0.4319490759300d0,         -0.0713667109254d0,    &
               -5.7355681145000d0,          0.2453899178800d0,    &
                1.9182935600000d0,         -0.8046823582800d0,    &
               -0.3195218351600d0,          2.8729822928000d0,    &
                1.0635609391000d0,          9.2265313852000d0,    &
                0.7251500085700d0,         -2.7967499362000d0,    &
                1.5206420102000d0,          1.4593614017000d0,    &
               -2.9642804677000d0,          3.2487049433000d0,    &
                0.1229424186200d0,          0.4167365700300d0,    &
                0.0620006068076d0,         -0.1762960266400d0,    &
               -0.2256117906100d0,         -0.3415679219500d0,    &
               -0.2668408887300d0,         -7.9406032686000d0,    &
                0.3540262949300d0,         -0.1455534440400d0,    &
                1.7975699308000d0,          3.8836218205000d0,    &
               -0.6764019762700d0,          2.3016763088000d0,    &
               -0.5444686138500d0,          9.8030725901000d0,    &
               -1.5416606060000d0,         -2.2373172321000d0,    &
               -2.6023941196000d0,          2.2602726645000d0,    &
               -5.7111452522000d0,          4.7410569153000d0,    &
               -0.2482473033900d0,         -1.4029413676000d0,    &
               -0.8589057500500d0,          0.0091771315168d0,    &
               -1.2333202989000d0,          1.6066404971000d0,    &
                0.0478728232987d0,          0.1541190582400d0,    &
               -0.0588862181175d0,         -0.0108121883177d0,    &
               -0.0132894733562d0,          0.0619638498480d0,    &
                0.0231719565950d0,         -0.0660801250822d0,    &
               -0.0461694467795d0,         -0.1516865386600d0,    &
                0.0479104224690d0,          0.0362415211105d0,    &
               -0.0724255820419d0,          0.0207335956633d0,    &
               -0.1844482042200d0,         -0.0956695207522d0,    &
                0.0140608433356d0,          0.0673301170893d0,    &
                0.0257971805930d0,         -0.0273836711324d0,    &
                0.0391764562038d0,         -0.1278186792500d0,    &
               -0.0072793750300d0,          0.6273646940100d0,    &
                0.1332137934800d0,         -0.1664107312500d0,    &
               -0.6208379000000d0,          0.3211611067100d0,    &
               -0.0090707842476d0,         -1.1288983317000d0,    &
               -0.6187650780900d0,         -2.3276110174000d0,    &
               -0.6434187451800d0,          0.4534205849900d0,    &
               -0.4818887040000d0,          0.3482176865200d0,    &
               -1.0795323815000d0,         -1.0240189329000d0,    &
                0.0371773136738d0,          0.1166515934800d0,    &
                0.0542991993670d0,         -0.0467953765346d0,    &
                0.0385810178128d0,         -0.2928129123800d0,    &
                0.0181555204426d0,          0.0335853602180d0,    &
                0.0653384186762d0,         -0.0463644522671d0,    &
               -0.1428821742600d0,          0.0729217310861d0,    &
                0.0250618969447d0,         -0.2042260650400d0,    &
               -0.0842372588342d0,         -0.2812004156500d0,    &
                0.0550804158046d0,         -0.0887081607644d0,    &
               -0.1429490808000d0,         -0.0593167699282d0,    &
               -0.0651479456220d0,          0.0256186148088d0,    &
                0.0220819488970d0,          0.1165563109000d0,    &
                0.0402023760380d0,         -0.0433503883276d0,    &
                0.0708186774452d0,         -0.1811937043800d0,    &
               -0.0261431662752d0,          0.1537908687000d0,    &
                0.1944534776300d0,         -0.0768373166092d0,    &
               -0.1552986522400d0,         -0.0063007557535d0,    &
               -0.0783724660876d0,         -0.2905414460900d0,    &
               -0.0753874846174d0,         -0.4225982140200d0,    &
               -0.2270998501700d0,          0.3363619888300d0,    &
               -0.3323888667200d0,          0.3049281389000d0,    &
               -0.0078657322122d0,         -0.1876481442800d0,    &
                0.0182802553162d0,          0.0846331217835d0,    &
                0.0429462059457d0,         -0.0238196214711d0,    &
                0.0531865033782d0,         -0.1256435994600d0,    &
               -0.0250036359396d0,       -513.8788867200000d0,    &
                9.4315319897000d0,        428.5021273100000d0,    &
                0.7593464139000d0,        -90.6462598880000d0,    &
               -0.3462912945300d0,       -160.0955381300000d0,    &
               -0.1027751393300d0,         62.4418040340000d0,    &
               -0.1448721404700d0,        -10.5847198480000d0,    &
               -0.0114738583785d0,         30.1666281900000d0,    &
                0.1231037802500d0,         34.3728929160000d0,    &
               -1.0778014170000d0,          6.2956940751000d0,    &
                0.2286053461200d0,          5.4243522820000d0,    &
                0.4580366116900d0,         41.7746663480000d0,    &
                0.2080889072300d0,        -32.7528565350000d0,    &
                0.1087930509700d0,         10.8330620320000d0,    &
                0.1349483211700d0,        -24.0447739210000d0,    &
               -0.3091090178100d0,         35.9370954760000d0,    &
               -0.6179304112300d0,         -0.9662878893300d0,    &
               -0.0692860535601d0,         -2.8838638059000d0,    &
               -0.4042108437300d0,          1.5261533017000d0,    &
               -0.0152269006284d0,          6.9464900376000d0,    &
               -0.0287945699967d0,        -22.9251697250000d0,    &
               -0.3280697152500d0,         -3.7484303164000d0,    &
                0.0325215954268d0,        -10.5137169230000d0,    &
                0.0046696637309d0,          1.7898976326000d0,    &
                0.0344425589293d0,         -9.8669401681000d0,    &
                0.6012008353900d0,          7.7045100329000d0,    &
                0.3870680530200d0,         -7.1733828091000d0,    &
               -0.1205977417400d0,         -2.8732894379000d0,    &
                1.3755548818000d0,         37.8423497400000d0,    &
                0.0157386939490d0,          1.8576251235000d0,    &
               -0.1061679293600d0,         14.8951710950000d0,    &
               -0.1532317492000d0,          2.5498378182000d0,    &
               -0.0334507800637d0,          6.4759928876000d0,    &
                0.0467327564761d0                            ] 

!-----------------------------------------------------------------
! The following lines are the code that evaluates my fitted function.
! The xnuc are the nuclear coordinates, as found in your table.
! Routine getd0 evaluates my internal coordinates d0(0:5,0:5), which are
! functions of the interparticle distances.  I've defined d0 both above
! and below the diagonal, but it is symmetric (d0(i,j)=d0(j,i)), and d0
! vanishes on the diagonal.  Then routine vec evaluates the (m) basis
! functions at the configuration described by d0, and finally f0 is the
! fitted potential.  The basis functions are polynomials of the entries
! in d0, obeying all the expected symmetries.
!-----------------------------------------------------------------

      call getd0 (xn, dc0, dc1, dw0, dw1, d0, r0)
      call getvec (m, d0, vec(0:m-1))
      call getrvec (3, r0, rvec)
      do l = 0, mr-1
         do k = 0, 1
            vec(m+2*l+k) = rvec(k+1)*vec(l)
         enddo
      enddo

      f0 = dot_product(coef,vec)

!.... Now the code is serving for PE calculation, gradient
!...  is ignored for decreasing computational cost
!       do i = 0, 2
!          do j = 0, 5
!             xn1 = xn ; xn1(i,j) = xn1(i,j)-dd
!             call getd0 (xn1, dc0, dc1, dw0, dw1, d0, r0)
!             call getvec (m, d0, vec(0:m-1))
!             call getrvec (3, r0, rvec)
!             do l = 0, mr-1
!                do k = 0, 1
!                vec(m+2*l+k) = rvec(k+1)*vec(l)
!             enddo
!          enddo
!           t0 = dot_product(coef,vec)
!           xn1 = xn ; xn1(i,j) = xn1(i,j)+dd
!           call getd0 (xn1, dc0, dc1, dw0, dw1, d0, r0)
!           call getvec (m, d0, vec(0:m-1))
!           call getrvec (3, r0, rvec)
!          do l = 0, mr-1
!             do k = 0, 1
!                vec(m+2*l+k) = rvec(k+1)*vec(l)
!             enddo
!          enddo
!           t1 = dot_product(coef,vec)
!
! Note that gf0 will be the negative gradient
!
!           gf0(i,j) = -(t1-t0)/(2*dd)
!         enddo
!       enddo

  END SUBROUTINE getfit

  !****************************************************************************

  SUBROUTINE getd0 (xn, dc0, dc1, dw0, dw1, d0, r0)
!********************************************************************
!
!********************************************************************

      double precision, dimension(0:2,0:5) :: xn
      double precision :: dc0, dc1, dw0, dw1
      double precision, dimension(0:5,0:5) :: d0, r0
      double precision, parameter :: p = 2.0d0
      integer :: i, j
      double precision :: t0

    !--------------------------------------------------------------------------

! Note: CH5+.  Nuclei 0..5; i=0 for the C, i in 1..5 for the H.

      d0(0,0) = 0.d0
      r0(0,0) = 0.d0

      do j = 1, 5
         t0 = sqrt((xn(0,j)-xn(0,0))**2+(xn(1,j)-xn(1,0))**2+ & 
              (xn(2,j)-xn(2,0))**2)
!         d0(0,j) = (log(t0)-dc0)/dw0
!         d0(0,j) = (dexp(-t0/3.d0)-dc0)/dw0
         d0(0,j) = (dexp(-t0/p)-dc0)/dw0
         d0(j,0) = d0(0,j)
         r0(0,j) = t0
         r0(j,0) = t0
      end do

      do i = 1, 5
         d0(i,i) = 0.d0
         r0(i,i) = 0.d0
         do j = i+1, 5
            t0 = sqrt((xn(0,j)-xn(0,i))**2+(xn(1,j)-xn(1,i))**2+ &
                 (xn(2,j)-xn(2,i))**2)
!           d0(i,j) = (log(t0)-dc1)/dw1
!           d0(i,j) = (dexp(-t0/p)-dc1)/dw1
           d0(i,j) = (dexp(-t0/p)-dc1)/dw1
            d0(j,i) = d0(i,j)
            r0(i,j) = t0
            r0(j,i) = t0
         end do
      end do
  END SUBROUTINE getd0

  !****************************************************************************

  SUBROUTINE getvec (m, d, vec)

      integer :: m

      double precision, dimension(0:5,0:5) :: d
      double precision, dimension(0:m-1) :: vec

      integer :: j0, j1, j2, j3, j4, j5, j, k, k0
      double precision,dimension(0:1) :: x
      double precision,dimension(0:3) :: y
      double precision,dimension(0:9) :: z
      double precision,dimension(0:21) :: u
      double precision,dimension(0:41) :: v
      double precision,dimension(0:59) :: w
      double precision,dimension(0:5,0:5) :: d2,d3,d4,d5
      double precision :: t0
      double precision :: her2, her3, her4, her5
!      double precision :: her6, her7

    !--------------------------------------------------------------------------

      her2(t0) = (4*t0**2-2)/sqrt(dble(8*2))
      her3(t0) = (8*t0**2-12)*t0/sqrt(dble(16*6))
      her4(t0) = ((16*t0**2-48)*t0**2+12)/sqrt(dble(32*24))
      her5(t0) = ((32*t0**2-160)*t0**2+120)*t0/sqrt(dble(64*120))
!      her6(t0) = (((64*t0**2-480)*t0**2+720)*t0**2-120)/sqrt(dble(128*720))
!      her7(t0) = (((128*t0**2-1344)*t0**2+3360)*t0**2-1680)*t0/sqrt(dble(256*5040))
!     ------------------------------------------------------------------
!     Test for compatibility
      if (.not.(m.eq.1.or.m.eq.3.or.m.eq.10.or.m.eq.32.or. &
          m.eq.101.or.m.eq.299.or.m.eq.849.or.m.eq.2239)) then
       stop 'getvec - wrong dimension'
      endif

!     Computation
      x = 0 ; y = 0 ; z = 0 ; u = 0 ; v = 0 ; w = 0
      do j0 = 0, 5
       do j1 = j0+1, 5
        d2(j0,j1) = her2(d(j0,j1))
        d2(j1,j0) = d2(j0,j1)
        d3(j0,j1) = her3(d(j0,j1))
        d3(j1,j0) = d3(j0,j1)
        d4(j0,j1) = her4(d(j0,j1))
        d4(j1,j0) = d4(j0,j1)
        d5(j0,j1) = her5(d(j0,j1))
        d5(j1,j0) = d5(j0,j1)
       enddo
      enddo

      do j1 = 1, 5
       x(0) = x(0)+d(0,j1)/5
       y(0) = y(0)+d2(0,j1)/5
       z(0) = z(0)+d3(0,j1)/5
       u(0) = u(0)+d4(0,j1)/5
       v(0) = v(0)+d5(0,j1)/5
!!$       w() = w()+her6(d(0,j1))/5
       do j2 = 1, 5
       if (j2.ne.j1) then
        y(1) = y(1)+d(0,j1)*d(j1,j2)/20
        z(1) = z(1)+d2(0,j1)*d(j1,j2)/20
        z(2) = z(2)+d(0,j1)*d2(j1,j2)/20
        z(3) = z(3)+d(0,j1)*d(0,j2)*d(j1,j2)/20
        u(1) = u(1)+d3(0,j1)*d(j1,j2)/20
        u(2) = u(2)+d2(0,j1)*d2(j1,j2)/20
        u(3) = u(3)+d(0,j1)*d3(j1,j2)/20
        u(4) = u(4)+d2(0,j1)*d(0,j2)*d(j1,j2)/20
        u(5) = u(5)+d(0,j1)*d(0,j2)*d2(j1,j2)/20
        v(1) = v(1)+d4(0,j1)*d(j1,j2)/20
        v(2) = v(2)+d3(0,j1)*d2(j1,j2)/20
        v(3) = v(3)+d2(0,j1)*d3(j1,j2)/20
        v(4) = v(4)+d(0,j1)*d4(j1,j2)/20
        v(5) = v(5)+d3(0,j1)*d(0,j2)*d(j1,j2)/20
        v(6) = v(6)+d2(0,j1)*d(0,j2)*d2(j1,j2)/20
        v(7) = v(7)+d(0,j1)*d(0,j2)*d3(j1,j2)/20
        w(0) = w(0)+d4(0,j1)*d2(j1,j2)/20
        w(1) = w(1)+d3(0,j1)*d3(j1,j2)/20
        w(2) = w(2)+d2(0,j1)*d4(j1,j2)/20
        w(3) = w(3)+d(0,j1)*d5(j1,j2)/20
        w(4) = w(4)+d3(0,j1)*d2(0,j2)*d(j1,j2)/20
        w(5) = w(5)+d3(0,j1)*d(0,j2)*d2(j1,j2)/20
        w(6) = w(6)+d2(0,j1)*d(0,j2)*d3(j1,j2)/20
        w(7) = w(7)+d(0,j1)*d(0,j2)*d4(j1,j2)/20
!!$        w() = w()+d5(0,j1)*d(j1,j2)/20
!!$        w() = w()+d4(0,j1)*d(0,j2)*d(j1,j2)/20
!!$        w() = w()+d2(0,j1)*d2(0,j2)*d2(j1,j2)/20
        do j3 = 1, 5
        if (j3.ne.j2.and.j3.ne.j1) then
         z(4) = z(4)+d(0,j1)*d(j1,j2)*d(j2,j3)/60
         z(5) = z(5)+d(0,j1)*d(j1,j2)*d(j1,j3)/60
         u(6) = u(6)+d(0,j1)*d2(j1,j2)*d(j2,j3)/60
         u(7) = u(7)+d(0,j1)*d(j1,j2)*d2(j2,j3)/60
         u(8) = u(8)+d(0,j1)*d2(j1,j2)*d(j1,j3)/60
         u(9) = u(9)+d(0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)/60
         u(10) = u(10)+d2(0,j1)*d(j1,j2)*d(j2,j3)/60
         u(11) = u(11)+d2(0,j1)*d(j1,j2)*d(j1,j3)/60
         u(12) = u(12)+d(0,j1)*d(0,j2)*d(j1,j3)*d(j2,j3)/60
         v(8) = v(8)+d3(0,j1)*d(j1,j2)*d(j2,j3)/60
         v(9) = v(9)+d2(0,j1)*d2(j1,j2)*d(j2,j3)/60
         v(10) = v(10)+d2(0,j1)*d(j1,j2)*d2(j2,j3)/60
         v(11) = v(11)+d(0,j1)*d3(j1,j2)*d(j2,j3)/60
         v(12) = v(12)+d(0,j1)*d2(j1,j2)*d2(j2,j3)/60
         v(13) = v(13)+d(0,j1)*d(j1,j2)*d3(j2,j3)/60
         v(14) = v(14)+d3(0,j1)*d(j1,j2)*d(j1,j3)/60
         v(15) = v(15)+d2(0,j1)*d2(j1,j2)*d(j1,j3)/60
         v(16) = v(16)+d(0,j1)*d3(j1,j2)*d(j1,j3)/60
         v(17) = v(17)+d(0,j1)*d2(j1,j2)*d2(j1,j3)/60
         v(18) = v(18)+d2(0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)/60
         v(19) = v(19)+d(0,j1)*d2(j1,j2)*d(j1,j3)*d(j2,j3)/60
         v(20) = v(20)+d(0,j1)*d(j1,j2)*d(j1,j3)*d2(j2,j3)/60
         v(21) = v(21)+d2(0,j1)*d(0,j2)*d(j1,j3)*d(j2,j3)/60
         v(22) = v(22)+d(0,j1)*d(0,j2)*d2(j1,j3)*d(j2,j3)/60
         v(23) = v(23)+d(0,j1)*d(0,j2)*d(j1,j3)*d(j2,j3)*d(j1,j2)/60
         v(24) = v(24)+d(0,j1)*d(0,j2)*d(j1,j2)*d2(j1,j3)/60
!!$         w() = w()+d4(0,j1)*d(j1,j2)*d(j2,j3)/60
         w(8) = w(8)+d(0,j1)*d(j1,j2)*d4(j2,j3)/60
         w(9) = w(9)+d(0,j1)*d4(j1,j2)*d(j2,j3)/60
         w(10) = w(10)+d3(0,j1)*d2(j1,j2)*d(j2,j3)/60
         w(11) = w(11)+d(0,j1)*d2(j1,j2)*d3(j2,j3)/60
         w(12) = w(12)+d2(0,j1)*d3(j1,j2)*d(j2,j3)/60
         w(13) = w(13)+d(0,j1)*d3(j1,j2)*d2(j2,j3)/60
         w(14) = w(14)+d3(0,j1)*d(j1,j2)*d2(j2,j3)/60
         w(15) = w(15)+d2(0,j1)*d(j1,j2)*d3(j2,j3)/60
         w(16) = w(16)+d2(0,j1)*d2(j1,j2)*d2(j2,j3)/60
!!$         w() = w()+d4(0,j1)*d(j1,j2)*d(j1,j3)/60
         w(17) = w(17)+d(0,j1)*d4(j1,j2)*d(j1,j3)/60
         w(18) = w(18)+d3(0,j1)*d2(j1,j2)*d(j1,j3)/60
         w(19) = w(19)+d2(0,j1)*d3(j1,j2)*d(j1,j3)/60
         w(20) = w(20)+d(0,j1)*d3(j1,j2)*d2(j1,j3)/60
         w(21) = w(21)+d2(0,j1)*d2(j1,j2)*d2(j1,j3)/60
         w(22) = w(22)+d3(0,j1)*d(0,j2)*d(j1,j3)*d(j2,j3)/60
         w(23) = w(23)+d(0,j1)*d(0,j2)*d3(j1,j3)*d(j2,j3)/60
         w(44) = w(44)+d2(0,j1)*d2(0,j2)*d(j1,j3)*d(j2,j3)/60
         w(24) = w(24)+d2(0,j1)*d(0,j2)*d2(j1,j3)*d(j2,j3)/60
         w(25) = w(25)+d(0,j1)*d(0,j2)*d2(j1,j3)*d2(j2,j3)/60
         w(26) = w(26)+d2(0,j1)*d(0,j2)*d(j1,j3)*d2(j2,j3)/60
         w(45) = w(45)+d3(0,j1)*d(0,j2)*d(j1,j2)*d(j2,j3)/60
         w(27) = w(27)+d(0,j1)*d(j1,j2)*d(j1,j3)*d3(j2,j3)/60
         w(28) = w(28)+d(0,j1)*d(0,j2)*d3(j1,j2)*d(j2,j3)/60
         w(46) = w(46)+d3(0,j1)*d(0,j2)*d(j1,j2)*d(j1,j3)/60
!!$         w() = w()+d3(0,j1)*d(0,j2)*d(0,j3)*d(j1,j2)/60
         w(29) = w(29)+d(0,j1)*d3(j1,j2)*d(j1,j3)*d(j2,j3)/60
!!$         w() = w()+d(0,j1)*d(0,j2)*d(j1,j2)*d3(j2,j3)/60
         w(30) = w(30)+d3(0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)/60
         w(31) = w(31)+d2(0,j1)*d(0,j2)*d2(j1,j2)*d(j2,j3)/60
!!$         w() = w()+d2(0,j1)*d2(0,j2)*d(j1,j2)*d(j2,j3)/60
!!$         w() = w()+d2(0,j1)*d(0,j2)*d(0,j3)*d2(j1,j2)/60
         w(32) = w(32)+d(0,j1)*d2(j1,j2)*d(j1,j3)*d2(j2,j3)/60
!!$         w() = w()+d2(0,j1)*d(0,j2)*d(j1,j2)*d2(j2,j3)/60
         w(33) = w(33)+d2(0,j1)*d(j1,j2)*d(j1,j3)*d2(j2,j3)/60
!!$         w() = w()+d2(0,j1)*d(0,j2)*d2(j1,j2)*d(j1,j3)/60
!!$         w() = w()+d2(0,j1)*d2(0,j2)*d(0,j3)*d(j1,j2)/60
         w(34) = w(34)+d(0,j1)*d2(j1,j2)*d2(j1,j3)*d(j2,j3)/60
!!$         w() = w()+d(0,j1)*d(0,j2)*d2(j1,j2)*d2(j2,j3)/60
!!$         w() = w()+d(0,j1)*d2(0,j2)*d(j1,j2)*d2(j2,j3)/60
         w(35) = w(35)+d2(0,j1)*d(j1,j2)*d2(j1,j3)*d(j2,j3)/60
         w(36) = w(36)+d2(0,j1)*d(0,j2)*d(0,j3)*d(j1,j2)*d(j2,j3)/60
!!$         w() = w()+d2(0,j1)*d(0,j2)*d(j1,j2)*d(j1,j3)*d(j2,j3)/60
!!$         w() = w()+d(0,j1)*d(0,j2)*d(0,j3)*d2(j1,j2)*d(j2,j3)/60
         w(37) = w(37)+d(0,j1)*d(0,j2)*d(j1,j2)*d2(j1,j3)*d(j2,j3)/60
         w(38) = w(38)+d2(0,j1)*d(0,j2)*d(0,j3)*d(j1,j2)*d(j1,j3)/60
         w(39) = w(39)+d(0,j1)*d(0,j2)*d2(j1,j2)*d(j1,j3)*d(j2,j3)/60
!!$       w() = w()+d(0,j1)*d(0,j2)*d(0,j3)*d(j1,j2)*d(j1,j3)*d(j2,j3)/60
         do j4 = 1, 5
         if (j4.ne.j3.and.j4.ne.j2.and.j4.ne.j1) then
          u(13) = u(13)+d(0,j1)*d(j1,j2)*d(j1,j3)*d(j3,j4)/120
          u(14) = u(14)+d(0,j1)*d(j1,j2)*d(j1,j3)*d(j1,j4)/120
          v(25) = v(25)+d(0,j1)*d2(j1,j2)*d(j2,j3)*d(j3,j4)/120
          v(26) = v(26)+d2(0,j1)*d(j1,j2)*d(j1,j3)*d(j3,j4)/120
          v(27) = v(27)+d(0,j1)*d2(j1,j2)*d(j1,j3)*d(j3,j4)/120
          v(28) = v(28)+d(0,j1)*d(j1,j2)*d2(j1,j3)*d(j3,j4)/120
          v(29) = v(29)+d(0,j1)*d(j1,j2)*d(j1,j3)*d2(j3,j4)/120
          v(30) = v(30)+d2(0,j1)*d(j1,j2)*d(j1,j3)*d(j1,j4)/120
          v(31) = v(31)+d(0,j1)*d2(j1,j2)*d(j1,j3)*d(j1,j4)/120
          w(40) = w(40)+d(0,j1)*d3(j1,j2)*d(j1,j3)*d(j1,j4)/120
          w(41) = w(41)+d2(0,j1)*d(j1,j2)*d2(j2,j3)*d(j3,j4)/120
          w(42) = w(42)+d(0,j1)*d(j1,j2)*d2(j2,j3)*d2(j3,j4)/120
          w(43) = w(43)+d(0,j1)*d2(j1,j2)*d2(j1,j3)*d(j1,j4)/120
          w(44) = w(44)+d(0,j1)*d(j1,j2)*d(j2,j3)*d3(j3,j4)/120
          w(45) = w(45)+d(0,j1)*d2(j1,j2)*d(j2,j3)*d2(j3,j4)/120
          w(46) = w(46)+d(0,j1)*d(j1,j2)*d(j1,j3)*d2(j2,j3)*d(j1,j4)/120
!!$          w() = w()+d3(0,j1)*d(j1,j2)*d(j2,j3)*d(j3,j4)/120
!!$          w() = w()+d2(0,j1)*d2(j1,j2)*d(j2,j3)*d(j3,j4)/120
!!$          w() = w()+d3(0,j1)*d(j1,j2)*d(j1,j3)*d(j1,j4)/120
!!$          w() = w()+d2(0,j1)*d2(j1,j2)*d(j1,j3)*d(j1,j4)/120
!!$          w() = w()+d(0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)*d2(j1,j4)/120
!!$          w() = w()+d2(0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)*d(j1,j4)/120
!!$          w() = w()+d(0,j1)*d2(j1,j2)*d(j1,j3)*d(j2,j3)*d(j1,j4)/120
         endif
         enddo
        endif
        enddo
       endif
       enddo
      enddo
      do j0 = 1, 5
       do j1 = 1, 5
       if (j1.ne.j0) then
        x(1) = x(1)+d(j0,j1)/4
        y(2) = y(2)+d2(j0,j1)/4
        z(6) = z(6)+d3(j0,j1)/4
        u(15) = u(15)+d4(j0,j1)/4
        v(32) = v(32)+d5(j0,j1)/4
!!$        w() = w()+her6(d(j0,j1))/4
        do j2 = 1, 5
        if (j2.ne.j1.and.j2.ne.j0) then
         y(3) = y(3)+d(j0,j1)*d(j1,j2)/12
         z(7) = z(7)+d2(j0,j1)*d(j1,j2)/12
         z(8) = z(8)+d(j0,j1)*d(j0,j2)*d(j1,j2)/12
         u(16) = u(16)+d3(j0,j1)*d(j1,j2)/12
         u(17) = u(17)+d2(j0,j1)*d2(j1,j2)/12
         u(18) = u(18)+d2(j0,j1)*d(j0,j2)*d(j1,j2)/12
         v(33) = v(33)+d4(j0,j1)*d(j1,j2)/12
         v(34) = v(34)+d3(j0,j1)*d2(j1,j2)/12
         v(35) = v(35)+d3(j0,j1)*d(j0,j2)*d(j1,j2)/12
         v(36) = v(36)+d2(j0,j1)*d2(j0,j2)*d(j1,j2)/12
         w(47) = w(47)+d4(j0,j1)*d2(j1,j2)/12
         w(48) = w(48)+d3(j0,j1)*d3(j1,j2)/12
         w(49) = w(49)+d2(j0,j1)*d2(j0,j2)*d2(j1,j2)/12
         w(50) = w(50)+d4(j0,j1)*d(j0,j2)*d(j1,j2)/12
!!$         w() = w()+d3(j0,j1)*d2(j0,j2)*d(j1,j2)/12
!!$         w() = w()+d5(j0,j1)*d(j1,j2)/12
         do j3 = 1, 5
         if (j3.ne.j2.and.j3.ne.j1.and.j3.ne.j0) then
          z(9) = z(9)+d(j0,j1)*d(j0,j2)*d(j0,j3)/24
          u(19) = u(19)+d2(j0,j1)*d(j0,j2)*d(j0,j3)/24
          u(20) = u(20)+d(j0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)/24
          u(21) = u(21)+d(j0,j1)*d(j0,j2)*d(j1,j3)*d(j2,j3)/24
          v(37) = v(37)+d3(j0,j1)*d(j0,j2)*d(j0,j3)/24
          v(38) = v(38)+d2(j0,j1)*d2(j0,j2)*d(j0,j3)/24
          v(39) = v(39)+d2(j0,j1)*d(j1,j2)*d(j1,j3)*d(j2,j3)/24
          v(40) = v(40)+d(j0,j1)*d2(j1,j2)*d(j1,j3)*d(j2,j3)/24
          v(41) = v(41)+d(j0,j1)*d(j1,j2)*d(j1,j3)*d2(j2,j3)/24
          w(51) = w(51)+d4(j0,j1)*d(j1,j2)*d(j2,j3)/24
          w(52) = w(52)+d(j0,j1)*d4(j1,j2)*d(j2,j3)/24
          w(53) = w(53)+d3(j0,j1)*d2(j1,j2)*d(j2,j3)/24
          w(54) = w(54)+d3(j0,j1)*d(j1,j2)*d2(j2,j3)/24
          w(55) = w(55)+d2(j0,j1)*d3(j1,j2)*d(j2,j3)/24
          w(56) = w(56)+d2(j0,j1)*d2(j1,j2)*d2(j2,j3)/24
          w(57) = w(57)+d4(j0,j1)*d(j0,j2)*d(j0,j3)/24
          w(58) = w(58)+d2(j0,j1)*d2(j0,j2)*d2(j0,j3)/24
          w(59) = w(59)+d3(j0,j1)*d2(j0,j2)*d(j0,j3)/24
         endif
         enddo
        endif
        enddo
       endif
       enddo
      enddo
      vec(0) = 1
      if (3.le.m) then
       vec(1) = x(0)
       vec(2) = x(1)
      endif
      if (10.le.m) then
       vec(3) = her2(x(0))
       vec(4) = x(0)*x(1)
       vec(5) = her2(x(1))
       do k = 0, 3
        vec(6+k) = y(k)
       enddo
      endif
!! third order terms
      if (32.le.m) then
       k0 = 10
       vec(k0) = her3(x(0))
       vec(k0+1) = her2(x(0))*x(1)
       vec(k0+2) = x(0)*her2(x(1))
       vec(k0+3) = x(0)*y(0)
       vec(k0+4) = x(0)*y(1)
       vec(k0+5) = x(0)*y(2)
       vec(k0+6) = x(0)*y(3)
       vec(k0+7) = her3(x(1))
       vec(k0+8) = x(1)*y(0)
       vec(k0+9) = x(1)*y(1)
       vec(k0+10) = x(1)*y(2)
       vec(k0+11) = x(1)*y(3)
       k0 = 22
       do k = 0, 9
        vec(k0+k) = z(k)
       enddo
      endif
!! fourth order terms
      if (101.le.m) then
       k0 = 32
       vec(k0) = her4(x(0))
       vec(k0+1) = her3(x(0))*x(1)
       vec(k0+2) = her2(x(0))*her2(x(1))
       do k = 0, 3
        vec(k0+3+k) = her2(x(0))*y(k)
       enddo
       vec(k0+7) = x(0)*her3(x(1))
       do k = 0, 3
        vec(k0+8+k) = x(0)*x(1)*y(k)
       enddo
       do k = 0, 9
        vec(k0+12+k) = x(0)*z(k)
       enddo
       vec(k0+22) = her4(x(1))
       do k = 0, 3
        vec(k0+23+k) = her2(x(1))*y(k)
       enddo
       do k = 0, 9
        vec(k0+27+k) = x(1)*z(k)
       enddo
       k0 = k0+37
       do j = 0, 3
        do k = j, 3
         vec(k0) = y(j)*y(k)
         k0 = k0+1
        enddo
       enddo
       if (k0.ne.79) then
        stop 'getvec: counting error'
       endif
       do k = 0, 21
        vec(k0+k) = u(k)
       enddo
      endif
!! fifth order terms
      if (299.le.m) then
       k0 = 101
       vec(k0) = her5(x(0))
       vec(k0+1) = her4(x(0))*x(1)
       vec(k0+2) = her3(x(0))*her2(x(1))
       do k = 0, 3
        vec(k0+3+k) = her3(x(0))*y(k)
       enddo
       vec(k0+7) = her2(x(0))*her3(x(1))
       do k = 0, 3
        vec(k0+8+k) = her2(x(0))*x(1)*y(k)
       enddo
       do k = 0, 9
        vec(k0+12+k) = her2(x(0))*z(k)
       enddo
       vec(k0+22) = x(0)*her4(x(1))
       do k = 0, 3
        vec(k0+23+k) = x(0)*her2(x(1))*y(k)
       enddo
       do k = 0, 9
        vec(k0+27+k) = x(0)*x(1)*z(k)
       enddo
       k0 = k0+37
       do j = 0, 3
        do k = j, 3
         vec(k0) = x(0)*y(j)*y(k)
         k0 = k0+1
        enddo
       enddo
       if (k0.ne.148) then
        stop 'getvec: counting error'
       endif
       do k = 0, 21
        vec(k0+k) = x(0)*u(k)
       enddo
       vec(k0+22) = her5(x(1))
       do k = 0, 3
        vec(k0+23+k) = her3(x(1))*y(k)
       enddo
       do k = 0, 9
        vec(k0+27+k) = her2(x(1))*z(k)
       enddo
       k0 = k0+37
       do j = 0, 3
        do k = j, 3
         vec(k0) = x(1)*y(j)*y(k)
         k0 = k0+1
        enddo
       enddo
       if (k0.ne.195) then
        stop 'getvec: counting error'
       endif
       do k = 0, 21
        vec(k0+k) = x(1)*u(k)
       enddo
       do j = 0, 3
        do k = 0, 9
         vec(k0+22+10*j+k) = y(j)*z(k)
        enddo
       enddo
       k0 = 257
       do k = 0, 41
        vec(k0+k) = v(k)
       enddo
      endif
!! sixth order terms
      if (849.le.m) then
       k0 = 299
       do k = 0, 197
        vec(k0+k) = x(0)*vec(101+k)
       enddo
       do k = 0, 128
        vec(k0+198+k) = x(1)*vec(170+k)
       enddo
       do k = 0, 31
        vec(k0+327+k) = y(0)*vec(69+k)
       enddo
       do k = 0, 27
        vec(k0+359+k) = y(1)*vec(73+k)
       enddo
       do k = 0, 24
        vec(k0+387+k) = y(2)*vec(76+k)
       enddo
       do k = 0, 22
        vec(k0+412+k) = y(3)*vec(78+k)
       enddo
       k0 = k0+435
       do j = 0, 9
        do k = j, 9
         vec(k0) = z(j)*z(k)
         k0 = k0+1
        enddo
       enddo
       if (k0.ne.789) then
        stop 'getvec: counting error'
       endif
       do k = 0, 59
        vec(k0+k) = w(k)
       enddo
      endif
!! seventh order terms (incomplete)
      if (2239.le.m) then
       k0 = 849
       do k = 0, 549 ! 849-299-1
        vec(k0+k) = x(0)*vec(299+k)
       enddo
       do k = 0, 351 ! 849-497-1
        vec(k0+550+k) = x(1)*vec(497+k)
       enddo
       do k = 0, 81 ! 299-217-1
        vec(k0+902+k) = y(0)*vec(217+k)
       enddo
       do k = 0, 71
        vec(k0+984+k) = y(1)*vec(227+k)
       enddo
       do k = 0, 61
        vec(k0+1056+k) = y(2)*vec(237+k)
       enddo
       do k = 0, 51
        vec(k0+1118+k) = y(3)*vec(247+k)
       enddo
       k0 = k0+1170
       do j = 0, 9
        do k = 0, 21
         vec(k0+22*j+k) = z(j)*u(k)
        enddo
       enddo
      endif
      return

  END SUBROUTINE getvec

  !****************************************************************************

  SUBROUTINE getrvec (m, r, vec)

      integer :: m

      double precision, dimension(0:5,0:5) :: r, r1
      double precision, dimension(0:m-1) :: vec
      integer :: j0, j1, j2, i, j, k
      double precision, dimension(0:1) :: x
      double precision, dimension(0:3) :: y

    !--------------------------------------------------------------------------


!     ------------------------------------------------------------------
!     Test for compatibility
      if (.not.(m.eq.1.or.m.eq.3.or.m.eq.10)) then
       stop 'getrvec - wrong dimension'
      endif
!     Computation
      x = 0 ; y = 0
      do i = 0, 5
       do j = 0, 5
        if (i.eq.j) then
         r1(i,j) = 0
        else
         r1(i,j) = exp(-r(i,j))/r(i,j)
        endif
       enddo
      enddo
      do j1 = 1, 5
       x(0) = x(0)+r1(0,j1)/5
       y(0) = y(0)+r1(0,j1)**2/5
       do j2 = 1, 5
       if (j2.ne.j1) then
        y(1) = y(1)+r1(0,j1)*r1(j1,j2)/20
       endif
       enddo
      enddo
      do j0 = 1, 5
       do j1 = 1, 5
       if (j1.ne.j0) then
        x(1) = x(1)+r1(j0,j1)/4
        y(2) = y(2)+r1(j0,j1)**2/4
        do j2 = 1, 5
        if (j2.ne.j1.and.j2.ne.j0) then
         y(3) = y(3)+r1(j0,j1)*r1(j1,j2)/12
        endif
        enddo
       endif
       enddo
      enddo
      vec(0) = 1
      if (3.le.m) then
       vec(1) = x(0)
       vec(2) = x(1)
      endif
      if (10.le.m) then
       vec(3) = x(0)**2
       vec(4) = x(0)*x(1)
       vec(5) = x(1)**2
       do k = 0, 3
        vec(6+k) = y(k)
       enddo
      endif

  END SUBROUTINE getrvec

  !****************************************************************************

  SUBROUTINE getlr(r,rh2,theta,elr,error_code)
!******************************************************************************
!.... Subroutine to obtain the long-range interaction
!
!.... 02/14/05   Zhong      SUBROUTINE created
!.... 02/22/05   Zhong      Modified for the long range effect
!******************************************************************************

      double precision :: r, rh2, theta
      double precision :: alpha, alpha0, alpha90
      double precision :: u3, u4, p2, q
      double precision :: epot, elr
      double precision :: pi
      INTEGER, INTENT( OUT ) :: error_code

    !--------------------------------------------------------------------------

      pi = 4.d0 * datan(1.d0)
      call cspline(rh2,alpha0,alpha90,q,error_code) 

!      write(*,'a4,1x,f9.5')'rh2=',rh2
      theta = theta / 180.d0 * pi
      alpha = 1.d0/3.d0*(alpha0 + 2*alpha90)
      p2 = 0.5d0*(3*(cos(theta))**2 - 1.d0)
      u3 = q*p2/r**3
      u4 = -(0.5d0*alpha + 1.d0/3.d0*(alpha0 - alpha90)*p2)/r**4
      elr = u3 + u4 

  END SUBROUTINE getlr

  !****************************************************************************

  DOUBLE PRECISION FUNCTION sw(a,b,x)

      IMPLICIT NONE
      DOUBLE PRECISION :: a, b, x

    !--------------------------------------------------------------------------

      sw = 0.5d0*a*x**2 + (10.d0 + b/2.d0 - 3.d0*a/2.d0)*x**3 + &
           (3.d0*a/2.d0 - b - 15.d0)*x**4 + (6.d0 + b/2.d0 - a/2.d0)*x**5

  END FUNCTION sw

  !****************************************************************************

END MODULE ch5_pes
