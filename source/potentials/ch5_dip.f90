MODULE ch5_dip
  !> @brief Calculation of dipole moment and its time derivative for an example
  !> of the CH5+ system.

  use kinds
  IMPLICIT NONE
  logical, save :: use_ch5dip=.FALSE.

  PRIVATE

  PUBLIC :: get_dipole_ch5
  PUBLIC :: get_dip_derivative_ch5
  PUBLIC :: use_ch5dip

  !****************************************************************************

CONTAINS

  SUBROUTINE get_dipole_ch5( coord, dipole_moment )
    !> @brief Calculation of the electric dipole moment vector.
    !!
    !! Input variables:
    !!    coord - Atomic coordinates
    !!
    !! Output variable:
    !!    dipole_moment - Dipole moment vector
    !!

! Previous comments:
!.... Subroutine to obtain dipole moment from a fit to
!.... the ab initio data
!
!.... Current code owner: A. Brown
!
!.....History:
!.... Date     Modified by  Comment
!.... ----     -----------  -------
!.... 24/03/03    AB        Data md,coefd moved to external code
!.... 24/03/03    AB        Subroutine created on BB original code
!
!.... Fit is to cc-PVTZ/MP2 data 
!.... Fit is to a combined data set with data from
!.... 1 trajectory at 12000cm-1
!.... 1 trajectory at 14000cm-1 AND
!.... 1 trajectory at 16000cm-1
!.... adding dditional 7619 ab initio data points
!

    real(dp), DIMENSION(:,:), INTENT( IN )  :: coord
    real(dp), DIMENSION(0:2), INTENT( OUT ) :: dipole_moment

    !! Subroutine internal variables and parameters
    INTEGER, PARAMETER                              :: md = 5

    DOUBLE PRECISION, DIMENSION(0:2,0:md)           :: xn
    DOUBLE PRECISION, DIMENSION(0:2,0:md-1)         :: dvec
    DOUBLE PRECISION, DIMENSION(0:md-1), PARAMETER  :: coefd = [ &
                                                  13.37554540d0, &
                                                 -11.34879657d0, &
                                                  1.795454621d0, &
                                                 0.1007412419d0, &
                                                   1.043459412d0 &
                                                  ]

    !--------------------------------------------------------------------------

    xn = TRANSPOSE( coord )
    CALL getdvec (md, xn, dvec)

    !! Calculated dipole moment vector
    dipole_moment = MATMUL(dvec,coefd)

  END SUBROUTINE get_dipole_ch5

  !****************************************************************************

  SUBROUTINE get_dip_derivative_ch5( coord, velocity, dip_grad, delta_in )
    !> @brief Numerical time derivative of the dipole moment along a specified displacement vector.
    !> Subroutine calls get_dipole subroutine for dipole moment calculations.
    !!
    !! Input variables:
    !!    coord - Atomic coordinates
    !!    velocity - Atomic velocities
    !!    delta_in - Numerical time step for derivation (Optional)
    !!               Default value set to 1.D-4 a.u.
    !!
    !! Output variables:
    !!    dip_grad - Time derivative of the dipole moment along the velocity vector
    !!

    real(dp), DIMENSION(:,:), INTENT( IN )  :: coord
    real(dp), DIMENSION(:,:), INTENT( IN )  :: velocity
    real(dp), INTENT( IN ), OPTIONAL        :: delta_in
    real(dp), DIMENSION(3), INTENT( OUT )   :: dip_grad

    !! Local variables
    DOUBLE PRECISION                                :: delta
    DOUBLE PRECISION, DIMENSION(SIZE( coord, 1 ),3) :: coord_temp
    DOUBLE PRECISION, DIMENSION(3)                  :: neg_dis_dip
    DOUBLE PRECISION, DIMENSION(3)                  :: pos_dis_dip
    INTEGER :: i
    INTEGER :: j

    !--------------------------------------------------------------------------

    IF( .NOT.PRESENT( delta_in ) ) THEN
        delta = 1.D-4
    ELSE
        delta = delta_in
    END IF

    dip_grad = 0.D0
    DO i = 1, SIZE( coord, 1 )
        DO j = 1, SIZE( coord, 2 )
            coord_temp = coord
            coord_temp(i,j) = coord(i,j) - delta
            CALL get_dipole_ch5( coord_temp, neg_dis_dip )
            coord_temp(i,j) = coord(i,j) + delta
            CALL get_dipole_ch5( coord_temp, pos_dis_dip )
            dip_grad = (pos_dis_dip - neg_dis_dip) * velocity(i,j)
        END DO
    END DO
    dip_grad = 0.5D0 * dip_grad / delta

  END SUBROUTINE get_dip_derivative_ch5

  !****************************************************************************

  SUBROUTINE getdvec( md, xn, dvec )
    !> @brief Calculation of the CH5+ system electric dipole moment.

    integer :: md
    integer :: i
    integer :: k
    double precision, dimension(0:2,0:5) ::  xn
    double precision, dimension(0:2,0:md-1) :: dvec
    double precision, dimension(0:2) :: x
    double precision, dimension(0:2) :: q0
    double precision, dimension(0:2) :: d
    double precision, dimension(0:2,0:2) :: disp
    double precision :: dis0
    double precision :: t0
    double precision :: t1

    !--------------------------------------------------------------------------

    x = 0
    do i = 1, 5
        x = x+xn(0:2,i)/5.0d0
    enddo
    if (5.le.md) then
        disp = 0.0d0
        q0 = 0.0d0
        do i = 1, 5
            d = xn(0:2,i)-x(0:2)
            do k = 0, 2
                disp(0:2,k) = disp(0:2,k)+d(0:2)*d(k)
            enddo
            q0 = q0+dot_product(d,d)*d/5.0d0
        enddo
        dis0 = disp(0,0)+disp(1,1)+disp(2,2)
        do k = 0, 2
            disp(k,k) = disp(k,k)-dis0/3.0d0
        enddo
        t0 = dot_product(x,x)
        t1 = dot_product(x,q0)
    endif
    if (md.eq.1) then
        dvec(0:2,0) = x
    else if (md.eq.5) then
        dvec(0:2,0) = x/(5.0d0*t0+dis0)
        dvec(0:2,1) = 5.0d0*t0*dvec(0:2,0)
        dvec(0:2,2) = dis0*dvec(0:2,0)
        dvec(0:2,3) = matmul(disp,dvec(0:2,0))
        dvec(0:2,4) = q0/(5.0d0*t0+dis0)
    else
        stop 'Error in getdvec.'
    endif

  END SUBROUTINE getdvec

  !****************************************************************************

END MODULE ch5_dip 
