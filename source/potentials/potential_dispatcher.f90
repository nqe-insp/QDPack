module potential_dispatcher
	USE kinds
	USE nested_dictionaries
	USE string_operations
	USE model_potentials
	USE qtip4pf_potential
	USE h2o_pat
	USE ch5_pes
	USE ff_co2
	USE ff_cso2h3
	USE file_handler, only: output
	USE diatomic
	USE potential
	USE atomic_units
	USE boundaries
  IMPLICIT NONE

	CHARACTER(:), allocatable, SAVE :: pot_name
	INTEGER, SAVE :: n_threads

	PRIVATE
	PUBLIC :: initialize_potential, finalize_potential,pot_name
	
contains

	subroutine initialize_potential(param_library,nat,ndim)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), INTENT(IN) :: param_library
		INTEGER, intent(in) :: nat,ndim
		INTEGER :: i
		CHARACTER(:), allocatable :: xyz_file
		LOGICAL :: file_exists

		
		pot_name = param_library%get("POTENTIAL/pot_name")
		!! @input_file POTENTIAL/pot_name [STRING] (potential_dispatcher)
		pot_name=to_upper_case(trim(pot_name))

    hessian_available=.FALSE.
		
    SELECT CASE(pot_name)
		CASE("QTIP4PF")
			call simulation_box%initialize(param_library)
			if(simulation_box%ndim /= 3) STOP "[potential_dispatcher.f90] Error: provided simulation box must be 3-dimensional for QTIP4PF"
			if(.not. simulation_box%is_diagonal()) then
				write(*,*) "[potential_dispatcher.f90] Error: cell matrix must be diagonal for QTIP4PF potential. You must specify:"
				write(*,*) "BOUNDARIES{"
				write(*,*) "  a1= a 0. 0."
				write(*,*) "  a2= 0. b 0."
				write(*,*) "  a3= 0. 0. c"
				write(*,*) "}"
				write(*,*)
				STOP "Execution stopped."				
			endif
			write_pot_components =param_library%get("POTENTIAL/write_pot_components",default=.FALSE.) 
			!! @input_file POTENTIAL/write_pot_components [STRING] (potential_dispatcher, default=.FALSE., if(pot_name=="QTIP4PF"))
			if(write_pot_components) then
				n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
				write_stride=param_library%get("POTENTIAL/write_stride",default=1) 
				!! @input_file POTENTIAL/write_stride [INTEGER] (potential_dispatcher, default=1, if(pot_name=="QTIP4PF"))
				allocate(write_now(n_threads) &
								,unit_pot(n_threads) &
								,write_count(n_threads) &
								,vlj_mean(n_threads) &
								,vewald_mean(n_threads) &
								,vstretch_mean(n_threads) &
								,vbend_mean(n_threads) &
				)
				vlj_mean=0; vewald_mean=0; vstretch_mean=0; vbend_mean=0
				write_count=0
				write_now=.FALSE.
				DO i=1,n_threads
					open(newunit=unit_pot(i),file=output%working_directory//"/pot_components_"//int_to_str(i)//".out")
					if(nat>3) then					
						write(unit_pot(i),*) "#V_lj   V_coulomb   V_stretching    V_bending"
					else
						write(unit_pot(i),*) "#V_stretching    V_bending"
					endif
				ENDDO
			endif
			get_pot_info => get_qtip4pf_pot_info
			Pot => Pot_qtip4pf
			dPot => dPot_qtip4pf
			compute_hessian => hessian_qtip4pf
      hessian_available=.TRUE.
		CASE("H2O_PAT")
			!call initialize_simulation_box(param_library,ndim)			
			get_pot_info => get_h2opat_pot_info
			Pot => Pot_h2opat
			dPot => dPot_h2opat
			compute_hessian => hessian_h2opat
      hessian_available=.TRUE.
			call initialize_h2opat()
		CASE("CH5")
			!call initialize_simulation_box(param_library,ndim)			
			get_pot_info => get_ch5_pot_info
			Pot => Pot_ch5
			dPot => dPot_ch5
			compute_hessian => hessian_finitediff
      hessian_available=.TRUE.
		CASE("FFCO2")
			call simulation_box%initialize(param_library)
			if(simulation_box%ndim /= ndim) STOP "[potential_dispatcher.f90] Error: provided simulation &
																							&box must be 3-dimensional for QTIP4PF"
			if(.not. simulation_box%is_diagonal()) then
				write(*,*) "[potential_dispatcher.f90] Error: cell matrix must be diagonal for FFCO2 potential. You must specify:"
				write(*,*) "BOUNDARIES{"
				write(*,*) "  a1= a 0. 0."
				write(*,*) "  a2= 0. b 0."
				write(*,*) "  a3= 0. 0. c"
				write(*,*) "}"
				write(*,*)
				STOP "Execution stopped."				
			endif
			get_pot_info => get_ffco2_pot_info
			Pot => Pot_ffco2
			dPot => dPot_ffco2
			compute_hessian => hessian_ffco2
      hessian_available=.FALSE.
		CASE("CSO2H3")
			call simulation_box%initialize(param_library)
			if(simulation_box%ndim /= ndim) STOP "[potential_dispatcher.f90] Error: provided simulation &
																							&box must be 3-dimensional for QTIP4PF"
			get_pot_info => get_cso2h3_pot_info
			Pot => Pot_cso2h3
			dPot => dPot_cso2h3
			compute_hessian => hessian_cso2h3
			call initialize_cso2h3(param_library)
      hessian_available=.FALSE.
		CASE("DIATOMIC")
			get_pot_info => get_diatomic_pot_info
			Pot => Pot_diatomic
			dPot => dPot_diatomic
			compute_hessian => hessian_diatomic
			call initialize_diatomic(param_library)
      hessian_available=.FALSE.
		CASE DEFAULT
			call initialize_model_potentials(param_library,nat,ndim,pot_name)
		END SELECT

	end subroutine initialize_potential


	subroutine finalize_potential()
		IMPLICIT NONE
		INTEGER :: i

		SELECT CASE(pot_name)
		CASE("QTIP4PF")
			if(write_pot_components) then
				DO i=1,n_threads
			 		close(unit_pot(i))
				ENDDO
			endif
		END SELECT

	end subroutine finalize_potential
    
end module potential_dispatcher
