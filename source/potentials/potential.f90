module potential
  use kinds
  implicit none

  PRIVATE
  PUBLIC :: Pot, dPot, compute_hessian &
						,get_pot_info &
            ,hessian_available &
            ,POT_CAT &
            ,hessian_finitediff, dpot_finitediff

  LOGICAL, SAVE :: hessian_available
	CHARACTER(*), PARAMETER :: POT_CAT="potential"
  
  abstract interface
		function pot_type(X) result(U)
		!! abstract interface for potential function
			import :: dp
			IMPLICIT NONE
			REAL(dp),INTENT(in)  :: X(:,:)
				!! positions of the atoms
			REAL(dp) :: U
		end function pot_type
	end interface
	
	abstract interface
		function dpot_type(X) result(dU)
		!! abstract interface for potential derivative function
			import :: dp
			IMPLICIT NONE
			REAL(dp),INTENT(in)  :: X(:,:)
				!! positions of the atoms
			REAL(dp)	:: dU(size(X,1),size(X,2))
		end function dpot_type
	end interface

	abstract interface
			function hessian_type(X) result(H)
		!! abstract interface for potential hessian
			import :: dp
			implicit none
			real(dp),intent(in)  :: X(:,:)		
				!! positions of the atoms
			real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		end function hessian_type
	end interface

	abstract interface
		subroutine get_pot_info_type(X,Pot,Forces,hessian,vir)
		!! abstract interface for all-in-one potential info (e.g. for ab initio potentials)
			import :: dp			
			IMPLICIT NONE
			real(dp), intent(in)  :: X(:,:)
			real(dp), intent(out) :: Forces(size(X,1),size(X,2))
			real(dp), intent(out) :: Pot
			real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
			real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
		end subroutine get_pot_info_type
	end interface

	PROCEDURE(pot_type), POINTER, SAVE :: Pot => pot_null
		!! pointer to the potential function
	PROCEDURE(dpot_type), POINTER, SAVE :: dPot => dpot_null
		!! pointer to the potential derivative function
	PROCEDURE(hessian_type), POINTER, SAVE :: compute_hessian => hessian_null
		!! pointer to the potential derivative function
	PROCEDURE(get_pot_info_type), POINTER, SAVE :: get_pot_info => get_pot_info_null
		!! pointer to the all-in-one potential function
   
CONTAINS

  function pot_null(X) result(U)
  !! null default potential
    IMPLICIT NONE
    REAL(dp),INTENT(in)  :: X(:,:)
      !! positions of the atoms
    REAL(dp) :: U

    U=0._dp
  end function pot_null

  function dpot_null(X) result(dU)
		!! null default dPot
    IMPLICIT NONE
    REAL(dp),INTENT(in)  :: X(:,:)
      !! positions of the atoms
    REAL(dp)	:: dU(size(X,1),size(X,2))

    dU(:,:) = 0._dp
  end function dpot_null

  function hessian_null(X) result(H)
  !! null default hessian
    implicit none
    real(dp),intent(in)  :: X(:,:)		
      !! positions of the atoms
    real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

    H(:,:) = 0._dp
  end function hessian_null

  subroutine get_pot_info_null(X,Pot,Forces,hessian,vir)
  !! default null all-in-one potential info
    IMPLICIT NONE
    real(dp), intent(in)  :: X(:,:)
    real(dp), intent(out) :: Forces(size(X,1),size(X,2))
    real(dp), intent(out) :: Pot
    real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
    real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))

    Forces(:,:) = 0._dp
    Pot = 0._dp
    if(present(vir)) vir(:,:) = 0._dp
    if(present(hessian)) hessian(:,:) = 0._dp 
  end subroutine get_pot_info_null
  
	function dpot_finitediff(X) result(dU)
		!! null default dPot
    IMPLICIT NONE
    REAL(dp),INTENT(in)  :: X(:,:)
      !! positions of the atoms
    REAL(dp)	:: dU(size(X,1),size(X,2))
		real(dp) :: Up,Um,Xtemp(size(X,1),size(X,2))
		real(dp),parameter :: disp=1.d-4
    integer :: i,j

		dU(:,:)=0._dp
		Xtemp(:,:)=X(:,:)
		do j=1,size(X,2); do i=1,size(X,1)
      Xtemp(i,j) = X(i,j)-disp
      Um = Pot(Xtemp)
      Xtemp(i,j) = X(i,j)+disp
      Up = Pot(Xtemp)
      dU(i,j) = (Up-Um)/(2._dp*disp)
		enddo; enddo

  end function dpot_finitediff

  function hessian_finitediff(X) result(H)
    implicit none
    real(dp),intent(in)  :: X(:,:)
    real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
    real(dp) :: Xp(size(X,1),size(X,2)), Xm(size(X,1),size(X,2))
    real(dp) :: dUp(size(X,1),size(X,2)),dUm(size(X,1),size(X,2))
    integer :: i,j,c
    real(dp), PARAMETER :: disp=1.d-4
     
  
    H(:,:)=0._dp
    c=0
    do j=1,size(X,2); do i=1,size(X,1) 
      c=c+1
      Xp(:,:)=X(:,:) ;  Xm(:,:)=X(:,:)
      Xp(i,j) = Xp(i,j) + disp
      Xm(i,j) = Xm(i,j) - disp
  
      dUp = dPot(Xp)
      dUm = dPot(Xm)
      H(:,c)= RESHAPE((dUp - dUm) / (2*disp), (/size(X,1)*size(X,2)/))
    enddo ; enddo
  
    H = 0.5*(H + transpose(H))
     
  end function hessian_finitediff

end module potential
