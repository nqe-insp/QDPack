PROGRAM crystalGen
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use string_operations
	use timer_module
  use atomic_units
  use boundaries
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library
  TYPE(DICT_STRUCT), pointer ::  atoms

  TYPE(timer_type) :: full_timer
  real :: cpu_time_start,cpu_time_finish
  integer :: n_threads

  integer :: nat, ndim,nat_per_cell

  integer :: i,j,c,uout,i1,i2,i3
  integer, allocatable :: ncell(:)
  character(:), allocatable :: at_key, outfile
  character(2), allocatable :: species(:)
  real(dp), allocatable :: posunit(:,:), positions(:,:),xyz(:)
  real(dp) :: outmult
  logical :: angstrom_output



  call full_timer%start()
  call cpu_time(cpu_time_start)

  call initialize_framework(param_library,initialize_output=.FALSE.)
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	
  !----------------------------------------------
  !! SIMULATION LOGIC GOES HERE
  call simulation_box%initialize(param_library)
  ndim = simulation_box%ndim

  nat_per_cell=0
  DO    
    at_key = "crystal_gen/atom"//int_to_str(nat_per_cell+1)
    if(param_library%has_key(at_key)) then
      nat_per_cell=nat_per_cell+1
    else
      exit
    endif
  ENDDO
  if(nat_per_cell==0) then
    write(0,*) "[crystalGen.f90] Error: no atoms were defined in the unit cell!"
    write(0,*) "  provide at least one atom with for example:"
    write(0,*) "atom1{"
    write(0,*) "  type = H"
    write(0,*) "  relpos = 0. 0. 0."
    write(0,*) "}"
    write(0,*)
    STOP "Execution stopped."
  else
    write(*,*) "Found "//int_to_str(nat_per_cell)//" atom(s) in the unit cell."
  endif

  ncell = param_library%get("crystal_gen/n_cells")
  if(any(ncell<=0)) STOP "[crystalGen.f90] Error: all components of 'crystal_gen/n_cells' must be strictly positive!"
  if(size(ncell)/=ndim) STOP "[crystalGen.f90] Error: 'crystal_gen/n_cells' must be of the same size as 'BOUNDARIES/a1'"

  nat=nat_per_cell*product(ncell)
  allocate(species(nat),posunit(nat_per_cell,ndim), positions(nat,ndim))
  DO i=1,nat_per_cell
    at_key = "crystal_gen/atom"//int_to_str(i)
    species(i) = param_library%get(at_key//"/type")
    xyz = param_library%get(at_key//"/cryspos")
    if(size(xyz)/=ndim) then 
      write(*,*) "[crystalGen.f90] Error: '"//at_key//"/cryspos' must be of the same size as 'BOUNDARIES/a1'"
      STOP "Execution stopped."
    endif
    if(any(xyz>=1.) .OR. any(xyz<0.)) then
      write(*,*) "[crystalGen.f90] Error: all components of '"//at_key//"/cryspos' must be between 0. (included) and 1. (excluded)."
    endif
    posunit(i,:) = matmul(simulation_box%hcell,xyz)
  ENDDO

  
  if(ndim==1) then
    c=0
    DO i1=0,ncell(1)-1   
      do j=1,nat_per_cell
        c=c+1
        positions(c,:)= posunit(j,:) + i1*simulation_box%a1
        species(c)=species(j)
      enddo    
    ENDDO
  elseif(ndim==2) then
    c=0
    DO i1=0,ncell(1)-1 ; DO i2=0,ncell(2)-1   
      do j=1,nat_per_cell
        c=c+1
        positions(c,:)= posunit(j,:) + i1*simulation_box%a1 &
                                     + i2*simulation_box%a2
        species(c)=species(j)
      enddo    
    ENDDO; ENDDO
  elseif(ndim==3) then
    c=0
    DO i1=0,ncell(1)-1 ; DO i2=0,ncell(2)-1  ;  DO i3=0,ncell(3)-1 
      do j=1,nat_per_cell
        c=c+1
        positions(c,:)= posunit(j,:) + i1*simulation_box%a1 &
                                     + i2*simulation_box%a2 &
                                     + i3*simulation_box%a3
        species(c)=species(j)
      enddo    
    ENDDO; ENDDO ; ENDDO
  endif 

  outfile=param_library%get("crystal_gen/output_file")
  angstrom_output=param_library%get("crystal_gen/angstrom_output",default=.FALSE.)
  open(newunit=uout,file=outfile)
  write(uout,'(A)') int_to_str(nat)
  if(angstrom_output) then
    outmult=au%bohr
    write(uout,*) "# unit= angstrom, cell= ",transpose(simulation_box%hcell)*au%bohr
  else
    outmult=1._dp
    write(uout,*) "# unit= bohr, cell= ",transpose(simulation_box%hcell)
  endif
  DO i=1,nat
    write(uout,'(A,'//int_to_str(ndim)//'G15.7)') species(i), positions(i,:)*outmult
  ENDDO	
  close(uout)
  write(*,*) "structure file written to '"//outfile//"'."

  

  !----------------------------------------------

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"


END PROGRAM crystalGen
