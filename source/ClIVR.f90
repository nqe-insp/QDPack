PROGRAM ClIVR
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use system_commons
  use potential
  use correlation_functions
  use matrix_operations, only: sym_mat_diag
  use classical_md
  use condkin
  use trajectory_files
  implicit none
	type(DICT_STRUCT) :: param_library	
  type(tcf_type) :: tcf

  real(dp) :: dt_dyn, gamma_dyn, spectrum_cutoff
  real(dp), allocatable :: X(:,:),P(:,:,:),Xs(:,:),Ps(:,:)
  logical :: ignore_point

  real(dp) :: time_dyn
  integer :: nsteps_dyn, nsteps_btwn_samples, nsamples
  integer :: n_momenta_per_sample
  integer :: nsteps_aux, nsteps_aux_therm    
  integer :: write_stride

  integer :: isample,ux,up,ip
  logical :: verbose  
  logical :: save_samples

  integer :: avg_counter
  integer :: n_threads
  real(dp), allocatable :: Ek_dyn(:,:,:)
  real(dp), allocatable :: Ek_dyn_avg(:,:)
  real(dp) :: temp_avg,Qekin, pot_avg
  real(dp), allocatable :: Qtemp(:)
  integer :: nan_count

  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real :: dynamics_time_avg,lgv_time_avg
  real ::  remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  character(:), allocatable :: tcf_name
  logical :: apply_eckart,use_WK

  TYPE(TRAJ_FILE) :: samplefile_xyz  

  call full_timer%start
  call cpu_time(cpu_time_start)


  call initialize_framework(param_library)	

  ! GET NUMBER OF THREADS        
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)   

  write_stride = param_library%get("OUTPUT/write_stride",default=1)
  !! @input_file OUTPUT/write_stride [INTEGER] (IVR, default=1)) 
  avg_counter = 0
  lgv_time_avg = 0.
  step_time_avg = 0.
  dynamics_time_avg = 0.
  step_cpu_time_avg = 0.
  
  !----------------------------------------
  ! INITIALIZE SAMPLER 
  nsteps_btwn_samples = param_library%get("sampler/nsteps_btwn_samples")
  !! @input_file sampler/nsteps_btwn_samples [INTEGER] (IVR) 
  nsamples = param_library%get("parameters/nsamples")  
  !! @input_file parameters/nsamples [INTEGER] (IVR) 
  call initialize_cl_sampler(param_library,tcf_name,category="sampler")
  
  save_samples = param_library%get("OUTPUT/save_samples",default=.TRUE.)
  !! @input_file parameters/save_samples [LOGICAL] (IVR, default=.TRUE.)
  if(qtb_thermostat) then
    n_momenta_per_sample=1
  else  
    n_momenta_per_sample = param_library%get("parameters/n_momenta_per_sample",default=1)
    !! @input_file parameters/n_momenta_per_sample [INTEGER] (IVR, default=1)
  endif
  allocate(X(nat,ndim), P(nat,ndim,n_momenta_per_sample))
  allocate(Xs(nat,ndim), Ps(nat,ndim))
  allocate(Qtemp(n_momenta_per_sample))
  X(:,:) = Xinit(:,:)
  Xs(:,:) = Xinit(:,:)
  P(:,:,:) = 0._dp  
  Ps(:,:) = 0._dp  
  pot_avg = 0._dp
  !WARM UP SAMPLER
  write(*,*)
  write(*,*) "Warming up sampler..."
  call update_cl_sample(Xs,Ps,nsteps_btwn_samples,.FALSE.)
  write(*,*) "done."

  !----------------------------------------
  !INITIALIZE DYNAMICS
  time_dyn = param_library%get("dynamics/time")
  !! @input_file dynamics/time [REAL] (IVR) 
  dt_dyn = param_library%get("dynamics/dt")
  !! @input_file dynamics/dt [REAL] (IVR) 
  nsteps_dyn = nint(time_dyn/dt_dyn)
  gamma_dyn = param_library%get("dynamics/gamma",default=-1._dp)
  !! @input_file dynamics/gamma [REAL] (IVR, default=-1.) 
  spectrum_cutoff = param_library%get("dynamics/spectrum_cutoff",default=-1._dp)   
  !! @input_file dynamics/spectrum_cutoff [REAL] (IVR, default=-1.) 
  allocate(Ek_dyn(ndof,nsteps_dyn,n_momenta_per_sample))  
  apply_eckart=param_library%get("dynamics/apply_eckart",default=.FALSE.)
  !! @input_file dynamics/apply_eckart [LOGICAL] (ClIVR, default=FALSE) 
  use_WK=param_library%get("dynamics/use_wk",default=.FALSE.)
  use_WK=use_WK .and. (.not. qtb_thermostat)

  !----------------------------------------
  !INITIALIZE TCF
  if(qtb_thermostat) then
    tcf_name="QTBIVR"
  else
    tcf_name="ClIVR"
  endif  
  call initialize_tcf(tcf,tcf_name,dt_dyn,nsteps_dyn,n_momenta_per_sample&
                    ,gamma_dyn,spectrum_cutoff,kubo_transform=qtb_thermostat)
  allocate(Ek_dyn_avg(ndof,nsteps_dyn)) 
  Ek_dyn_avg(:,:)=0._dp
  temp_avg = 0._dp
  nan_count = 0
  !----------------------------------------
  
  
  !----------------------------------------
  call open_output_files(param_library)
  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  if(save_samples) then
    if(.not.param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")
    if(xyz_output) then
      call samplefile_xyz%init("sample_position.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
      call samplefile_xyz%open(output%working_directory,append=.TRUE.)
    else
      open(newunit=ux,file=output%working_directory//"sample_position")
    endif
    open(newunit=up,file=output%working_directory//"sample_momentum")
  endif
  verbose=.FALSE.

  write(*,*)
  write(*,*) "Initialization done."

  !MAIN SAMPLE LOOP
  DO isample = 1,nsamples
    !START TIMERS
    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1
    
    ! DETERMINE IF WE SHOULD PRINT SOME INFO
    if(mod(isample,write_stride)==0) then
      write(*,*)
      write(*,*) "SAMPLE "//int_to_str(isample)//" / "//int_to_str(nsamples)
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    !-------------------------------
    ! POSITION SAMPLING (PIMD PROPAGATION)
    call step_timer%start()
    call update_cl_sample(Xs,Ps,nsteps_btwn_samples,verbose)
    X(:,:)=Xs(:,:)
    if(qtb_thermostat) P(:,:,1)=Ps(:,:)

    pot_avg = pot_avg + (Pot(X)  - pot_avg)/avg_counter   

    lgv_time_avg =  lgv_time_avg  &
      + (step_timer%elapsed_time() - lgv_time_avg)/avg_counter
    ! END OF POSITION SAMPLING
    !-------------------------------  
    ! NVE trajectories
      call step_timer%start()
      !$OMP PARALLEL DO PRIVATE(Qekin)
      DO ip=1,n_momenta_per_sample        
          if(.not. qtb_thermostat) &
            call sample_classical_momentum(P(:,:,ip))
          if(apply_eckart) call apply_eckart_conditions(X,P(:,:,ip))
          call compute_kinetic_energy(P(:,:,ip),Qekin,Qtemp(ip))
          if(charges_provided) then
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf%Cpp(:,:,ip),Ek_dyn(:,:,ip),tcf%Cmumu(:,:,ip) &
                    ,use_wk=use_wk)
          else
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf%Cpp(:,:,ip),Ek_dyn(:,:,ip) &
                    ,use_wk=use_wk)
          endif
      ENDDO
      !$OMP END PARALLEL DO
      dynamics_time_avg =  dynamics_time_avg  &
        + (step_timer%elapsed_time() - dynamics_time_avg)/avg_counter
      
       if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf%Cpp)) .OR.  ANY(ISNAN(tcf%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf%Cpp))
      endif

      if(ignore_point) then
        nan_count = nan_count + 1
      else
        temp_avg = temp_avg + (SUM(Qtemp)/n_momenta_per_sample - temp_avg)/avg_counter
        Ek_dyn_avg = Ek_dyn_avg + (SUM(Ek_dyn,dim=3)/n_momenta_per_sample - Ek_dyn_avg)/isample
        call update_tcf_avg(tcf)

        ! WRITE SAMPLES
        if(save_samples) then
          do ip = 1,n_momenta_per_sample
            if(xyz_output) then
              call samplefile_xyz%dump_xyz(X,element_symbols)
            else
              write(ux,*) X(:,:)
            endif
            write(up,*) P(:,:,ip)
          enddo
        endif
      endif

    !-------------------------------  

    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter

    if(verbose) then
      ! PRINT SYSTEM INFO
      if(nan_count>0) write(*,*) "NaN_count= "//int_to_str(nan_count)
      write(*,*) "average_potential_energy=",real(pot_avg*au%kcalpermol),"kcal/mol"    
      ! PRINT TIMINGS  
      write(*,*) "AVERAGE TIMINGS PER SAMPLE:"
      write(*,*) "  sampling :",lgv_time_avg, "s. (for "//int_to_str(nsteps_btwn_samples)//" steps of Langevin dynamics)"
      write(*,*) "  NVE propagation :",dynamics_time_avg,"s."
      write(*,*) "  total time: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."

      remaining_time = step_time_avg*(nsamples-isample)
      job_time = full_timer%elapsed_time() + remaining_time
      write(*,*) "estimated job time : "//sec2human(job_time)
      write(*,*) "estimated remaining time : "//sec2human(remaining_time)

      ! RESET WINDOW AVERAGES
      avg_counter=0      
      lgv_time_avg = 0._dp 
      dynamics_time_avg=0._dp

      ! WRITE INTERMEDIATE RESULTS
      call write_ivr_results("Ekin_ClIVR",Ek_dyn_avg,nsteps_dyn,dt_dyn,.FALSE.)
      call write_ivr_results("temperature_ClIVR",2._dp*Ek_dyn_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
      call write_tcf_results(tcf,1._dp)

    endif
  ENDDO

  ! WRITE FINAL RESULTS
  call write_ivr_results("Ekin_ClIVR",Ek_dyn_avg,nsteps_dyn,dt_dyn,.FALSE.)
  call write_ivr_results("temperature_ClIVR",2._dp*Ek_dyn_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
  call write_tcf_results(tcf,1._dp)
  if(save_samples) then
    if(xyz_output) then
     call samplefile_xyz%destroy()
    else
      close(ux)
    endif
    close(up)
  endif
  

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"


END PROGRAM ClIVR
