PROGRAM TRPMD
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
  use PI_sampler
  use system_commons
  use correlation_functions
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use trajectory_files
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library	
  type(tcf_type) :: tcf

  integer :: nbeads

  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real ::  remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  integer :: avg_counter
  logical :: verbose

  integer :: nan_count,max_nan,c
  integer :: up,uxc
  integer, allocatable :: ux(:)
  logical :: save_trajectory,ignore_point, save_all_beads
  logical :: save_samples, save_centroid
  integer :: nbeads_save

  integer :: nsteps,nblocks,iblock,write_stride,istep,n_threads,nblocks_therm
  real(dp) :: block_time
  logical :: resample_momentum
  real(dp) :: gamma_smooth, spectrum_cutoff
  real(dp) :: dt

  integer :: i
  real(dp), allocatable :: X(:,:,:),EigP(:,:,:),Xtraj(:,:,:,:),Pctraj(:,:,:),mutraj(:,:)
  real(dp), allocatable :: mu0(:),mutmp(:),Xc(:,:)

  character(:), allocatable :: tcf_name

  type(TRAJ_FILE), allocatable :: trajfile_xyz(:)
  type(TRAJ_FILE) :: trajfile_centroid

  call full_timer%start()
  call cpu_time(cpu_time_start)
  avg_counter = 0
  step_time_avg=0.
  step_cpu_time_avg=0.

  call initialize_framework(param_library)
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	
  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE  
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)

  call initialize_trpmd(param_library,nbeads,tcf_name,dt)
  block_time = param_library%get("parameters/block_time") 
  !! @input_file parameters/block_time [REAL] (TRPMD)
  nsteps=nint(block_time/dt)
  nblocks = param_library%get("parameters/nblocks")
  !! @input_file parameters/nblocks [INTEGER] (TRPMD)
  nblocks_therm = param_library%get("parameters/nblocks_therm",default=0)
  !! @input_file parameters/nblocks_therm [INTEGER] (TRPMD, default=0)
  write_stride = param_library%get("parameters/write_stride",default=1)
  !! @input_file parameters/write_stride [INTEGER] (TRPMD, default=1)
  resample_momentum = param_library%get("parameters/resample_momentum",default=.FALSE.)
  !! @input_file parameters/resample_momentum [LOGICAL] (TRPMD, default=.FALSE.)
  save_trajectory = param_library%get("OUTPUT/save_trajectory",default=.FALSE.)
  !! @input_file OUTPUT/save_trajectory [LOGICAL] (TRPMD, default=.TRUE.)
  save_all_beads = param_library%get("OUTPUT/save_all_beads",default=.TRUE.)
  !! @input_file OUTPUT/save_all_beads [LOGICAL] (TRPMD, default=.TRUE.) 
  save_centroid = param_library%get("OUTPUT/save_centroid",default=.FALSE.)
  !! @input_file OUTPUT/save_centroid [LOGICAL] (TRPMD, default=.FALSE.) 
  save_samples = param_library%get("OUTPUT/save_samples",default=.TRUE.)
  !! @input_file OUTPUT/save_samples [LOGICAL] (TRPMD, default=.TRUE.)

  gamma_smooth = param_library%get("parameters/gamma_smooth",default=-1._dp)
  !! @input_file parameters/gamma_smooth [REAL] (TRPMD, default=-1.)
  spectrum_cutoff = param_library%get("parameters/spectrum_cutoff",default=-1._dp)   
  !! @input_file parameters/spectrum_cutoff [REAL] (TRPMD, default=-1.)
  call initialize_tcf(tcf,tcf_name,dt,nsteps,1 &
                        ,gamma_smooth,spectrum_cutoff,kubo_transform=.FALSE.)

  !! initialize position and momentum
  allocate(X(nat,ndim,nbeads),EigP(nat,ndim,nbeads))
  do i=1,nbeads
    X(:,:,i)=Xinit(:,:)
  enddo
  call sample_PI_momentum(EigP)
  
  allocate(Xtraj(nat,ndim,nbeads,nsteps),Pctraj(nat,ndim,nsteps))
  Xtraj(:,:,:,:)=0._dp
  Pctraj(:,:,:)=0._dp
  if(charges_provided) then
    allocate(mutraj(ndim,nsteps),mu0(ndim),mutmp(ndim))
    mutraj(:,:)=0._dp
  endif
  if(save_centroid) then
    allocate(Xc(nat,ndim))
    Xc(:,:)=SUM(X,dim=3)/nbeads
  endif

  max_nan = param_library%get("parameters/max_nan",default=10)
  !! @input_file parameters/max_nan [INTEGER] (TRPMD, default=10)
  nan_count=0

  !! open trajectory files
  nbeads_save=1
  if(save_trajectory) then
    if(.not.param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")
    if(save_all_beads) nbeads_save=nbeads
    if(xyz_output) then
      allocate(trajfile_xyz(nbeads_save))
      do i=1,nbeads_save
        call trajfile_xyz(i)%init("trajectory.bead"//int_to_str(i,nbeads)//".xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call trajfile_xyz(i)%open(output%working_directory,append=.TRUE.)
      enddo
      if(save_centroid) then
        call trajfile_centroid%init("trajectory.centroid.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call trajfile_centroid%open(output%working_directory,append=.TRUE.)
      endif
    else
      allocate(ux(nbeads_save))
      do i=1,nbeads_save
        open(newunit=ux(i),file=output%working_directory//"X.traj.bead"//int_to_str(i,nbeads))
      enddo    
      if(save_centroid) then
        open(newunit=uxc,file=output%working_directory//"X_centroid.traj")
      endif
    endif
    open(newunit=up,file=output%working_directory//"P_centroid.traj")
  endif
  !! open sample files
  if(save_samples) then
    if(.not.param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")
    if(save_all_beads) nbeads_save=nbeads
    if(xyz_output) then
      do i=1,nbeads_save
        call output%create_file("sample_position.bead"//int_to_str(i,nbeads)//".xyz",tinker_xyz=tinker_xyz)
        call output%add_xyz_to_file(X(:,:,i),element_symbols,unit=xyz_unit)
      enddo
      if(save_centroid) then
        call output%create_file("sample_position.centroid.xyz",tinker_xyz=tinker_xyz)
        call output%add_xyz_to_file(Xc(:,:),element_symbols,unit=xyz_unit)
      endif
    else 
      do i=1,nbeads_save
        call output%create_file("sample_position.bead"//int_to_str(i,nbeads))
        call output%add_array_to_file(X(:,:,i),nat*ndim)
      enddo
      if(save_centroid) then
        call output%create_file("sample_position.centroid",tinker_xyz=tinker_xyz)
        call output%add_array_to_file(Xc(:,:),nat*ndim)
      endif
    endif
  endif

  !-----------------------------------
  call open_output_files(param_library)

  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

   if(nblocks_therm>0) then
    write(*,*)
    write(*,*) "Starting "//int_to_str(nblocks_therm)//" blocks of thermalization"
    !! thermalization loop
    DO iblock=1,nblocks_therm
      call compute_block(iblock,.TRUE.)
    ENDDO
    write(*,*) "Thermalization done."
    write(*,*)
  endif
  
  if(allocated(qtbs)) then
    !! save QTB thermalization spectra and restart the averaging
    if(qtbs(1)%save_average_spectra) then    
      do i=1,nbeads
        call qtbs(i)%write_average_spectra("PIQTB_spectra.thermalization.out",restart_average=.TRUE.)
      enddo
    endif
  endif
  
  !! main loop
  DO iblock=1,nblocks
    call compute_block(iblock,.FALSE.)
  ENDDO

  !! save final TCFs
  call write_tcf_results(tcf,1._dp)
  !! close trajectory files
  if(save_trajectory) then
    do i=1,nbeads_save
      if(xyz_output) then
        call trajfile_xyz(i)%destroy()
      else
        close(ux(i))
      endif
    enddo    
    close(up)
  endif     

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS

  subroutine compute_block(iblock,thermalization)
    IMPLICIT NONE
    integer, intent(in) :: iblock
    logical, intent(in) :: thermalization
    logical :: save_results

    save_results = .NOT. thermalization

    !! start timers
    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1

    !! some printing
    if(mod(iblock,write_stride)==0) then
      write(*,*)
      if(thermalization) then
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks_therm)
      else
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks)
      endif
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    if(resample_momentum) call sample_PI_momentum(EigP)

    !! continue trajectory and generate a new block
    call update_block_trpmd(X,EigP,nsteps,verbose,Xtraj,Pctraj)       

    ignore_point = ANY(ISNAN(Xtraj)) .OR. ANY(ISNAN(Pctraj))
    
    if(ignore_point) then !! ignore the block if the trajectory exploded 
      !! update NaN count
      nan_count = nan_count+1
      if(nan_count>=max_nan) then
        write(0,*) "[TRPMD.f90] Error:",max_nan," NaN trajectories reached!"
        STOP "Execution stopped."
      endif
      !! reset to a random momentum
      call sample_PI_momentum(EigP)
      X=Xtraj(:,:,:,1)

    elseif(save_results) then
      !! compute time correlation functions from the last block
      call compute_autoTCF_WK(RESHAPE(Pctraj,(/ndof,nsteps/)),nsteps,dt,tcf%Cpp(:,:,1))
      if(charges_provided) then !! compute dipole velocity trajectory
        DO istep=1,nsteps
          call get_dipole_ring_polymer(Xtraj(:,:,:,istep),Pctraj(:,:,istep),mutraj(:,istep))      
        ENDDO
        call compute_autoTCF_WK(mutraj,nsteps,dt,tcf%Cmumu(:,:,1))
      endif
      !! update the average TCFs
      call update_tcf_avg(tcf)

      !! save last block if necessary
      if(save_trajectory) then
        !! write block
        DO istep=1,nsteps
          write(up,*) Pctraj(:,:,istep)
          if(save_centroid) then
            Xc(:,:)=SUM(Xtraj(:,:,:,istep),dim=3)/nbeads
            if(xyz_output) then
              call trajfile_centroid%dump_xyz(Xc,element_symbols) 
            else
              write(uxc,*) Xc(:,:)
            endif
          endif
          do i=1,nbeads_save
            if(xyz_output) then
              call trajfile_xyz(i)%dump_xyz(Xtraj(:,:,i,istep),element_symbols) 
            else
              write(ux(i),*) Xtraj(:,:,i,istep)
            endif
          enddo
        ENDDO
      endif

      call output%write_all()

    endif 

    !! update timer averages
    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter
    
    !! print some info and save intermediate results
    if(verbose) then
      if(nan_count>0) write(*,*) "NaN_count= "//int_to_str(nan_count)
      write(*,*) "Average time per block: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."
       if(thermalization) then
        remaining_time = step_time_avg*(nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated thermalization time : "//sec2human(job_time)
        write(*,*) "estimated remaining thermalization time : "//sec2human(remaining_time)
        remaining_time = step_time_avg*(nblocks+nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      else
        remaining_time = step_time_avg*(nblocks-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      endif

      ! RESET WINDOW AVERAGES
      avg_counter=0
      step_time_avg=0.
      step_cpu_time_avg=0.

      ! WRITE INTERMEDIATE RESULTS
      if(save_results) then
        call write_tcf_results(tcf,1._dp)
      endif
    endif
  
  end subroutine compute_block

END PROGRAM TRPMD
