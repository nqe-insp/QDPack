MODULE qtb_types
  USE kinds
  USE atomic_units
  USE random
  USE file_handler, only : output
  USE nested_dictionaries
  USE string_operations
  USE system_commons
  USE glassman_fft
  USE matrix_operations, only : linear_interpolation, linear_fit &
                  ,sym_mat_inverse,linear_interpolation_clip
  IMPLICIT NONE

  complex(dp), parameter :: eye = cmplx (0._dp,1._dp,dp)

  TYPE  QTB_TYPE
    REAL(dp) :: dt

    TYPE(DICT_STRUCT), pointer :: element_dict
    LOGICAL :: write_spectra_by_element
    LOGICAL :: adapt_by_element

    REAL(dp) :: gamma0
    REAL(dp) :: gamma_exp

    CHARACTER(:), allocatable :: method
    LOGICAL :: adQTBr

    INTEGER :: n_rand
    INTEGER :: selector
    REAL(dp) :: omegacut
    REAL(dp) :: domega
    REAL(dp) :: omega_smear
    integer :: nom

    LOGICAL :: OU_integrator

    INTEGER :: i_traj
    LOGICAL :: save_average_spectra

  !FOR TESTING : white noise
    LOGICAL :: noQTB

  !FOR PI-QTB
    LOGICAL :: PI_QTB
    INTEGER :: n_beads
    REAL(dp) :: kernel_mixing
    REAL(dp) :: kernel_iter_prec
    INTEGER :: kernel_max_iter
    REAL(dp), ALLOCATABLE :: power_spectrum(:)
    REAL(dp), ALLOCATABLE :: kernel_weights(:)
    REAL(dp) :: kernel_sigma
    LOGICAL :: beads_same_temperature
    INTEGER :: bead_index
    REAL(dp), ALLOCATABLE :: omK2(:)
    LOGICAL :: write_bead_index
    LOGICAL :: classical_centroid


  ! For QTB analysis
    real(dp), allocatable :: vfspec_aver(:,:)
    real(dp), allocatable :: vvspec_aver(:,:)
    real(dp), allocatable :: ffspec_aver(:,:)
    real(dp), allocatable :: vfspec(:,:)
    real(dp), allocatable :: vvspec(:,:)
    real(dp), allocatable :: ffspec(:,:)
    real(dp), allocatable   :: d_FDR(:,:)
    real(dp), allocatable   :: d_FDR_aver(:,:)

    real(dp), allocatable :: veloc_store(:,:,:)

    ! QTB FORCE VECTOR
    real(dp), allocatable :: Force(:,:,:)
    ! gamma_random vector
    real(dp), allocatable :: gammar(:,:)
    real(dp) :: gammar_min
    REAL(dp), allocatable :: white_noise(:,:,:)

  ! For adQTB
    REAL(dp) :: Agammas

    INTEGER :: n_slices
    INTEGER :: n_start_adapt

    

    INTEGER, ALLOCATABLE :: gamma_io_unit(:)
    LOGICAL :: save_every_gamma
    LOGICAL :: custom_power_spectrum
  ! For adQTB-r

  CONTAINS
    PROCEDURE :: initialize => QTB_status_initialize
    PROCEDURE :: allocate => QTB_allocate
    PROCEDURE :: deallocate => QTB_deallocate
    PROCEDURE :: store_velocities => QTB_store_velocities
    PROCEDURE :: store_velocity_one_dof => QTB_store_velocity_one_dof
    PROCEDURE :: fill_random_vector => QTB_fill_random_vector
    PROCEDURE :: compute_spectra => QTB_compute_spectra
    PROCEDURE :: average_spectra => QTB_average_spectra
    PROCEDURE :: write_average_spectra => QTB_write_spectra
    PROCEDURE :: corr_colored_OU => QTB_corr_colored_OU
    PROCEDURE :: ff_theo => QTB_ff_theo
    PROCEDURE :: get_force => QTB_random_force
    PROCEDURE :: read_gammar => QTB_read_gammar
    PROCEDURE :: cutoff_fermi => QTB_cutoff_fermi
  END TYPE

  PRIVATE
  PUBLIC :: QTB_TYPE, QTB_theta

CONTAINS

  subroutine QTB_status_initialize(qtb,param_library,method_name, dt &
          ,gamma_in, PI_QTB, power_spectrum_in,OmK,classical_kernel,bead_index)
    IMPLICIT NONE
    CLASS(QTB_type), INTENT(inout) :: qtb
    TYPE(DICT_STRUCT), intent(in) :: param_library
    TYPE(DICT_STRUCT), pointer :: th_lib
    LOGICAL, intent(in) :: PI_QTB
    REAL(dp), INTENT(in) :: gamma_in,dt
    REAL(dp), INTENT(in), OPTIONAL :: power_spectrum_in(:)
    REAL(dp), INTENT(in), OPTIONAL :: OmK(:)
    LOGICAL, INTENT(in), OPTIONAL :: classical_kernel
    INTEGER, INTENT(in), OPTIONAL :: bead_index
    CHARACTER(:), ALLOCATABLE, INTENT(inout) :: method_name
    REAL(dp) :: mp
    CHARACTER(2) :: el
    INTEGER :: k,i,j
    real(dp) :: block_time

    qtb%element_dict => param_library%get_child("ELEMENTS")
    qtb%n_slices=1
    qtb%i_traj=0

    th_lib => param_library%get_child("QTB")

    qtb%save_average_spectra=th_lib%get("write_spectra" &
                          ,default=.TRUE.)
    !! @input_file QTB/write_spectra [LOGICAL] (qtb_types, default=.TRUE.)
    if(qtb%save_average_spectra) then
      qtb%write_spectra_by_element=th_lib%get("write_spectra_by_element" &
                          ,default=.TRUE.)
      !! @input_file QTB/write_spectra_by_element [LOGICAL] (qtb_types, default=.TRUE.)
    endif

    !GET QTB PARAMETERS
    qtb%dt=dt
    !! GET omegacut
    qtb%omegacut  = th_lib%get("cutoff")
    !! @input_file QTB/cutoff [REAL] (qtb_types)
    block_time = param_library%get("parameters/block_time")
    !! @input_file parameters/block_time [REAL] (qtb_types)
    qtb%n_rand = th_lib%get("n_rand",default=nint(block_time/qtb%dt))
    !! @input_file QTB/n_rand [INTEGER] (qtb_types, default=nint(block_time/dt))
    qtb%omega_smear = qtb%omegacut/50._dp
    qtb%domega      = 2._dp*pi/(3._dp*qtb%n_rand*qtb%dt) !2._dp*pi/(qtb%n_rand*qtb%dt)
    qtb%nom         = nint(qtb%omegacut/qtb%domega)

    !! GET gamma0
    qtb%gamma0=gamma_in
    qtb%gammar_min = qtb%gamma0/100._dp

    if(qtb%gamma0<=0) STOP "[qtb_types.f90] Error: 'damping' is not properly defined!"
    write(*,*) "qtb damping=",qtb%gamma0*au%THz,"THz"
    qtb%gamma_exp=exp(-qtb%gamma0*qtb%dt)

    if ( qtb%omegacut >= pi/qtb%dt ) &
      STOP '[qtb_types.f90] Error: the QTB cutoff frequency should be significantly &
          &less than 1/dt !'

    !CHECK IF PATH INTEGRALS CALCULATION
    qtb%write_bead_index=.FALSE.
    if(PI_QTB) then

      if(.not. present(omk)) STOP "[qtb_types.f90] Error: Omk must be provided for PIQTB"
      qtb%n_beads = size(omk)
      allocate(qtb%omK2(qtb%n_beads))      
      qtb%omK2 = omk**2
      !qtb%omegacut=qtb%omegacut + 1.5*omk(qtb%n_beads)

      qtb%PI_QTB=.TRUE.
      mp=th_lib%get("piqtb_mixing_power" &
              ,default=1._dp )
      !! @input_file QTB/piqtb_mixing_power [REAL] (qtb_types, dedault=1.)
      qtb%kernel_mixing=1._dp/real(qtb%n_beads,dp)**mp
      qtb%kernel_max_iter=th_lib%get("piqtb_kernel_max_iter" &
                      ,default=10000 )
      !! @input_file QTB/piqtb_kernel_max_iter [INTEGER] (qtb_types, dedault=10000)
      qtb%kernel_iter_prec=th_lib%get("piqtb_kernel_iter_prec" &
                      ,default=0.001_dp )
      !! @input_file QTB/piqtb_kernel_iter_prec [REAL] (qtb_types, dedault=0.001)
      qtb%kernel_sigma=th_lib%get("piqtb_corr_prop" &
                    ,default=0.2_dp )
      !! @input_file QTB/piqtb_corr_prop [REAL] (qtb_types, dedault=0.2)

      qtb%beads_same_temperature = th_lib%get("piqtb_beads_same_temperature",default=.FALSE.)      
      !! @input_file QTB/piqtb_beads_same_temperature [LOGICAL] (qtb_types, dedault=.FALSE.)
    
      qtb%classical_centroid = th_lib%get("piqtb_classical_centroid", default=.FALSE.)
      !! @input_file QTB/piqtb_classical_centroid [LOGICAL] (qtb_types, dedault=.FALSE.)
      if(present(bead_index)) then
          qtb%bead_index=bead_index
          qtb%write_bead_index = .TRUE.
      elseif(qtb%beads_same_temperature) then
        STOP "[qtb_types.f90] Error: bead_index not passed when initializing &
              & 'piqtb_beads_same_temperature'!"
      endif

      write(*,*) 'PI-QTB: initialization of mode '//int_to_str(qtb%bead_index,qtb%n_beads)

    ELSE
      qtb%PI_QTB=.FALSE.
      qtb%n_beads = 1
      if(present(bead_index)) then
        qtb%bead_index=bead_index
        qtb%write_bead_index=.TRUE.
      endif
    ENDIF

    
    qtb%noQTB=th_lib%get("noqtb", default=.FALSE.)
    !! @input_file QTB/noqtb [LOGICAL] (qtb_types, dedault=.FALSE.)
    if(qtb%noQTB) then
      write(*,*) "Using noQTB (white noise). ONLY FOR TESTING!"
    endif
    if(present(classical_kernel)) then
      if(classical_kernel) qtb%noQTB=.TRUE.
    endif


    !GET QTB method
    qtb%adQTBr=.FALSE.
    qtb%adapt_by_element=.FALSE.
    qtb%method = th_lib%get("qtb_method",DEFAULT="qtb_basic")
    !! @input_file QTB/qtb_method [STRING] (qtb_types, dedault="qtb_basic")
    qtb%method = to_lower_case(qtb%method)
        
    SELECT CASE(qtb%method)
    CASE("qtb_basic")
      write(*,*) "Initialize basic QTB."
      if(PI_QTB) then
        method_name="PIQTB"
      else
        method_name="QTB"
      endif
    CASE("adqtb-r","adqtbr","adqtb")
      write(*,*) "Initialize adQTB-r."
      if(PI_QTB) then
        method_name="PIadQTB"
      else
        method_name="adQTB"
      endif
      qtb%save_average_spectra=.TRUE.
      qtb%adQTBr=.TRUE.
      qtb%Agammas = th_lib%get("adqtb_agammas",default=10._dp)
      !! @input_file QTB/adqtb_agammas [REAL] (qtb_types, dedault=10.)

      !qtb%Agammax = th_lib%get("adqtb_agammax",default=1.0e-13)
      !write(*,*) "agammax=",qtb%Agammax

      qtb%n_start_adapt = th_lib%get("adqtb_n_start_adapt", default=5)
      !! @input_file QTB/adqtb_n_start_adapt [INTEGER] (qtb_types, dedault=5)
      qtb%adapt_by_element = th_lib%get("adqtb_adapt_by_element",default=.TRUE.)
      !! @input_file QTB/adqtb_adapt_by_element [LOGICAL] (qtb_types, dedault=.TRUE.)
      if(qtb%adapt_by_element) then
        qtb%write_spectra_by_element = .TRUE.
        write(*,*) "adQTB-r: adaptation by element"
      endif
    CASE DEFAULT
      write(0,*) "[qtb_types.f90] Error: unkown QTB method '"//qtb%method//"' !"
      STOP "Execution stopped"
    END SELECT

    !! ALLOCATE QTB ARRAYS
    call qtb%allocate()

    if(th_lib%has_key("gammar_file")) then
      call qtb%read_gammar(th_lib%get("gammar_file"))
      !! @input_file QTB/gammar_file [STRING] (qtb_types, default="")
    else
      qtb%gammar(:,:) = qtb%gamma0
    endif


    ! If the integrator contains an Ornstein-Uhlenbeck process,
    ! correct the colored noise accordingly
    qtb%OU_integrator = .true.

    qtb%selector=0

    if(present(power_spectrum_in) .AND. (.NOT. qtb%beads_same_temperature)) then
      if(size(power_spectrum_in) /= qtb%nom) &
        STOP "[qtb_types.f90] Error: wrong size of input QTB power spectrum!"
      qtb%power_spectrum=power_spectrum_in
      qtb%custom_power_spectrum = .TRUE.
    else
      call compute_power_spectrum(qtb)
    endif

    do j=1,ndim; do i=1,nat
      call randGaussN(qtb%white_noise(:,i,j))
    enddo; enddo
    call qtb%fill_random_vector()

    if(qtb%adQTBr) then
      qtb%save_every_gamma = th_lib%get("adqtb_save_every_gamma",default=.FALSE.)
      !! @input_file QTB/adqtb_save_every_gamma [LOGICAL] (qtb_types, default=.FALSE.)
      if(qtb%save_every_gamma) then
        if(qtb%adapt_by_element) then
          allocate(qtb%gamma_io_unit(n_elements))
          DO k=1,n_elements
            el=element_set(k)
            IF(qtb%write_bead_index) then
              open(newunit=qtb%gamma_io_unit(k), file=output%working_directory//&
                  &'/PIadQTB_gammas.out.'//trim(el)//'.mode'//int_to_str(qtb%bead_index,qtb%n_beads))
            else
              open(newunit=qtb%gamma_io_unit(k), file=output%working_directory//&
                  &'/adQTB_gammas.out.'//trim(el))
            endif
          ENDDO
        else
          allocate(qtb%gamma_io_unit(1))
          IF(qtb%write_bead_index) then
            open(newunit=qtb%gamma_io_unit(1), file=output%working_directory//&
                &'/PIadQTB_gammas.out.mode'//int_to_str(qtb%bead_index,qtb%n_beads))
          else
            open(newunit=qtb%gamma_io_unit(1), file=output%working_directory//&
                &'/adQTB_gammas.out')
          endif
        endif
      endif
    endif

  end subroutine QTB_status_initialize

!---------------------------------------------------
! QTB FORCE CALCULATIONS

  !FILL THE FORCE VECTOR WITH RANDOM FORCES (QTB SPECTRUM)
  subroutine QTB_fill_random_vector(qtb)
    IMPLICIT NONE
    CLASS(QTB_type), INTENT(inout) :: qtb

  ! local variables
    integer :: i,j,iom, ifail
    real(dp) :: omega
    real(dp), allocatable :: omega_range(:)
    real(dp) :: fmax, aux, interp
    complex(dp) :: U(3*qtb%n_rand), work(3*qtb%n_rand)
    real(dp) :: R(qtb%n_rand),Htilde(3*qtb%n_rand)
    integer :: itype,ntype

    if(qtb%custom_power_spectrum) then
      allocate(omega_range(qtb%nom))
      !INITIALIZE POWER SPECTRUM RANGE
      do iom=1,qtb%nom
        omega_range(iom)=real(iom-1,dp)*qtb%domega
      enddo
    endif

    Htilde(:)=0._dp
    Htilde(1)=1._dp
    if(qtb%OU_integrator)  Htilde(1) = sqrt(qtb%corr_colored_OU(0._dp))
    do iom=2,(3*qtb%n_rand)/2
      omega=(iom-1)*qtb%domega
      aux=qtb%cutoff_fermi(omega)
      if(qtb%custom_power_spectrum) then
        if (iom <= qtb%nom) then
          aux = aux * qtb%power_spectrum(iom)
        else
          interp=linear_interpolation( &
              omega,qtb%power_spectrum,omega_range,prop_ex=0.05)
          aux = aux * interp
        endif
      else
        aux=aux*QTB_theta(omega,kT)/kT
      endif
      if (qtb%OU_integrator) aux = aux * qtb%corr_colored_OU(omega)
      Htilde(iom)=sqrt(aux)
      Htilde(3*qtb%n_rand-iom+2)=sqrt(aux)
    enddo

    itype=1
    do j = 1,ndim ; do i = 1, nat
      if(qtb%write_spectra_by_element) then
        itype=qtb%element_dict%get(element_symbols(i)//"/set_index")
      else
        itype=(j-1)*nat+i
      endif

      U(1:2*qtb%n_rand) = cmplx(qtb%white_noise(:,i,j),kind=dp)
      qtb%white_noise(1:qtb%n_rand,i,j)=qtb%white_noise(qtb%n_rand+1:2*qtb%n_rand,i,j)
      call RandGaussN(R)
      qtb%white_noise(qtb%n_rand+1:2*qtb%n_rand,i,j)=R(:)
      U(2*qtb%n_rand+1:3*qtb%n_rand) = cmplx(R(:),kind=dp)

      call SPCFFT(U,3*qtb%n_rand,-1,work)
      U(:)=U(:)*Htilde(:)
      do iom=1,qtb%nom
        aux=sqrt(qtb%gammar(iom,itype)/qtb%gamma0)
        U(iom)=U(iom)*aux
        U(3*qtb%n_rand-iom+1)=U(3*qtb%n_rand-iom+1)*aux
      enddo
      call SPCFFT(U,3*qtb%n_rand,1,work)

      qtb%force(i,j,:)=real(U(qtb%n_rand+1:2*qtb%n_rand))/(3*qtb%n_rand) &
          *sqrt(2._dp*mass(i)*qtb%gamma0*qtb%n_beads*kT/qtb%dt)

    enddo ;enddo

  end subroutine QTB_fill_random_vector

  !returns a random force distributed with QTB
  function QTB_random_force(qtb,P) result(F)
    implicit none
    CLASS(QTB_type), INTENT(inout) :: qtb
    REAL(dp), intent(in) :: P(:,:)
    real(dp) :: F(nat,ndim)

    qtb%selector=qtb%selector+1
    IF(qtb%selector>qtb%n_rand) THEN
      CALL QTB_reinit(qtb)
    ENDIF

    F(:,:)=qtb%Force(:,:,qtb%selector)

    if(qtb%save_average_spectra) then
      !if(.NOT. qtb%PI_QTB) &
      call qtb%store_velocities(SQRT(qtb%gamma_exp)*P(:,:) &
               + 0.5_dp*qtb%dt*F(:,:))
    endif

  end function QTB_random_force


  !REINITIALIZE FORCE VECTOR IF EVERYTHING USED
  subroutine QTB_reinit(qtb)
    implicit none
    CLASS(QTB_type), INTENT(inout) :: qtb
    INTEGER :: i,j

    qtb%n_slices=qtb%n_slices+1

    if(qtb%save_average_spectra) then
      call qtb%compute_spectra()
    endif

    if (qtb%adQTBr .AND. (qtb%n_slices >= qtb%n_start_adapt)) then
      call adapt_gamma_r(qtb)
      call write_gamma_adapt(qtb)
    endif

    if(qtb%save_average_spectra) then
      call qtb%average_spectra()
      if(qtb%PI_QTB) then
        call qtb%write_average_spectra("PIQTB_spectra.out")
      else
        call qtb%write_average_spectra("QTB_spectra.out")
      endif
    endif

    call qtb%fill_random_vector()

    qtb%selector=1

  end subroutine QTB_reinit

!---------------------------------------------------

  function QTB_corr_colored_OU(qtb,omega) result(r)
   ! computes correction for a Ornstein-Uhlenbeck process
   ! with colored noise
    IMPLICIT NONE
    CLASS(QTB_type), INTENT(inout) :: qtb
    real(dp) :: omega,r

    r = ( 1._dp - 2._dp*exp(-qtb%gamma0*qtb%dt) * cos(omega*qtb%dt) &
      + exp(-2._dp*qtb%gamma0 *qtb%dt) ) / (qtb%dt**2 * (qtb%gamma0 **2 + omega **2))

  end function QTB_corr_colored_OU

  function QTB_theta(omega,kBT) result(r)
    implicit none
    real(dp) :: r,omega,kBT
    real(dp), parameter :: omega_min = TINY(omega)

    if (omega  > omega_min) then
      r = hbar*omega*0.5_dp/tanh(0.5_dp*(hbar*omega/kBT))
    !    r =  hbar*omega*(0.5_dp+1._dp/(exp(hbar*omega/kBT)-1._dp))
    else
      r = kBT
    endif

  end function QTB_theta

  function QTB_cutoff_fermi(qtb,omega) result(fermi)
    real(dp) :: fermi
    real(dp), intent(in) :: omega
    CLASS(QTB_type), INTENT(in) :: qtb

    fermi = 1._dp/(1._dp+exp((omega-qtb%omegacut)/qtb%omega_smear))

  end function QTB_cutoff_fermi


  function QTB_ff_theo(qtb,omega,mass) result(ff_theo)
    implicit none
    CLASS(QTB_type), INTENT(inout) :: qtb
    real(dp) :: ff_theo, omega, mass

    ff_theo = 2._dp * mass * qtb%gamma0 * QTB_theta(omega,kT)

  end function QTB_ff_theo

!----------------------------------------------------
! ADAPTATION ROUTINES

  subroutine adapt_gamma_r(qtb)
    implicit none
    CLASS(QTB_type), INTENT(inout) :: qtb
    integer :: i,j,iom,iel,k,n_atoms_of,ntype
    INTEGER, ALLOCATABLE :: indices(:)
    REAL(dp) :: AA
    CHARACTER(2) :: el

    if(qtb%adapt_by_element) then
      ntype=n_elements
    else
      ntype=ndof
    endif

    AA=qtb%Agammas*qtb%gamma0*real(qtb%n_rand,dp)*qtb%dt
    do k=1,ntype
      qtb%gammar(:,k)=max(qtb%gammar_min &
        ,qtb%gammar(:,k)+ AA*qtb%d_FDR(:,k)  &
                      /NORM2(qtb%d_FDR(:,k)) &
      )
    enddo

  end subroutine adapt_gamma_r


  subroutine write_gamma_adapt(qtb)
    implicit none
    CLASS(QTB_type), INTENT(inout) :: qtb
    integer :: iom,u,k,i,j,iel,n_atoms_of
    integer, allocatable :: indices(:)
    real(dp) :: omega,gamma_r, gamma_f ,n_dof_of
    CHARACTER(2) :: el

    if(qtb%adapt_by_element) then
      gamma_f=qtb%gamma0
      DO k=1,n_elements
        el=element_set(k)
        n_atoms_of=qtb%element_dict%get(el//"/n")
        n_dof_of=REAL(n_atoms_of*ndim,dp)
        indices=qtb%element_dict%get(el//"/indices")

        IF(qtb%write_bead_index) then
          open(newunit=u, file=output%working_directory//&
              &'/PIadQTB_gammas.last.'//trim(el)//'.mode'//int_to_str(qtb%bead_index,qtb%n_beads))
        else
          open(newunit=u, file=output%working_directory//&
              &'/adQTB_gammas.last.'//trim(el))
        endif
        !open(newunit=u,file=output%working_directory//"/adQTB_gammas.last."//trim(el))
        DO iom=1,qtb%nom
          omega = real(iom-1,dp) * qtb%domega
          if(qtb%save_every_gamma)  write(qtb%gamma_io_unit(k),*) omega*au%cm1 , qtb%gammar(iom,k)*au%THz
          write(u,*) omega*au%cm1 ,  qtb%gammar(iom,k)*au%THz
        ENDDO
        CLOSE(u)
        deallocate(indices)
      ENDDO

    else
      IF(qtb%write_bead_index) then
        open(newunit=u, file=output%working_directory//&
            &'/PIadQTB_gammas.last.mode'//int_to_str(qtb%bead_index,qtb%n_beads))
      else
        open(newunit=u, file=output%working_directory//&
            &'/adQTB_gammas.last')
      endif

      do iom = 1, qtb%nom
        omega = real(iom-1,dp) * qtb%domega
        gamma_r = sum(qtb%gammar(iom,:)) / real(ndof,dp)

        if(qtb%save_every_gamma) write(qtb%gamma_io_unit(1),*) omega*au%cm1 , gamma_r
        write(u,*) omega*au%cm1 , gamma_r*au%THz
      enddo

      close(u)
    endif

  end subroutine write_gamma_adapt

  subroutine QTB_read_gammar(qtb,gammar_file)
    IMPLICIT NONE
    CLASS(QTB_type), INTENT(inout) :: qtb
    CHARACTER(*), INTENT(in) :: gammar_file
    INTEGER :: u,iostat,i,k,iom,j,iel,n_atoms_of
    REAL(dp) :: om
    LOGICAL :: file_exist
    CHARACTER(2) :: el
    CHARACTER(:), ALLOCATABLE :: filename
    REAL(dp) :: gammar0
    INTEGER, ALLOCATABLE :: indices(:)

    if(qtb%write_spectra_by_element) then
      DO k=1,n_elements
        el=element_set(k)
        filename=gammar_file//"."//trim(el)
        IF(qtb%write_bead_index) filename=filename//".mode"//int_to_str(qtb%bead_index,qtb%n_beads)          
        INQUIRE(file=filename,exist=file_exist)
        if(.NOT. file_exist) then
          INQUIRE(file=output%working_directory//"/"//filename &
                ,exist=file_exist)
          if(.NOT. file_exist) then
            write(0,*) "[qtb_types.f90] Error: specified gammar file '"//filename&
                    &//"' does not exist!"
            STOP
          else
            open(newunit=u,file=output%working_directory//"/"//filename)
            write(*,*) "QTB: recovering gammar from '"&
                  &//output%working_directory//"/"//filename//"'"
          endif
        else
          open(newunit=u,file=filename)
          write(*,*) "QTB: recovering gammar from '"&
                  &//filename//"'"
        endif

        n_atoms_of=qtb%element_dict%get(el//"/n")
        indices=qtb%element_dict%get(el//"/indices")
        DO iom=1,qtb%nom
          read(u,*) om,gammar0
          qtb%gammar(iom,k)=gammar0/au%THz
        ENDDO
        close(u)
      ENDDO
    else
      filename=gammar_file
      IF(qtb%write_bead_index) filename=filename//".mode"//int_to_str(qtb%bead_index,qtb%n_beads)    
      INQUIRE(file=filename,exist=file_exist)
      if(.NOT. file_exist) then
        INQUIRE(file=output%working_directory//"/"//filename,exist=file_exist)
        if(.NOT. file_exist) then
          STOP "[qtb_types.f90] Error: specified gammar_file does not exist!"
        else
          open(newunit=u,file=output%working_directory//"/"//filename)
          write(*,*) "QTB: recovering gammar from '"&
                  &//output%working_directory//"/"//filename//"'"
        endif
      else
        open(newunit=u,file=filename)
        write(*,*) "QTB: recovering gammar from '"&
                  &//filename//"'"
      endif
      DO i=1,qtb%nom
        read(u,*) om,gammar0
        qtb%gammar(i,:)=gammar0/au%THz
      ENDDO
      close(u)
    endif

  end subroutine QTB_read_gammar


!----------------------------------------------------
! QTB ANALYSIS TOOLS
! Compute velocity autocorrelation and velocity random force cross-correlation
! spectra
! Mandatory for adQTB-r and highly recommended for QTB and adQTB-f

  ! velocity storage
  subroutine QTB_store_velocities(qtb,momentum)
  implicit none
    CLASS(QTB_TYPE), INTENt(inout) :: qtb
    REAL(dp), INTENT(in) :: momentum(:,:)
    INTEGER             :: i

  DO i=1,ndim
    qtb%veloc_store(:,i,qtb%selector) = momentum(:,i)/mass(:)
  ENDDO

  end subroutine QTB_store_velocities

  subroutine QTB_store_velocity_one_dof(qtb,momentum,at,dim)
  implicit none
    CLASS(QTB_TYPE), INTENt(inout) :: qtb
    REAL(dp), INTENT(in) :: momentum(:,:)
    INTEGER, INTENT(in) :: at, dim
    INTEGER             :: i

  qtb%veloc_store(at,dim,qtb%selector) = momentum(at,dim)/mass(at)

  end subroutine QTB_store_velocity_one_dof


  subroutine QTB_compute_spectra(qtb)
    implicit none
    CLASS(QTB_TYPE), INTENt(inout) :: qtb
    integer :: i, j, istep,jstep, ifail,count,k
    complex(dp) :: Uv(3*qtb%n_rand),Uf(3*qtb%n_rand), work(3*qtb%n_rand)
    real(dp) :: norm,n_dof_of
    integer :: itype,n_atoms_of

    norm=2._dp*qtb%dt/(3._dp*qtb%n_rand)
    do j = 1,ndim ;do i=1,nat
      if(qtb%write_spectra_by_element) then
        itype=qtb%element_dict%get(element_symbols(i)//"/set_index")
      else
        itype=(j-1)*nat+i
      endif

      Uv = cmplx(0._dp,kind=dp)
      Uf = cmplx(0._dp,kind=dp)
      Uv(1:qtb%n_rand) = cmplx(qtb%veloc_store(i,j,1:qtb%n_rand),kind=dp)
      Uf(1:qtb%n_rand) = cmplx(qtb%Force(i,j,1:qtb%n_rand),kind=dp)
      call SPCFFT(Uv,3*qtb%n_rand,-1,work)
      call SPCFFT(Uf,3*qtb%n_rand,-1,work)

      do istep = 1, qtb%nom
        qtb%vfspec(istep,itype) = qtb%vfspec(istep,itype) &
          + norm*(real(Uv(istep))*real(Uf(istep)) + aimag(Uv(istep))*aimag(Uf(istep)) ) &
          / qtb%gammar(istep,itype)
        qtb%vvspec(istep,itype) = qtb%vvspec(istep,itype) &
          + norm*(real(Uv(istep))**2 + aimag(Uv(istep))**2)*mass(i)
        qtb%ffspec(istep,itype) = qtb%ffspec(istep,itype) &
          + norm*(real(Uf(istep))**2 + aimag(Uf(istep))**2)
      enddo
    
    enddo; enddo

    if(qtb%write_spectra_by_element) then
      do k=1,n_elements        
        n_atoms_of=qtb%element_dict%get(element_set(k)//"/n")
        n_dof_of=real(ndim*n_atoms_of,dp)
        qtb%vfspec(:,k)=qtb%vfspec(:,k)/n_dof_of
        qtb%vvspec(:,k)=qtb%vvspec(:,k)/n_dof_of
        qtb%ffspec(:,k)=qtb%ffspec(:,k)/n_dof_of
        qtb%d_FDR(:,k) = qtb%gammar(:,k) &
          *(qtb%vfspec(:,k) - qtb%vvspec(:,k))
      enddo
    else
      do k = 1,ndof
        qtb%d_FDR(:,k) = qtb%gammar(:,k) &
          *(qtb%vfspec(:,k) - qtb%vvspec(:,k))
      enddo
    endif    

  end subroutine QTB_compute_spectra


  subroutine QTB_average_spectra(qtb)
    implicit none
    CLASS(QTB_TYPE), INTENT(inout) :: qtb

    qtb%i_traj=qtb%i_traj+1

    qtb%ffspec_aver = qtb%ffspec_aver &
      + (qtb%ffspec(:,:)-qtb%ffspec_aver)/REAL(qtb%i_traj,dp)
    qtb%vvspec_aver = qtb%vvspec_aver &
      + (qtb%vvspec(:,:)-qtb%vvspec_aver)/REAL(qtb%i_traj,dp)
    qtb%vfspec_aver = qtb%vfspec_aver &
      + (qtb%vfspec(:,:)-qtb%vfspec_aver)/REAL(qtb%i_traj,dp)
    qtb%d_FDR_aver = qtb%d_FDR_aver &
      + (qtb%d_FDR(:,:)-qtb%d_FDR_aver)/REAL(qtb%i_traj,dp)


  end subroutine QTB_average_spectra


  subroutine QTB_write_spectra(qtb,filename,restart_average)
    implicit none
    integer   :: l_step,istep,jstep,u,u1,i,j,k,n_atoms_of,iel,count
    real(dp)  :: omega,n_dof_of,om1,om2
    CLASS(QTB_TYPE), INTENT(inout) :: qtb
    LOGICAL, INTENT(in), OPTIONAL :: restart_average
    CHARACTER(*), INTENT(in) :: filename
    INTEGER, allocatable :: indices(:)
    LOGICAL :: restart_after_thermalization
    CHARACTER(2) :: el
    CHARACTER(:), allocatable :: outfile

    restart_after_thermalization=.FALSE.

    if(qtb%write_spectra_by_element) then
      do k=1,n_elements
        outfile = trim(filename)//"."//trim(element_set(k))
        IF(qtb%write_bead_index) outfile=outfile//".mode"//int_to_str(qtb%bead_index,qtb%n_beads)
        open(newunit=u, file=output%working_directory//'/'//outfile)
        write(u,*) "#omega(cm^-1)  mCvv   Cvf/gammar   dFDT   gammar"
        DO l_step = 1, qtb%nom
          omega = real(l_step-1,dp) * qtb%domega
          write(u,*) omega* au%cm1 ,  qtb%vvspec_aver(l_step,k) &
                , qtb%vfspec_aver(l_step,k), qtb%d_FDR_aver(l_step,k) &
                , qtb%gammar(l_step,k)*au%THz
        ENDDO
        close(u)
      enddo
    else
      outfile = trim(filename)//".mean"
      IF(qtb%write_bead_index) outfile=outfile//".mode"//int_to_str(qtb%bead_index,qtb%n_beads)          
      open(newunit=u, file=output%working_directory//'/'//outfile)
      write(u,*) "#omega(cm^-1)    mCvv   Cvf/gammar   dFDT   gammar  Cff   Cff_theo"
      DO l_step = 1, qtb%nom
        omega = real(l_step-1,dp) * qtb%domega
        write(u,*) omega* au%cm1 , SUM(qtb%vvspec_aver(l_step,:)) / real(ndof , dp) &
        , sum(qtb%vfspec_aver(l_step,:)) / real(ndof , dp) &
        , sum(qtb%d_FDR_aver(l_step,:))/real(ndof,dp) &
        , sum(qtb%gammar(l_step,:))/real(ndof,dp)*au%THz &
        , sum(qtb%ffspec_aver(l_step,:)) / real(ndof, dp) &
        , qtb%ff_theo(omega, sum(mass/real(nat , dp))) 
      ENDDO
      close(u)
    endif

    if(present(restart_average)) then
      if(restart_average) then
        qtb%ffspec_aver=0._dp
        qtb%vvspec_aver=0._dp
        qtb%vfspec_aver=0._dp
        qtb%d_FDR_aver=0._dp
        qtb%i_traj=0
      endif
    endif

  end subroutine QTB_write_spectra

!----------------------------------------------------------

  subroutine compute_power_spectrum(qtb)
    IMPLICIT NONE
    CLASS(QTB_TYPE), intent(inout) :: qtb
    REAL(dp) :: P,du,u,u0, omegamin2,bead_contrib,u02
    REAL(dp) ,ALLOCATABLE :: u_range(:),uk_range(:,:), Fp(:) &
                  ,Fp_prev(:),Fp0(:),targ(:),mu(:) &
                  , err_it(:), err_w(:)!, u0k(:)
    REAL(dp), DIMENSION(:,:), ALLOCATABLE :: A,gmat,gmat_inv
    REAL(dp) :: umax,tmax
    INTEGER :: i,k,unit,unit_err,nom,iom
    LOGICAL :: converged,converged_weights

    qtb%custom_power_spectrum=.TRUE.

    if(qtb%noQTB) then  !QTB with white noise for TESTING purpose
      converged_weights=.TRUE.
      qtb%power_spectrum(:)=1._dp
    elseif(qtb%PI_QTB) then
      P=real(qtb%n_beads,dp)
      IF(qtb%beads_same_temperature) THEN
        converged_weights=.TRUE.
        if(qtb%bead_index==0 .AND. qtb%classical_centroid) then
          qtb%power_spectrum(:)=1._dp
        else
          omegamin2 = qtb%omK2(qtb%bead_index+1)
          do i=1,qtb%nom
            u=(i-1)*qtb%domega
            u02 = u*u - omegamin2
            if(u02 > 0) then
              u0 = sqrt(u02)
              bead_contrib=0._dp
              DO k=2,qtb%n_beads
                bead_contrib = bead_contrib &
                  + u02/(u02 + qtb%omK2(k))
              ENDDO
              if(qtb%classical_centroid) then
                qtb%power_spectrum(i) = (QTB_theta(u0,kT)/kT-1._dp)/bead_contrib
              else
                qtb%power_spectrum(i) = QTB_theta(u0,kT)/kT/(bead_contrib+1._dp)
              endif
            else
              ! bead_contrib=0._dp
              ! DO k=2,qtb%n_beads
              !   bead_contrib = bead_contrib &
              !     + 1/qtb%omK2(k)
              ! ENDDO
              qtb%power_spectrum(i)=1._dp !P*kT !P/(12*kT*bead_contrib)
            endif
          enddo
        endif
        open(newunit=unit, file=output%working_directory//"PIQTB_kernel.out.mode"&
                &//int_to_str(qtb%bead_index,qtb%n_beads))
        do i=1,qtb%nom
          write(unit,*) (i-1)*qtb%domega*au%cm1, qtb%power_spectrum(i)!*au%kelvin/P
        enddo
        close(unit)
      ELSE
        !PI-QTB ITERATIVE SCHEME (cf. Fabien Brieuc)
        du=0.5_dp*qtb%domega/kT
        nom=qtb%nom  
        write(*,*) "du_nom_PIQTB",du  ,nom        
        do          
          umax=du*nom      
          tmax=tanh(umax/P)/tanh(umax)
          if(abs(tmax-1._dp)<1._dp/qtb%n_beads**2) exit
          nom=nom*2
        enddo   
        write(*,*) "du_nom_PIQTB_after=",du,nom,du*nom

        allocate(u_range(nom),Fp(nom),Fp0(nom),Fp_prev(nom),targ(nom))
        allocate(uk_range(nom,0:qtb%n_beads-1))!,u0k(0:qtb%n_beads-1))

        !initialize u_range and first guess for Fp
        do  i=1,nom
          u=i*du
          u_range(i)=u
          do k=0,qtb%n_beads-1
            uk_range(i,k)=SQRT(u**2 + (sin(k*pi/P))**2)
          enddo
          !write(0,*) u*(2*P*kT)*THz/(2*pi),uk_range(i,:)*THz/(2*pi)*(2*P*kT)
        enddo
        !INITIAL GUESS
        Fp(:)=1._dp
        targ(:)=P*tanh(u_range/P)/tanh(u_range)
        !write(0,*) targ

        qtb%kernel_mixing = 1._dp/P

      ! n_ex=max(2,nint(0.2*qtb%nom))
        converged=.FALSE.
        do i=1,qtb%kernel_max_iter
        ! call linear_fit(u_range(qtb%nom-n_ex+1:),Fp(qtb%nom-n_ex+1:),0.0001_dp,beta)
          Fp0(:)=0._dp
          DO k=1,qtb%n_beads-1
            Fp0(:)=Fp0(:) + u_range*tanh(u_range/P)/(uk_range(:,k)*tanh(uk_range(:,k)/P)) &
              *linear_interpolation_clip(uk_range(:,k),Fp,u_range,1._dp,1._dp)
          ENDDO
          err_it=abs(Fp+Fp0-targ)/targ
          !write(*,*) i,maxval(err_it)*100.
          if( maxval(err_it) <= qtb%kernel_iter_prec ) then
            write(*,*) "PI-QTB: kernel converged in",i,"iterations."
            converged=.TRUE.
            exit
          endif
          Fp=(targ-Fp0)*qtb%kernel_mixing + (1._dp-qtb%kernel_mixing)*Fp
          !Fp=kernel_iteration(Fp_prev,u_range,uk_range,qtb%kernel_mixing,qtb%n_beads)
        enddo
        if(.not. converged) then
          write(0,*) "[qtb_types.f90] Error: PI-QTB kernel not converged in ",qtb%kernel_max_iter,"iterations !"
          STOP "Execution stopped."
        endif

        !STORE POWER SPECTRUM
        qtb%power_spectrum(1)=1._dp !P*kT
        do i=2,qtb%nom
          qtb%power_spectrum(i)=Fp(i)*u_range(i)/(P*tanh(u_range(i)/P))  
        enddo 

        open(newunit=unit,file=output%working_directory//"/PIQTB_kernel.out")
        open(newunit=unit_err,file=output%working_directory//"/PIQTB_kernel_errors.out")
        do  i=1,qtb%nom
          !u=(i-1)*du*(2*P*kT)
          u=(i-1)*qtb%domega
          write(unit,*) u*au%cm1,  qtb%power_spectrum(i), QTB_theta(u,kT)/kT !kT*Fp(i)*au%kelvin,kT*Fp0(i)*au%kelvin
          write(unit_err,*) u*au%cm1, err_it(i),Fp(i)+Fp0(i),targ(i)
        enddo
        write(*,*) "PI-QTB: kernel max error=",maxval(err_it),"%"
        close(unit)
        close(unit_err)
      ENDIF
    else !STANDARD QTB
      qtb%custom_power_spectrum=.FALSE.
      do i=1,qtb%nom
        u=(i-1)*qtb%domega
        qtb%power_spectrum(i)=QTB_theta(u,kT)/kT
      enddo
      converged_weights=.TRUE.
    endif

    if(.not. converged_weights) then
      write(0,*) "[qtb_types.f90] Error: could not converge QTB kernel in ",qtb%kernel_max_iter,"iterations !"
      STOP "Execution stopped."
    endif

  end subroutine compute_power_spectrum

  subroutine compute_intermediate_matrices(qtb,A,gmat,gmat_inv,mu,uk,nom)
    IMPLICIT NONE
    CLASS(QTB_TYPE), intent(inout) :: qtb
    INTEGER, INTENT(in) :: nom
    REAL(dp), DIMENSION(:,:), INTENT(INOUT) :: A,gmat,gmat_inv
    REAL(dp), DIMENSION(:), INTENT(INOUT) :: mu
    REAL(dp), INTENT(in) :: uk(nom,0:qtb%n_beads-1)
    INTEGER :: k, INFO,i
    REAL(dp), allocatable :: gk(:,:),v(:),Id(:,:)
    REAL(dp) :: lamb

    allocate(Id(nom,nom))
    Id=0
    lamb=0.0001
    do i=1,nom ; Id(i,i)=1 ; enddo
    gmat=gauss_kernel(uk(:,0),uk(:,0),qtb%kernel_sigma)
    gmat_inv=sym_mat_inverse(gmat+lamb*Id,INFO)
    if(INFO/=0) &
      STOP "[qtb_types.f90] Error: could not inverse gaussian kernel matrix"
    write(*,*) "PI-QTB: gaussian kernel inversion error:",sum((matmul(gmat,gmat_inv)-Id)**2)

    allocate(gk(nom,nom),v(nom))
    mu=0._dp
    A=0._dp
    do k=0,qtb%n_beads-1
      gk=gauss_kernel(uk(:,k),uk(:,0),qtb%kernel_sigma)
      v=(uk(:,0)/uk(:,k))**2
      mu=mu+h(uk(:,k))*v(:)
      DO i=1,nom
        A(i,:)=A(i,:)+gk(i,:)*v(i)
      ENDDO
    enddo

  end subroutine compute_intermediate_matrices

  function predict_F(qtb,u_n,u) result(F)
    CLASS(QTB_TYPE), intent(inout) :: qtb
    REAL(dp), INTENT(in) :: u(:),u_n(:)
    REAL(dp) :: F(size(u_n)), g(size(u_n),size(u))

    g=gauss_kernel(u_n,u,qtb%kernel_sigma)
    F=matmul(g,qtb%kernel_weights)+h(u_n)

  end function predict_F

  function gauss_kernel(u,v,width) result(g)
    IMPLICIT NONE
    REAL(dp), INTENT(in) :: u(:), v(:), width
    REAL(dp) :: g(size(u),size(v))
    INTEGER :: i,j

    do i=1,size(u) ; do j=1,size(v)
      g(i,j)=u(i)-v(j)
    enddo ; enddo
    g=exp(-g**2/(2._dp*width**2))

  end function gauss_kernel

  function get_kernel_errors_weights(qtb,uk,nom) result(error)
    IMPLICIT NONE
    CLASS(QTB_TYPE), intent(inout) :: qtb
    INTEGER, INTENT(in) :: nom
    REAL(dp), INTENT(in) :: uk(nom,0:qtb%n_beads-1)
    REAL(dp) :: error(nom),F(nom,0:qtb%n_beads-1),hp(nom)
    INTEGER :: k ,iom

    DO k=0,qtb%n_beads-1
      F(:,k)=predict_F(qtb,uk(:,k),uk(:,0))
    ENDDO
    hp=h(real(qtb%n_beads,dp)*uk(:,0))

    DO iom=1,nom
      error(iom)=( hp(iom)-sum(F(iom,:)*(uk(iom,0)/uk(iom,:))**2) )/hp(iom)
    ENDDO

  end function get_kernel_errors_weights

  function get_kernel_errors_iter(qtb,uk,F0,nom) result(error)
    IMPLICIT NONE
    CLASS(QTB_TYPE), intent(inout) :: qtb
    INTEGER, INTENT(in) :: nom
    REAL(dp), INTENT(in) :: uk(nom,0:qtb%n_beads-1),F0(:)
    REAL(dp) :: error(nom),F(nom,0:qtb%n_beads-1),hp(nom)
    INTEGER :: k ,iom

    DO k=0,qtb%n_beads-1
      F(:,k)=linear_interpolation(uk(:,k),F0,uk(:,0),prop_ex=0.05)
    ENDDO
    hp=h(real(qtb%n_beads,dp)*uk(:,0))

    DO iom=1,nom
      error(iom)=( hp(iom)-sum(F(iom,:)*(uk(iom,0)/uk(iom,:))**2) )/hp(iom)
    ENDDO

  end function get_kernel_errors_iter

  function h(u)
    IMPLICIT NONE
    REAL(dp), intent(in) :: u(:)
    REAL(dp) :: h(size(u))
    integer :: i

    DO i=1,size(u)
      if(u(i)<=TINY(u(i))) then
        h=1._dp
      else
        h=u(i)/tanh(u(i))
      endif
    ENDDO


  end function h

  ! function kernel_iteration(Fp,u_range,uk_range,alpha,n_beads,beta) result(Fp_new)
  !     IMPLICIT NONE
  !     REAL(dp),intent(in):: Fp(:), u_range(:),uk_range(:,:), alpha
  !     REAL(dp), intent(in), OPTIONAL :: beta(2)
  !     INTEGER, intent(in) :: n_beads
  !     REAL(dp) :: Fp_new(size(Fp))
  !     INTEGER :: i,k
  !     REAL(dp) :: u,uk,P

  !     P=real(n_beads,dp)

  !     do i=1,size(Fp)
  !         u=u_range(i)
  !         Fp_new(i)=h(u)
  !         do k=1,n_beads-1
  !             uk=uk_range(i,k)
  !             Fp_new(i)=Fp_new(i)-((u/uk)**2)*linear_interpolation(uk*sqrt(P),Fp,u_range,prop_ex=0.05)
  !         enddo
  !       !  Fp_new(i)=Fp_new(i)*alpha/P + (1-alpha)*Fp(i)
  !     enddo

  !     !MIXING
  !     Fp_new=Fp_new*alpha/P + (1-alpha)*Fp

  ! end function kernel_iteration

!-------------------------------------------------------

  subroutine QTB_deallocate(qtb)
    IMPLICIT NONE
    CLASS(QTB_TYPE) :: qtb

    ! Deallocation of QTB variables
    IF ( allocated(qtb%Force) ) deallocate(qtb%Force)
    IF ( allocated(qtb%gammar) ) deallocate(qtb%gammar)
    IF ( allocated(qtb%power_spectrum) ) deallocate(qtb%power_spectrum)

    IF ( allocated(qtb%d_FDR) ) deallocate( qtb%d_FDR )
    IF ( allocated( qtb%d_FDR_aver )) deallocate( qtb%d_FDR_aver )
    !QTB ANALYSIS
    IF ( allocated(qtb%vfspec) ) deallocate( qtb%vfspec )
    IF ( allocated(qtb%vvspec) ) deallocate( qtb%vvspec )
    IF ( allocated(qtb%ffspec) ) deallocate( qtb%ffspec )
    IF ( allocated(qtb%veloc_store) ) deallocate( qtb%veloc_store )
    IF ( allocated(qtb%ffspec_aver) ) deallocate( qtb%ffspec_aver )
    IF ( allocated(qtb%vfspec_aver) ) deallocate( qtb%vfspec_aver )
    IF ( allocated(qtb%vvspec_aver) ) deallocate( qtb%vvspec_aver )

  end subroutine QTB_deallocate

  subroutine QTB_allocate(qtb)
    IMPLICIT NONE
    CLASS(QTB_TYPE) :: qtb
    integer :: ntype

    call qtb%deallocate()

    if(qtb%write_spectra_by_element) then
      ntype=n_elements
    else
      ntype=ndof
    endif

    ! Allocate mandatory QTB Variables
    allocate( qtb%Force(nat,ndim,qtb%n_rand))
    allocate( qtb%gammar(qtb%nom,ntype))
    allocate( qtb%power_spectrum(qtb%nom) )
    allocate( qtb%white_noise(2*qtb%n_rand,nat,ndim) )

    !QTB analysis
    if  (qtb%save_average_spectra) then
      allocate( qtb%d_FDR(qtb%nom,ntype) )
      allocate( qtb%vfspec(qtb%nom,ntype) )
      allocate( qtb%vvspec(qtb%nom,ntype) )
      allocate( qtb%ffspec(qtb%nom,ntype) )
      allocate( qtb%veloc_store(nat,ndim,qtb%n_rand) )
      allocate( qtb%d_FDR_aver(qtb%nom,ntype) )
      allocate( qtb%vfspec_aver(qtb%nom,ntype)  )
      allocate( qtb%vvspec_aver(qtb%nom,ntype)  )
      allocate( qtb%ffspec_aver(qtb%nom,ntype)  )
      qtb%vfspec_aver(:,:) = 0._dp
      qtb%vvspec_aver(:,:) = 0._dp
      qtb%ffspec_aver(:,:) = 0._dp
      qtb%d_FDR_aver(:,:) = 0._dp
    endif

  end subroutine QTB_allocate

END MODULE qtb_types
