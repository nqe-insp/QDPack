PROGRAM ClMD
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
  use classical_md
  use system_commons
  use correlation_functions
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use trajectory_files
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library	
  type(tcf_type) :: tcf

  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real ::  remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  integer :: avg_counter
  logical :: verbose

  integer :: nan_count,max_nan,c
  integer :: ux,up
  logical :: save_trajectory,ignore_point, kubo_transform=.FALSE.

  integer :: nsteps,nblocks,iblock,write_stride,istep,n_threads,nblocks_therm
  real(dp) :: block_time,dt
  logical :: resample_momentum
  real(dp) :: gamma_smooth, spectrum_cutoff

  real(dp), allocatable :: X(:,:),P(:,:),Xtraj(:,:,:),Ptraj(:,:,:),mutraj(:,:)
  real(dp), allocatable :: mu0(:),mutmp(:)

  character(:), allocatable :: tcf_name

  TYPE(TRAJ_FILE) :: trajfile_xyz,samplefile_xyz
  logical :: save_samples

  call full_timer%start()
  call cpu_time(cpu_time_start)
  avg_counter = 0
  step_cpu_time_avg = 0
  step_time_avg =0

  call initialize_framework(param_library)
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	
  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE  
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)

  call initialize_cl_sampler(param_library,tcf_name,dt,"parameters")
  block_time = param_library%get("parameters/block_time")
  !! @input_file parameters/block_time [REAL] (ClMD) 
  nsteps=nint(block_time/dt)
  nblocks = param_library%get("parameters/nblocks")
  !! @input_file parameters/nblocks [INTEGER] (ClMD) 
  nblocks_therm = param_library%get("parameters/nblocks_therm",default=0)
  !! @input_file parameters/nblocks_therm [INTEGER] (ClMD, default=0) 
  write_stride = param_library%get("OUTPUT/write_stride",default=1)
  !! @input_file OUTPUT/write_stride [INTEGER] (ClMD, default=1) 
  resample_momentum = param_library%get("parameters/resample_momentum",default=.FALSE.)
  !! @input_file parameters/resample_momentum [LOGICAL] (ClMD, default=.FALSE.) 
  save_trajectory = param_library%get("OUTPUT/save_trajectory",default=.FALSE.)
  !! @input_file OUTPUT/save_trajectory [LOGICAL] (ClMD, default=.FALSE.) 
  save_samples = param_library%get("OUTPUT/save_samples",default=.TRUE.)
  !! @input_file OUTPUT/save_samples [LOGICAL] (ClMD, default=.TRUE.)

  gamma_smooth = param_library%get("parameters/gamma_smooth",default=-1._dp)
  !! @input_file parameters/gamma_smooth [REAL] (ClMD, default=-1.) 
  spectrum_cutoff = param_library%get("parameters/spectrum_cutoff",default=-1._dp)  
  !! @input_file parameters/spectrum_cutoff [REAL] (ClMD, default=-1.)  
  
  if(to_upper_case(tcf_name)/="CLMD") then
    kubo_transform = .TRUE.
    resample_momentum = .FALSE.
    write (*,*) "QTB simulation => apply kubo transform"
  endif
  call initialize_tcf(tcf,tcf_name,dt,nsteps,1 &
                          ,gamma_smooth,spectrum_cutoff,kubo_transform)
  
  !! initialize position and momentum
  allocate(X(nat,ndim),P(nat,ndim))
  X(:,:)=Xinit(:,:)
  call sample_classical_momentum(P)
  
  allocate(Xtraj(nat,ndim,nsteps),Ptraj(nat,ndim,nsteps))
  Xtraj(:,:,:)=0._dp
  Ptraj(:,:,:)=0._dp
  if(charges_provided) then
    allocate(mutraj(ndim,nsteps),mu0(ndim),mutmp(ndim))
    mutraj(:,:)=0._dp
  endif

  max_nan = param_library%get("parameters/max_nan",default=10)
  !! @input_file parameters/max_nan [INTEGER] (ClMD, default=10)  
  nan_count=0

  !! open trajectory files
  if(save_trajectory) then
    if(.not.param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")
    if(xyz_output) then
      call trajfile_xyz%init("trajectory.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
      call trajfile_xyz%open(output%working_directory,append=.TRUE.)
    else 
      open(newunit=ux,file=output%working_directory//"X.traj")
    endif
    open(newunit=up,file=output%working_directory//"P.traj")
  endif
  !! open sample files
  if(save_samples) then
    if(.not.param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")
    if(xyz_output) then
      call output%create_file("sample_position.xyz",tinker_xyz=tinker_xyz)
      call output%add_xyz_to_file(X,element_symbols,unit=xyz_unit)
    else 
      call output%create_file("sample_position")
      call output%add_array_to_file(X,nat*ndim)
    endif
    call output%create_file("sample_momentum")
    call output%add_array_to_file(P,nat*ndim)
  endif


  !-----------------------------------
  call open_output_files(param_library)

  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  ! THERMALIZATION
  if(nblocks_therm>0) then
    write(*,*)
    write(*,*) "Starting "//int_to_str(nblocks_therm)//" blocks of thermalization"
    !! thermalization loop
    DO iblock=1,nblocks_therm
      call compute_block(iblock,.TRUE.)
    ENDDO
    write(*,*) "Thermalization done."
    write(*,*)
  endif

  if(qtb_thermostat) then
    !! save QTB thermalization spectra and restart averaging
    if(qtb%save_average_spectra) &
      call qtb%write_average_spectra("QTB_spectra.thermalization.out",restart_average=.TRUE.)
  endif
  
  write(*,*)
  write(*,*) "Starting "//int_to_str(nblocks)//" blocks of "//tcf_name
  !! main loop
  DO iblock=1,nblocks    
    call compute_block(iblock,.FALSE.)
  ENDDO

  !! save final TCFs
  call write_tcf_results(tcf,1._dp)
  !! close trajectory files
  if(save_trajectory) then
    if(xyz_output) then
     call trajfile_xyz%destroy()
    else
      close(ux)
    endif
    close(up)
  endif
  
  call output%destroy()
  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS

  subroutine compute_block(iblock,thermalization)
    IMPLICIT NONE
    integer, intent(in) :: iblock
    logical, intent(in) :: thermalization
    logical :: save_results

    save_results = .NOT. thermalization

    !! start timers
    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1

    !! print current block
    if(mod(iblock,write_stride)==0) then
      write(*,*)
      if(thermalization) then
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks_therm)
      else
        write(*,*) "BLOCK "//int_to_str(iblock)//" / "//int_to_str(nblocks)
      endif
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    if(resample_momentum) call sample_classical_momentum(P)
    !! continue trajectory and generate a new block
    call update_cl_sample(X,P,nsteps,verbose,Xtraj,Ptraj)       

    ignore_point = ANY(ISNAN(Xtraj)) .OR. ANY(ISNAN(Ptraj))

    if(ignore_point) then !! ignore the block if the trajectory exploded 

      !! update NaN count
      nan_count = nan_count+1
      if(nan_count>=max_nan) then
        write(0,*) "[ClMD.f90] Error: ", max_nan," NaN trajectories reached!"
        STOP "Execution stopped."
      endif
      !! reset to a random momentum
      call sample_classical_momentum(P)
      X=Xtraj(:,:,1)

    elseif(save_results) then
      call output%write_all()
      !! compute time correlation functions from the last block
      call compute_autoTCF_WK(RESHAPE(Ptraj,(/ndof,nsteps/)),nsteps,dt,tcf%Cpp(:,:,1))
      if(charges_provided) then !! compute dipole velocity trajectory
        DO istep=1,nsteps
          call get_dipole(Xtraj(:,:,istep),Ptraj(:,:,istep),mutraj(:,istep))          
        ENDDO
        call compute_autoTCF_WK(mutraj,nsteps,dt,tcf%Cmumu(:,:,1))
      endif
      !! update the average TCFs
      call update_tcf_avg(tcf)
      
      !! save last block if necessary
      if(save_trajectory) then
        !! write block
        if(xyz_output) then
          DO istep=1,nsteps
            call trajfile_xyz%dump_xyz(Xtraj(:,:,istep),element_symbols)
            write(up,*) Ptraj(:,:,istep)
          ENDDO
        else
          DO istep=1,nsteps
            write(ux,*) Xtraj(:,:,istep)
            write(up,*) Ptraj(:,:,istep)
          ENDDO
        endif
        !! close trajectory files if necessary
      endif

    endif 

    !! update timer averages
    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter
    
    !! print some info and save intermediate results
    if(verbose) then
      if(nan_count>0) write(*,*) "NaN_count= "//int_to_str(nan_count)
      write(*,*) "Average time per block: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."
      
      if(thermalization) then
        remaining_time = step_time_avg*(nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated thermalization time : "//sec2human(job_time)
        write(*,*) "estimated remaining thermalization time : "//sec2human(remaining_time)
        remaining_time = step_time_avg*(nblocks+nblocks_therm-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      else
        remaining_time = step_time_avg*(nblocks-iblock)
        job_time = full_timer%elapsed_time() + remaining_time
        write(*,*) "estimated job time : "//sec2human(job_time)
        write(*,*) "estimated remaining time : "//sec2human(remaining_time)
      endif

      ! RESET WINDOW AVERAGES
      avg_counter=0
      step_time_avg=0.
      step_cpu_time_avg=0.

      ! WRITE INTERMEDIATE RESULTS
      if(save_results) then
        call write_tcf_results(tcf,1._dp)
      endif
    endif

  end subroutine compute_block

END PROGRAM ClMD
