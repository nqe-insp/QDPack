module system_commons
  use kinds
  use nested_dictionaries
  use periodic_table
  use atomic_units
	use string_operations
	use random
	use h2o_dip
	use ch5_dip
	use matrix_operations, only:cross_product
  implicit none

  integer, save :: nat, ndim, ndof
  real(dp), save :: kT, beta, total_mass
	logical,save :: charges_provided=.FALSE., linear_dipole=.TRUE.
  real(dp), allocatable, save :: mass(:), Xinit(:,:), charges(:)
	CHARACTER(2), ALLOCATABLE,save :: element_symbols(:),element_set(:)
	integer, save :: n_elements
	CHARACTER(:), allocatable :: xyz_unit
	logical :: xyz_output=.FALSE.,tinker_xyz=.FALSE.
CONTAINS

  subroutine initialize_system(param_library)
	!! CHECK IF XYZ FILE IS GIVEN AS INPUT AND EXTRACT DATA
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		CHARACTER(:), allocatable :: xyz_file
		REAL(dp) :: unit
		LOGICAL :: num_is_indicated, blank_line
		INTEGER :: i,j,length,spos,n_atoms_of
		INTEGER, ALLOCATABLE :: indices(:),element_indices(:)
		TYPE(DICT_STRUCT), POINTER :: parameters_cat
		CHARACTER(:), ALLOCATABLE :: elements
		REAL(dp), allocatable :: X0_default(:)


		kT = param_library%get("parameters/temperature")
		!! @input_file parameters/temperature [REAL] (system_commons)
    beta = 1._dp / kT
		

		call init_periodic_table()
		parameters_cat => param_library%get_child("PARAMETERS")

		xyz_file=parameters_cat%get("xyz_file", default="null")
		!! @input_file parameters/xyz_file [STRING] (system_commons, default="null")
		xyz_unit=parameters_cat%get("xyz_unit", default="angstrom")
		!! @input_file parameters/xyz_unit [STRING] (system_commons, default="angstrom")
		xyz_output=param_library%get("OUTPUT/xyz_output", default=.FALSE.)
		!! @input_file OUTPUT/xyz_output [LOGICAL] (system_commons, default=.FALSE.)
		tinker_xyz=parameters_cat%get("tinker_xyz", default=.FALSE.)
		!! @input_file parameters/tinker_xyz [LOGICAL] (system_commons, default=.FALSE.)	
		write(*,*) "XYZ input : "//xyz_file
		if(xyz_file/="null") then !! XYZ file provided
			unit=atomic_units_get_multiplier(xyz_unit)	
			blank_line=.TRUE.
			num_is_indicated=parameters_cat%get("xyz_num_indicated", default=.FALSE.)	
			!! @input_file parameters/xyz_num_indicated [LOGICAL] (system_commons, default=.FALSE.)
			if(tinker_xyz) then
				num_is_indicated=.TRUE. ; blank_line=.FALSE.
			endif

      ndim = param_library%get("parameters/n_dim",default=3)
			if(ndim<1 .OR. ndim>3) STOP "[system_commons.f90] Error: 'parameters/n_dim' must be 1, 2 or 3"
			if(ndim/=3) then
				write(*,*) "[system_commons.f90] Warning: specified a 'xyz_file' with 'n_dim' /= 3."
				write(*,*) "  Make sure that the xyz file is in the correct format!" 
			endif
			call get_info_from_xyz_file(xyz_file,ndim,nat,Xinit,mass,num_is_indicated,blank_line,indices=indices)
			Xinit = Xinit / unit
			mass = mass*au%Mprot
			call parameters_cat%store("element_index",indices)
			call param_library%store("parameters/mass",mass)	

		else !! XYZ file NOT provided

      nat = param_library%get("parameters/n_atoms")
			!! @input_file parameters/n_atoms [INTEGER] (system_commons)
      ndim = param_library%get("parameters/n_dim",default=3)
			!! @input_file parameters/n_dim [INTEGER] (system_commons, default=3)
			if(ndim<1 .OR. ndim>3) STOP "[system_commons.f90] Error: 'parameters/n_dim' must be 1, 2 or 3"
      ndof = nat*ndim

      allocate(Xinit(nat,ndim),X0_default(ndof))
      X0_default(:) = 0._dp
			X0_default=parameters_cat%get("xinit",default=X0_default)
			!! @input_file parameters/xinit [REAL_ARRAY] (system_commons, default=[0.]*n_atoms*n_dim)	
			if(size(X0_default) /= ndim*nat) &
				STOP "[system_commons.f90] Error: specified 'xinit' size does not match 'n_dim*n_atoms'"	
			Xinit=RESHAPE(X0_default,(/nat,ndim/))
			deallocate(X0_default)

      !GET mass
      allocate(mass(nat))
      if(param_library%has_key("parameters/mass")) then
        mass=param_library%get("parameters/mass")
        !! @input_file parameters/n_dim [REAL_ARRAY] (system_commons, default=[Mprot]*n_atoms*n_dim)	
        if(size(mass) /= nat) &
          STOP "[system_commons.f90] Error: specified 'mass' size does not match 'n_atoms'"
        do i=1,nat
          if(mass(i)<=0) STOP "[system_commons.f90] Error: 'mass' is not properly defined!"
        enddo		
      else
        write(0,*) "[system_commons.f90] Warning: 'mass' not specified, using default of 1 a.m.u. for all atoms."
        mass=au%Mprot
        call param_library%store("parameters/mass",mass)
      endif

    endif

    total_mass = SUM(mass)
    ndof = nat*ndim
    

		call param_library%add_child("ELEMENTS")
		if(allocated(indices)) deallocate(indices)
		allocate(element_symbols(nat), indices(nat))
		element_symbols="Xx"
		if(parameters_cat%has_key("element_index")) then
			indices=parameters_cat%get("element_index")
			!! @input_file parameters/element_index [INTEGER_ARRAY] (system_commons, optional)	
			if(size(indices) /= nat) &
				STOP "[system_commons.f90] Error: specified 'element_index' size does not match 'n_atoms'"
			elements=""
			n_elements=0
			DO i=1,nat
				element_symbols(i)=get_atomic_symbol(indices(i))
				if(index(elements,trim(element_symbols(i)))==0) then
					n_elements=n_elements+1
					elements=elements//"_"//trim(element_symbols(i))
				endif
			ENDDO
			allocate(element_set(n_elements),element_indices(nat))	
			DO i=1,n_elements
				length=len(elements)
				spos=index(elements,"_",BACK=.TRUE.)
				element_set(i)=elements(spos+1:length)
				call param_library%add_child("ELEMENTS/"//element_set(i))
				if(spos>1) elements=elements(1:spos-1)
				n_atoms_of=0
				DO j=1,nat
					if(element_set(i) == element_symbols(j)) then
						n_atoms_of=n_atoms_of+1
						element_indices(n_atoms_of) = j 
					endif
				ENDDO
				call param_library%store("ELEMENTS/"//element_set(i)//"/n",n_atoms_of)
				call param_library%store("ELEMENTS/"//element_set(i)//"/set_index",i)
				call param_library%store("ELEMENTS/"//element_set(i)//"/symbol",element_set(i))
				call param_library%store("ELEMENTS/"//element_set(i)//"/indices",element_indices(1:n_atoms_of))
			ENDDO
			write(*,*) "n_elements= "//int_to_str(n_elements)//" -> ",element_set//" "
		else
			n_elements=1
			allocate(element_set(n_elements))
			allocate(element_indices(nat))	
			element_set(1)="Xx"
			DO j=1,nat ; element_indices(j)=j ; ENDDO
			call param_library%add_child("ELEMENTS/"//element_set(1))
			call param_library%store("ELEMENTS/"//element_set(1)//"/n",nat)
			call param_library%store("ELEMENTS/"//element_set(1)//"/set_index",1)
			call param_library%store("ELEMENTS/"//element_set(1)//"/symbol",element_set(1))
			call param_library%store("ELEMENTS/"//element_set(1)//"/indices",element_indices(1:nat))
		endif

		call initialize_charges(param_library)
	
	end subroutine initialize_system

	subroutine initialize_charges(param_library)
		implicit none
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), POINTER :: charge_lib
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)
		REAL(dp) :: charge_tmp
		INTEGER :: i
		CHARACTER(2) :: symbol
		CHARACTER(:), allocatable :: pot_name

		charges_provided = .FALSE.

		pot_name = param_library%get("POTENTIAL/pot_name")
		!! @input_file POTENTIAL/pot_name [STRING] (system_commons)	
		pot_name=to_upper_case(trim(pot_name))
		if(pot_name == "H2O_PAT") then			
			use_h2odip = param_library%get("POTENTIAL/use_h2odip",default=.TRUE.)
			if(use_h2odip) then
				write(*,*) "Using Partridge-Schwenke dipole moment"
				charges_provided = .TRUE.
				linear_dipole = .FALSE.
				call initialize_h2odip()
				return
			else
				write(*,*) "Warning: not using Partridge-Schwenke dipole moment"
			endif
		elseif(pot_name == "CH5") then
		  use_ch5dip =  param_library%get("POTENTIAL/use_ch5dip",default=.TRUE.)
			if(use_ch5dip) then
				write(*,*) "Using CH5 non-linear dipole moment"
				charges_provided = .TRUE.
				linear_dipole = .FALSE.
				return
			else
				write(*,*) "Warning: not using CH5 dipole moment"
			endif
		endif
		
		if(param_library%has_key("PARAMETERS/charges")) then
			allocate(charges(nat))
			charges_provided=.TRUE.
			linear_dipole=.TRUE.
			charge_lib => param_library%get_child("PARAMETERS/charges")
			!! @input_file PARAMETERS/charges [DICT_STRUCT] (system_commons, optional)	
			write(*,*)
			write(*,*) "Charges:"
			call dict_list_of_keys(charge_lib,keys,is_sub)
			DO i=1,size(keys)
				if(is_sub(i)) CYCLE
				charge_tmp=charge_lib%get(keys(i))
				symbol=trim(keys(i))
				symbol(1:1)=to_upper_case(symbol(1:1))
				write(*,*) symbol," : ", charge_tmp,"au"
			ENDDO
			write(*,*)
			do i=1, nat
				charges(i)=charge_lib%get(trim(element_symbols(i)),default=-1001._dp)
				if(charges(i)<=-1000._dp) then
					write(0,*) "[system_commons.f90] Error: incorrect charge for element "//trim(element_symbols(i))
					STOP "Execution stopped."
				endif             
			enddo
		endif

	end subroutine initialize_charges

	subroutine sample_classical_momentum(P)
		implicit none
		real(dp), intent(inout) :: P(:,:)
		integer :: i,j

		do j=1,ndim
			call randGaussN(P(:,j))
			P(:,j)=P(:,j)*sqrt(mass(:)*kT)
		enddo

	end subroutine sample_classical_momentum

	subroutine get_dipole(X,P,mu)
		implicit none
		real(dp), intent(in) :: X(:,:)
		real(dp), intent(in) :: P(:,:)
		real(dp), intent(inout) :: mu(:)
		real(dp), allocatable :: vel(:,:)
		integer :: i,j
		
		allocate(vel(nat,ndim))
		do j=1,ndim
			vel(:,j)=P(:,j)/mass(:)
		enddo

		mu(:)=0._dp
		if(use_h2odip) then
			call get_dip_derivative_h2o(X,vel,mu)
		elseif(use_ch5dip) then
			call get_dip_derivative_ch5(X,vel,mu)
		elseif(charges_provided) then		
			DO j=1,ndim ; DO i=1,nat
				mu(j)=mu(j) + charges(i)*vel(i,j)					
			ENDDO ; ENDDO
		endif

	end subroutine get_dipole

	subroutine compute_kinetic_energy(P,Ekin,temp)
    IMPLICIT NONE
		real(dp), intent(in) :: P(:,:)
    real(dp), intent(out) :: Ekin,temp
    integer :: j

    temp=0._dp
    Ekin = 0._dp
    do j=1,ndim
      Ekin = Ekin + SUM(P(:,j)**2/(2*mass(:)))
    enddo
    temp = au%kelvin*2._dp*Ekin/ndof

  end subroutine compute_kinetic_energy

  subroutine compute_kinetic_energy_array(P,Ekin)
    IMPLICIT NONE
		real(dp), intent(in) :: P(:,:)
    real(dp), intent(out) :: Ekin(:)
    integer :: i,j,c

    c=0
    do j=1,ndim ; do i=1,nat
      c=c+1
      Ekin(c) = P(i,j)**2/(2*mass(i))
    enddo ; enddo

  end subroutine compute_kinetic_energy_array

	SUBROUTINE apply_eckart_conditions( X,P )
		!! ADAPTED FROM PaPIM !!

    !> @brief Application of Eckart's conditions to the sampled phase space point i.e. 
    !> centering the molecule within its center of mass and removing all translational
    !> and rotational components of velocity.
    !> 
    !> @detail This subroutine calls external "lapack" procedure "dsysv" for solving a set of linear 
    !> equations. Failure with compiling this subroutine my be due to linking of this 
    !> external subroutine. Please check the "lapack" linking fags in the Makefile.
    !>
    IMPLICIT NONE
		REAL(dp), intent(inout) :: X(:,:), P(:,:)
    !! Local variables
    INTEGER :: i_atom
    INTEGER :: i_index
    INTEGER :: j_index
    INTEGER, DIMENSION(3) :: ipiv
    INTEGER :: error_info
    REAL( dp ) :: r2
    REAL( dp ) :: reduced_mass
    REAL( dp ), DIMENSION(3) :: center_of_mass
    REAL( dp ), DIMENSION(3) :: cm_velocity
    REAL( dp ), DIMENSION(3) :: total_angular_momentum
    REAL( dp ), DIMENSION(3) :: bond
    REAL( dp ), DIMENSION(3) :: work
    REAL( dp ), DIMENSION(3,3) :: moment_of_inertia
		REAL(dp) :: total_mass_inv

		if(ndim/=3) return

		total_mass_inv = 1._dp / total_mass

    center_of_mass = 0.0_dp
    cm_velocity = 0.0_dp
    DO i_atom = 1, nat
        center_of_mass = center_of_mass + X(i_atom,:) * mass(i_atom) 
        cm_velocity = cm_velocity + P(i_atom,:)
    END DO
    center_of_mass = center_of_mass * total_mass_inv
    cm_velocity = cm_velocity * total_mass_inv
    DO i_atom = 1, nat
        X(i_atom,:) = X(i_atom,:) - center_of_mass
        P(i_atom,:) = P(i_atom,:) - cm_velocity * mass(i_atom)
    END DO


    IF( nat > 2 ) THEN

        total_angular_momentum = 0.0_dp
        moment_of_inertia = 0.0_dp
        DO i_atom = 1, nat
            total_angular_momentum = total_angular_momentum + cross_product( &
                                    X(i_atom,:), P(i_atom,:) )
            r2 = DOT_PRODUCT( X(i_atom,:), X(i_atom,:) )
            DO i_index = 1, 3
                DO j_index = i_index, 3
                    IF( i_index == j_index ) THEN
                        moment_of_inertia(i_index,j_index) = moment_of_inertia(i_index,j_index) + &
                            mass(i_atom) * (r2 - X(i_atom,i_index) ** 2)
                    ELSE
                        moment_of_inertia(i_index,j_index) = moment_of_inertia(i_index,j_index) - &
                            mass(i_atom) * X(i_atom,i_index) * X(i_atom,j_index)
                        moment_of_inertia(j_index,i_index) = moment_of_inertia(i_index,j_index)
                    END IF
                END DO
            END DO
        END DO

        !! Call to external library subroutine
        !! Lapack subroutine for solving the system of linear equations. The input "variable total angular
        !! momentum" takes the output value of "total nagular velocity" or "omega" of the system
        CALL dsysv( 'u', 3, 1, moment_of_inertia, 3, ipiv, total_angular_momentum, 3, work, 3, error_info )
        !! Call to external library subroutine
        IF( error_info /= 0 ) STOP  "Error in eckart_conditions. Matrix Inversion Problem." 

        DO i_atom = 1, nat
            P(i_atom,:) = P(i_atom,:) - &
                mass(i_atom) * cross_product( total_angular_momentum, &
                                              X(i_atom,:) )
        END DO

    ELSE IF( nat == 2 ) THEN

        bond = X(1,:) - X(2,:)
        r2 = DOT_PRODUCT( bond, bond )

        reduced_mass = mass(1) * mass(2) * total_mass_inv

        X(1,:) = bond * mass(2) * total_mass_inv
        X(2,:) = -bond * mass(1) * total_mass_inv
 
        total_angular_momentum = cross_product( bond, P(1,:) )

        !! Optionally save the total angular momentum
        !IF( save_cm_prop ) trajectory%angular_momentum = total_angular_momentum

        total_angular_momentum = total_angular_momentum / ( r2 * reduced_mass )

        total_angular_momentum = cross_product( total_angular_momentum, bond )
 
        P(1,:) = P(1,:) - reduced_mass * &
                                       &total_angular_momentum
        P(2,:) = -P(1,:)

    END IF


  END SUBROUTINE apply_eckart_conditions

end module system_commons