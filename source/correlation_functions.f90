module correlation_functions
  use kinds
  use system_commons
  use atomic_units
  use file_handler
  use glassman_fft
  implicit none

  TYPE tcf_type
    integer :: avg_counter = 0
    character(:), allocatable :: label
    integer :: nsteps, per_sample
    logical :: kubo_transform
    real(dp) :: dt, gamma, cutoff

    real(dp), allocatable :: Cpp_avg(:,:), Cpp(:,:,:), mCvv_avg(:,:)
    real(dp), allocatable :: Cmumu_avg(:,:), Cmumu(:,:,:)

  END TYPE tcf_type

CONTAINS

  subroutine initialize_tcf(tcf,label,dt,nsteps,per_sample,gamma,cutoff,kubo_transform)
    implicit none
    type(tcf_type), intent(inout) :: tcf
    character(*), intent(in) :: label
    real(dp), intent(in) :: dt,gamma,cutoff
    integer, intent(in) :: nsteps, per_sample
    logical, intent(in) :: kubo_transform

    tcf%label = label
    tcf%avg_counter = 0
    tcf%nsteps = nsteps
    tcf%per_sample = per_sample
    tcf%kubo_transform = kubo_transform
    tcf%dt = dt
    tcf%gamma = gamma
    tcf%cutoff = cutoff

    if(allocated(tcf%Cpp_avg)) deallocate(tcf%Cpp_avg)
    allocate(tcf%Cpp_avg(ndof,nsteps))
    if(allocated(tcf%Cpp)) deallocate(tcf%Cpp)
    allocate(tcf%Cpp(ndof,nsteps,per_sample))
    if(allocated(tcf%mCvv_avg)) deallocate(tcf%mCvv_avg)
    allocate(tcf%mCvv_avg(ndim,nsteps))

    tcf%Cpp_avg = 0._dp
    tcf%Cpp = 0._dp
    tcf%mCvv_avg = 0._dp

    if(charges_provided) then
      if(allocated(tcf%Cmumu_avg)) deallocate(tcf%Cmumu_avg)
      allocate(tcf%Cmumu_avg(ndim,nsteps))
      if(allocated(tcf%Cmumu)) deallocate(tcf%Cmumu)
      allocate(tcf%Cmumu(ndim,nsteps,per_sample))
      tcf%Cmumu_avg = 0._dp
      tcf%Cmumu = 0._dp
    endif

  end subroutine initialize_tcf

  subroutine update_tcf_avg(tcf)
    implicit none
    type(tcf_type), intent(inout) :: tcf

    tcf%avg_counter = tcf%avg_counter + 1
    tcf%Cpp_avg = tcf%Cpp_avg + (SUM(tcf%Cpp,dim=3)/tcf%per_sample - tcf%Cpp_avg)/tcf%avg_counter    
    if(charges_provided) then
      tcf%Cmumu_avg = tcf%Cmumu_avg + (SUM(tcf%Cmumu,dim=3)/tcf%per_sample - tcf%Cmumu_avg)/tcf%avg_counter
    endif

  end subroutine update_tcf_avg

  subroutine write_tcf_results(tcf,norm)
    implicit none
    type(tcf_type), intent(inout) :: tcf
    real(dp), intent(in) :: norm

    call write_ivr_results("Cpp_"//tcf%label,tcf%Cpp_avg/norm &
      ,tcf%nsteps,tcf%dt,.TRUE.,tcf%gamma,tcf%cutoff,tcf%kubo_transform)

    call compute_mCvv(tcf%Cpp_avg,tcf%mCvv_avg,tcf%nsteps)
    call write_ivr_results("mCvv_"//tcf%label,tcf%mCvv_avg/norm &
      ,tcf%nsteps,tcf%dt,.TRUE.,tcf%gamma,tcf%cutoff,tcf%kubo_transform)

    if(charges_provided) then
      call write_ivr_results("Cmumu_"//tcf%label,tcf%Cmumu_avg/norm &
        ,tcf%nsteps,tcf%dt,.TRUE.,tcf%gamma,tcf%cutoff,tcf%kubo_transform)
    endif

  end subroutine write_tcf_results

  subroutine write_ivr_results(prefix,Cpp,nsteps,dt,write_spectrum,gamma,cutoff,kubo_transform)
    implicit none
    real(dp), intent(in) :: Cpp(:,:), dt
    integer, intent(in) :: nsteps
    logical, intent(in) :: write_spectrum
    real(dp), intent(in), OPTIONAL :: gamma, cutoff
    character(*),intent(in) :: prefix
    logical, intent(in), OPTIONAL :: kubo_transform
    integer :: istep
    real(dp) :: domega,t,u0
    real(dp), allocatable :: spectrum(:,:),Cppdamp(:,:)
    integer :: ncut_spectrum
    real(dp) :: gamma_
    logical :: kubo_transform_

    ! WRITE RAW Cpp AND mCvv
    call write_Cpp_array(Cpp,nsteps,dt*au%fs,name=prefix//"_ivr.out")   
    if(.not. write_spectrum) return

    if(present(kubo_transform)) then
      kubo_transform_=kubo_transform
    else
      kubo_transform_=.FALSE.
    endif


    allocate(spectrum(size(Cpp,1),nsteps))
    ! COMPUTE AND WRITE RAW SPECTRUM
    call compute_spectrum(Cpp,nsteps,dt,spectrum,domega)
    ncut_spectrum = nsteps
    if(present(cutoff)) then
      if(cutoff>0) then
        ncut_spectrum = nint(cutoff/domega)
      endif
    endif  
    call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_raw.out")

    ! KUBO TRANSFORM
    if(kubo_transform_) then
      DO istep=2,nsteps
        u0=0.5_dp*(istep-1)*domega/kT
        spectrum(:,istep) = spectrum(:,istep)*(tanh(u0)/u0)
      enddo    
      call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_raw_kubo.out")
    endif

    if(present(gamma)) then
      if(gamma>0) then
        ! WRITE DATA WITH GAUSSIAN TIME WINDOW SMOOTHING
        allocate(Cppdamp(size(Cpp,1),nsteps))
        Cppdamp(:,:)=0._dp
        do istep=1,nsteps
          t=(istep-1)*dt
          if(t<1/gamma) &
            Cppdamp(:,istep) = Cpp(:,istep)*cos(0.5*pi*t*gamma)**2
          !Cppdamp(:,istep) = Cpp(:,istep)*exp(-0.5_dp*(t*gamma)**2)
        enddo
        call write_Cpp_array(Cppdamp,nsteps,dt*au%fs,name=prefix//"_ivr_damp.out")   
        call compute_spectrum(Cppdamp,nsteps,dt,spectrum,domega)
        call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_damp.out")
        ! KUBO TRANSFORM
        if(kubo_transform_) then
          DO istep=2,nsteps
            u0=0.5_dp*(istep-1)*domega/kT
            spectrum(:,istep) = spectrum(:,istep)*(tanh(u0)/u0)
          enddo
          call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_damp_kubo.out")
        endif
        deallocate(Cppdamp)
      endif
    endif

    deallocate(spectrum)

  end subroutine write_ivr_results

  subroutine write_Cpp_array(Cpp,nsteps,dx,name)
    implicit none
    real(dp), intent(in) :: Cpp(:,:), dx
    character(*), intent(in) :: name
    integer, intent(in) :: nsteps
    integer :: istep,uf

    open(newunit=uf,file=output%working_directory//name)
    do istep = 1,nsteps
      write(uf,*) (istep-1)*dx , Cpp(:,istep)
    enddo
    close(uf)

  end subroutine write_Cpp_array

  subroutine compute_mCvv(Cpp,mCvv,nsteps)
    implicit none
    real(dp), intent(in) :: Cpp(:,:)
    integer, intent(in) :: nsteps
    real(dp), intent(inout) :: mCvv(:,:)
    integer :: istep,j,i,c
    
    mCvv(:,:) = 0._dp
    do istep = 1,nsteps   
      c=0   
      do j=1,ndim; do i=1,nat
        c=c+1
        mCvv(j,istep) = mCvv(j,istep) + Cpp(c,istep)/(2*mass(i))
      enddo; enddo
    enddo

  end subroutine compute_mCvv

  subroutine compute_spectrum(Cpp,nsteps,dt,spectrum,domega)
    real(dp), intent(in) :: Cpp(:,:), dt
    integer, intent(in) :: nsteps
    real(dp), intent(inout) :: spectrum(:,:)
    real(dp), intent(out) :: domega
    complex(dp), allocatable :: fr(:),work(:)
    integer :: ifail,i,k
    real(dp) :: norm

    norm = dt !*sqrt(real(2*nsteps))
    allocate(fr(2*nsteps),work(2*nsteps))

    do i=1,size(Cpp,1)
      fr(1:nsteps) = cmplx(Cpp(i,:),kind=dp)
      fr(nsteps+1)=fr(nsteps) ! SYMMETRIZE Cpp
      do k=1,nsteps-1
        fr(nsteps+k+1)=fr(nsteps-k+1)
      enddo
      ifail=0
      call SPCFFT(fr,2*nsteps,-1,work)
      !call C06FCF(fr,fi,2*nsteps,work,ifail) ! FFT
      spectrum(i,:) = real(fr(1:nsteps))*norm
    enddo
    domega = 2._dp*pi/(dt*(2*nsteps))

    deallocate(fr)
    deallocate(work)
    
  end subroutine compute_spectrum

  subroutine compute_autoTCF_WK(Ptraj,nsteps,dt,Cpp)
    implicit none
    integer, intent(in) :: nsteps
    real(dp), intent(in) :: Ptraj(:,:),dt
    real(dp), intent(inout) :: Cpp(:,:)
    complex(dp), allocatable :: fr(:),work(:)
    real(dp) :: norm,normtcf
    integer :: c,ifail

    allocate(fr(2*nsteps),work(2*nsteps))
    norm = 2.*dt /(2*nsteps)
    normtcf = dt *2*nsteps !*sqrt(real(2*nsteps))
    do c=1,size(Ptraj,1)
      fr(:)=0._dp
      fr(1:nsteps) = cmplx(Ptraj(c,:),kind=dp)
      call SPCFFT(fr,2*nsteps,-1,work)
      !call C06FCF(fr,fi,2*nsteps,work,ifail)

      fr(:) = norm*real(fr*conjg(fr),kind=dp)
      call SPCFFT(fr,2*nsteps,1,work)
      !call C06FCF(fr,fi,2*nsteps,work,ifail)
      Cpp(c,:) = fr(1:nsteps)/normtcf
    enddo

  end subroutine compute_autoTCF_WK


end module correlation_functions
