PROGRAM IVR
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use system_commons
  use wigner_EW
  use potential
  use PI_sampler
  use correlation_functions
  use fk_initial
  use matrix_operations, only: sym_mat_diag
  use condkin
  use trajectory_files
  use classical_md, only: classical_dynamics_IVR
  implicit none
	type(DICT_STRUCT) :: param_library	
  type(WIG_AUX_SIM_TYPE) :: EW_auxsim, CK_auxsim
  type(tcf_type) :: tcf_EW0,tcf_EW4,tcf_EW6, tcf_TRPMD, tcf_FK, tcf_LGA,tcf_CK,tcf_LGAFK

  real(dp) :: dt_dyn, gamma_dyn, spectrum_cutoff
  real(dp), allocatable :: X(:,:),P(:,:,:),Xc(:,:)
  real(dp), allocatable :: X_PI(:,:,:)
  real(dp), allocatable :: X_FK(:,:,:)
  logical :: compute_TRPMD
  logical :: compute_EW
  logical :: compute_CK
  logical :: compute_FK
  logical :: compute_LGA
  logical :: compute_LGAFK
  logical :: ignore_point

  real(dp) :: time_dyn
  integer :: nsteps_dyn, nsteps_btwn_samples, nsamples
  integer :: n_momenta_per_sample
  integer :: nsteps_aux, nsteps_aux_therm    
  integer :: num_aux_sim,write_stride
  integer :: nbeads_sampler

  integer :: isample,ux,up,ip,uk2,uk2var,uew ,uewcorr
  integer :: uxFK,upFK
  integer :: uxLGAFK,upLGAFK
  integer :: uxCK,upCK
  integer :: uxLGA,upLGA
  logical :: verbose  
  integer :: iconv_FK
  real(dp) :: iconv_FK_avg
  real(dp) :: iconv_LGAFK_avg
  logical :: LGA_classical_rotations
  logical :: save_samples
  logical :: apply_eckart

  integer :: avg_counter
  integer :: n_threads
  real(dp), allocatable :: Ek_dyn_EW_avg(:,:),Ek_dyn_FK_avg(:,:), Ek_dyn(:,:,:),Ek_dyn_CK_avg(:,:)
  real(dp), allocatable :: Ek_dyn_LGA_avg(:,:),Ek_dyn_LGAFK_avg(:,:)
  real(dp) :: temp_EW_avg,temp_CK_avg,temp_FK_avg,temp_LGAFK_avg,temp_LGA_avg,Qekin, pot_avg, pot_step
  real(dp), allocatable :: Qtemp(:),Cew(:,:),weight_EW(:)
  real(dp) :: Cew_avg(2),k22,weight_EW_avg
  logical :: correct_stat_EW
  integer :: EW_nan_count, LGA_nan_count, FK_nan_count,LGAFK_nan_count, TRPMD_nan_count, CK_nan_count
  real(dp) :: EW_control_weight,EW_control_weight_avg

  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real :: EW_momentum_time_avg,EW_dynamics_time_avg,lgv_time_avg,TRPMD_time_avg
  real :: FK_computation_time_avg,FK_dynamics_time_avg
  real :: LGAFK_computation_time_avg,LGAFK_dynamics_time_avg
  real :: CK_momentum_time_avg,CK_dynamics_time_avg
  real :: LGA_momentum_time_avg,LGA_dynamics_time_avg
  real ::  remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  type(TRAJ_FILE) :: sampleEW_xyz, sampleCK_xyz, sampleLGA_xyz,sampleFK_xyz,sampleLGAFK_xyz

  call full_timer%start
  call cpu_time(cpu_time_start)


  call initialize_framework(param_library)	

  ! GET NUMBER OF THREADS        
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)   

  write_stride = param_library%get("OUTPUT/write_stride",default=1)
  !! @input_file parameters/write_stride [INTEGER] (IVR, default=1)) 
  avg_counter = 0
  EW_momentum_time_avg = 0.
  EW_dynamics_time_avg = 0.
  CK_momentum_time_avg = 0.
  CK_dynamics_time_avg = 0.
  lgv_time_avg = 0.
  step_time_avg = 0.
  TRPMD_time_avg = 0.
  FK_computation_time_avg = 0.
  FK_dynamics_time_avg = 0.
  LGAFK_computation_time_avg = 0.
  LGAFK_dynamics_time_avg = 0.
  LGA_momentum_time_avg = 0.
  LGA_dynamics_time_avg = 0.
  step_cpu_time_avg = 0.
  
  save_samples = param_library%get("OUTPUT/save_samples",default=.TRUE.)
  !! @input_file OUTPUT/save_samples [LOGICAL] (IVR, default=.TRUE.)
  n_momenta_per_sample = param_library%get("parameters/n_momenta_per_sample",default=1)
  !! @input_file parameters/n_momenta_per_sample [INTEGER] (IVR, default=1)
  allocate(X(nat,ndim), P(nat,ndim,n_momenta_per_sample))
  allocate(Qtemp(n_momenta_per_sample))
  X(:,:) = 0._dp
  P(:,:,:) = 0._dp  
  pot_avg = 0._dp
  

  !----------------------------------------
  ! INITIALIZE SAMPLER 
  nsteps_btwn_samples = param_library%get("sampler/nsteps_btwn_samples")
  !! @input_file sampler/nsteps_btwn_samples [INTEGER] (IVR) 
  nsamples = param_library%get("parameters/nsamples")  
  !! @input_file parameters/nsamples [INTEGER] (IVR) 
  call initialize_PI_sampler(param_library,nbeads_sampler)
  allocate(X_PI(nat,ndim,nbeads_sampler))
  do ip=1,nbeads_sampler
    X_PI(:,:,ip)=Xinit(:,:)
  enddo
  !WARM UP SAMPLER
  write(*,*)
  write(*,*) "Warming up PI sampler..."
  call update_sample_position(X_PI,nsteps_btwn_samples,.FALSE.)
  write(*,*) "done."

  !----------------------------------------
  !INITIALIZE DYNAMICS
  time_dyn = param_library%get("dynamics/time")
  !! @input_file dynamics/time [REAL] (IVR) 
  dt_dyn = param_library%get("dynamics/dt")
  !! @input_file dynamics/dt [REAL] (IVR) 
  nsteps_dyn = nint(time_dyn/dt_dyn)
  gamma_dyn = param_library%get("dynamics/gamma",default=-1._dp)
  !! @input_file dynamics/gamma [REAL] (IVR, default=-1.) 
  spectrum_cutoff = param_library%get("dynamics/spectrum_cutoff",default=-1._dp)   
  !! @input_file dynamics/spectrum_cutoff [REAL] (IVR, default=-1.) 
  allocate(Ek_dyn(ndof,nsteps_dyn,n_momenta_per_sample))  
  apply_eckart=param_library%get("parameters/apply_eckart",default=.FALSE.)
  !! @input_file dynamics/apply_eckart [LOGICAL] (IVR, default=FALSE) 

  !----------------------------------------
  ! INITIALIZE EW AUXILIARY SIMULATION
  compute_EW = param_library%get("dynamics/compute_EW",default=.TRUE.)
  !! @input_file dynamics/compute_EW [LOGICAL] (IVR, default=.TRUE.)) 
  if(compute_EW) then    
    write(*,*)
    call initialize_wigner_commons(param_library) 
    allocate(Cew(n_momenta_per_sample,2))
    allocate(Ek_dyn_EW_avg(ndof,nsteps_dyn)) 
    Cew_avg(:) = 1._dp
    Cew(:,:) = 1._dp
    temp_EW_avg = 0._dp
    EW_nan_count = 0
    EW_control_weight_avg = 0._dp
    EW_control_weight = 0._dp

    nsteps_aux = param_library%get(WIGAUX//"/nsteps")  
    !! @input_file EW_NLGA/nsteps [INTEGER] (IVR) 
    nsteps_aux_therm = param_library%get(WIGAUX//"/nsteps_therm")
    !! @input_file EW_NLGA/nsteps_therm [INTEGER] (IVR) 
    num_aux_sim = param_library%get(WIGAUX//"/num_aux_sim")
    !! @input_file EW_NLGA/num_aux_sim [INTEGER] (IVR) 
    if(num_aux_sim>1) write(*,*) "EW: "//int_to_str(num_aux_sim)//" auxiliary simulations"    
    EW_auxsim%nsteps = nsteps_aux
    EW_auxsim%nsteps_therm = nsteps_aux_therm

    correct_stat_EW = param_library%get(WIGAUX//"/noise_correction",default=(num_aux_sim>1))  
    !! @input_file EW_NLGA/noise_correction [LOGICAL] (IVR, default=EW_NLGA/num_aux_sim>1) 
    if(correct_stat_EW) then
      if(num_aux_sim<=1) then
        STOP "[IVR.f90] Error: num_aux_sim must be >1 to correct EW statistical noise."
      else
        write(*,*) "EW: correction of statistical noise."
      endif
    endif  
        
    allocate(weight_EW(n_momenta_per_sample))
    weight_EW = 1._dp
    weight_EW_avg = 0._dp

    EW_auxsim%compute_Cew = param_library%get(WIGAUX//"/compute_cew",default=(ndof==1))  
    !! @input_file EW_NLGA/compute_cew [LOGICAL] (IVR, default=(ndof==1)) 

    call initialize_auxiliary_simulation(num_aux_sim,EW_auxsim)
    ! WARM UP AUXILIARY SIMULATION  
    !write(*,*) "EW: Warming up auxiliary calculation..."
    !call compute_EW_parameters(Xinit,EW_auxsim)
    !write(*,*) "done."

    call initialize_tcf(tcf_EW0,"EW0",dt_dyn,nsteps_dyn,n_momenta_per_sample&
                        ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
    if(EW_auxsim%compute_Cew) then
      call initialize_tcf(tcf_EW4,"EW4",dt_dyn,nsteps_dyn,n_momenta_per_sample&
                          ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
      if(ndof==1) then        
        call initialize_tcf(tcf_EW6,"EW6",dt_dyn,nsteps_dyn,n_momenta_per_sample&
                          ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
      endif
    endif

    write(*,*) "EW initialized"
  endif
  !----------------------------------------

  !----------------------------------------
  ! INITIALIZE CK AUXILIARY SIMULATION
  compute_CK = param_library%get("dynamics/compute_CK",default=.FALSE.)
  !! @input_file dynamics/compute_CK [LOGICAL] (IVR, default=.FALSE.) 
  if(compute_CK) then    
    write(*,*)
    call initialize_CK_commons(param_library) 
    allocate(Ek_dyn_CK_avg(ndof,nsteps_dyn)) 
    temp_CK_avg = 0._dp
    CK_nan_count = 0

    nsteps_aux = param_library%get(WIGAUX//"/nsteps")  
    nsteps_aux_therm = param_library%get(WIGAUX//"/nsteps_therm")
    num_aux_sim = param_library%get(WIGAUX//"/num_aux_sim")
    if(num_aux_sim>1) write(*,*) "CK: "//int_to_str(num_aux_sim)//" auxiliary simulations"    
    CK_auxsim%nsteps = nsteps_aux
    CK_auxsim%nsteps_therm = nsteps_aux_therm

    call initialize_CK_auxiliary_simulation(num_aux_sim,CK_auxsim)
    ! WARM UP AUXILIARY SIMULATION  
    !write(*,*) "EW: Warming up auxiliary calculation..."
    !call compute_EW_parameters(Xinit,EW_auxsim)
    !write(*,*) "done."

    call initialize_tcf(tcf_CK,"CK",dt_dyn,nsteps_dyn,n_momenta_per_sample&
                        ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
    write(*,*) "CK initialized"
  endif
  !----------------------------------------

  !----------------------------------------
  ! INITIALIZE FK
  compute_FK = param_library%get("dynamics/compute_FKLPI",default=.FALSE.)
  !! @input_file dynamics/compute_FK [LOGICAL] (IVR, default=.FALSE.)
  if(compute_FK) then  
    write(*,*)  
    call initialize_FK(param_library)
    call initialize_tcf(tcf_FK,"FKLPI",dt_dyn,nsteps_dyn,n_momenta_per_sample &
                        ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
    allocate(X_FK(nat,ndim,n_momenta_per_sample))
    allocate(Xc(nat,ndim))
    allocate(Ek_dyn_FK_avg(ndof,nsteps_dyn)) 
    X_FK(:,:,:)=0._dp
    iconv_FK_avg=0._dp
    Ek_dyn_FK_avg(:,:) = 0._dp
    temp_FK_avg = 0._dp
    FK_nan_count = 0
    write(*,*) "FK LPI initialized."
  endif
  !----------------------------------------

  !----------------------------------------
  !INITIALIZE TRPMD
  compute_TRPMD=param_library%get("dynamics/compute_TRPMD",default=.FALSE.)
  !! @input_file dynamics/compute_TRPMD [LOGICAL] (IVR, default=.FALSE.)
  if(compute_TRPMD) then 
    write(*,*)     
    call initialize_tcf(tcf_TRPMD,"TRPMD",dt_dyn,nsteps_dyn,n_threads &
                        ,gamma_dyn,spectrum_cutoff,kubo_transform=.FALSE.)
    TRPMD_nan_count = 0
    write(*,*) "TRPMD initialized."
  endif
  !----------------------------------------

  !----------------------------------------
  !INITIALIZE LGA
  compute_LGA=param_library%get("dynamics/compute_lga",default=.FALSE.)
  !! @input_file dynamics/compute_lga [LOGICAL] (IVR, default=.FALSE.)
  if(compute_LGA) then
    write(*,*)
    if(hessian_available) then
      call initialize_tcf(tcf_LGA,"LGA",dt_dyn,nsteps_dyn,n_momenta_per_sample&
                        ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
      allocate(Ek_dyn_LGA_avg(ndof,nsteps_dyn)) 
      Ek_dyn_LGA_avg(:,:)=0._dp
      temp_LGA_avg = 0._dp
      LGA_nan_count = 0
      if(ndim==3 .AND. nat>2) then
        LGA_classical_rotations = param_library%get("dynamics/lga_classical_rotations",default=.FALSE.)
        !! @input_file dynamics/lga_classical_rotations [LOGICAL] (IVR, default=.FALSE.)
        if(LGA_classical_rotations) write(*,*) "LGA: classical rotations"
      else
        LGA_classical_rotations = .FALSE.
      endif
      write(*,*) "LGA initialized"
    else
      write(0,*) "[IVR.f90] Warning: Hessian not available, ignoring LGA."
      compute_LGA=.FALSE.
    endif
  endif


  !----------------------------------------

  !----------------------------------------
  !INITIALIZE LGA/FK
  compute_LGAFK=param_library%get("dynamics/compute_lgafk",default=.FALSE.)
  !! @input_file dynamics/compute_lgafk [LOGICAL] (IVR, default=.FALSE.)
  if(compute_LGAFK) then
    write(*,*)
    if(hessian_available) then
      call initialize_tcf(tcf_LGAFK,"LGAFK",dt_dyn,nsteps_dyn,n_momenta_per_sample&
                        ,gamma_dyn,spectrum_cutoff,kubo_transform=.TRUE.)
      allocate(Ek_dyn_LGAFK_avg(ndof,nsteps_dyn)) 
      Ek_dyn_LGAFK_avg(:,:)=0._dp
      temp_LGAFK_avg = 0._dp
      LGAFK_nan_count = 0
      if(ndim==3 .AND. nat>2) then
        LGA_classical_rotations = param_library%get("dynamics/lga_classical_rotations",default=.FALSE.)
        !! @input_file dynamics/lga_classical_rotations [LOGICAL] (IVR, default=.FALSE.)
        if(LGA_classical_rotations) write(*,*) "LGA: classical rotations"
      else
        LGA_classical_rotations = .FALSE.
      endif
      if(.not. compute_FK) then
        call initialize_FK(param_library)
        allocate(Xc(nat,ndim))
      endif
      write(*,*) "LGAFK initialized"
    else
      write(0,*) "[IVR.f90] Warning: Hessian not available, ignoring LGAFK."
      compute_LGAFK=.FALSE.
    endif
  endif


  !----------------------------------------
  
  
  !----------------------------------------
  call open_output_files(param_library)
  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  if(save_samples) then
    if(.not.param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")
    if(compute_EW) then
      if(xyz_output) then
        call sampleEW_xyz%init("sampleEW.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call sampleEW_xyz%open(output%working_directory,append=.TRUE.)
      else
        open(newunit=ux,file=output%working_directory//"sample_EW_position")
      endif
      open(newunit=up,file=output%working_directory//"sample_EW_momentum")
      open(newunit=uk2,file=output%working_directory//"sample_k2")
      if(num_aux_sim > 1) then
        open(newunit=uk2var,file=output%working_directory//"sample_k2_stddev")
      endif
      if(EW_auxsim%compute_Cew) then 
        open(newunit=uew,file=output%working_directory//"sample_Cew")
      endif
      if(correct_stat_EW) then
        open(newunit=uewcorr,file=output%working_directory//"sample_EW_stat_weights")
      endif
    endif
    if(compute_CK) then
      if(xyz_output) then
        call sampleCK_xyz%init("sampleCK.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call sampleCK_xyz%open(output%working_directory,append=.TRUE.)
      else
        open(newunit=uxCK,file=output%working_directory//"sample_CK_position")
      endif
      open(newunit=upCK,file=output%working_directory//"sample_CK_momentum")
    endif
    if(compute_LGA) then
      if(xyz_output) then
        call sampleLGA_xyz%init("sampleLGA.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call sampleLGA_xyz%open(output%working_directory,append=.TRUE.)
      else
        open(newunit=uxLGA,file=output%working_directory//"sample_LGA_position")
      endif
      open(newunit=upLGA,file=output%working_directory//"sample_LGA_momentum")
    endif
    if(compute_FK) then
      if(xyz_output) then
        call sampleFK_xyz%init("sampleFK.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call sampleFK_xyz%open(output%working_directory,append=.TRUE.)
      else
        open(newunit=uxFK,file=output%working_directory//"sample_FK_position")
      endif
      open(newunit=upFK,file=output%working_directory//"sample_FK_momentum")
    endif
    if(compute_LGAFK) then
      if(xyz_output) then
        call sampleLGAFK_xyz%init("sampleLGAFK.xyz",unit=xyz_unit,tinker_xyz=tinker_xyz)
        call sampleLGAFK_xyz%open(output%working_directory,append=.TRUE.)
      else
        open(newunit=uxLGAFK,file=output%working_directory//"sample_LGAFK_position")
      endif
      open(newunit=upLGAFK,file=output%working_directory//"sample_LGAFK_momentum")
    endif
  endif
  verbose=.FALSE.

  write(*,*)
  write(*,*) "Initialization done."

  !MAIN SAMPLE LOOP
  DO isample = 1,nsamples
    !START TIMERS
    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1
    
    ! DETERMINE IF WE SHOULD PRINT SOME INFO
    if(mod(isample,write_stride)==0) then
      write(*,*)
      write(*,*) "SAMPLE "//int_to_str(isample)//" / "//int_to_str(nsamples)
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    !-------------------------------
    ! POSITION SAMPLING (PIMD PROPAGATION)
    call step_timer%start()
    call update_sample_position(X_PI,nsteps_btwn_samples,verbose)  

    pot_step=0._dp
    do ip=1,nbeads_sampler
      pot_step = pot_step + Pot(X_PI(:,:,ip)) 
    enddo
    pot_avg = pot_avg + (pot_step/nbeads_sampler - pot_avg)/avg_counter   

    lgv_time_avg =  lgv_time_avg  &
      + (step_timer%elapsed_time() - lgv_time_avg)/avg_counter
    ! END OF POSITION SAMPLING
    !-------------------------------

    !-------------------------------
    ! TRPMD calculation
    if(compute_TRPMD) then
      call step_timer%start()
      !$OMP PARALLEL DO
      DO ip=1,n_threads  
        if(charges_provided) then
          call TRPMD_dynamics_IVR(X_PI,nsteps_dyn,dt_dyn,tcf_TRPMD%Cpp(:,:,ip),tcf_TRPMD%Cmumu(:,:,ip))
        else
          call TRPMD_dynamics_IVR(X_PI,nsteps_dyn,dt_dyn,tcf_TRPMD%Cpp(:,:,ip))
        endif
      ENDDO
      !$OMP END PARALLEL DO

      if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf_TRPMD%Cpp)) .OR.  ANY(ISNAN(tcf_TRPMD%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf_TRPMD%Cpp))
      endif

      if(ignore_point) then
        TRPMD_nan_count = TRPMD_nan_count + 1
      else
        call update_tcf_avg(tcf_TRPMD)
      endif
      TRPMD_time_avg =  TRPMD_time_avg  &
      + (step_timer%elapsed_time() - TRPMD_time_avg)/avg_counter
    endif
    ! END OF TRPMD calculation
    !-------------------------------

    ! CHOOSE A RANDOM BEAD FOR EW AND LGA
    call choose_random_bead(X,X_PI)

    !-------------------------------
    ! EDGEWORTH NLGA calculation
    if(compute_EW) then      
      ! EW MOMENTUM SAMPLING
      call step_timer%start()
      call sample_momentum_EW(X,P,EW_auxsim,verbose)
      if(correct_stat_EW) then
        call compute_P_weights_EW(P,EW_auxsim,weight_EW)   
        EW_control_weight =  compute_EW_control(P, EW_auxsim, weight_EW) 
        EW_control_weight_avg = EW_control_weight_avg + (EW_control_weight - EW_control_weight_avg)/isample 
      else
        weight_EW(:)=1._dp
      endif
      if(EW_auxsim%compute_Cew) then 
        DO ip=1,n_momenta_per_sample  
          Cew(ip,:) = compute_EW_correction(P(:,:,ip),EW_auxsim)
        ENDDO
      endif
      EW_momentum_time_avg =  EW_momentum_time_avg  &
        + (step_timer%elapsed_time() - EW_momentum_time_avg)/avg_counter
      ! CLASSICAL DYNAMICS
      call step_timer%start()
      !$OMP PARALLEL DO PRIVATE(Qekin)
      DO ip=1,n_momenta_per_sample        
          call compute_kinetic_energy(P(:,:,ip),Qekin,Qtemp(ip))
          if(apply_eckart) call apply_eckart_conditions(X,P(:,:,ip))
          if(charges_provided) then
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf_EW0%Cpp(:,:,ip),Ek_dyn(:,:,ip),tcf_EW0%Cmumu(:,:,ip))
          else
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf_EW0%Cpp(:,:,ip),Ek_dyn(:,:,ip))
          endif
      ENDDO
      !$OMP END PARALLEL DO
      EW_dynamics_time_avg =  EW_dynamics_time_avg  &
        + (step_timer%elapsed_time() - EW_dynamics_time_avg)/avg_counter

      ! GLOBAL AVERAGES
      if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf_EW0%Cpp)) .OR.  ANY(ISNAN(tcf_EW0%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf_EW0%Cpp))
      endif

      if(ignore_point) then
        EW_nan_count = EW_nan_count + 1
      else
        weight_EW_avg = weight_EW_avg + (SUM(weight_EW)/n_momenta_per_sample-1._dp - weight_EW_avg)/isample

        ! apply statistical weights and update averages
        temp_EW_avg = temp_EW_avg + (SUM(Qtemp*Cew(:,1)*weight_EW(:))/n_momenta_per_sample - temp_EW_avg)/avg_counter
        DO ip=1,n_momenta_per_sample    
          tcf_EW0%Cpp(:,:,ip) = tcf_EW0%Cpp(:,:,ip) * weight_EW(ip)
          Ek_dyn(:,:,ip) = Ek_dyn(:,:,ip) * weight_EW(ip)
          if(charges_provided) then
            tcf_EW0%Cmumu(:,:,ip) = tcf_EW0%Cmumu(:,:,ip) * weight_EW(ip)
          endif
        ENDDO
        
        call update_tcf_avg(tcf_EW0) 

        if(EW_auxsim%compute_Cew) then 
          DO ip=1,n_momenta_per_sample  
            tcf_EW4%Cpp(:,:,ip) = tcf_EW0%Cpp(:,:,ip) * Cew(ip,1)
            Ek_dyn(:,:,ip) = Ek_dyn(:,:,ip) * Cew(ip,1)
            if(charges_provided) then
              tcf_EW4%Cmumu(:,:,ip) = tcf_EW0%Cmumu(:,:,ip) * Cew(ip,1)
            endif

            if(ndof==1) then
              tcf_EW6%Cpp(:,:,ip) = tcf_EW0%Cpp(:,:,ip) *Cew(ip,2)            
              if(charges_provided) then
                tcf_EW6%Cmumu(:,:,ip) = tcf_EW0%Cmumu(:,:,ip) * Cew(ip,2)
              endif
            endif

          ENDDO
          do ip=1,2
            Cew_avg(ip) = Cew_avg(ip) + (SUM(Cew(:,ip))/n_momenta_per_sample - Cew_avg(ip))/isample
          enddo

          call update_tcf_avg(tcf_EW4)   
          if(ndof==1) call update_tcf_avg(tcf_EW6)         
        endif

        Ek_dyn_EW_avg = Ek_dyn_EW_avg + (SUM(Ek_dyn,dim=3)/n_momenta_per_sample - Ek_dyn_EW_avg)/isample

        ! WRITE SAMPLES
        if(save_samples) then
          do ip = 1,n_momenta_per_sample
            if(xyz_output) then
              call sampleEW_xyz%dump_xyz(X,element_symbols)
            else
              write(ux,*) X(:,:)
            endif
            write(up,*) P(:,:,ip)
            write(uk2,*) EW_auxsim%k2(:,:)
            if(num_aux_sim > 1) then
              write(uk2var,*) sqrt(EW_auxsim%k2var(:,:))
            endif
            if(EW_auxsim%compute_Cew) then 
              if(ndof==1) then
                write(uew,*) Cew(ip,:)
              else
                write(uew,*) Cew(ip,1)
              endif
            endif
            if(correct_stat_EW) then
              write(uewcorr,*) weight_EW(ip)
            endif
          enddo
        endif

      endif
    endif
    ! END OF EDGEWORTH NLGA calculation
    !-------------------------------   

    !-------------------------------
    ! Conditional Kinetic calculation
    if(compute_CK) then
      call step_timer%start()
      call compute_CK_parameters(X,CK_auxsim)
      DO ip=1,n_momenta_per_sample 
        call sample_wigner_momentum(P(:,:,ip),CK_auxsim)
      ENDDO
      CK_momentum_time_avg =  CK_momentum_time_avg  &
        + (step_timer%elapsed_time() - CK_momentum_time_avg)/avg_counter
      
      ! CLASSICAL DYNAMICS
      call step_timer%start()
      !$OMP PARALLEL DO PRIVATE(Qekin)
      DO ip=1,n_momenta_per_sample      
          call compute_kinetic_energy(P(:,:,ip),Qekin,Qtemp(ip))
          if(apply_eckart) call apply_eckart_conditions(X,P(:,:,ip))
          if(charges_provided) then
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf_CK%Cpp(:,:,ip),Ek_dyn(:,:,ip),tcf_CK%Cmumu(:,:,ip))
          else
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf_CK%Cpp(:,:,ip),Ek_dyn(:,:,ip))
          endif
      ENDDO
      !$OMP END PARALLEL DO
      CK_dynamics_time_avg =  CK_dynamics_time_avg  &
        + (step_timer%elapsed_time() - CK_dynamics_time_avg)/avg_counter
      
       if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf_CK%Cpp)) .OR.  ANY(ISNAN(tcf_CK%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf_CK%Cpp))
      endif

      if(ignore_point) then
        !write(*,*) "ignored:", CK_auxsim%k2
        CK_nan_count = CK_nan_count + 1
      else
        temp_CK_avg = temp_CK_avg + (SUM(Qtemp)/n_momenta_per_sample - temp_CK_avg)/avg_counter
        Ek_dyn_CK_avg = Ek_dyn_CK_avg + (SUM(Ek_dyn,dim=3)/n_momenta_per_sample - Ek_dyn_CK_avg)/isample
        call update_tcf_avg(tcf_CK)

        ! WRITE SAMPLES
        if(save_samples) then
          do ip = 1,n_momenta_per_sample
            if(xyz_output) then
              call sampleCK_xyz%dump_xyz(X,element_symbols)
            else
              write(uxCK,*) X(:,:)
            endif
            write(upCK,*) P(:,:,ip)
          enddo
        endif
      endif

    endif
    ! END OF LGA calculation
    !------------------------------- 

    !-------------------------------
    ! LGA calculation
    if(compute_LGA) then
      call step_timer%start()
      call sample_momentum_LGA(X,P)
      LGA_momentum_time_avg =  LGA_momentum_time_avg  &
        + (step_timer%elapsed_time() - LGA_momentum_time_avg)/avg_counter
      
      ! CLASSICAL DYNAMICS
      call step_timer%start()
      !$OMP PARALLEL DO PRIVATE(Qekin)
      DO ip=1,n_momenta_per_sample        
          call compute_kinetic_energy(P(:,:,ip),Qekin,Qtemp(ip))
          if(apply_eckart) call apply_eckart_conditions(X,P(:,:,ip))
          if(charges_provided) then
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf_LGA%Cpp(:,:,ip),Ek_dyn(:,:,ip),tcf_LGA%Cmumu(:,:,ip))
          else
            call classical_dynamics_IVR(X,P(:,:,ip),nsteps_dyn,dt_dyn,tcf_LGA%Cpp(:,:,ip),Ek_dyn(:,:,ip))
          endif
      ENDDO
      !$OMP END PARALLEL DO
      LGA_dynamics_time_avg =  LGA_dynamics_time_avg  &
        + (step_timer%elapsed_time() - LGA_dynamics_time_avg)/avg_counter
      
       if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf_LGA%Cpp)) .OR.  ANY(ISNAN(tcf_LGA%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf_LGA%Cpp))
      endif

      if(ignore_point) then
        LGA_nan_count = LGA_nan_count + 1
      else
        temp_LGA_avg = temp_LGA_avg + (SUM(Qtemp)/n_momenta_per_sample - temp_LGA_avg)/avg_counter
        Ek_dyn_LGA_avg = Ek_dyn_LGA_avg + (SUM(Ek_dyn,dim=3)/n_momenta_per_sample - Ek_dyn_LGA_avg)/isample
        call update_tcf_avg(tcf_LGA)

        ! WRITE SAMPLES
        if(save_samples) then
          do ip = 1,n_momenta_per_sample
            if(xyz_output) then
              call sampleLGA_xyz%dump_xyz(X,element_symbols)
            else
              write(uxLGA,*) X(:,:)
            endif
            write(upLGA,*) P(:,:,ip)
          enddo
        endif
      endif

    endif
    ! END OF LGA calculation
    !-------------------------------


    !-------------------------------
    ! FK LPI calculation
    if(compute_FK) then 
      call step_timer%start()
      ! COMPUTE FK PARAMETERS AT CENTROID POSITION (WARNING: X is overwritten here !)
      Xc(:,:)=SUM(X_PI,dim=3)/nbeads_sampler
      call compute_FK_parameters(Xc,iconv_FK)
      FK_computation_time_avg =  FK_computation_time_avg  &
        + (step_timer%elapsed_time() - FK_computation_time_avg)/avg_counter

      call step_timer%start()
      !$OMP PARALLEL DO PRIVATE(Qekin)
      DO ip=1,n_momenta_per_sample        
          call sample_fk_initial(Xc,X_FK(:,:,ip),P(:,:,ip))
          call compute_kinetic_energy(P(:,:,ip),Qekin,Qtemp(ip))
          if(apply_eckart) call apply_eckart_conditions(X,P(:,:,ip))
          if(charges_provided) then
            call classical_dynamics_IVR(X(:,:),P(:,:,ip),nsteps_dyn,dt_dyn,tcf_FK%Cpp(:,:,ip),Ek_dyn(:,:,ip),tcf_FK%Cmumu(:,:,ip))
          else
            call classical_dynamics_IVR(X(:,:),P(:,:,ip),nsteps_dyn,dt_dyn,tcf_FK%Cpp(:,:,ip),Ek_dyn(:,:,ip))
          endif
      ENDDO
      !$OMP END PARALLEL DO
      FK_dynamics_time_avg =  FK_dynamics_time_avg  &
        + (step_timer%elapsed_time() - FK_dynamics_time_avg)/avg_counter

      if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf_FK%Cpp)) .OR.  ANY(ISNAN(tcf_FK%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf_FK%Cpp))
      endif

      if(ignore_point) then
        FK_nan_count = FK_nan_count + 1
      else
        iconv_FK_avg = iconv_FK_avg +(real(iconv_FK,dp) - iconv_FK_avg)/avg_counter
        temp_FK_avg = temp_FK_avg + (SUM(Qtemp)/n_momenta_per_sample - temp_FK_avg)/avg_counter
        Ek_dyn_FK_avg = Ek_dyn_FK_avg + (SUM(Ek_dyn,dim=3)/n_momenta_per_sample - Ek_dyn_FK_avg)/isample
        call update_tcf_avg(tcf_FK)

        ! WRITE SAMPLES
        if(save_samples) then
          do ip = 1,n_momenta_per_sample
            if(xyz_output) then
              call sampleFK_xyz%dump_xyz(X,element_symbols)
            else
              write(uxFK,*) X(:,:)
            endif
            write(upFK,*) P(:,:,ip)
          enddo
        endif
      endif
    endif
    ! END OF FK LPI calculation
    !-------------------------------   

    !-------------------------------
    ! LGA/FK calculation
    if(compute_LGAFK) then 
      call step_timer%start()
      if(.not. compute_FK) then
      ! COMPUTE FK PARAMETERS AT CENTROID POSITION (WARNING: X is overwritten here !)
        Xc(:,:)=SUM(X_PI,dim=3)/nbeads_sampler
        call compute_FK_parameters(Xc,iconv_FK)
      endif
      call sample_momentum_LGAFK(X,P)
      LGAFK_computation_time_avg =  LGAFK_computation_time_avg  &
        + (step_timer%elapsed_time() - LGAFK_computation_time_avg)/avg_counter

      call step_timer%start()
      !$OMP PARALLEL DO PRIVATE(Qekin)
      DO ip=1,n_momenta_per_sample   
          call compute_kinetic_energy(P(:,:,ip),Qekin,Qtemp(ip))
          if(apply_eckart) call apply_eckart_conditions(X,P(:,:,ip))
          if(charges_provided) then
            call classical_dynamics_IVR(X(:,:),P(:,:,ip),nsteps_dyn,dt_dyn,tcf_LGAFK%Cpp(:,:,ip) &
              ,Ek_dyn(:,:,ip),tcf_LGAFK%Cmumu(:,:,ip))
          else
            call classical_dynamics_IVR(X(:,:),P(:,:,ip),nsteps_dyn,dt_dyn,tcf_LGAFK%Cpp(:,:,ip) &
            ,Ek_dyn(:,:,ip))
          endif
      ENDDO
      !$OMP END PARALLEL DO
      LGAFK_dynamics_time_avg =  LGAFK_dynamics_time_avg  &
        + (step_timer%elapsed_time() - LGAFK_dynamics_time_avg)/avg_counter

      if(charges_provided) then
        ignore_point = ANY(ISNAN(tcf_LGAFK%Cpp)) .OR.  ANY(ISNAN(tcf_LGAFK%Cmumu))
      else
        ignore_point = ANY(ISNAN(tcf_LGAFK%Cpp))
      endif

      if(ignore_point) then
        LGAFK_nan_count = LGAFK_nan_count + 1
      else
        iconv_LGAFK_avg = iconv_LGAFK_avg +(real(iconv_FK,dp) - iconv_LGAFK_avg)/avg_counter
        temp_LGAFK_avg = temp_LGAFK_avg + (SUM(Qtemp)/n_momenta_per_sample - temp_LGAFK_avg)/avg_counter
        Ek_dyn_LGAFK_avg = Ek_dyn_LGAFK_avg + (SUM(Ek_dyn,dim=3)/n_momenta_per_sample - Ek_dyn_LGAFK_avg)/isample
        call update_tcf_avg(tcf_LGAFK)

        ! WRITE SAMPLES
        if(save_samples) then
          do ip = 1,n_momenta_per_sample
            if(xyz_output) then
              call sampleLGAFK_xyz%dump_xyz(X,element_symbols)
            else
              write(uxLGAFK,*) X(:,:)
            endif
            write(upLGAFK,*) P(:,:,ip)
          enddo
        endif
      endif
    endif
    ! END OF LGA/FK calculation
    !-------------------------------    

    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter

    if(verbose) then
      ! PRINT SYSTEM INFO
      if(compute_EW) then
        write(*,*) "EW_effective_temperature=",real(temp_EW_avg/Cew_avg(1)),"K"        
        if(EW_auxsim%compute_Cew) then
          write(*,*) "avergage_Cew=",real(Cew_avg)
        endif
        if(correct_stat_EW) then
          write(*,*) "EW_weight_control=",100.*real(1._dp-EW_control_weight_avg/(1._dp+weight_EW_avg)),"%"
          write(*,*) "EW_avergage_weight=",real(1._dp+weight_EW_avg)
        endif
        if(EW_nan_count>0) write(*,*) "EW_NaN_count= "//int_to_str(EW_nan_count)
      endif
      if(compute_CK) then
        write(*,*) "CK_effective_temperature=",real(temp_CK_avg),"K"
        if(CK_nan_count>0) write(*,*) "CK_NaN_count= "//int_to_str(CK_nan_count)
      endif
      if(compute_LGA) then
        write(*,*) "LGA_effective_temperature=",real(temp_LGA_avg),"K"
        if(LGA_nan_count>0) write(*,*) "LGA_NaN_count= "//int_to_str(LGA_nan_count)
      endif
      if(compute_FK) then
        write(*,*) "FK_average_iconv=",real(iconv_FK_avg,sp)
        write(*,*) "FK_effective_temperature=",real(temp_FK_avg),"K"
        if(FK_nan_count>0) write(*,*) "FK_NaN_count= "//int_to_str(FK_nan_count)
      endif
      if(compute_LGAFK) then
        write(*,*) "LGAFK_average_iconv=",real(iconv_LGAFK_avg,sp)
        write(*,*) "LGAFK_effective_temperature=",real(temp_LGAFK_avg),"K"
        if(LGAFK_nan_count>0) write(*,*) "LGAFK_NaN_count= "//int_to_str(LGAFK_nan_count)
      endif
      if(compute_TRPMD) then
        if(TRPMD_nan_count>0) write(*,*) "TRPMD_NaN_count= "//int_to_str(TRPMD_nan_count)
      endif
      write(*,*) "average_potential_energy=",real(pot_avg*au%kcalpermol),"kcal/mol"    
      ! PRINT TIMINGS  
      write(*,*) "AVERAGE TIMINGS PER SAMPLE:"
      write(*,*) "  position sampling :",lgv_time_avg, "s. (for "//int_to_str(nsteps_btwn_samples)//" steps of PIMD)"
      if(compute_EW) then
        write(*,*) "  EW momentum sampling :",EW_momentum_time_avg,"s."
        write(*,*) "  EW classical propagation :",EW_dynamics_time_avg,"s."
      endif
      if(compute_CK) then
        write(*,*) "  CK momentum sampling :",CK_momentum_time_avg,"s."
        write(*,*) "  CK classical propagation :",CK_dynamics_time_avg,"s."
      endif
      if(compute_LGA) then
        write(*,*) "  LGA momentum sampling :",LGA_momentum_time_avg,"s."
        write(*,*) "  LGA classical propagation :",LGA_dynamics_time_avg,"s."
      endif
      if(compute_FK) then
        write(*,*) "  FK autocoherent calculation :",FK_computation_time_avg,"s."
        write(*,*) "  FK classical propagation :",FK_dynamics_time_avg,"s."
      endif
      if(compute_LGAFK) then
        write(*,*) "  LGAFK momentum sampling :",LGAFK_computation_time_avg,"s."
        write(*,*) "  LGAFK classical propagation :",LGAFK_dynamics_time_avg,"s."
      endif
      if(compute_TRPMD) then
        write(*,*) "  TRPMD propagation :",TRPMD_time_avg,"s."
      endif
      write(*,*) "  total time: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."

      remaining_time = step_time_avg*(nsamples-isample)
      job_time = full_timer%elapsed_time() + remaining_time
      write(*,*) "estimated job time : "//sec2human(job_time)
      write(*,*) "estimated remaining time : "//sec2human(remaining_time)

      ! RESET WINDOW AVERAGES
      avg_counter=0      
      lgv_time_avg = 0._dp      
      TRPMD_time_avg = 0._dp
      EW_momentum_time_avg = 0._dp
      EW_dynamics_time_avg=0._dp
      CK_momentum_time_avg = 0._dp
      CK_dynamics_time_avg=0._dp
      LGA_momentum_time_avg = 0._dp
      LGA_dynamics_time_avg=0._dp
      FK_computation_time_avg = 0._dp
      FK_dynamics_time_avg = 0._dp
      LGAFK_computation_time_avg = 0._dp
      LGAFK_dynamics_time_avg = 0._dp

      ! WRITE INTERMEDIATE RESULTS
      if(compute_EW) then
        call write_ivr_results("Ekin_EW",Ek_dyn_EW_avg/Cew_avg(1),nsteps_dyn,dt_dyn,.FALSE.)
        call write_ivr_results("temperature_EW",2._dp*Ek_dyn_EW_avg*au%kelvin/Cew_avg(1),nsteps_dyn,dt_dyn,.FALSE.)
        call write_tcf_results(tcf_EW0,1._dp+weight_EW_avg)
        if(EW_auxsim%compute_Cew) then
          call write_tcf_results(tcf_EW4,Cew_avg(1))
          if(ndof==1) then             
            call write_tcf_results(tcf_EW6,Cew_avg(2))
          endif
        endif
      endif
      if(compute_CK) then
        call write_ivr_results("Ekin_CK",Ek_dyn_CK_avg,nsteps_dyn,dt_dyn,.FALSE.)
        call write_ivr_results("temperature_CK",2._dp*Ek_dyn_CK_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
        call write_tcf_results(tcf_CK,1._dp)
      endif
      if(compute_LGA) then
        call write_ivr_results("Ekin_LGA",Ek_dyn_LGA_avg,nsteps_dyn,dt_dyn,.FALSE.)
        call write_ivr_results("temperature_LGA",2._dp*Ek_dyn_LGA_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
        call write_tcf_results(tcf_LGA,1._dp)
      endif
      if(compute_FK) then
        call write_ivr_results("Ekin_FK",Ek_dyn_FK_avg,nsteps_dyn,dt_dyn,.FALSE.)
        call write_ivr_results("temperature_FK",2._dp*Ek_dyn_FK_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
        call write_tcf_results(tcf_FK,1._dp)
      endif
      if(compute_LGAFK) then
        call write_ivr_results("Ekin_LGAFK",Ek_dyn_LGAFK_avg,nsteps_dyn,dt_dyn,.FALSE.)
        call write_ivr_results("temperature_LGAFK",2._dp*Ek_dyn_LGAFK_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
        call write_tcf_results(tcf_LGAFK,1._dp)
      endif
      if(compute_TRPMD) call write_tcf_results(tcf_TRPMD,1._dp)

    endif
  ENDDO

  ! WRITE FINAL RESULTS
  if(compute_EW) then
    call write_ivr_results("Ekin_EW",Ek_dyn_EW_avg/Cew_avg(1),nsteps_dyn,dt_dyn,.FALSE.)
    call write_ivr_results("temperature_EW",2._dp*Ek_dyn_EW_avg*au%kelvin/Cew_avg(1),nsteps_dyn,dt_dyn,.FALSE.)
    call write_tcf_results(tcf_EW0,1._dp+weight_EW_avg)
    if(EW_auxsim%compute_Cew) then
      call write_tcf_results(tcf_EW4,Cew_avg(1))
      if(ndof==1) then             
        call write_tcf_results(tcf_EW6,Cew_avg(2))
      endif
    endif
    if(save_samples) then
      if(xyz_output) then
        call sampleEW_xyz%destroy()
      else
        close(ux)
      endif
      close(up)
      close(uk2)
    endif
    if(num_aux_sim > 1) close(uk2var)
    if(EW_auxsim%compute_Cew) close(uew)
    if(correct_stat_EW) close(uewcorr)
  endif
  if(compute_CK) then
    call write_ivr_results("Ekin_CK",Ek_dyn_CK_avg,nsteps_dyn,dt_dyn,.FALSE.)
    call write_ivr_results("temperature_CK",2._dp*Ek_dyn_CK_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
    call write_tcf_results(tcf_CK,1._dp)
    if(save_samples) then
      if(xyz_output) then
        call sampleCK_xyz%destroy()
      else
        close(uxCK)
      endif
      close(upCK)
    endif
  endif
  if(compute_LGA) then
    call write_ivr_results("Ekin_LGA",Ek_dyn_LGA_avg,nsteps_dyn,dt_dyn,.FALSE.)
    call write_ivr_results("temperature_LGA",2._dp*Ek_dyn_LGA_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
    call write_tcf_results(tcf_LGA,1._dp)
    if(save_samples) then
      if(xyz_output) then
        call sampleLGA_xyz%destroy()
      else
        close(uxLGA)
      endif
      close(upLGA)
    endif
  endif
  if(compute_FK) then
    call write_ivr_results("Ekin_FK",Ek_dyn_FK_avg,nsteps_dyn,dt_dyn,.FALSE.)
    call write_ivr_results("temperature_FK",2._dp*Ek_dyn_FK_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
    call write_tcf_results(tcf_FK,1._dp)
    if(save_samples) then
      if(xyz_output) then
        call sampleFK_xyz%destroy()
      else
        close(uxFK)
      endif
      close(upFK)
    endif
  endif
  if(compute_LGAFK) then
    call write_ivr_results("Ekin_LGAFK",Ek_dyn_LGAFK_avg,nsteps_dyn,dt_dyn,.FALSE.)
    call write_ivr_results("temperature_LGAFK",2._dp*Ek_dyn_LGAFK_avg*au%kelvin,nsteps_dyn,dt_dyn,.FALSE.)
    call write_tcf_results(tcf_LGAFK,1._dp)
    if(save_samples) then
      if(xyz_output) then
        call sampleLGAFK_xyz%destroy()
      else
        close(uxLGAFK)
      endif
      close(upLGAFK)
    endif
  endif
  if(compute_TRPMD) call write_tcf_results(tcf_TRPMD,1._dp)
  

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS


  subroutine choose_random_bead(X0,Xbeads)
    implicit none
    real(dp), intent(inout) :: X0(:,:)
    real(dp), intent(in) :: Xbeads(:,:,:)
    real :: R
    integer :: ibead,nbeads

    nbeads = SIZE(Xbeads,dim=3)
    call random_number(R)
    ibead = ceiling(R*nbeads)

    X0(:,:)=Xbeads(:,:,ibead)

  end subroutine choose_random_bead

  subroutine sample_momentum_LGA(X,P)
		IMPLICIT NONE
		REAL(dp), intent(in) :: X(:,:)
    REAL(dp), intent(inout) :: P(:,:,:)
		REAL(dp), ALLOCATABLE :: hess(:,:),eigvals(:),Q(:),eigMat(:,:)
		REAL(dp) :: omega2,u,k2
		INTEGER :: c, i,j,k,INFO,n_momenta,ip
    real(dp), parameter :: eigval_thr = 1.0e-9

    n_momenta = size(P,dim=3)
		allocate(hess(ndof,ndof))
    
    hess = compute_hessian(X)
    !COMPUTE MASS WEIGHTED HESSIAN
		c=0
		DO j=1,ndim; do i=1,nat
				c=c+1
				DO k=1,ndim
						hess((k-1)*nat+1:k*nat,c) &
								=hess((k-1)*nat+1:k*nat,c) &
										/sqrt(mass(i)*mass(:))
				ENDDO
		ENDDO ; ENDDO

		allocate(eigvals(ndof),eigMat(ndof,ndof))
		!DIAGONALIZE HESSIAN
    call sym_mat_diag(hess,eigvals,eigMat,INFO)            
    IF(INFO/=0) STOP "[IVR.f90] Error: could not diagonalize mass weighted hessian for LGA!"
		deallocate(hess)

    if(LGA_classical_rotations) eigvals(1:6) = 0._dp

    allocate(Q(ndof))		
		!COMPUTE QUANTUM CORRECTION TO KINETIC ENERGY IN NORMAL MODES		
    DO i=1,ndof
      omega2=eigvals(i)
      if(abs(omega2)<eigval_thr) then
        Q(i)=1._dp
      elseif(omega2>=0) then 
        ! HARMONIC WHEN REAL FREQUENCY
        u=0.5_dp*beta*sqrt(omega2)
        Q(i)=u/tanh(u)
      else 
        ! LIU & MILLER ANSATZ WHEN IMAGINARY FREQUENCY
        u=0.5_dp*beta*sqrt(-omega2)
        Q(i)=tanh(u)/u
      endif
    ENDDO    

    do ip=1,n_momenta
      call randGaussN(eigvals)
      eigvals(:) = eigvals(:)*sqrt(Q(:)/beta)
      P(:,:,ip) = RESHAPE(matmul(EigMat,eigvals) &
                ,(/nat,ndim/) )
      DO i=1,ndim
        P(:,i,ip)=P(:,i,ip)*sqrt(mass(:))
      ENDDO
    enddo
			
	end subroutine sample_momentum_LGA

   subroutine sample_momentum_LGAFK(X,P)
		IMPLICIT NONE
		REAL(dp), intent(in) :: X(:,:)
    REAL(dp), intent(inout) :: P(:,:,:)
		REAL(dp), ALLOCATABLE :: hess(:,:),eigvals(:),Q(:),eigMat(:,:)
		REAL(dp), ALLOCATABLE ::sigma2LGA(:,:),sigma2(:,:)
		REAL(dp) :: omega2,u,k2
		INTEGER :: c, i,j,k,INFO,n_momenta,ip
    real(dp), parameter :: eigval_thr = 1.0e-9

    n_momenta = size(P,dim=3)
		allocate(hess(ndof,ndof),sigma2LGA(ndof,ndof),sigma2(ndof,ndof))
    
    hess = compute_hessian(X)
    !COMPUTE MASS WEIGHTED HESSIAN
		c=0
		DO j=1,ndim; do i=1,nat
				c=c+1
				DO k=1,ndim
						hess((k-1)*nat+1:k*nat,c) &
								=hess((k-1)*nat+1:k*nat,c) &
										/sqrt(mass(i)*mass(:))
				ENDDO
		ENDDO ; ENDDO

		allocate(eigvals(ndof),eigMat(ndof,ndof))
		!DIAGONALIZE HESSIAN
    call sym_mat_diag(hess,eigvals,eigMat,INFO)            
    IF(INFO/=0) STOP "[IVR.f90] Error: could not diagonalize mass weighted hessian for LGA!"
		deallocate(hess)

    if(LGA_classical_rotations) eigvals(1:6) = 0._dp

    allocate(Q(ndof))		
		!COMPUTE QUANTUM CORRECTION TO KINETIC ENERGY IN NORMAL MODES		
    sigma2LGA=0._dp
    DO i=1,ndof
      omega2=eigvals(i)
      if(abs(omega2)<eigval_thr) then
        Q(i)=1._dp
      elseif(omega2>=0) then 
        ! HARMONIC WHEN REAL FREQUENCY
        u=0.5_dp*beta*sqrt(omega2)
        Q(i)=u/tanh(u)
      else 
        ! LIU & MILLER ANSATZ WHEN IMAGINARY FREQUENCY
        u=0.5_dp*beta*sqrt(-omega2)
        Q(i)=tanh(u)/u
      endif
      sigma2LGA(i,i)=Q(i)/beta
    ENDDO    

    sigma2LGA = matmul(eigMat,matmul(sigma2LGA,transpose(eigMat)))
    call get_sigma2P_fk(sigma2)
    sigma2=0.5_dp*(sigma2+sigma2LGA)
    call sym_mat_diag(sigma2,eigvals,eigMat,INFO)   

    do ip=1,n_momenta
      call randGaussN(Q)
      Q(:) = Q(:)*sqrt(eigvals(:))
      P(:,:,ip) = RESHAPE(matmul(EigMat,Q) &
                ,(/nat,ndim/) )
      DO i=1,ndim
        P(:,i,ip)=P(:,i,ip)*sqrt(mass(:))
      ENDDO
    enddo
			
	end subroutine sample_momentum_LGAFK

END PROGRAM IVR
