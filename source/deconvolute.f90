PROGRAM deconvolute
	use kinds
	use nested_dictionaries
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use spectrum_deconvolution
  use qtb_types
  use system_commons
  
  implicit none
	type(DICT_STRUCT) :: param_library

  TYPE(timer_type) :: full_timer
  real :: cpu_time_start,cpu_time_finish

  INTEGER :: n_threads

  character(:), allocatable ::  input_file, method_name

  type(QTB_type), save :: qtb
  INTEGER :: u,nom,INFO,i,niter,idof,nfields
  LOGICAL :: kubo_input
  INTEGER :: method            ! 1 -> QTB or adQTB   | 2 -> classical Langevin 
  REAL(dp), ALLOCATABLE :: s0(:,:),sout(:,:),omega(:),srec(:,:),omegasym(:),s0sym(:,:)
  REAL(dp) :: om,stmp,gamma,ommin=TINY(1.),omega0, dt
  LOGICAL :: symmetrize, freq0_exist,use_corr_pot
  REAL(dp) :: input_unit,omegacut,omegasmear,corrfact
  CHARACTER (:), allocatable :: line
  CHARACTER (LEN = 256), DIMENSION(:), allocatable :: fields
  CHARACTER (LEN = 32), DIMENSION(:), allocatable  :: field_types

  call full_timer%start
  call cpu_time(cpu_time_start)

  call initialize_framework(param_library,initialize_output=.FALSE.)	

  ! GET NUMBER OF THREADS        
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
  !----------------------------------------

  call initialize_system(param_library)

  dt = param_library%get("parameters/dt")
  gamma = param_library%get("parameters/gamma")

  input_unit = atomic_units_get_multiplier( &
                  param_library%get("deconvolution/frequency_unit",default="cm-1") &
                )
  kubo_input = param_library%get("deconvolution/kubo_input")
  niter = param_library%get("deconvolution/n_iter")
  input_file = TRIM(param_library%get("deconvolution/input_file"))
  
  
  !----------------------------------------
  !call open_output_files(param_library)
  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  write(*,*)
  write(*,*) "Initialization done."

  
  OPEN(newunit=u,file=input_file,action='READ')
  
  ! read number of columns
  ALLOCATE(character(len=30*(ndof+1)) :: line)
  allocate(fields(ndof+1),field_types(ndof+1))
  READ( UNIT=u, FMT='(A)' ,iostat=INFO) line
  IF(INFO/=0) STOP "[deconvolute.f90] Error: empty spectrum file !"
  CALL split_line( line, nfields, fields, field_types )
  write(*,*) "detected "//int_to_str(nfields)//" fields"
  deallocate(line)
  deallocate(field_types, fields)
  
  write(*,*) "Reading number of frequencies..."
  nom=1
  DO    
    READ(u,*,iostat=INFO) om,stmp
    IF(INFO/=0) exit
    nom=nom+1
  ENDDO
  write(*,*) "done!"
  
  
  write(*,*)
  write(*,*) "Storing input spectrum..."
  REWIND(u)
  ALLOCATE(s0(nom,nfields-1),omega(nom))  
  DO i=1,nom
    READ(u,*) omega(i),s0(i,:)
  ENDDO
  CLOSE(u)  
  omega=omega/input_unit

  ALLOCATE(sout(nom,nfields-1),srec(nom,nfields-1))
  ALLOCATE(s0sym(nom,nfields-1),omegasym(nom))    
  s0sym=s0
  omegasym=omega   
  sout=0._dp
  srec=0._dp

  write(*,*) "nom=",nom 

  if (.not. kubo_input) then
    DO i=1,nom
      if(abs(omegasym(i))>ommin) then
        om=0.5*omegasym(i)/kT
        s0sym(i,:)=s0sym(i,:)*tanh(om)/om
      endif
    ENDDO
  endif

  do idof=1,nfields-1
    call deconvolute_spectrum(s0sym(:,idof),omegasym,niter,.false.,1.0d-10,gamma,kT &
            ,kernel_lorentz,sout(:,idof),srec(:,idof),verbose=.TRUE.)

    if (.not. kubo_input) then
      DO i=1,nom
        if(abs(omegasym(i))>ommin) then
          om=0.5*omegasym(i)/kT
          s0sym(i,idof)=s0sym(i,idof)*om/tanh(om)
          srec(i,idof)=srec(i,idof)*om/tanh(om)
          sout(i,idof)=sout(i,idof)*om/tanh(om)
        endif
      ENDDO
    endif
  enddo

  OPEN(newunit=u,file=input_file//".deconv")
  DO i=1,nom
    WRITE(u,*) omegasym(i)*input_unit,sout(i,:)
  ENDDO
  CLOSE(u)
  write(*,*) " deconvoluted spectrum written to '"//input_file//".deconv'."

  OPEN(newunit=u,file=input_file//".reconv")
  DO i=1,nom
    WRITE(u,*) omegasym(i)*input_unit,srec(i,:)
  ENDDO
  CLOSE(u)
  write(*,*) " deconvoluted spectrum written to '"//input_file//".reconv'."

  OPEN(newunit=u,file=input_file//".ratio")
  DO i=1,nom
    WRITE(u,*) omegasym(i)*input_unit,sout(i,:)/s0sym(i,:)
  ENDDO
  CLOSE(u)
  write(*,*) " deconvolution ratio written to '"//input_file//".ratio'."

  write(*,*)
  write(*,*) "************************************************"
  write(*,*) "sum of the initial spectrum: ", sum(s0sym)
  write(*,*) "sum of the deconvoluted spectrum: ", sum(sout)
  write(*,*) "sum of the reconvoluted spectrum: ", sum(srec)
  write(*,*) "deconv to reconv ratio: ", sum(srec)/sum(sout)
  write(*,*) "deconv to initial ratio: ", sum(s0sym)/sum(sout)
  write(*,*) "************************************************"
  write(*,*)
  

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

END PROGRAM deconvolute
