MODULE kinds
!! Definition of kinds and constants
  IMPLICIT NONE
  
  !KINDS
  INTEGER, PARAMETER, PUBLIC :: sp = selected_real_kind(6, 37)
    !! simple precision
  INTEGER, PARAMETER, PUBLIC :: dp = selected_real_kind(15, 307)
    !! double precision

	CHARACTER(*), PARAMETER :: null_type="NULL" &
                            ,int_type="INTEGER",real_type="REAL" &
                            ,int_array_type="INTEGER_ARRAY",real_array_type="REAL_ARRAY" &
                            ,logical_type="LOGICAL",string_type="STRING" &
                            ,logical_array_type="LOGICAL_ARRAY"
      !! define data_type key strings

  !CONSTANTS
  REAL(dp), PARAMETER :: pi=acos(-1._dp)
    !! Pi constant
  REAL(dp), PARAMETER :: hbar = 1._dp 
    !! Planck constant

END MODULE kinds