module boundaries
  use kinds
  use nested_dictionaries
  use atomic_units
  implicit none

  TYPE SIMULATION_BOX_TYPE
		INTEGER :: ndim
		LOGICAL :: initialized=.FALSE.
		REAL(dp), ALLOCATABLE :: hcell(:,:) , hcell_inv(:,:)
    REAL(dp), ALLOCATABLE :: box_lengths(:)
    REAL(dp), ALLOCATABLE :: a1(:),a2(:),a3(:)
	CONTAINS
		PROCEDURE :: apply_pbc => simulation_box_apply_pbc
		PROCEDURE :: is_diagonal => simulation_box_is_diagonal
    PROCEDURE :: initialize => simulation_box_initialize
	END TYPE SIMULATION_BOX_TYPE

	TYPE(SIMULATION_BOX_TYPE), SAVE :: simulation_box

  PRIVATE
  PUBLIC :: simulation_box, SIMULATION_BOX_TYPE

contains

  subroutine simulation_box_apply_pbc(self,X)
		IMPLICIT NONE
		CLASS(SIMULATION_BOX_TYPE), intent(in) :: self
		REAL(dp), INTENT(inout) :: X(:,:)
		REAL(dp), ALLOCATABLE :: q(:,:)
		INTEGER :: i
		
		allocate(q(size(X,2),size(X,1)))

		q=matmul(self%hcell_inv,transpose(X))
		q=q-int(q)
		X=transpose(matmul(self%hcell,q))

		deallocate(q)

	end subroutine simulation_box_apply_pbc

	function simulation_box_is_diagonal(self) result(diag)
		IMPLICIT NONE
		CLASS(SIMULATION_BOX_TYPE), intent(inout) :: self
		LOGICAL :: diag
    REAL(dp), PARAMETER :: boxthr=1.e-8

		diag = (abs(self%a1(2))<boxthr) .AND.  (abs(self%a1(3))<boxthr) &
				 .AND. (abs(self%a2(1))<boxthr) .AND.  (abs(self%a2(3))<boxthr) &
				 .AND. (abs(self%a3(1))<boxthr) .AND.  (abs(self%a2(3))<boxthr)

	end function simulation_box_is_diagonal

	subroutine simulation_box_initialize(self,param_library)
	!! INITIALIZE PERIODIC BOUNDARIES AND SIMULATION BOX
		IMPLICIT NONE
    CLASS(SIMULATION_BOX_TYPE), intent(inout) :: self
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: ndim, i,j,INFO
		REAL(dp), allocatable :: hcellin(:,:)
		INTEGER, allocatable :: ipiv(:)

		! if(ndim/=3) then
		! 	write(*,*) "Warning: ndim must be 3 for PBC! Ignoring box input."
		! 	return
		! endif

		!if(.NOT. simulation_box%use_pbc) return	

		self%initialized=.TRUE.	
			
		self%a1 = param_library%get("BOUNDARIES/a1")
		!! @input_file BOUNDARIES/a1 [REAL_ARRAY] (boundaries)  
    ndim = size(self%a1)
    self%ndim=ndim
    if(ndim>3) STOP "[boundaries.f90] Error: maximum size for 'BOUNDARIES/a1' is 3."
    allocate(self%hcell(ndim,ndim))
		allocate(self%hcell_inv(ndim,ndim))
		allocate(self%box_lengths(ndim))	

		self%hcell(:,1)=self%a1(:)
		self%box_lengths(1)=NORM2(self%a1)
		if(ndim>=2) then
			self%a2 = param_library%get("BOUNDARIES/a2")
			!! @input_file BOUNDARIES/a2 [REAL_ARRAY] (boundaries)  
			if(size(self%a2)/=ndim) STOP "[boundaries.f90] Error: 'BOUNDARIES/a2' must be the same size as 'BOUNDARIES/a1'"
			self%hcell(:,2)=self%a2(:)
			self%box_lengths(2)=NORM2(self%a2)
		endif
		if(ndim==3) then
			self%a3 = param_library%get("BOUNDARIES/a3")
			!! @input_file BOUNDARIES/a1 [REAL_ARRAY] (boundaries)  
			if(size(self%a3)/=ndim) STOP "[boundaries.f90] Error: 'BOUNDARIES/a3' must be the same size as 'BOUNDARIES/a1'"
			self%hcell(:,3)=self%a3(:)
			self%box_lengths(3)=NORM2(simulation_box%a3)
		endif

		write(*,*) "CELL MATRIX  (Angstrom)  | CELL MATRIX INVERSE (1/Angstrom)"
	  allocate(hcellin(ndim,ndim),ipiv(ndim))
    hcellin=self%hcell
    self%hcell_inv=0._dp
    DO i=1,ndim
      self%hcell_inv(i,i)=1._dp
    ENDDO
    CALL DGESV(ndim,ndim,hcellin,ndim,ipiv,simulation_box%hcell_inv,ndim,INFO)
    do i=1,ndim
			write(*,*) self%hcell(i,:)*au%bohr, "  |  ", self%hcell_inv(i,:)/au%bohr
		enddo
	
	end subroutine simulation_box_initialize

end module boundaries