!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Following is a FFT implementation based on algorithm proposed by
! Glassman, a general FFT algorithm supporting arbitrary input length.
!
! W. E. Ferguson, Jr., "A simple derivation of Glassman general-n fast
! Fourier transform," Comput. and Math. with Appls., vol. 8, no. 6, pp.
! 401-411, 1982.
!
! Original implemtation online at http://www.jjj.de/fft/fftpage.html
!
! Updated  
!  -  to handle double-precision as well
!  -  unnecessary scaling code removed
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module glassman_fft
  use kinds
  implicit none

contains
  SUBROUTINE SPCFFT(U,N,ISIGN,WORK)
    
    IMPLICIT NONE
    
    LOGICAL :: INU
    INTEGER :: A,B,C,N,I,ISIGN
    COMPLEX(dp) :: U(*),WORK(*)
    !U AND WORK AT LEAST OF SIZE N
    
    A = 1
    B = N
    C = 1
    INU = .TRUE.
    
    DO WHILE ( B .GT. 1 )
        A = C * A
        C = 2
        DO WHILE ( MOD(B,C) .NE. 0 )
          C = C + 1
        END DO
        B = B / C
        IF ( INU ) THEN
          CALL SPCPFT (A,B,C,U,WORK,ISIGN)
        ELSE
          CALL SPCPFT (A,B,C,WORK,U,ISIGN)
        END IF
        INU = ( .NOT. INU )
    END DO
    
    IF ( .NOT. INU ) THEN
        DO I = 1, N
          U(I) = WORK(I)
        END DO
    END IF
    
    RETURN
  END SUBROUTINE SPCFFT
  
  
  SUBROUTINE SPCPFT( A, B, C, UIN, UOUT, ISIGN )
    
    IMPLICIT NONE
    
    INTEGER :: ISIGN,A,B,C,IA,IB,IC,JCR,JC
    
    DOUBLE PRECISION :: ANGLE
    
    COMPLEX(dp) :: UIN(B,C,A),UOUT(B,A,C),DELTA,OMEGA,SUM
    
    ANGLE = 6.28318530717958_dp / REAL( A * C, kind=dp)
    OMEGA = CMPLX( 1.0, 0.0, kind=dp )
    
    IF( ISIGN .EQ. 1 ) THEN
        DELTA = CMPLX( DCOS(ANGLE), DSIN(ANGLE), kind=dp )
    ELSE
        DELTA = CMPLX( DCOS(ANGLE), -DSIN(ANGLE), kind=dp )
    END IF
    
    DO IC = 1, C
        DO IA = 1, A
          DO IB = 1, B
              SUM = UIN( IB, C, IA )
              DO JCR = 2, C
                JC = C + 1 - JCR
                SUM = UIN( IB, JC, IA ) + OMEGA * SUM
              END DO
              UOUT( IB, IA, IC ) = SUM
          END DO
          OMEGA = DELTA * OMEGA
        END DO
    END DO
    
    RETURN
  END SUBROUTINE SPCPFT
end module glassman_fft