module PI_sampler
  use kinds
  use system_commons
  use nested_dictionaries
  use file_handler
  use matrix_operations
  use potential
  use qtb_types
  implicit none

  real(dp), save :: gamma0
  real(dp), save :: dt,dt2
  integer, save :: avg_counter
  real(dp), save :: temp_avg
  real(dp), save :: Ekin_avg,temp_centroid_avg
  logical, save :: fast_forward_langevin
  integer, save :: nu
  real(dp), allocatable, save :: EigMat(:,:),EigMatTr(:,:),Omk(:),m_scale(:)
  real(dp), save :: TRPMD_lambda,adiab_scale

  logical, save :: qtb_thermostat
  logical, save :: ACMD
  integer, save :: nsteps_multi
  real(dp), save :: dt_multi,dt2_multi
  logical, save :: initialization_done=.FALSE.

  TYPE(QTB_TYPE), allocatable :: qtbs(:)

  PRIVATE
  PUBLIC :: initialize_PI_sampler, update_sample_position, TRPMD_dynamics_ivr &
              , initialize_trpmd, update_block_trpmd, sample_PI_momentum &
              , get_PI_number_of_beads, get_PI_Omk, get_dipole_ring_polymer &
              , BAOAB_step_PI, to_normal_modes_vector, from_normal_modes_vector, qtbs
contains

  subroutine initialize_PI_sampler(param_library,nbeads_sampler)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    integer, intent(out) :: nbeads_sampler
    integer :: stat,i,j,k

    if(initialization_done) then
      write(0,*) "[PI_sampler.f90] Warning: PI already initialized!"
    else
      initialization_done = .TRUE.
    endif

    dt = param_library%get("sampler/dt")
    !! @input_file sampler/dt [REAL] (PI_sampler/IVR) 
    if(dt<=0) STOP "[PI_sampler.f90] Error: parameters/dt must be positive"
    dt2 = 0.5_dp*dt

    nu =param_library%get("parameters/nbeads")
    !! @input_file parameters/nbeads [INTEGER] (PI_sampler/IVR) 
    nbeads_sampler=nu
    call initialize_normal_modes()
    qtb_thermostat = param_library%get("parameters/use_qtb_thermostat",default=.FALSE.)
    !! @input_file parameters/use_qtb_thermostat [LOGICAL] (PI_sampler/IVR, default=.FALSE.) 

    if(qtb_thermostat) then
      fast_forward_langevin = .FALSE.
      TRPMD_lambda = 0._dp
    else
      fast_forward_langevin = param_library%get("sampler/sampler_fast_forward_langevin",default=.FALSE.)
      !! @input_file sampler/sampler_fast_forward_langevin [LOGICAL] (PI_sampler/IVR, default=.FALSE.) 
      if(fast_forward_langevin) write(*,*) "fast forward langevin for PI sampler"
      TRPMD_lambda = param_library%get("sampler/trpmd_lambda",default=1._dp)
      !! @input_file sampler/trpmd_lambda [LOGICAL] (PI_sampler/IVR, default=1.) 
    endif

    gamma0 = param_library%get("sampler/gamma") 
    !! @input_file sampler/gamma [REAL] (PI_sampler/IVR)    
    if(qtb_thermostat) call initialize_RPQTB(param_library,gamma0,dt)

    Ekin_avg = 0._dp
    temp_centroid_avg = 0._dp
    temp_avg = 0._dp
    avg_counter = 0    

  end subroutine initialize_PI_sampler

  function get_PI_number_of_beads() result(nbeads)
    integer :: nbeads
    if(.not. initialization_done) then
      STOP "[PI_sampler.f90] Error: PI not initialized!"
    endif
    nbeads = nu    
  end function get_PI_number_of_beads

  function get_PI_Omk() result(omk_out)
    real(dp) :: omk_out(nu)
    if(.not. initialization_done) then
      STOP "[PI_sampler.f90] Error: PI not initialized!"
    endif
    omk_out(:) = Omk    
  end function get_PI_Omk

  function get_PI_Eigmat() result(Eigmat_out)
    real(dp) :: Eigmat_out(nu,nu)
    if(.not. initialization_done) then
      STOP "[PI_sampler.f90] Error: PI not initialized!"
    endif
    Eigmat_out(:,:) = Eigmat(:,:)
  end function get_PI_Eigmat

  subroutine initialize_trpmd(param_library,nbeads_sampler,tcf_name,dt_sampler)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    integer, intent(out) :: nbeads_sampler
    real(dp), intent(out) :: dt_sampler
    character(:), allocatable, intent(inout) :: tcf_name
    integer :: i

    if(initialization_done) then
      write(0,*) "[PI_sampler.f90] Warning: PI already initialized!"
    else
      initialization_done = .TRUE.
    endif

    dt = param_library%get("parameters/dt")
    !! @input_file parameters/dt [REAL] (PI_sampler/TRPMD) 
    dt_sampler = dt
    if(dt<=0) STOP "parameters/dt must be positive"
    dt2 = 0.5_dp*dt   

    nu =param_library%get("parameters/nbeads")
    !! @input_file parameters/nbeads [INTEGER] (PI_sampler/TRPMD) 
    nbeads_sampler=nu
    call initialize_normal_modes()

    qtb_thermostat = param_library%get("parameters/use_qtb_thermostat",default=.FALSE.)
    !! @input_file parameters/use_qtb_thermostat [LOGICAL] (PI_sampler/TRPMD, default=.FALSE.) 

    gamma0 = param_library%get("parameters/gamma",default=0._dp)
    !! @input_file parameters/gamma [REAL] (PI_sampler/TRPMD, default=0.) 
    if(gamma0<0._dp) gamma0=0._dp
    
    if(qtb_thermostat) then
      tcf_name = "RPQTB"
      TRPMD_lambda=0._dp   
      fast_forward_langevin = .FALSE.   
    else
      TRPMD_lambda = param_library%get("parameters/trpmd_lambda",default=1._dp)
      !! @input_file parameters/trpmd_lambda [REAL] (PI_sampler/TRPMD, default=1.) 
      if(TRPMD_lambda > 0._dp) then      
        tcf_name = "TRPMD"
      else
        tcf_name = "RPMD"
        TRPMD_lambda=0._dp
      endif
      fast_forward_langevin = param_library%get("parameters/fast_forward_langevin",default=.FALSE.)  
      !! @input_file parameters/fast_forward_langevin [LOGICAL] (PI_sampler/TRPMD, default=.FALSE.)   
      if(fast_forward_langevin) write(*,*) "fast forward langevin for sampler"
    endif    

    ACMD = param_library%get("parameters/ACMD",default=.FALSE.)
    !! @input_file parameters/ACMD [LOGICAL] (PI_sampler/TRPMD, default=.FALSE.)
    if(ACMD) then
      tcf_name = "ACMD"
      adiab_scale = param_library%get("parameters/acmd_scale",default=64._dp)
      !! @input_file parameters/acmd_scale [REAL] (PI_sampler/TRPMD, default=64.)
      nsteps_multi = param_library%get("parameters/acmd_nsteps_multi",default=nint(adiab_scale))
      !! @input_file parameters/acmd_nsteps_multi [INTEGER] (PI_sampler/TRPMD, default=nint(acmd_scale))
      m_scale(:) = (Omk(:)/(adiab_scale*nu*kT))**2
      m_scale(1) = 1._dp !do not scale the mass of the centroid     
      dt_multi = dt/nsteps_multi
      dt2_multi = 0.5_dp*dt_multi
      dt = nsteps_multi*dt_multi
      dt2 = 0.5_dp*dt
      Omk(:)=Omk(:)/sqrt(m_scale(:))
      write(*,*) "#ACMD time step is divided in "//int_to_str(nsteps_multi)//" steps of ",dt_multi*au%fs,"fs"  
      write(*,*) "#ACMD SCALED POLYMER FREQUENCIES:"
      write(*,*) 1, 0._dp,"THz (infinity fs)"
      do i=2,nu
        write(*,*) i,Omk(i)*au%THz/(2*pi),"THz (",2*pi*au%fs/Omk(i),"fs)"
      enddo           
    else      
      m_scale(:)=1._dp
      dt_multi = dt
      dt2_multi = dt2
      nsteps_multi = 1
    endif

    if(qtb_thermostat) call initialize_RPQTB(param_library,gamma0,dt_multi)    

    Ekin_avg = 0._dp
    temp_centroid_avg = 0._dp
    temp_avg = 0._dp
    avg_counter = 0    

  end subroutine initialize_trpmd

  subroutine update_block_trpmd(X,EigP,nsteps,verbose,Xtraj,Pctraj)
    implicit none
    real(dp), intent(inout) :: X(:,:,:),EigP(:,:,:)
    real(dp), intent(inout) :: Xtraj(:,:,:,:),Pctraj(:,:,:)
    integer, intent(in) :: nsteps
    logical, intent(in) :: verbose
    integer :: istep,i,j,imulti
    real(dp) :: temp,temp_sum,sqrtnu
    real(dp) :: Ekin,Ekin_sum,temp_centroid_sum,temp_centroid
    real(dp), allocatable :: F(:,:,:)
    real(dp), allocatable :: EigX(:,:,:),EigF(:,:,:)

    allocate(F(nat,ndim,nu),EigF(nat,ndim,nu),EigX(nat,ndim,nu))

    sqrtnu=sqrt(real(nu,dp))
    temp_sum = 0.
    Ekin_sum = 0.
    temp_centroid_sum = 0.
    avg_counter = avg_counter + 1

    ! GET NORMAL MODES POSITIONS
    EigX=to_normal_modes_vector(X)

    call update_PI_forces(X,F,EigF)

    do istep = 1,nsteps 
      do imulti = 1,nsteps_multi
        call BAOAB_step_PI(X,EigX,EigP,F,EigF,dt_multi,gamma0,.TRUE.)
      enddo

      Xtraj(:,:,:,istep)=X(:,:,:)
      Pctraj(:,:,istep)=EigP(:,:,1)/sqrtnu
      call compute_PI_kinetic_energy(X,Pctraj(:,:,istep),F,Ekin,temp,temp_centroid)
      temp_sum = temp_sum + temp
      temp_centroid_sum = temp_centroid_sum + temp_centroid
      Ekin_sum = Ekin_sum + Ekin
    enddo  

    temp_centroid_avg = temp_centroid_avg  &
        + (temp_centroid_sum/nsteps - temp_centroid_avg)/avg_counter
    temp_avg = temp_avg + (temp_sum/nsteps - temp_avg)/avg_counter
    Ekin_avg = Ekin_avg + (Ekin_sum/nsteps - Ekin_avg)/avg_counter

    if(verbose) then
      write(*,*) "PI_effective_temperature= ",real(temp_avg,sp),"K"
      write(*,*) "PI_centroid_temperature= ",real(temp_centroid_avg,sp),"K"
      write(*,*) "Ekin= ",real(Ekin_avg,sp)
      temp_avg=0._dp
      temp_centroid_avg = 0._dp
      Ekin_avg = 0._dp
      avg_counter=0
    endif

    deallocate(F,EigF,EigX)

  end subroutine update_block_trpmd

  subroutine update_sample_position(X,nsteps,verbose)
    implicit none
    real(dp), intent(inout) :: X(:,:,:)
    integer, intent(in) :: nsteps
    logical, intent(in) :: verbose
    integer :: istep,i,j
    real(dp) :: Xtmp(nu)
    real(dp) :: temp,temp_sum,temp_centroid_sum,temp_centroid
    real(dp), allocatable :: EigP(:,:,:),EigX(:,:,:),F(:,:,:),EigF(:,:,:)
    real(dp) :: Ekin,Ekin_sum,sqrtnu

    allocate(EigP(nat,ndim,nu),F(nat,ndim,nu),EigF(nat,ndim,nu),EigX(nat,ndim,nu))
    call sample_PI_momentum(EigP)
    call update_PI_forces(X,F,EigF)

    temp_sum = 0.
    Ekin_sum = 0.
    temp_centroid_sum = 0.
    avg_counter = avg_counter + 1

    EigX=to_normal_modes_vector(X)
    sqrtnu=sqrt(real(nu,dp))

    do istep = 1,nsteps 
      call BAOAB_step_PI(X,EigX,EigP,F,EigF,dt,gamma0,.TRUE.)

      call compute_PI_kinetic_energy(X,EigP(:,:,1)/sqrtnu,F,Ekin,temp,temp_centroid)
      temp_sum = temp_sum + temp
      temp_centroid_sum = temp_centroid_sum + temp_centroid
      Ekin_sum = Ekin_sum + Ekin
    enddo  

    temp_centroid_avg = temp_centroid_avg  &
        + (temp_centroid_sum/nsteps - temp_centroid_avg)/avg_counter
    temp_avg = temp_avg + (temp_sum/nsteps - temp_avg)/avg_counter
    Ekin_avg = Ekin_avg + (Ekin_sum/nsteps - Ekin_avg)/avg_counter

    if(verbose) then
      write(*,*) "PI_effective_temperature= ",real(temp_avg,sp),"K"
      write(*,*) "PI_centroid_temperature= ",real(temp_centroid_avg,sp),"K"
      write(*,*) "PI_Ekin_vir= ",real(Ekin_avg,sp)
      temp_avg = 0._dp
      temp_centroid_avg = 0._dp
      Ekin_avg = 0._dp
      avg_counter = 0
    endif

  end subroutine update_sample_position

  subroutine compute_PI_kinetic_energy(X,Pc,F,Ekin,temp,temp_centroid)
    IMPLICIT NONE
		real(dp), intent(in) :: X(:,:,:),Pc(:,:),F(:,:,:)
    real(dp), intent(out) :: Ekin,temp,temp_centroid
    integer :: i,j,k
    real(dp), allocatable :: Xc(:,:)
    real(dp) :: Ekc

    allocate(Xc(nat,ndim))
    Xc(:,:)=SUM(X,dim=3)/nu

    temp=0._dp
    Ekin = 0._dp
    Ekc = 0._dp

    do j=1,ndim ; do i=1,nat
      !Ekin=Ekin+(X(i,j,k)-Xc(i,j))*F(i,j,k)
      Ekc=Ekc + Pc(i,j)**2/mass(i)
    enddo ; enddo
    Ekc = 0.5_dp*Ekc

    do k=1,nu
      do j=1,ndim ; do i=1,nat
        !Ekin=Ekin+(X(i,j,k)-Xc(i,j))*F(i,j,k)
        Ekin=Ekin+(X(i,j,k)-Xc(i,j))*F(i,j,k)
      enddo ; enddo
    enddo   
    Ekin =  -0.5_dp*Ekin/nu
    
    if(qtb_thermostat) then
      Ekin = Ekc + Ekin
    else
      Ekin=0.5_dp*ndof*kT+Ekin
    endif
    
    temp_centroid = au%kelvin*2._dp*Ekc/ndof
    temp = au%kelvin*2._dp*Ekin/ndof

    deallocate(Xc)

  end subroutine compute_PI_kinetic_energy

  subroutine sample_PI_momentum(P)
    real(dp), intent(inout) :: P(:,:,:)
    integer :: j,k

    do k=1,nu
      do j=1,ndim
          call randGaussN(P(:,j,k))
          P(:,j,k)=P(:,j,k) &
              *SQRT(mass(:)*m_scale(k)*nu*kT)
      enddo
    enddo

  end subroutine sample_PI_momentum

!---------------------------------------------------------

  subroutine initialize_normal_modes()
		implicit none
		integer :: i,j,k,INFO
		real(dp),allocatable :: WORK(:)

		allocate(EigMat(nu,nu),EigMatTr(nu,nu), Omk(nu))
		allocate(WORK(3*nu-1))
    allocate(m_scale(nu))
    m_scale(:) = 1._dp
		
		EigMat=0

		do i=1,nu-1
			EigMat(i,i)=2
			EigMat(i,i+1)=-1
			EigMat(i+1,i)=-1
		enddo
		EigMat(1,nu)=-1
		EigMat(nu,1)=-1
		EigMat(nu,nu)=2
		
		call DSYEV('V','U',nu,EigMat,nu,Omk,WORK,3*nu-1,INFO)		
		if(INFO/=0) then
			STOP "[PI_sampler.f90] Error: could not find the polymer normal modes!"
		endif
		
		Omk(1)=0
		Omk(:)=sqrt(Omk(:))*nu*kT
		
		!COMPUTE INVERSE TRANSFER MATRIX
		EigMatTr=transpose(EigMat)
		
		deallocate(WORK)		
		
	end subroutine initialize_normal_modes

!-------------------------------------------------

  subroutine apply_A(EigX,EigP,tau,move_centroid)
    implicit none
    real(dp), intent(inout) :: EigX(:,:,:),EigP(:,:,:)
    real(dp), intent(in) :: tau
    integer :: i,j,k
    real(dp) :: EigX0,EigP0
    logical, intent(in) :: move_centroid

    if(move_centroid) then
      do j=1,ndim
        EigX(:,j,1)=EigX(:,j,1)+tau*EigP(:,j,1)/mass(:)
      enddo
    endif

    do k=2,nu
      do j=1,ndim ; do i=1,nat
        EigX0=EigX(i,j,k)
        EigP0=EigP(i,j,k)
        EigX(i,j,k)=EigX0*cos(Omk(k)*tau) &
          +EigP0*sin(Omk(k)*tau)/(mass(i)*m_scale(k)*Omk(k))

        EigP(i,j,k)=EigP0*cos(Omk(k)*tau) &
          -mass(i)*m_scale(k)*Omk(k)*EigX0*sin(Omk(k)*tau)
      enddo ; enddo
    enddo

  end subroutine apply_A

  subroutine apply_O(EigP,tau,gamma0,move_centroid)
    implicit none
    real(dp), intent(inout) :: EigP(:,:,:)
    real(dp), intent(in) :: tau, gamma0
    real(dp), allocatable :: R(:)
    integer :: j,k
    real(dp) :: sigma2,gamma,gamma_exp
    logical, intent(in) :: move_centroid
    integer :: i0

    allocate(R(nat))    
    
    i0=merge(1, 2, mask=move_centroid)
    
    do k=i0,nu
      gamma = max(TRPMD_lambda*Omk(k),gamma0)
      gamma_exp = exp(-gamma*tau)
      sigma2 = nu*kT*(1._dp-gamma_exp**2)
      do j=1,ndim
        call randGaussN(R)
        EigP(:,j,k) = EigP(:,j,k)*gamma_exp &
          + R(:)*SQRT(mass(:)*m_scale(k)*sigma2)
      enddo
    enddo
  end subroutine apply_O

  subroutine apply_O_RPQTB(EigP,tau,gamma0,move_centroid)
    implicit none
    real(dp), intent(inout) :: EigP(:,:,:)
    real(dp), intent(in) :: tau, gamma0
    real(dp), allocatable :: R(:,:)
    integer :: j,k
    real(dp) :: sigma2,gamma,gamma_exp
    logical, intent(in) :: move_centroid
    integer :: i0

    allocate(R(nat,ndim))      
    i0=merge(1, 2, mask=move_centroid)
    gamma_exp = exp(-gamma0*tau)

    do k=i0,nu
      R = tau * qtbs(k)%get_force(EigP(:,:,k))
      do j=1,ndim
        EigP(:,j,k) = EigP(:,j,k)*gamma_exp &
          + R(:,j)*SQRT(m_scale(k))
      enddo
    enddo
  end subroutine apply_O_RPQTB

  subroutine apply_O_FFL(EigP,tau,gamma0,move_centroid)
    implicit none
    real(dp), intent(inout) :: EigP(:,:,:)
    real(dp), intent(in) :: tau, gamma0
    real(dp) :: R(ndim)
    integer :: i,k
    real(dp) :: sigma2,gamma,gamma_exp
    real(dp) :: Pold, Pnew
    logical, intent(in) :: move_centroid
    integer :: i0

    i0=merge(1, 2, mask=move_centroid)

    do k=i0,nu
      gamma = max(TRPMD_lambda*Omk(k),gamma0)
      gamma_exp = exp(-gamma*tau)
      sigma2 = nu*kT*(1._dp-gamma_exp**2)
      do i=1,nat
        Pold = NORM2(EigP(i,:,k))
        call randGaussN(R)
        Pnew = NORM2(EigP(i,:,k)*gamma_exp &
          + R(:)*SQRT(mass(i)*m_scale(k)*sigma2))
        EigP(i,:,k) = Pnew * EigP(i,:,k) / Pold
      enddo
    enddo
  end subroutine apply_O_FFL

  subroutine update_PI_forces(X,F,EigF)
    implicit none
    real(dp), intent(in) :: X(:,:,:)
    real(dp), intent(inout) :: F(:,:,:),EigF(:,:,:)
    integer :: k

    do k=1,nu
      F(:,:,k)=-dPot(X(:,:,k)) 
    enddo
    !COMPUTE FORCE FOR NORMAL MODES
    EigF = to_normal_modes_vector(F)

  end subroutine update_PI_forces

  subroutine apply_B(EigP,EigF,tau,move_centroid)
    implicit none
    real(dp), intent(inout) :: EigP(:,:,:)
    real(dp), intent(in) :: EigF(:,:,:)
    real(dp), intent(in) :: tau
    logical, intent(in) :: move_centroid
    integer :: i0

    i0=merge(1, 2, mask=move_centroid)
    EigP(:,:,i0:nu) = EigP(:,:,i0:nu) + tau*EigF(:,:,i0:nu)

  end subroutine apply_B

  subroutine BAOAB_step_PI(X,EigX,EigP,F,EigF,tau,gamma0,move_centroid)
    implicit none
    real(dp), intent(inout) :: X(:,:,:),EigX(:,:,:),EigF(:,:,:),EigP(:,:,:),F(:,:,:)
    real(dp), intent(in) :: tau,gamma0
    logical, intent(in) :: move_centroid

    ! EigF must already be set from the previous step !
    call apply_B(EigP,EigF,0.5*tau,move_centroid)

    call apply_A(EigX,EigP,0.5*tau,move_centroid)
    if(qtb_thermostat) then
      call apply_O_RPQTB(EigP,tau,gamma0,move_centroid)
    elseif(fast_forward_langevin) then
      call apply_O_FFL(EigP,tau,gamma0,move_centroid)
    else
      call apply_O(EigP,tau,gamma0,move_centroid)
    endif
    call apply_A(EigX,EigP,0.5*tau,move_centroid)
    X=from_normal_modes_vector(EigX)

    call update_PI_forces(X,F,EigF)
    call apply_B(EigP,EigF,0.5*tau,move_centroid)

  end subroutine BAOAB_step_PI

!-------------------------------------------------
! TRPMD tcf

  subroutine TRPMD_dynamics_IVR(X0,nsteps,dt_dyn,Cpp,Cmumu)
    implicit none
    real(dp), intent(in) :: X0(:,:,:)
    integer, intent(in) :: nsteps
    real(dp), intent(in) :: dt_dyn
    real(dp), intent(inout) :: Cpp(:,:)
    real(dp), intent(inout), OPTIONAL ::  Cmumu(:,:)
    real(dp), allocatable :: X(:,:,:),EigP(:,:,:),EigX(:,:,:),F(:,:,:),EigF(:,:,:),Pc(:,:)
    real(dp), allocatable :: mutraj(:,:)
    real(dp) :: sqrtnu
    real(dp) :: mu0(ndim),mu(ndim)
    integer :: istep,i,j,k

    allocate(X(nat,ndim,nu),EigP(nat,ndim,nu),F(nat,ndim,nu),EigF(nat,ndim,nu),EigX(nat,ndim,nu))
    allocate(Pc(nat,ndim))
    call sample_PI_momentum(EigP)
    call update_PI_forces(X0,F,EigF)

    sqrtnu=sqrt(real(nu,dp))
    X(:,:,:)=X0(:,:,:)

    Pc(:,:) = EigP(:,:,1)/sqrtnu
    DO istep = 1, nsteps
      Cpp(:,istep) = RESHAPE(Pc(:,:),(/ndof/))
    ENDDO 
    Cpp(:,1) =  Cpp(:,1)*RESHAPE(Pc(:,:),(/ndof/))    

    EigX=to_normal_modes_vector(X)

    if(present(Cmumu)) then
      allocate(mutraj(ndim,0:nsteps+1))
      Cmumu(:,:) = 0._dp 
      call get_dipole_ring_polymer(X,Pc,mu0) 
      mutraj(:,1)=mu0
    endif

    do istep = 2,nsteps 
      call BAOAB_step_PI(X,EigX,EigP,F,EigF,dt_dyn, 0._dp,.TRUE.)

      Pc(:,:)=EigP(:,:,1)/sqrtnu
      Cpp(:,istep) = Cpp(:,istep)*RESHAPE(Pc(:,:),(/ndof/))
      if(present(Cmumu)) then
        call get_dipole_ring_polymer(X,Pc,mutraj(:,istep)) 
      endif
    enddo

    if(present(Cmumu)) then
      do istep=1,nsteps
        Cmumu(:,istep)=mu0(:)*mutraj(:,istep)
      enddo
      deallocate(mutraj)
    endif

    deallocate(X,EigP,F,EigF,EigX)
    deallocate(Pc)

  end subroutine TRPMD_dynamics_IVR

  subroutine get_dipole_ring_polymer(X,Pc,mu)
    real(dp), intent(in) :: X(:,:,:),Pc(:,:)
    real(dp), intent(inout) :: mu(:)
    real(dp) :: mui(3)
    integer :: i
    real(dp), allocatable :: Xc(:,:)    
   
    if(linear_dipole) then
      call get_dipole(X(:,:,1),Pc,mu) !linear dipole, use only Pc
    else
      if(ACMD) then
        allocate(Xc(nat,ndim))
        Xc(:,:)=SUM(X,dim=3)/nu
        call get_dipole(Xc,Pc,mu)
      else
        mu(:) = 0._dp
        do i=1,nu
          call get_dipole(X(:,:,i),Pc,mui) ! non linear dipole, average over all beads X(:,:,i)
          mu(:)=mu(:)+mui(:)
        enddo
        mu(:)=mu(:)/real(nu,dp)
      endif
    endif

  end subroutine get_dipole_ring_polymer

  function to_normal_modes_vector(X) result(EigX)
    implicit none
    real(dp), intent(in) :: X(nat,ndim,nu)
    real(dp) :: EigX(nat,ndim,nu)
    integer :: i,j

    do j=1,ndim ; do i=1,nat
      !call DGEMV('N',nu,nu,1._8,EigMatTr,nu,X(i,j,:),1,0._8,EigX(i,j,:),1)
      EigX(i,j,:)=matmul(EigMatTr,X(i,j,:))
    enddo ; enddo

  end function to_normal_modes_vector

  function from_normal_modes_vector(EigX) result(X)
    implicit none
    real(dp), intent(in) :: EigX(nat,ndim,nu)
    real(dp) :: X(nat,ndim,nu)
    integer :: i,j
    
    do j=1,ndim ; do i=1,nat
      !call DGEMV('N',nu,nu,1._8,EigMatTr,nu,X(i,j,:),1,0._8,EigX(i,j,:),1)
      X(i,j,:)=matmul(EigMat,EigX(i,j,:))
    enddo ; enddo

  end function from_normal_modes_vector

  subroutine initialize_RPQTB(param_library,gamma,dt)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    real(dp), intent(in) :: gamma, dt
    character(:), allocatable :: qtb_method
    integer :: i

    allocate(qtbs(nu))
    call qtbs(1)%initialize(param_library,qtb_method, dt &
            ,gamma, .TRUE.,bead_index=0, omk=Omk)
    do i=2,nu
      call qtbs(i)%initialize(param_library,qtb_method, dt &
            ,gamma, .TRUE.,bead_index=i-1, omk=Omk,power_spectrum_in=qtbs(1)%power_spectrum)
    enddo

  end subroutine initialize_RPQTB

end module PI_sampler