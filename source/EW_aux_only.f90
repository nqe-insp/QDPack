PROGRAM EW_aux_only
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use system_commons
  use wigner_EW
  use potential
  use matrix_operations, only: sym_mat_diag
  implicit none
	type(DICT_STRUCT) :: param_library	
  type(WIG_AUX_SIM_TYPE) :: EW_auxsim
  real(dp), allocatable :: X(:,:)

  integer :: nsteps_aux, nsteps_aux_therm    
  integer :: num_aux_sim
  integer :: u,n_threads

  TYPE(timer_type) :: full_timer
  real :: cpu_time_start,cpu_time_finish

  call full_timer%start
  call cpu_time(cpu_time_start)


  call initialize_framework(param_library)	

  ! GET NUMBER OF THREADS        
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)     

  allocate(X(nat,ndim))
  X(:,:) = Xinit(:,:)
 
  write(*,*)
  call initialize_wigner_commons(param_library)

  nsteps_aux = param_library%get(WIGAUX//"/nsteps")  
  !! @input_file EW_NLGA/nsteps [INTEGER] (EW_aux_only) 
  nsteps_aux_therm = param_library%get(WIGAUX//"/nsteps_therm")
  !! @input_file EW_NLGA/nsteps_therm [INTEGER] (EW_aux_only) 
  num_aux_sim = param_library%get(WIGAUX//"/num_aux_sim")
  !! @input_file EW_NLGA/num_aux_sim [INTEGER] (EW_aux_only) 
  if(num_aux_sim>1) write(*,*) "EW: "//int_to_str(num_aux_sim)//" auxiliary simulations"    
  EW_auxsim%nsteps = nsteps_aux
  EW_auxsim%nsteps_therm = nsteps_aux_therm
  EW_auxsim%compute_WiLD_forces = .TRUE.

  call initialize_auxiliary_simulation(num_aux_sim,EW_auxsim)
  write(*,*) "EW initialized"
  !----------------------------------------
  
  !----------------------------------------
  call open_output_files(param_library)
  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  call compute_EW_parameters(X,EW_auxsim)
  open(newunit=u,file=output%working_directory//"/position.in")
  write(u,*) X(:,:)
  close(u)

  open(newunit=u,file=output%working_directory//"/k2.out")
  write(u,*) EW_auxsim%k2(:,:)
  close(u)

  open(newunit=u,file=output%working_directory//"/k2_var.out")
  write(u,*) EW_auxsim%k2var(:,:)
  close(u)

  open(newunit=u,file=output%working_directory//"/k2_std.out")
  write(u,*) sqrt(EW_auxsim%k2var(:,:))
  close(u)

  open(newunit=u,file=output%working_directory//"/k2_relvar.out")
  write(u,*) sqrt(EW_auxsim%k2var(:,:))/ EW_auxsim%k2(:,:)*100.
  close(u)

  open(newunit=u,file=output%working_directory//"/FWiLD.out")
  write(u,*) EW_auxsim%Fwild
  close(u)

  open(newunit=u,file=output%working_directory//"/CWiLD.out")
  write(u,*) EW_auxsim%Cwild
  close(u)  

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

END PROGRAM EW_aux_only
