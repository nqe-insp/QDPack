PROGRAM template
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use string_operations
	use timer_module
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  !! FOR ATOMISTIC SIMULATIONS
  ! use potential_dispatcher
  ! use system_commons
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library	

  TYPE(timer_type) :: full_timer
  real :: cpu_time_start,cpu_time_finish
  integer :: n_threads

  call full_timer%start()
  call cpu_time(cpu_time_start)

  call initialize_framework(param_library,initialize_output=.TRUE.)
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	
  !----------------------------------------------
  !! SIMULATION LOGIC GOES HERE

  !! INITIALIZE SYSTEM AND POTENTIAL FOR ATOMISTIC SIMULATIONS
  ! call initialize_system(param_library)
  ! call initialize_potential(param_library,nat,ndim)   

  call say_hello()

  !----------------------------------------------

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS

  subroutine say_hello()
    IMPLICIT NONE

    write(*,*) "Hello World!"

  end subroutine say_hello

END PROGRAM template
