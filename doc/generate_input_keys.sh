#!/bin/bash
source=$(find ../source/ -name "*.f90")

TMP="/tmp/tmpjeys"
rm -f $TMP
for f in $source; do
  grep '@input_file' $f | sed 's/.*@input_file //g' >> $TMP
done

sort -k1,1 < $TMP | column -t  > input_keys.txt
rm $TMP